package com.xsolla.android.sdk.view.fragment;

/**
 *
 */
public interface IShopInteractionListener {
    void onMenuSelected(int action);
    void onCountryChanged(String iso);
}
