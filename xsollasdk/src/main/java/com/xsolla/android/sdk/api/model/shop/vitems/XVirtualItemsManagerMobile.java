package com.xsolla.android.sdk.api.model.shop.vitems;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 */
public class XVirtualItemsManagerMobile implements IParseble {

    private ArrayList<SimpleGroup> listGroups;//groups

    public ArrayList<SimpleGroup> getListGroups() {
        return listGroups;
    }

    public ArrayList<Object> getSimplifiedList() {
        ArrayList<Object> objects = new ArrayList<>();
        for (SimpleGroup group : listGroups) {
            objects.add(new XVirtualItemGroup(group.getId(), group.getName()));
            for (XVirtualItem item : group.getListItems()) {
                objects.add(item);
            }
        }
        return objects;
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONArray jarrItems = jobj.optJSONArray("groups");
        if(jarrItems != null) {
            listGroups = new ArrayList<>(jarrItems.length());
            for (int i = 0; i < jarrItems.length(); i++) {
                SimpleGroup newItem = new SimpleGroup();
                newItem.parse(jarrItems.optJSONObject(i));
                listGroups.add(newItem);
            }
        }
    }

    @Override
    public String toString() {
        return "XVirtualItemsManagerMobile{" +
                "listGroups=" + listGroups +
                '}';
    }

    public class SimpleGroup implements IParseble {
        private String                  id;//id
        private String                  name;//name
        private ArrayList<XVirtualItem> listItems;//virtual_items

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public ArrayList<XVirtualItem> getListItems() {
            return listItems;
        }

        @Override
        public void parse(JSONObject jobj) {
            id                  = jobj.optString("id");
            name                = jobj.optString("name");
            JSONArray jarrItems = jobj.optJSONArray("virtual_items");
            if(jarrItems != null) {
                listItems = new ArrayList<>(jarrItems.length());
                for (int i = 0; i < jarrItems.length(); i++) {
                    XVirtualItem newItem = new XVirtualItem();
                    newItem.parse(jarrItems.optJSONObject(i));
                    listItems.add(newItem);
                }
            }
        }

        @Override
        public String toString() {
            return "SimpleGroup{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", listItems=" + listItems +
                    '}';
        }
    }
}
