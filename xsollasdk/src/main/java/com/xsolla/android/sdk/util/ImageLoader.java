package com.xsolla.android.sdk.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.X509TrustManager;

import info.guardianproject.netcipher.NetCipher;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
public class ImageLoader {

    private static volatile ImageLoader instance;

    private HashMap<String, Bitmap> images;

    public ImageLoader() {
        images = new HashMap<>();
    }

    public static ImageLoader getInstance() {
        ImageLoader localInstance = instance;
        if (localInstance == null) {
            synchronized (ImageLoader.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ImageLoader();
                }
            }
        }
        return localInstance;
    }

    final X509TrustManager trustManager = new X509TrustManager() {
        @Override
        public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

        }

        @Override
        public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    };

    public void loadImage(final ImageView imageView, final String url) {
        if(images.containsKey(url)) {
            imageView.setImageBitmap(images.get(url));
        } else {
            Observable<Bitmap> bitmapObservable = Observable.create(new Observable.OnSubscribe<Bitmap>() {
                @Override
                public void call(Subscriber<? super Bitmap> subscriber) {
                    subscriber.onStart();
                    try {
                        Bitmap bmp = null;
//                        URL imageUrl = new URL(url);
//                        URI uri = new URI(imageUrl.getProtocol(), imageUrl.getUserInfo(), imageUrl.getHost(), imageUrl.getPort(), imageUrl.getPath(), imageUrl.getQuery(), imageUrl.getRef());
//                        imageUrl = uri.toURL();
//                        SSLContext sc = SSLContext.getInstance("TLSv1.2");
//                        // Init the SSLContext with a TrustManager[] and SecureRandom()
//                        sc.init(null, new TrustManager[]{trustManager}, new java.security.SecureRandom());
                        HttpsURLConnection connection = NetCipher.getHttpsURLConnection(url);// = (HttpsURLConnection)imageUrl.openConnection();
//                        connection.setSSLSocketFactory(sc.getSocketFactory());
                        bmp = BitmapFactory.decodeStream(connection.getInputStream());
                        subscriber.onNext(bmp);
                        subscriber.onCompleted();
                    } catch (IOException e) {
                        subscriber.onError(e);
                    }
                }
            });

            bitmapObservable
                    .subscribeOn(Schedulers.newThread())
                    .cache()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Bitmap>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(Bitmap bitmap) {
                            images.put(url, bitmap);
                            imageView.setImageBitmap(bitmap);
                        }
                    });
        }

    }

    public void loadImageCompaund(final TextView textView, final String url) {
        if(images.containsKey(url)) {
            textView.setCompoundDrawablesWithIntrinsicBounds(null, null, new BitmapDrawable(images.get(url)), null);
        } else {
            Observable<Bitmap> bitmapObservable = Observable.create(new Observable.OnSubscribe<Bitmap>() {
                @Override
                public void call(Subscriber<? super Bitmap> subscriber) {
                    subscriber.onStart();
                    try {
                        Bitmap bmp = null;
                        URL imageUrl = new URL(url);
                        URI uri = new URI(imageUrl.getProtocol(), imageUrl.getUserInfo(), imageUrl.getHost(), imageUrl.getPort(), imageUrl.getPath(), imageUrl.getQuery(), imageUrl.getRef());
                        imageUrl = uri.toURL();
                        bmp = BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream());
                        subscriber.onNext(bmp);
                        subscriber.onCompleted();
                    } catch (IOException e) {
                        subscriber.onError(e);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }
            });

            bitmapObservable
                    .subscribeOn(Schedulers.newThread())
                    .cache()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Bitmap>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(Bitmap bitmap) {
                            images.put(url, bitmap);
                            textView.setCompoundDrawablesWithIntrinsicBounds(null, null, new BitmapDrawable(bitmap), null);
                        }
                    });
        }

    }


}
