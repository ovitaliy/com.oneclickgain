package com.xsolla.android.sdk.api;


import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class XPurchase {

    private static final String TAG = "XPurchase";

    private HashMap<Part, HashMap<String, Object>> mapPurchase;
    private Part        lastAdded;
    private int         counter;
    private boolean     isSandbox;

    public enum Part {
        TOKEN, PURCHASE, PAYMENT_SYSTEM, XPS, COUNTRY, NULL
    }

    public XPurchase(){
        mapPurchase = new HashMap<>();
        lastAdded   = Part.NULL;
        counter     = 0;
    }

    public XPurchase(String token, boolean isSandbox){
        this();
        add(Part.TOKEN, "access_token", token);
        this.isSandbox = isSandbox;
    }

    public void add(Part part, HashMap<String, Object> newMap)
    {
        mapPurchase.put (part, newMap);
        lastAdded = part;
        counter++;
    }

    public void add(Part part, String newKey, Object newValue)
    {
        if(mapPurchase.get(part) != null) {
            mapPurchase.get(part).put(newKey, newValue);
        } else {
            HashMap<String, Object> newMap = new HashMap<>();
            newMap.put(newKey, newValue);
            add(part, newMap);
        }
    }


    public void removeAllExceptToken(){
        Set<Part> keyList = mapPurchase.keySet();
        for (Part part : keyList) {
            if(part != Part.TOKEN)
                remove(part);
        }
        lastAdded = Part.TOKEN;
        counter = 1;
    }

    public boolean removeLast(){
        if (isActive () && mapPurchase.containsKey (lastAdded)) {
            remove (lastAdded);
            counter--;
            return  true;
        }
        return false;
    }

    public boolean removeLastExceptToken(){
        if (isActive () && mapPurchase.containsKey (lastAdded) && lastAdded != Part.TOKEN) {
            remove (lastAdded);
            counter--;
            return  true;
        }
        return false;
    }

    public boolean remove(Part part){
        if (mapPurchase.containsKey (part)) {
            mapPurchase.remove (part);
            counter--;
            return true;
        }
        return false;
    }


    public void containsKey(Part part){
        mapPurchase.containsKey (part);
    }

    public HashMap<String, Object> getPart(Part part){
        return mapPurchase.get(part);
    }

    public boolean isSandbox() {
        return isSandbox;
    }

    public HashMap<String, Object> getMergedMap(){
        HashMap<String, Object> finalPurchase = new HashMap<>();
        for (Map<String, Object> stringObjectMap : mapPurchase.values()) {
            finalPurchase.putAll(stringObjectMap);
        }
        return finalPurchase;
    }

    public boolean isActive()
    {
        return lastAdded != Part.NULL;
    }
}
