package com.xsolla.android.sdk.api.model.psystems;

import java.util.ArrayList;

/**
 *
 */
public class XPaymentSystemsManager {

    private XPSQuickManager quickManger;
    private XPSAllManager   allManager;
    private XCountryManager countryManager;

    public XPaymentSystemsManager() {
    }

    public XPSQuickManager getQuickManger() {
        return quickManger;
    }

    public void setQuickManger(XPSQuickManager quickManger) {
        this.quickManger = quickManger;
    }

    public XPSAllManager getAllManager() {
        return allManager;
    }

    public void setAllManager(XPSAllManager allManager) {
        this.allManager = allManager;
    }

    public ArrayList<XPaymentSystem> getMergedList() {
        ArrayList<XPaymentSystemBase> quickList = getQuickManger().getList();
        ArrayList<XPaymentSystem> mergedList = new ArrayList<>(getAllManager().getList());
//        int size = quickList.size() > 2 ? 2 : quickList.size();
        for(int i = 0; i < quickList.size(); i++) {
            XPaymentSystemBase basePs = quickList.get(i);
            XPaymentSystem newPs;
            if(basePs.getId() != 1738)
                newPs = new XPaymentSystem(basePs, "creditCards");
            else
                newPs = new XPaymentSystem(basePs, "mobile");
            mergedList.add(i, newPs);
        }
        return mergedList;
    }

    public XCountryManager getCountryManager() {
        return countryManager;
    }

    public void setCountryManager(XCountryManager countryManager) {
        this.countryManager = countryManager;
    }

    @Override
    public String toString() {
        return "XPaymentSystemsManager{" +
                "quickManger=" + quickManger +
                ", allManager=" + allManager +
                ", countryManager=" + countryManager +
                '}';
    }
}
