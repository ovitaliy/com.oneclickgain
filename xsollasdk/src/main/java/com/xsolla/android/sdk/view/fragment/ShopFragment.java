package com.xsolla.android.sdk.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.psystems.XCountryManager;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.view.XsollaActivity;
import com.xsolla.android.sdk.view.generator.ShopViewGenerator;

/**
 *
 */
public class ShopFragment extends XFragment {

    private XUtils          mUtils;
    private XCountryManager mManager;

    private IShopInteractionListener mListener;

    public ShopFragment() {
        // Required empty public constructor
    }

    public static ShopFragment newInstance() {
        return new ShopFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setTitle(mUtils.getProject().getName());
        View rootView = inflater.inflate(R.layout.xsolla_shop, null);
        ShopViewGenerator.findAndAndInitView(getContext(), rootView, mUtils, mManager, mListener);
        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IShopInteractionListener) {
            mListener   = (IShopInteractionListener) context;
            mUtils      = ((XsollaActivity)getActivity()).getUtils();
            mManager    = ((XsollaActivity)getActivity()).getCountries();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
