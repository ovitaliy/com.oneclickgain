package com.xsolla.android.sdk.api.model.additional;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONObject;

/**
 */
public class XToken implements IParseble {

    private String token;

    public String getToken() {
        return token;
    }

    @Override
    public void parse(JSONObject jobj) {
        token = jobj.optString("token");
    }

    @Override
    public String toString() {
        return "XToken{" +
                "token='" + token + '\'' +
                '}';
    }
}
