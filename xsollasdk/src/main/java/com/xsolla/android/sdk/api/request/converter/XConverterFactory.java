package com.xsolla.android.sdk.api.request.converter;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 */
public class XConverterFactory extends Converter.Factory {

    private static final String TAG = XConverterFactory.class.getSimpleName();

    private IParseble objectToParse;

    public XConverterFactory(IParseble objectToParse) {
        this.objectToParse = objectToParse;
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        return new XResponseBodyConverter<>(objectToParse);
    }

    class XResponseBodyConverter<T extends IParseble> implements Converter<ResponseBody, T> {
        private IParseble parseble;
        XResponseBodyConverter(IParseble parseble) {
            this.parseble = parseble;
        }

        @Override public T convert(ResponseBody responseBody) throws IOException {
            try {
                JSONObject jsonObject = new JSONObject(responseBody.string());
                parseble.parse(jsonObject);
                return (T) parseble;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return (T) objectToParse;
        }
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type, Annotation[] parameterAnnotations, Annotation[] methodAnnotations, Retrofit retrofit) {
        return super.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit);
    }

    @Override
    public Converter<?, String> stringConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        return super.stringConverter(type, annotations, retrofit);
    }
}
