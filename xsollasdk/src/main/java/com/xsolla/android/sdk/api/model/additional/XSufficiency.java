package com.xsolla.android.sdk.api.model.additional;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONObject;

/**
 *
 */
public class XSufficiency implements IParseble{

    private boolean isEnoughUserBalance;//"is_enough_user_balance":false,

    public boolean isEnoughUserBalance() {
        return isEnoughUserBalance;
    }

    @Override
    public void parse(JSONObject jobj) {
        isEnoughUserBalance = jobj.optBoolean("is_enough_user_balance");
    }

    @Override
    public String toString() {
        return "XSufficiency{" +
                "isEnoughUserBalance=" + isEnoughUserBalance +
                '}';
    }
}
