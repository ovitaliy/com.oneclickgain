package com.xsolla.android.sdk.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.util.XUtils.XActiveComponent;
import com.xsolla.android.sdk.view.widget.IconTextView;

import java.util.ArrayList;

/**
 *
 */
public class MenuElementsAdapter extends BaseAdapter {

    private Context                     mContext;
    private ArrayList<XActiveComponent> mList;

    public MenuElementsAdapter(Context mContext, ArrayList<XActiveComponent> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public XActiveComponent getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.xsolla_shop_menu_elem, null);
            holder.icon = (IconTextView) convertView.findViewById(R.id.xsolla_icon);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.xsolla_tv_title);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        XActiveComponent item = getItem(position);
        holder.icon.setText(item.getIcon());
        holder.tvTitle.setText(item.getTitle());
        return convertView;
    }

    class ViewHolder {
        IconTextView    icon;
        TextView        tvTitle;
    }

}
