package com.xsolla.android.sdk.util.validator;

/**
 *
 */
public class EmptyValidator extends BaseValidator {


    public EmptyValidator() {
        errorMsg = "Can't be empty";
    }

    @Override
    public boolean validate(String s) {
        return !"".equals(s);
    }
}
