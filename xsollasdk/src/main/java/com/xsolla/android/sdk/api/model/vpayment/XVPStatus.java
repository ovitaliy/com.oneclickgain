package com.xsolla.android.sdk.api.model.vpayment;

import com.xsolla.android.sdk.XsollaObject;
import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class XVPStatus extends XsollaObject implements IParseble {
    //	{
    //		"operation_id":"36786976",
    //		"operation_type":"purchase",
    //		"operation_created":"2016-04-08T13:37:24+03:00",
    //		"finance":{
    //			"total":{
    //				"vc_amount":100
    //			}
    //		},
    //		"purchase":{
    //			"virtual_items":[
    //			   {
    //				"name":"Test product 007",
    //				"quantity":1,
    //				"description":"Test product 007",
    //				"image_url":"\/\/cdn3.xsolla.com\/img\/misc\/merchant\/default-item.png"
    //			   }
    //			   ],
    //			"virtual_currency":[
    //			   ],
    //			"subscriptions":[
    //			   ]
    //		},
    //		"status":{
    //			"code":"done",
    //			"description":"Purchase successful!",
    //			"header":"Purchase completed. Thank you for your order!",
    //			"header_description":""
    //		},
    //		"back_url":null,
    //		"return_region":null,
    //		"back_url_caption":null,
    //		"api":{
    //			"ver":"1.0.1"
    //		}
    //	}

    private int vcAmount;

    private String operationId;
    private String operationType;
    private String operationCreated;
    private String backUrl;
    private String returnTegion;
    private String backUrlCaption;

    private String code;
    private String description;
    private String header;
    private String headerDescription;

    private List<XVirtualItemSimple> items;

    public String getVcAmount() {
        return Integer.toString(vcAmount);
    }

    public String getOperationId() {
        return operationId;
    }

    public String getOperationType() {
        return operationType;
    }

    public String getOperationCreated() {
        return operationCreated;
    }

    public String getBackUrl() {
        return backUrl;
    }

    public String getReturnTegion() {
        return returnTegion;
    }

    public String getBackUrlCaption() {
        return backUrlCaption;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String getHeader() {
        return header;
    }

    public String getHeaderDescription() {
        return headerDescription;
    }

    public List<XVirtualItemSimple> getItems() {
        return items;
    }

    public String getPurchase(int i) {
        XVirtualItemSimple item = items.get(i);
        return item.getQuantity() + " x " + item.getName();
    }

    @Override
    public void parse(JSONObject jobj) {
        operationId      = jobj.optString("operation_id");
        operationType    = jobj.optString("operation_type");
        operationCreated = jobj.optString("operation_created");
        vcAmount = jobj.optJSONObject("finance").optJSONObject("total").optInt("vc_amount");
        backUrl = jobj.optString("back_url");
        backUrlCaption = jobj.optString("back_url_caption");
        returnTegion = jobj.optString("return_region");

        JSONObject joStatus = jobj.optJSONObject("status");
        if(joStatus != null) {
            code              = joStatus.optString("code");
            description       = joStatus.optString("description");
            header            = joStatus.optString("header");
            headerDescription = joStatus.optString("header_description");
        }

        JSONObject joPurchase = jobj.optJSONObject("purchase");
        if(joPurchase != null) {
            JSONArray jaVirtualItems = joPurchase.optJSONArray("virtual_items");
            items = new ArrayList<>(jaVirtualItems.length());
            for(int i = 0; i < jaVirtualItems.length(); i++) {
                JSONObject joItem = jaVirtualItems.optJSONObject(i);
                XVirtualItemSimple newItem = new XVirtualItemSimple();
                newItem.parse(joItem);
                items.add(newItem);
            }
        }
    }

    @Override
    public String toString() {
        return "XVPStatus{" +
                "vcAmount=" + vcAmount +
                ", operationId='" + operationId + '\'' +
                ", operationType='" + operationType + '\'' +
                ", operationCreated='" + operationCreated + '\'' +
                ", backUrl='" + backUrl + '\'' +
                ", returnTegion='" + returnTegion + '\'' +
                ", backUrlCaption='" + backUrlCaption + '\'' +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", header='" + header + '\'' +
                ", headerDescription='" + headerDescription + '\'' +
                ", items=" + items +
                '}';
    }
}
