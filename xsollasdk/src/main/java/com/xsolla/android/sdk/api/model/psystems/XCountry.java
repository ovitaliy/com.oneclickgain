package com.xsolla.android.sdk.api.model.psystems;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONObject;

/**
 *
 */
public class XCountry implements IParseble{
    private String iso;// "ISO":"AF",
    private String aliases;// "aliases":null, //string
    private String name;// "name":"Afghanistan\r"

    public String getIso() {
        return iso;
    }

    public String getAliases() {
        return aliases;
    }

    public String getName() {
        return name;
    }

    @Override
    public void parse(JSONObject jobj) {
        iso     = jobj.optString("ISO");
        aliases = jobj.optString("aliases");
        name    = jobj.optString("name");
    }

    @Override
    public String toString() {
        return "\nXCountry{" +
                "iso='" + iso + '\'' +
                ", aliases='" + aliases + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

