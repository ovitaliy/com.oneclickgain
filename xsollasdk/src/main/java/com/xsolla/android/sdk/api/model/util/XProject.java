package com.xsolla.android.sdk.api.model.util;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

/**
 *
 */
enum ComponentType {
    VIRTUAL_CURRENCY, ITEMS, SUBSCRIPTIONS, COUPONS
}

/**
 *
 */
public class XProject implements IParseble {

    int     id;//"id":16184,
    int     merchantId;//"merchantId":15924,

    String  name;//"name":"Farm Fresh",
    String  nameEn;//"nameEn":"Farm Fresh",
    String  logo;//"logo":null,
    String  virtualCurrencyName;//"virtualCurrencyName":"Coins",
    String  virtualCurrencyImage;//"virtualCurrencyImage":"\/\/cdn3.xsolla.com\/img\/misc\/images\/91d3aecf770347428c8c6abdc8a260b8.png",
    String  projectUrl;//"projectUrl":"http:\/\/xsolla.com",
    String  returnUrl;//"returnUrl":"http:\/\/xsolla.com",
    String  customSupportUrl;//"customSupportUrl":null,

    boolean isDiscrete;//"isDiscrete":true,
    boolean isCanRepeatPayment;//"canRepeatPayment":true

    XEula eula;//"eula":{
    HashMap<String, XComponent> components; //"components":{


    public XProject() {
        this.eula       = new XEula();
        this.components = new HashMap<>(4);
    }

    public int getId() {
        return id;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public String getName() {
        return name;
    }

    public String getNameEn() {
        return nameEn;
    }

    public String getLogo() {
        return logo;
    }

    public String getVirtualCurrencyName() {
        return virtualCurrencyName;
    }

    public String getVirtualCurrencyImage() {
        if(!"null".equals(virtualCurrencyImage))
            return virtualCurrencyImage.startsWith("http") ? virtualCurrencyImage : "https:" + virtualCurrencyImage;
        else
            return "";
    }

    public String getProjectUrl() {
        return projectUrl;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public String getCustomSupportUrl() {
        return customSupportUrl;
    }

    public boolean isDiscrete() {
        return isDiscrete;
    }

    public boolean isCanRepeatPayment() {
        return isCanRepeatPayment;
    }

    public String getEulaLink() {
        return eula.getUrl();
    }

    public HashMap<String, XComponent> getComponents() {
        return components;
    }


    @Override
    public void parse(JSONObject jobj) {

        this.id                     = jobj.optInt("id");
        this.merchantId             = jobj.optInt("namerchantIdme");

        this.name                   = jobj.optString("name");
        this.nameEn                 = jobj.optString("nameEn");
        this.logo                   = jobj.optString("logo");
        this.virtualCurrencyName    = jobj.optString("virtualCurrencyName");
        this.virtualCurrencyImage   = jobj.optString("virtualCurrencyImage");
        this.projectUrl             = jobj.optString("projectUrl");
        this.returnUrl              = jobj.optString("returnUrl");
        this.customSupportUrl       = jobj.optString("customSupportUrl");

        this.isDiscrete             = jobj.optBoolean("isDiscrete");
        this.isCanRepeatPayment     = jobj.optBoolean("canRepeatPayment");

        JSONObject jobjEula = jobj.optJSONObject("eula");
        if(jobjEula != null)
            eula.parse(jobjEula);

        JSONObject jobjComponents = jobj.optJSONObject("components");
        if(jobjComponents != null)
            for(Iterator<String> iter = jobjComponents.keys(); iter.hasNext();) {
                String key = iter.next();
                JSONObject jobjComponent = jobjComponents.optJSONObject(key);
                XComponent component = new XComponent();
                component.parse(jobjComponent);
                components.put(key, component);
            }
    }

    @Override
    public String toString() {
        return "\n\tXProject{" +
                "id=" + id +
                ", merchantId=" + merchantId +
                ", name='" + name + '\'' +
                ", nameEn='" + nameEn + '\'' +
                ", logo='" + logo + '\'' +
                ", virtualCurrencyName='" + virtualCurrencyName + '\'' +
                ", virtualCurrencyImage='" + virtualCurrencyImage + '\'' +
                ", projectUrl='" + projectUrl + '\'' +
                ", returnUrl='" + returnUrl + '\'' +
                ", customSupportUrl='" + customSupportUrl + '\'' +
                ", isDiscrete=" + isDiscrete +
                ", isCanRepeatPayment=" + isCanRepeatPayment +
                ", eula=" + eula +
                ", components=" + components +
                '}';
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
    * INNER CLASSES
    * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /**
     *
     */
    class XEula implements IParseble {
        private String url;//"url":"http:\/\/xsolla.com\/termsandconditions\/?lang=en&ca=15924",
        private String label;//"label":null

        public String getUrl() {
            return url;
        }

        @Override
        public void parse(JSONObject jobj) {
            this.url    = jobj.optString("url");
            this.label  = jobj.optString("label");
        }

        @Override
        public String toString() {
            return "\n\t\tXEula{" +
                    "url='" + url + '\'' +
                    ", label='" + label + '\'' +
                    '}';
        }
    }

    /**
     *
     */
    public class XComponent implements IParseble {
        boolean isEnabled;//"enabled":true,
        String  name;//"name":null

        public boolean isEnabled() {
            return isEnabled;
        }

        public String getName() {
            return name;
        }

        @Override
        public void parse(JSONObject jobj) {
            this.isEnabled  = jobj.optBoolean("enabled");
            this.name       = jobj.optString("name");
        }

        @Override
        public String toString() {
            return "\n\t\tXComponent{" +
                    "isEnabled=" + isEnabled +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
