package com.xsolla.android.sdk.view.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.XsollaApiConst;
import com.xsolla.android.sdk.api.model.util.XTranslationsKeys;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.api.model.сheckout.CurrentCommand;
import com.xsolla.android.sdk.api.model.сheckout.XDirectpayment;
import com.xsolla.android.sdk.util.DeviceInfo;
import com.xsolla.android.sdk.util.ImageLoader;
import com.xsolla.android.sdk.view.XsollaActivity;
import com.xsolla.android.sdk.view.XsollaUIHelper;
import com.xsolla.android.sdk.view.generator.CCFormGenerator;
import com.xsolla.android.sdk.view.generator.ICheckoutGenerator;
import com.xsolla.android.sdk.view.generator.SimpleFormGeneratorImpl;
import com.xsolla.android.sdk.view.generator.SummaryGenerator;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

public class CheckoutFragment extends XFragment implements View.OnClickListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_XSOLLA_FORM = "xsolla_form";
    private static final String ARG_XSOLLA_TRANSLATIONS = "xsolla_translations";


    /* * * * * * * * * * * * * * * * * * * * * * * * *
    *  DATA
    * * * * * * * * * * * * * * * * * * * * * * * * * */
    private XDirectpayment          mForm;
    private XUtils                  mUtils;
    private ICheckoutGenerator      mFormGenerator;

    /* * * * * * * * * * * * * * * * * * * * * * * * *
    *  VIEW
    * * * * * * * * * * * * * * * * * * * * * * * * * */
    private ScrollView scrollView;
    private FrameLayout formContainer, summaryContainer;
    private ImageView ivPaymentSystem;
    private LinearLayout llInfo;
    private TextView tvPaymentSystem, tvTotal, tvEula, mTvChange, mTvError,
    /*SUPPORT TV*/ tvSupportPhone, tvNeedHelp, tvContactUs, tvSupportEmail, tvCustomerSupport;
    private Button button1, button2;

    private OnFragmentInteractionListener mListener;

    public static CheckoutFragment newInstance() {
        return new CheckoutFragment();
    }

    public CheckoutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // retain this fragment
        setRetainInstance(true);
        if (getArguments() != null) {
//            xsollaForm = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setTitle(mUtils.getTranslations().get(XTranslationsKeys.PAYMENT_PAGE_TITLE));//mForm.getTitle());
        View rootView = inflater.inflate(R.layout.xsolla_payment_form, null);
        findView(rootView);
        int counter = 0;
        for( XDirectpayment.XInfoError error : mForm.getErrors()){
            String s            =  error.getCode() + " : " + error.getMessage();
            ViewGroup errorView = (ViewGroup)inflater.inflate(R.layout.xsolla_elem_error, llInfo, true);
            TextView tvTitle    = (TextView) errorView.getChildAt(counter).findViewById(R.id.tvTitle);
            tvTitle.setText(s);
            counter++;
        }

        ArrayList<XDirectpayment.XInfoMessage> messages = mForm.getMessages();
        if(messages.size() > 0) {
            ViewGroup infoView = (ViewGroup) inflater.inflate(R.layout.xsolla_elem_info, llInfo, true);
            TextView tvTitle = (TextView) infoView.getChildAt(counter).findViewById(R.id.tvTitle);
            StringBuilder sb = new StringBuilder("");
            for (XDirectpayment.XInfoMessage message : mForm.getMessages()) {
                sb.append(message.getMessage()).append("\n");
                counter++;
            }
            sb.delete(sb.length() - 1, sb.length());
            tvTitle.setText(sb.toString());
        }

        int localCounter = 3;
        if(mForm.isAccount()) {
            LinearLayout xsollaContainer    = (LinearLayout) rootView.findViewById(R.id.mainContainer);
            View newAccount                 = renderAccount(inflater, mUtils.getTranslations().get(XTranslationsKeys.FORM_NUMBER_2PAY), mForm.getAccount());
            xsollaContainer.addView(newAccount, localCounter);
            localCounter++;
        }

        if(mForm.isXsollaAccount()) {
            LinearLayout xsollaContainer    = (LinearLayout) rootView.findViewById(R.id.mainContainer);
            View newAccount                 = renderAccount(inflater, mUtils.getTranslations().get(XTranslationsKeys.FORM_NUMBER_XSOLLA), mForm.getAccountXsolla());
            xsollaContainer.addView(newAccount, localCounter);
        }
        initView(mForm, mUtils);
        rootView.invalidate();
        return rootView;
    }

    private View renderAccount(LayoutInflater inflater, String title, String value) {
        View v              = inflater.inflate(R.layout.xsolla_elem_account, null);
        TextView tvTitle    =  (TextView) v.findViewById(R.id.xsolla_tv_title);
        TextView tvValue    =  (TextView) v.findViewById(R.id.xsolla_tv_value);
        tvTitle.setText(title);
        tvValue.setText(value);
        return v;
    }

    private void findView(View rootView) {
        scrollView = (ScrollView) rootView.findViewById(R.id.scrollView);

        llInfo              = (LinearLayout) rootView.findViewById(R.id.llInfo);
        mTvError            = (TextView) rootView.findViewById(R.id.tvError);
        formContainer       = (FrameLayout) rootView.findViewById(R.id.formContainer);
        ivPaymentSystem     = (ImageView) rootView.findViewById(R.id.ivPaymentSystem);
        tvPaymentSystem     = (TextView) rootView.findViewById(R.id.tvPaymentSystem);
        mTvChange           = (TextView) rootView.findViewById(R.id.xsolla_tv_change);
        tvTotal             = (TextView) rootView.findViewById(R.id.tvTotal);
        tvEula              = (TextView) rootView.findViewById(R.id.tvEula);
        button1             = (Button) rootView.findViewById(R.id.btnPay);
        button2             = (Button) rootView.findViewById(R.id.btnPay2);
        summaryContainer    = (FrameLayout) rootView.findViewById(R.id.xsolla_summary_container);
    }

    private void initView(XDirectpayment directpayment, XUtils utils) {
        HashMap<String, String> translations = utils.getTranslations();

        if(directpayment.getPid() == 1380)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ivPaymentSystem.setImageDrawable(getResources().getDrawable(R.drawable.xsolla_ic_cc, getResources().newTheme()));
            } else  {
                ivPaymentSystem.setImageDrawable(getResources().getDrawable(R.drawable.xsolla_ic_cc));
            }
        else
            ImageLoader.getInstance().loadImage(ivPaymentSystem, directpayment.getIconUrl());

        tvPaymentSystem.setText(directpayment.getTitle());
        mTvChange.setText(translations.get(XTranslationsKeys.FORM_HEADER_CHANGE));
        mTvChange.setOnClickListener(onPsChangeClickListener());
        boolean isDisallowChangePS = directpayment.getCurrentCommand() == CurrentCommand.ACCOUNT || utils.getPurchase().isPaymentSystem();
        if(isDisallowChangePS)
            mTvChange.setVisibility(View.GONE);

        String total = translations.get(XTranslationsKeys.TOTAL) + " " + directpayment.getSumTotal();
        tvTotal.setText(total);
        String eula = directpayment.getInstruction();
        boolean isCcRender  = directpayment.getCurrentCommand() == CurrentCommand.FORM && directpayment.isCreditCards();
        boolean isForm      = directpayment.getForm().getListVisible().size() > 0;
        if(isForm) {
            if (isCcRender) {
                mFormGenerator = new CCFormGenerator();
                XsollaUIHelper.replaceSingleView(formContainer,
                        mFormGenerator.generate(getActivity(), directpayment, translations));
                eula = translations.get(XTranslationsKeys.FORM_CC_EULA);
            } else {
                mFormGenerator = new SimpleFormGeneratorImpl();
                XsollaUIHelper.replaceSingleView(formContainer,
                        mFormGenerator.generate(getActivity(), directpayment, translations));
            }

        }

        //TODO use better condition
        boolean isErrorForm = mForm.getErrors().size() > 0;
        if((!isForm && isErrorForm) || (mForm.isAccount() || mForm.isXsollaAccount())) {
            ((View)tvTotal.getParent()).setVisibility(View.GONE);
            button1.setVisibility(View.GONE);
            button2.setVisibility(View.GONE);
        }

        tvEula.setVisibility(View.VISIBLE);
        tvEula.setText(Html.fromHtml(eula));
        tvEula.setMovementMethod(LinkMovementMethod.getInstance());
        String textBtnNext;
        if(!mForm.isNextText()) {
            switch (directpayment.getCurrentCommand()) {
                case CHECKOUT:
                    tvTotal.setVisibility(View.GONE);
                    textBtnNext = translations.get(XTranslationsKeys.FORM_CHECKOUT);
                    break;
                default:
                    textBtnNext = translations.get(XTranslationsKeys.FORM_CONTINUE);
            }
        } else {
            textBtnNext = mForm.getNextText();
        }

        button1.setText(textBtnNext);
        button2.setText(textBtnNext);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);

        if(directpayment.getSummary() != null) {
            View summary = SummaryGenerator.drawSummary(getActivity(), directpayment.getSummary(), translations);
            XsollaUIHelper.replaceSingleView(summaryContainer, summary);
        }

    }

    private void disableNextButtons() {
        button1.setEnabled(false);
        button2.setEnabled(false);
    }

    private void enableNextButtons() {
        button1.setEnabled(true);
        button2.setEnabled(true);
    }

    public void onButtonPressed(HashMap<String, Object> xpsMap) {
        if (mListener != null) {
            String errorMsg = null;
            if (mFormGenerator != null)
                errorMsg = mFormGenerator.validate();

            if(errorMsg == null)
                mListener.onClickPay(xpsMap);
            else
                Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    public View.OnClickListener onPsChangeClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClickChangePs();
            }
        };
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
            mForm = ((XsollaActivity)getActivity()).getDirectpayment();
            mUtils = ((XsollaActivity)getActivity()).getUtils();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        try {
            mForm.getForm().updateElement(XsollaApiConst.RF_DFP, DeviceInfo.getEncoded(getActivity()));
        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        }
        onButtonPressed(mForm.getForm().getMapXps());
    }

//    @Override
//    public void onActivityCreated(final @Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        if (savedInstanceState != null) {
////            try {
////                mMapTranslations = new XTranslationsKeys(savedInstanceState.getByteArray(ARG_XSOLLA_TRANSLATIONS));
////                mForm = new XsollaForm(savedInstanceState.getByteArray(ARG_XSOLLA_FORM));
////                initView(mXsollaForm, mTranslations);
////            } catch (IOException e) {
////                e.printStackTrace();
////            }
//        } else {
//
//        }
//    }

    public interface OnFragmentInteractionListener {
        void onClickPay(HashMap<String, Object> xpsMap);
        void onClickChangePs();
    }

}
