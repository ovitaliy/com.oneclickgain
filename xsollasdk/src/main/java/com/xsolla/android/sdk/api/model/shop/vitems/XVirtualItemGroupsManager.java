package com.xsolla.android.sdk.api.model.shop.vitems;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 */
public class XVirtualItemGroupsManager implements IParseble {

    ArrayList<XVirtualItemGroup> listGroups;

    public ArrayList<XVirtualItemGroup> getListGroups() {
        return listGroups;
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONArray jarrGroups = jobj.optJSONArray("groups");
        if(jarrGroups != null) {
            listGroups = new ArrayList<>(jarrGroups.length());
            for (int i = 0; i < jarrGroups.length(); i++) {
                XVirtualItemGroup pricepoint = new XVirtualItemGroup();
                pricepoint.parse(jarrGroups.optJSONObject(i));
                listGroups.add(pricepoint);
            }
        }
    }

    @Override
    public String toString() {
        return "XVirtualItemGroupsManager{" +
                "listGroups=" + listGroups +
                '}';
    }
}
