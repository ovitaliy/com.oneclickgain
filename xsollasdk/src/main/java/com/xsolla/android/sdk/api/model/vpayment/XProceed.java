package com.xsolla.android.sdk.api.model.vpayment;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 */
public class XProceed implements IParseble {

    private boolean isInvoiceCreated;//"invoice_created":true,
    private long    operationId;//"operation_id":36504230,
    private String  error;

    public boolean isInvoiceCreated() {
        return isInvoiceCreated;
    }

    public long getOperationId() {
        return operationId;
    }

    public String getError() {
        return error;
    }

    @Override
    public void parse(JSONObject jobj) {
        isInvoiceCreated    = jobj.optBoolean("invoice_created");
        operationId         = jobj.optLong("operation_id");
        JSONArray jaErrors  = jobj.optJSONArray("errors");
        if(jaErrors != null && jaErrors.length() > 0)
            error = jaErrors.optJSONObject(0).optString("message");
    }
}
