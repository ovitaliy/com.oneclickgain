package com.xsolla.android.sdk.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Pair;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.XPurchase;
import com.xsolla.android.sdk.api.model.XError;
import com.xsolla.android.sdk.api.model.psystems.XCountryManager;
import com.xsolla.android.sdk.api.model.psystems.XPaymentSystemsManager;
import com.xsolla.android.sdk.api.model.shop.XPricepointsManager;
import com.xsolla.android.sdk.api.model.shop.XSubscriptionsManager;
import com.xsolla.android.sdk.api.model.util.XTranslationsKeys;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.api.model.vpayment.XVPStatus;
import com.xsolla.android.sdk.api.model.vpayment.XVPSummary;
import com.xsolla.android.sdk.api.model.сheckout.CurrentCommand;
import com.xsolla.android.sdk.api.model.сheckout.XDirectpayment;
import com.xsolla.android.sdk.api.payment.XPayment;
import com.xsolla.android.sdk.api.payment.XPaymentController;
import com.xsolla.android.sdk.view.fragment.CheckoutFragment;
import com.xsolla.android.sdk.view.fragment.ErrorFragment;
import com.xsolla.android.sdk.view.fragment.IShopInteractionListener;
import com.xsolla.android.sdk.view.fragment.ItemFragment;
import com.xsolla.android.sdk.view.fragment.PaymentSystemsFragment;
import com.xsolla.android.sdk.view.fragment.ShopFragment;
import com.xsolla.android.sdk.view.fragment.StatusFragment;
import com.xsolla.android.sdk.view.fragment.VPSummaryFragment;
import com.xsolla.android.sdk.view.fragment.XFragment;
import com.xsolla.android.sdk.view.generator.ShopViewGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class XsollaActivity extends AppCompatActivity implements
        XFragment.OnFragmentAppearedListener,
        IShopInteractionListener,
        PaymentSystemsFragment.OnFragmentInteractionListener,
        CheckoutFragment.OnFragmentInteractionListener,
        StatusFragment.OnFragmentInteractionListener,
        ItemFragment.OnListFragmentInteractionListener,
        ErrorFragment.OnFragmentInteractionListener,
        NavigationView.OnNavigationItemSelectedListener,
        VPSummaryFragment.OnFragmentInteractionListener {

    private static final String TAG = "MainActivity";

    public static final int REQUEST_CODE            = 9875;
    public static final String EXTRA_OBJECT         = "result_object";
    public static final String EXTRA_OBJECT_ID      = "payment_result";
    public static final String ARG_TOKEN            = "arg_token";
    public static final String ARG_IS_MODE_SANDBOX  = "arg_is_mode_sandbox";

    private String              accessToken;
    private boolean             isModeSandbox;

    private Stack<Fragment>     fragmentStack;
    private FragmentManager     fragmentManager;
    private XPaymentController  mPaymentController;

    private DrawerLayout            mDrawerLayout;
    private NavigationView          mNavigationView;
    private ActionBarDrawerToggle   mDrawerToggle;

    private TextView    mProgressBar;
    private View        mMainContainer, mLogoUpButton;

    private TextView tvSupportPhone, tvNeedHelp, tvContactUs, tvSupportEmail, tvCustomerSupport;

    private XUtils                  mUtils;
    private XCountryManager         mCountryManager;
    private XPaymentSystemsManager  mXpsManager;
    private ArrayList<Object>       mGroupsItemsList;
    private XPricepointsManager     mPricepointsManager;
    private XSubscriptionsManager   mSubscriptionsManager;
    private XDirectpayment          mDirectpayment;
    private XVPSummary              mVPSummary;
    private XVPStatus               mVPStatus;
    private XError                  mError;

    private boolean isFinished;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xsolla_drawer_layout);//xsolla_activity_xsolla

        fragmentStack   = new Stack<>();
        fragmentManager = getSupportFragmentManager();

        mMainContainer  = findViewById(R.id.xsolla_ll);
        mProgressBar    = (TextView) findViewById(R.id.progressBar);

        tvSupportPhone      = (TextView) findViewById(R.id.tvSupportPhone);
        tvNeedHelp          = (TextView) findViewById(R.id.tvNeedHelp);
        tvContactUs         = (TextView) findViewById(R.id.tvContactUs);
        tvSupportEmail      = (TextView) findViewById(R.id.tvSupportEmail);
        tvCustomerSupport   = (TextView) findViewById(R.id.tvCustomerSupport);

        initActionBar();
        showProgress();
        activation(savedInstanceState);

    }

    private void activation(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            accessToken = savedInstanceState.getString(ARG_TOKEN);
            isModeSandbox = savedInstanceState.getBoolean(ARG_IS_MODE_SANDBOX, false);
        } else {
            if (getIntent() != null && getIntent().getExtras() != null) {
                accessToken = getIntent().getExtras().getString(ARG_TOKEN);
                isModeSandbox = getIntent().getExtras().getBoolean(ARG_IS_MODE_SANDBOX, false);
            }
        }
        XPurchase purchase = new XPurchase(accessToken, isModeSandbox);
        XPayment.Params params2 = new XPayment.Params(purchase);
        mPaymentController = new XPaymentController(this, params2, listener);
        mPaymentController.startPayment();
    }

    void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
//        ActionBar actionBar = getSupportActionBar();
        setSupportActionBar(toolbar);
//        if(actionBar == null) {
//            if (toolbar != null) {
//                toolbar.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        onBackPressed();
//                    }
//                });
//                toolbar.setVisibility(View.VISIBLE);
//                toolbar.setLogo(new IconDrawable(this, IconMaker.IconValue.ic_arrow_back));
//                setSupportActionBar(toolbar);
//            }
//        } else {
//            actionBar.setLogo(new IconDrawable(this, IconMaker.IconValue.ic_arrow_back)
//                    .colorRes(R.color.xsolla_text_main)
//                    .actionBarSize());
//            actionBar.setDisplayHomeAsUpEnabled(true);
//        }
//
        toolbar.setLogo(R.drawable.xsolla_ic_arrow_back);
        mLogoUpButton = toolbar.getChildAt(1);
        int padding = mLogoUpButton.getPaddingTop();
        mLogoUpButton.setPadding(0, padding, 40, padding);
        mLogoUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mDrawerLayout = (DrawerLayout) findViewById(R.id.xsolla_drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.xsolla_navigation_drawer_open, R.string.xsolla_navigation_drawer_close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mNavigationView = (NavigationView) findViewById(R.id.xsolla_nav_view);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.xsolla_purple_d));
        }
    }

    void handleDrawerButton(Fragment fragment) {
        if(fragment instanceof ShopFragment) {
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            mLogoUpButton.setVisibility(View.GONE);
//            getSupportActionBar().setDisplayUseLogoEnabled(false);
        } else {
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            mLogoUpButton.setVisibility(View.VISIBLE);
//            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return(true);
        }
        return(super.onOptionsItemSelected(item));
    }

    private void showProgress() {
        mProgressBar.startAnimation(AnimationUtils.loadAnimation(this, R.anim.xsolla_rotation));
        mProgressBar.setVisibility(View.VISIBLE);
        mMainContainer.setVisibility(View.GONE);
    }

    private void hideProgress() {
        mProgressBar.clearAnimation();
        mProgressBar.setVisibility(View.GONE);
        mMainContainer.setVisibility(View.VISIBLE);
    }

    private void hideDrawer() {
        if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
            mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    private void setActionBarTitle(String newTitle) {
        if(getSupportActionBar() != null)
            getSupportActionBar().setTitle(newTitle);
    }

    XPayment.XPaymentListener listener = new XPayment.XPaymentListener() {

        @Override
        public void onUtilsReceived(XUtils utils) {
            mUtils = utils;

            String projectName = "Shop";
            if(utils.getProject() != null && utils.getProject().getName() != null)
                projectName = utils.getProject().getName();
            if(getSupportActionBar() != null)
                getSupportActionBar().setTitle(projectName);
            // FILL FOOTER
            tvSupportPhone.setText(utils.getTranslations().get(XTranslationsKeys.SUPPORT_PHONE));
            tvNeedHelp.setText(utils.getTranslations().get(XTranslationsKeys.SUPPORT_NEED_HELP));
            String contactUs = "<a href=\"http://help.xsolla.com/\" data-translate=\"support_contact_us\" class=\"ng-scope\">" + utils.getTranslations().get(XTranslationsKeys.SUPPORT_CONTACT_US) + "</a>";
            tvContactUs.setText(Html.fromHtml(contactUs));
            tvSupportEmail.setText("support@xsolla.com");
            tvCustomerSupport.setText(utils.getTranslations().get(XTranslationsKeys.SUPPORT_CUSTOMER_SUPPORT));
            tvSupportPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + ((TextView) v).getText().toString()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });

            tvSupportEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", ((TextView) v).getText().toString(), null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Need help");
                    startActivity(Intent.createChooser(emailIntent, "Send email..."));
                }
            });
            // FILL FOOTER X

        }

        @Override
        public void onUtilsReceivedAlt(Pair<XUtils, XCountryManager> utilsCountries) {
            XUtils utils = utilsCountries.first;
            mUtils          = utils;
            mCountryManager = utilsCountries.second;

            String projectName = "Shop";
            if(utils.getProject() != null && utils.getProject().getName() != null)
                projectName = utils.getProject().getName();
            if(getSupportActionBar() != null)
                getSupportActionBar().setTitle(projectName);
            // FILL FOOTER
            tvSupportPhone.setText(utils.getTranslations().get(XTranslationsKeys.SUPPORT_PHONE));
            tvNeedHelp.setText(utils.getTranslations().get(XTranslationsKeys.SUPPORT_NEED_HELP));
            String contactUs = "<a href=\"http://help.xsolla.com/\" data-translate=\"support_contact_us\" class=\"ng-scope\">" + utils.getTranslations().get(XTranslationsKeys.SUPPORT_CONTACT_US) + "</a>";
            tvContactUs.setText(Html.fromHtml(contactUs));
            tvSupportEmail.setText("support@xsolla.com");
            tvCustomerSupport.setText(utils.getTranslations().get(XTranslationsKeys.SUPPORT_CUSTOMER_SUPPORT));

            tvSupportPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + ((TextView) v).getText().toString()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });


            tvContactUs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", ((TextView) v).getText().toString(), null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Need help");
                    startActivity(Intent.createChooser(emailIntent, "Send email..."));
                }
            });

            tvSupportEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", ((TextView) v).getText().toString(), null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Need help");
                    startActivity(Intent.createChooser(emailIntent, "Send email..."));
                }
            });

            ShopViewGenerator.findAndAndInitView(XsollaActivity.this, mNavigationView, utils, utilsCountries.second, XsollaActivity.this);
        }

        @Override
        public void onShopRequired(XUtils utils) {
            displayFragment(ShopFragment.newInstance());
        }

        @Override
        public void onVirtualItemsReceived(ArrayList<Object> groupsItemsList) {
            mGroupsItemsList = groupsItemsList;
            displayFragment(ItemFragment.newInstance(2));
        }

        @Override
        public void onPricepointsReceived(XPricepointsManager manager) {
            mPricepointsManager = manager;
            displayFragment(ItemFragment.newInstance(0));
        }

        @Override
        public void onSubscriptionsReceived(XSubscriptionsManager manager) {
            mSubscriptionsManager = manager;
            displayFragment(ItemFragment.newInstance(1));
        }

        @Override
        public void onPaymentSystemsReceived(XPaymentSystemsManager manager) {
            mXpsManager = manager;
            displayFragment(PaymentSystemsFragment.newInstance());

        }

        @Override
        public void onVpSummaryRecieved(XVPSummary vpSummary) {
            mVPSummary = vpSummary;
            displayFragment(VPSummaryFragment.newInstance());
        }

        @Override
        public void onVPStatusRecieved(XVPStatus vpStatus) {
            mVPStatus = vpStatus;
            displayFragment(StatusFragment.newInstance());
        }

        @Override
        public void onDirectpaymentReceived(XDirectpayment checkout) {
            mDirectpayment = checkout;
            if(mDirectpayment.getCurrentCommand() != CurrentCommand.STATUS) {
                if(!tryOpenWebView()) {
                    displayFragment(CheckoutFragment.newInstance());
                } else {
                    mPaymentController.nextStep(mDirectpayment.getForm().getMapXps());
                }
            } else {
                displayFragment(StatusFragment.newInstance());
            }
        }

        @Override
        public void onErrorReceived(XError error) {
            mError = error;
            displayFragment(ErrorFragment.newInstance(error.toString()));
            String projectName = "Shop";
            if(getSupportActionBar() != null)
                getSupportActionBar().setTitle(projectName);
        }

    };

    private void displayFragment(Fragment fragment) {
        if(!isFinished) {
            fragmentStack.add(fragment);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.xsolla_container, fragment)
                    .commitAllowingStateLoss();
        }
//        hideProgress();
    }

    private boolean tryOpenWebView() {
        String checkoutToken = mDirectpayment.getCheckoutToken();
        boolean isLinkRequired = !"".equals(checkoutToken) && !"null".equals(checkoutToken) && !"false".equals(checkoutToken);
        if(isLinkRequired) {
            String link = "https://secure.xsolla.com/pages/checkout/?token=" + checkoutToken;

            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            alertBuilder.setTitle(mDirectpayment.getTitle());
            LinearLayout wrapper = new LinearLayout(this);
            WebView webView = new WebView(this);
            EditText keyboardHack = new EditText(this);
            keyboardHack.setVisibility(View.GONE);

            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return false;
                }
            });
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webView.loadUrl(link);

            wrapper.setOrientation(LinearLayout.VERTICAL);
            wrapper.addView(webView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            wrapper.addView(keyboardHack, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            alertBuilder.setView(wrapper);
            //mUtils.getTranslations().get("dismiss")
            alertBuilder.setNegativeButton("X", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    mPaymentController.nextStep(mDirectpayment.getForm().getMapXps());
                }
            });
            alertBuilder.create().show();

            return true;
        }
        return false;
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    * * * * * * * * * * * * * * * * * * DATA GETTERS* * * * * * * * * * * * * * * * * * * * * * *
    * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    public XUtils getUtils() {
        return mUtils;
    }

    public XCountryManager getCountries() {
        return mCountryManager;
    }

    public XPaymentSystemsManager getXpsManager() {
        return mXpsManager;
    }

    public ArrayList<Object> getVirtulaItemsList() {
        return mGroupsItemsList;
    }

    public XPricepointsManager getPricepointsManager() {
        return mPricepointsManager;
    }

    public XSubscriptionsManager getSubscriptionsManager() {
        return mSubscriptionsManager;
    }

    public XDirectpayment getDirectpayment() {
        return mDirectpayment;
    }

    public XVPSummary getVPSummary() {
        return mVPSummary;
    }


    public XVPStatus getVPStatus() {
        return mVPStatus;
    }

    public XError getError() {
        if(mError != null)
            return mError;
        else
            return new XError("0", "Canceled");
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    * * * * * * * * * * * * * FRAGMENTS INTERACTIONS* * * * * * * * * * * * * * * * * * * * * * *
    * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    @Override
    public void onFragmentAppeared() {
        XFragment currentFragment = (XFragment) fragmentStack.peek();
        handleDrawerButton(currentFragment);
        if(getSupportActionBar() != null)
            if(currentFragment instanceof ErrorFragment || currentFragment instanceof StatusFragment) {
                getSupportActionBar().hide();
            } else {
                getSupportActionBar().show();
            }
        hideProgress();
    }

    //TODO fix it
    @Override
    public void onFragmentCreated() {
        XFragment currentFragment = (XFragment) fragmentStack.peek();
        setActionBarTitle(currentFragment.getTitle());
    }

    @Override
    public void onPaymentSystemSelected(long pid) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("pid", pid);
        map.put("returnUrl", "");
        mPaymentController.nextStep(map);
        showProgress();
    }

    @Override
    public void onClickPay(HashMap<String, Object> xpsMap) {
        mPaymentController.nextStep(xpsMap);
        showProgress();
    }

    @Override
    public void onClickChangePs() {
        mPaymentController.prevStep();
        mPaymentController.loadPaymentSystems();
        showProgress();
    }

    @Override
    public void onMenuSelected(int action) {
        hideDrawer();
        switch (action){
            case 0:
                mPaymentController.loadVirtualItems();//loadVirtualItemsGroups();
                break;
            case 1:
                mPaymentController.loadSubscriptions();
                break;
            case 2:
                mPaymentController.loadPricepoints();
                break;
        }
        showProgress();
    }

    @Override
    public void onCountryChanged(String iso) {
        mPaymentController.repeatWithNewCountry(iso);
        hideDrawer();
        showProgress();
    }

    @Override
    public void onOnProductSelected(HashMap<String, Object> map, boolean isVirtualPayment) {
        if(!isVirtualPayment) {
            mPaymentController.nextStep(map);
        } else {
            map.put("is_virtual_payment", 1);
            mPaymentController.loadVpSummary(map);
        }
        showProgress();
    }

    @Override
    public void onClickComplete() {
        long statusId;
        if(getDirectpayment() != null) {
            statusId = getDirectpayment().getStatus().registerObject();
        } else {
            statusId = getVPStatus().registerObject();
        }
        finishWithResult(true, statusId);
    }

    @Override
    public void onClickClose(String errorMsg) {
        long errorId = getError().registerObject();
        finishWithResult(false, errorId);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.xsolla_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (fragmentStack.size() >= 2) {
            boolean isStatus = fragmentStack.get(fragmentStack.size() -1 ) instanceof StatusFragment;
            if(!isStatus) {
                boolean b = fragmentStack.get(fragmentStack.size() - 1) instanceof ItemFragment;
                mPaymentController.prevStep();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                fragmentStack.lastElement().onPause();
                Class lastElementClass = fragmentStack.lastElement().getClass();
                ft.remove(fragmentStack.pop());
                toPrevStep(lastElementClass);
                if(fragmentStack.size() > 0) {
                    fragmentStack.lastElement().onResume();
                    ft.replace(R.id.xsolla_container, fragmentStack.lastElement());
                    ft.commit();
                } else {
                    long errorId = getError().registerObject();
                    finishWithResult(false, errorId);
                    super.onBackPressed();
                }
            } else {
                long statusId;
                if(getDirectpayment() != null) {
                    statusId = getDirectpayment().getStatus().registerObject();
                } else {
                    statusId = getVPStatus().registerObject();
                }
                finishWithResult(true, statusId);
            }
        } else {
            long errorId = getError().registerObject();
            finishWithResult(false, errorId);
            super.onBackPressed();
        }
    }

    private void toPrevStep(Class lastElementClass) {
        if(!fragmentStack.isEmpty()) {
            if(fragmentStack.lastElement().getClass() == lastElementClass) {
                fragmentStack.pop();
                toPrevStep(lastElementClass);
            }
        }
    }


    @Override
    public void onClickBuyVp(int isConfirmationDisabled) {
        HashMap<String, Object> map = new HashMap<>(1);
        map.put("dont_ask_again", isConfirmationDisabled);
        mPaymentController.proceedVP(map);
        showProgress();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        cancelPayment();
        outState.putString(ARG_TOKEN, accessToken);
        outState.putBoolean(ARG_IS_MODE_SANDBOX, isModeSandbox);
        //Save the fragment's instance
//        cancelPayment();
//        outState.putLong(REQUEST_ID, mRequestId);
        //outState.putSerializable(ARG_REQUEST_PARAMS, mParameters);
//        outState.putBoolean(ARG_IS_MODE_SANDBOX, mIsModeSandbox);
//        if (mContent != null) {
//            getSupportFragmentManager().putFragment(outState, "mContent", mContent);
//        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null) {
            accessToken = savedInstanceState.getString(ARG_TOKEN);
            isModeSandbox = savedInstanceState.getBoolean(ARG_IS_MODE_SANDBOX, false);
            XPurchase purchase = new XPurchase(accessToken, isModeSandbox);
            XPayment.Params params2 = new XPayment.Params(purchase);
            mPaymentController = new XPaymentController(this, params2, listener);
            mPaymentController.startPayment();
        }
    }

    private void finishWithResult(boolean isOk, long registeredObjectId) {
        cancelPayment();
        isFinished = true;
        Bundle data = new Bundle();
        data.putLong(EXTRA_OBJECT_ID, registeredObjectId);
        Intent intent = new Intent();
        intent.putExtras(data);
        int resultCode;
        if (isOk) {
            resultCode = RESULT_OK;
        } else {
            resultCode = RESULT_CANCELED;
        }

        if (getParent() == null) {
            setResult(resultCode, intent);
        } else {
            getParent().setResult(resultCode, intent);
        }
        finish();
    }

    private void cancelPayment(){
        if(mPaymentController != null) {
            mPaymentController.cancel();
            mPaymentController = null;
        }
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    * * * * * * * * * * * * * * * * * ACTIVITY STATE* * * * * * * * * * * * * * * * * * * * * * *
    * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mPaymentController == null) {
            if (getIntent() != null && getIntent().getExtras() != null) {
                accessToken = getIntent().getExtras().getString(ARG_TOKEN);
                isModeSandbox = getIntent().getExtras().getBoolean(ARG_IS_MODE_SANDBOX, false);
            }
            XPurchase purchase = new XPurchase(accessToken, isModeSandbox);
            XPayment.Params params2 = new XPayment.Params(purchase);
            mPaymentController = new XPaymentController(this, params2, listener);
            mPaymentController.startPayment();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

}
