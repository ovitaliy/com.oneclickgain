package com.xsolla.android.sdk.api.model.psystems;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
public class XCountryManager implements IParseble{

    private String                      iso;//"iso":"US",
    private ArrayList<XCountry>         countries;//countryList":[
    private HashMap<String, XCountry>   mapCountries;

    public void addCountry(XCountry country) {
        if(!mapCountries.containsKey(country.getIso())) {
            countries.add(country);
            mapCountries.put(country.getIso(), country);
        }
    }

    public XCountry getCountry(int position) {
        return countries.get(position);
    }

    public XCountry getCountry(String iso) {
        return mapCountries.get(iso);
    }

    public String getCurrentCountryName() {
        return getCountry(iso).getName();
    }

    public int getCurrentCountryIndex() {
        return  countries.indexOf(getCountry(iso));
    }

    public String[] getCountries(){
        String[] arr = new String[countries.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = countries.get(i).getName();
        }
        return arr;
    }

    @Override
    public void parse(JSONObject jobj) {
        iso = jobj.optString("iso");
        JSONArray jarr = jobj.optJSONArray("countryList");
        if(jarr != null) {
            countries = new ArrayList<>(jarr.length());
            mapCountries = new HashMap<>(jarr.length());
            for (int i = 0; i < jarr.length(); i++) {
                XCountry newItem = new XCountry();
                newItem.parse(jarr.optJSONObject(i));
                addCountry(newItem);
            }
        }
    }

    @Override
    public String toString() {
        return "XCountryManager{" +
                "iso='" + iso + '\'' +
                ", countries=" + countries +
                '}';
    }
}
