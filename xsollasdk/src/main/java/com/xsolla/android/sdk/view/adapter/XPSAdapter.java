package com.xsolla.android.sdk.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.psystems.XPaymentSystem;
import com.xsolla.android.sdk.util.ImageLoader;
import com.xsolla.android.sdk.view.fragment.PaymentSystemsFragment.OnFragmentInteractionListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class XPSAdapter extends RecyclerView.Adapter<XPSAdapter.ViewHolder> implements Filterable {

    private List<XPaymentSystem>            mListToShow;
    private List<XPaymentSystem>            mBasicList;
    private Map<String, XPaymentSystem>     mMapAll;
    private OnFragmentInteractionListener   mListener;


    public XPSAdapter(Map<String, XPaymentSystem> items, List<XPaymentSystem>  list, OnFragmentInteractionListener listener) {
        this.mMapAll = items;
        ArrayList<XPaymentSystem> listToSort = new ArrayList<>(mMapAll.values());
        mListToShow = list.subList(0, 31);
        mBasicList = list.subList(0, 31);
        this.mListener = listener;
    }

    public XPaymentSystem getItem(int position) {
        return mListToShow.get(position);
    }

    public void getDefault() {
        mListToShow = mBasicList;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mListToShow = (List<XPaymentSystem>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String constraintString = constraint.toString().toLowerCase();
                FilterResults results = new FilterResults();
                ArrayList<XPaymentSystem> filteredArray = new ArrayList<XPaymentSystem>();

                for (XPaymentSystem current : mMapAll.values()) {
                    String currentName  = current.getName().toLowerCase();
                    String currentId    = Integer.toString(current.getId()).toLowerCase();
                    if (currentName.contains(constraintString) || currentId.equals(constraintString)) {
                        filteredArray.add(current);
                    }
                }

                results.count = filteredArray.size();
                results.values = filteredArray;
                return results;
            }
        };
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.xsolla_ps_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        XPaymentSystem item = getItem(position);
        holder.mPaymentSystem = item;
        String imgUrl = item.getImgUrl();
        switch (imgUrl) {
            case "https:creditCards":
                holder.mImageView.setImageResource(R.drawable.xsolla_ic_cc);
                break;
            case "https:mobile":
                holder.mImageView.setImageResource(R.drawable.xsolla_ic_mobile);
                break;
            default:
                ImageLoader.getInstance().loadImage(holder.mImageView, imgUrl);
        }
        holder.mTvTitle.setText(item.getName());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPaymentSystemSelected(holder.mPaymentSystem.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListToShow.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        public final View        mView;
        public final ImageView   mImageView;
        public final TextView    mTvTitle;
        public XPaymentSystem mPaymentSystem;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mImageView = (ImageView)itemView.findViewById(R.id.xsolla_iv);
            mTvTitle = (TextView) itemView.findViewById(R.id.xsolla_tv_title);
        }
    }

}
