package com.xsolla.android.sdk.api.model.shop;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 */
public class XPricepointsManager implements IParseble {

    private ArrayList<XPricepoint>  listPricepoints;
    private String                  projectCurrency;

    public ArrayList<XPricepoint> getList() {
        return listPricepoints;
    }

    public String getProjectCurrency() {
        return projectCurrency;
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONArray jarrList = jobj.optJSONArray("list");
        if(jarrList != null) {
            listPricepoints = new ArrayList<>(jarrList.length());
            for (int i = 0; i < jarrList.length(); i++) {
                XPricepoint pricepoint = new XPricepoint();
                pricepoint.parse(jarrList.optJSONObject(i));
                listPricepoints.add(pricepoint);
            }
        }

        projectCurrency = jobj.optString("projectCurrency");
    }

    @Override
    public String toString() {
        return "XPricepointsManager{" +
                "listPricepoints=" + listPricepoints +
                ", projectCurrency='" + projectCurrency + '\'' +
                '}';
    }
}
