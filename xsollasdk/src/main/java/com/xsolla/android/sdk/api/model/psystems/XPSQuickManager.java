package com.xsolla.android.sdk.api.model.psystems;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 */
public class XPSQuickManager implements IParseble {

    private ArrayList<XPaymentSystemBase> listQuickPayments;

    public ArrayList<XPaymentSystemBase> getList() {
        return listQuickPayments;
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONArray jarr = jobj.optJSONArray("instances");
        if(jarr != null) {
            listQuickPayments = new ArrayList<>(jarr.length());
            for (int i = 0; i < jarr.length(); i++) {
                XPaymentSystemBase newItem = new XPaymentSystemBase();
                newItem.parse(jarr.optJSONObject(i));
                if(newItem.getId() != 3012)
                    listQuickPayments.add(newItem);
            }
        }
    }

    @Override
    public String toString() {
        return "XPSQuickManager{" +
                "listQuickPayments=" + listQuickPayments +
                '}';
    }
}
