package com.xsolla.android.sdk.api.model.сheckout;

import com.xsolla.android.sdk.api.model.IParseble;
import com.xsolla.android.sdk.util.PriceFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class XDirectpayment implements IParseble {

    private String                  title;// "title" : "Credit\/Debit Cards",
    private String                  account;// "account" : "", ?????? ????? Xsolla ?????
    private String                  accountXsolla;// "accountXsolla" : "", ?????? ????? Xsolla ?????
    private String                  onlineBanking;// "onlineBanking" : "",
    private String                  report;// "report" : null,
    private String                  backURL;// "backURL" : "",
    private String                  formURL;// "formURL" : "?theme=100&project=16184&marketplace=paystation&action=directpayment&signparams=email%2Cv1%2Cv2%2Cproject%2Clocal%2Ccurrency&v1=user_1&v2=John+Smith&sku%5B1488%5D=1&email=support%40xsolla.com§cy=USD&hidden=country&country=US&local=en&pid=1380",
    private String                  instructions;// "instruction" : "",
    private String                  currency;// "currency" : "USD",
    private String                  virtualCurrencyName;// "virtualCurrencyName" : "Coins",
    private String                  marketplace;// "marketplace" : "paystation",
    private String                  currentCommand;// "currentCommand" : "form",
    private String                  requestID;// "requestID" : null,
    private String                  iconUrl;// "iconUrl" : "\/\/cdn3.xsolla.com\/paymentoptions\/paystation\/theme_33\/143x83\/1380.1447067220.png",
    private String                  action;// "action" : null,
    private String                  checkoutForm;//checkoutForm: "<form action="https://www.paypal.com/cgi-bin/webscr" id="pay" target="_blank" method="GET"><input type="hidden" name="useraction" value="commit" /><input type="hidden" name="cmd" value="_express-checkout" /><input type="hidden" name="token" value="EC-4MA281074M132682T" /><input type="hidden" name="force_sa" value="true" /></form>"
    private String                  checkoutToken;//checkoutToken: "CouZEqk"

    private boolean                 isCheckoutToken;// "checkoutToken" : false,
    private boolean                 isBackVisible;// "backVisible" : true,
    private boolean                 isSkipForm;// "skipForm" : false,
    private boolean                 isSkipCheckout;// "skipCheckout" : false,
    private boolean                 isFormHidden;// "formIsHidden" : false,
    private boolean                 isFormBeenSubmitted;// "hasFormBeenSubmitted" : false,
    private boolean                 isInvoiceCreated;// "invoiceCreated" : false,

    private int                     schemeRedirect;// "schemeRedirect" : 0,
    private int                     pid;// "pid" : 1380,

    private ArrayList<XInfoError>   errors;// "errors" : [],
    private ArrayList<XInfoMessage> messages;// "messages" : [],
    private XCheckout               chekout;// "checkout" : {},
    private XFormParams             formParams;// "formParams" : {},
    private XBuyData                buyData;// "buyData" : {},
    private XStatus                 status;// "status" : null,
    private XForm                   form;// "form" : {},
    private XSummary                summary;// "summary" : {},

   public XDirectpayment() {
       errors       = new ArrayList<>();
       messages     = new ArrayList<>();
       chekout      = new XCheckout();
       formParams   = new XFormParams();
       buyData      = new XBuyData();
       status       = new XStatus();
       form         = new XForm();
   }


    public boolean isAccount() {
        return !"".equals(account);
    }

    public boolean isXsollaAccount() {
        return !"".equals(accountXsolla);
    }

    public String getAccount() {
        return account;
    }

    public String getAccountXsolla() {
        return accountXsolla;
    }

    public CurrentCommand getCurrentCommand() {
        switch (currentCommand) {
            case "form":
                return CurrentCommand.FORM;
            case "create":
                return CurrentCommand.CREATE;
            case "checkout":
                return CurrentCommand.CHECKOUT;
            case "check":
                return CurrentCommand.CHECK;
            case "status":
                return CurrentCommand.STATUS;
            case "account":
                return CurrentCommand.ACCOUNT;
            default:
                return CurrentCommand.UNKNOWN;
        }
    }

    public boolean isSkipForm() {
        return isSkipForm;
    }

    public XInfoError getXError() {
        if(errors.size() > 0)
            return errors.get(0);
        else
            return null;
    }

    public ArrayList<XInfoError> getErrors() {
        return errors;
    }

    public ArrayList<XInfoMessage> getMessages() {
        return messages;
    }


    public boolean isNextText() {
        return buyData != null && !"null".equals(buyData.getValue()) && !"".equals(buyData.getValue());
    }

    public String getNextText() {
        return buyData.getValue();
    }

    public XForm getForm() {
        return form;
    }

    public String getIconUrl() {
        return iconUrl.startsWith("https:") ? iconUrl : "https:" + iconUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getSumTotal() {
        if(summary != null && summary.getFinance() != null  && summary.getFinance().getTotal() != null) {
            return summary.getFinance().getTotal().getPrice();
        }
        return PriceFormatter.formatPrice(currency, buyData.sum);
    }

    public String getInstruction() {
        return instructions;
    }

    public String getCheckoutToken() {
        return checkoutToken;
    }

    public int getPid() {
        return pid;
    }

    public boolean isCreditCards() {
        return pid == 26 || pid == 1380;
    }

    public XStatus getStatus() {
        return status;
    }

    public XSummary getSummary() {
        return summary;
    }

    @Override
    public void parse(JSONObject jobj) {

        this.title                  = jobj.optString("title");
        this.account                = jobj.optString("account");
        this.accountXsolla          = jobj.optString("accountXsolla");
        this.onlineBanking          = jobj.optString("onlineBanking");
        this.report                 = jobj.optString("report");
        this.backURL                = jobj.optString("backURL");
        this.formURL                = jobj.optString("formURL");
        this.instructions           = jobj.optString("instruction");
        this.currency               = jobj.optString("currency");
        this.virtualCurrencyName    = jobj.optString("virtualCurrencyName");
        this.marketplace            = jobj.optString("marketplace");
        this.currentCommand         = jobj.optString("currentCommand");
        this.requestID              = jobj.optString("requestID");
        this.iconUrl                = jobj.optString("iconUrl");
        this.action                 = jobj.optString("action");
        this.checkoutForm           = jobj.optString("checkoutForm");
        this.checkoutToken          = jobj.optString("checkoutToken");

        this.isCheckoutToken        = jobj.optBoolean("checkoutToken");
        this.isBackVisible          = jobj.optBoolean("backVisible");
        this.isSkipForm             = jobj.optBoolean("skipForm");
        this.isSkipCheckout         = jobj.optBoolean("skipCheckout");
        this.isFormHidden           = jobj.optBoolean("formIsHidden");
        this.isFormBeenSubmitted    = jobj.optBoolean("hasFormBeenSubmitted");
        this.isInvoiceCreated       = jobj.optBoolean("invoiceCreated");

        this.schemeRedirect         = jobj.optInt("schemeRedirect");
        this.pid                    = jobj.optInt("pid");

        JSONArray jarrError = jobj.optJSONArray("errors");
        if(jarrError != null)
            for (int i = 0; i < jarrError.length(); i++) {
                JSONObject jobjError = jarrError.optJSONObject(i);
                XInfoError newError = new XInfoError();
                if(jobjError != null) {
                    newError.parse(jobjError);
                    errors.add(newError);
                }
            }
//        errors.add(new XInfoError(1002, "TEST ERROR"));

        JSONArray jarrMsg = jobj.optJSONArray("messages");
        if(jarrMsg != null)
            for (int i = 0; i < jarrMsg.length(); i++) {
                JSONObject jobjMsg = jarrMsg.optJSONObject(i);
                XInfoMessage newMsg = new XInfoMessage();
                if(jobjMsg != null) {
                    newMsg.parse(jobjMsg);
                    messages.add(newMsg);
                }
            }
//        messages.add(new XInfoMessage("SIGMA", "TEST MESSAGE"));
//        messages.add(new XInfoMessage("ZETA", " TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2 TEST MESSAGE2"));

        JSONObject jobjCheckout = jobj.optJSONObject("checkout");
        if(jobjCheckout != null)
            chekout.parse(jobjCheckout);

        JSONObject jobjFormParams = jobj.optJSONObject("formParams");
        if(jobjFormParams != null)
            formParams.parse(jobjFormParams);

        JSONObject jobjBuyData = jobj.optJSONObject("buyData");
        if(jobjBuyData != null)
            buyData.parse(jobjBuyData);

        JSONObject jobjStatus = jobj.optJSONObject("status");
        if(jobjStatus != null)
            status.parse(jobjStatus);

        JSONObject jobjSummary = jobj.optJSONObject("summary");
        if(jobjSummary != null) {
            summary = new XSummary();
            summary.parse(jobjSummary);
        }


        JSONObject jobjForm = jobj.optJSONObject("form");
        if(jobjForm != null)
            form.parse(jobjForm);

    }

    @Override
    public String toString() {
        return "XDirectpayment{" +
                "\n title='" + title + '\'' +
                ", \naccount='" + account + '\'' +
                ", \naccountXsolla='" + accountXsolla + '\'' +
                ", \nonlineBanking='" + onlineBanking + '\'' +
                ", \nreport='" + report + '\'' +
                ", \nbackURL='" + backURL + '\'' +
                ", \nformURL='" + formURL + '\'' +
                ", \ninstructions='" + instructions + '\'' +
                ", \ncurrency='" + currency + '\'' +
                ", \nvirtualCurrencyName='" + virtualCurrencyName + '\'' +
                ", \nmarketplace='" + marketplace + '\'' +
                ", \ncurrentCommand='" + currentCommand + '\'' +
                ", \nrequestID='" + requestID + '\'' +
                ", \niconUrl='" + iconUrl + '\'' +
                ", \naction='" + action + '\'' +
                ", \ncheckoutForm='" + checkoutForm + '\'' +
                ", \ncheckoutToken='" + checkoutToken + '\'' +
                ", \nisCheckoutToken=" + isCheckoutToken +
                ", \nisBackVisible=" + isBackVisible +
                ", \nisSkipForm=" + isSkipForm +
                ", \nisSkipCheckout=" + isSkipCheckout +
                ", \nisFormHidden=" + isFormHidden +
                ", \nisFormBeenSubmitted=" + isFormBeenSubmitted +
                ", \nisInvoiceCreated=" + isInvoiceCreated +
                ", \nschemeRedirect=" + schemeRedirect +
                ", \npid=" + pid +
                ", \nerrors=" + errors +
                ", \nmessages=" + messages +
                ", \nchekout=" + chekout +
                ", \nformParams=" + formParams +
                ", \nbuyData=" + buyData +
                ", \nsummary=" + summary +
                ", \nstatus=" + status  +
                ", \nform=" + form +
                '}';
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
    * INNER CLASSES
    * * * * * * * * * * * * * * * * * * * * * * * * * * */

    public class XInfoError implements IParseble {
        int     code;// "code":1015,
        String  message;// "message" : "Please enter a valid card number",
        String  elementName;// "element_name" : ""

        public XInfoError() {
        }

        public XInfoError(int code, String message) {
            this.code = code;
            this.message = message;
        }

        @Override
        public void parse(JSONObject jobj) {
            this.code           = jobj.optInt("code");
            this.message        = jobj.optString("message");
            this.elementName    = jobj.optString("elementName");
        }

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getElementName() {
            return elementName;
        }

        @Override
        public String toString() {
            return "\nXInfoError{" +
                    "code=" + code +
                    ", message='" + message + '\'' +
                    ", elementName='" + elementName + '\'' +
                    '}';
        }
    };

    public class XInfoMessage implements IParseble {
        String code;// "code":"statustext",
        String message;// "message":"In sandbox mode there is no redirect to the payment systems (in production mode user will be forwarded to the payment system). Press ìNextî to continue"

        public XInfoMessage() {
        }

        public XInfoMessage(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        @Override
        public void parse(JSONObject jobj) {
            this.code       = jobj.optString("code");
            this.message    = jobj.optString("message");
        }

        @Override
        public String toString() {
            return "\nXInfoMessage{" +
                    "code='" + code + '\'' +
                    ", message='" + message + '\'' +
                    '}';
        }
    };

    class XCheckout implements IParseble {
        String action;// "action":null,
        String method;// "method" : null,
        String data;// "data" : []

        @Override
        public void parse(JSONObject jobj) {
            this.action = jobj.optString("action");
            this.method = jobj.optString("method");
            this.data   = jobj.optString("data");
        }

        @Override
        public String toString() {
            return "\nXCheckout{" +
                    "action='" + action + '\'' +
                    ", method='" + method + '\'' +
                    ", data='" + data + '\'' +
                    '}';
        }
    };

    class XFormParams implements IParseble {
        int     pid;// "pid" : 1380
        int     theme;// "theme":100,
        int     project;// "project" : 16184,
        String  marketplace;// "marketplace" : "paystation",
        String  action;// "action" : "directpayment",
        String  signparams;// "signparams" : "email,v1,v2,project,local,currency",
        String  v1;// "v1" : "user_1",
        String  v2;// "v2" : "John Smith",
        String  sku;// "sku":{ "1488":"1" },
        String  email;// "email" : "support@xsolla.com",
        String  currency;// "currency" : "USD",
        String  hidden;// "hidden" : "country",
        String  country;// "country" : "US",
        String  local;// "local" : "en",
        String  idPackage;// "id_package" : "c9000261",

        @Override
        public void parse(JSONObject jobj) {

            this.pid        = jobj.optInt("pid");
            this.theme      = jobj.optInt("theme");
            this.project    = jobj.optInt("project");

            this.marketplace    = jobj.optString("marketplace");
            this.action         = jobj.optString("action");
            this.signparams     = jobj.optString("signparams");
            this.action         = jobj.optString("action");
            this.v1             = jobj.optString("v1");
            this.v2             = jobj.optString("v2");
            this.sku            = jobj.optString("sku");
            this.email          = jobj.optString("email");
            this.currency       = jobj.optString("currency");
            this.hidden         = jobj.optString("hidden");
            this.country        = jobj.optString("country");
            this.local          = jobj.optString("local");
            this.idPackage      = jobj.optString("id_package");
        }

        @Override
        public String toString() {
            return "\nXFormParams{" +
                    "pid=" + pid +
                    ", theme=" + theme +
                    ", project=" + project +
                    ", marketplace='" + marketplace + '\'' +
                    ", action='" + action + '\'' +
                    ", signparams='" + signparams + '\'' +
                    ", v1='" + v1 + '\'' +
                    ", v2='" + v2 + '\'' +
                    ", sku='" + sku + '\'' +
                    ", email='" + email + '\'' +
                    ", currency='" + currency + '\'' +
                    ", hidden='" + hidden + '\'' +
                    ", country='" + country + '\'' +
                    ", local='" + local + '\'' +
                    ", idPackage='" + idPackage + '\'' +
                    '}';
        }
    };

    class XBuyData implements IParseble {
        private boolean isEnabled;// "enabled" : true
        private String  name;// "name":"sub",
        private String  title;// "title" : null,
        private String  type;// "type" : "submit",
        private String  example;// "example" : null,
        private String  value;// "value" : "Pay Now",
        private String  buyData;// "options" : null,
        private String  isMandatory;// "isMandatory" : null,
        private String  isReadonly;// "isReadonly" : null,
        private String  isVisible;// "isVisible" : "1",
        private String  isPakets;// "isPakets" : null,
        private String  tooltip;// "tooltip" : null,
        private String  sum;// "sum" : "0.99",
        private String  currency;// "currency" : "USD",

        public boolean isEnabled() {
            return isEnabled;
        }

        public String getName() {
            return name;
        }

        public String getTitle() {
            return title;
        }

        public String getType() {
            return type;
        }

        public String getExample() {
            return example;
        }

        public String getValue() {
            return value;
        }

        public String getBuyData() {
            return buyData;
        }

        public String getIsMandatory() {
            return isMandatory;
        }

        public String getIsReadonly() {
            return isReadonly;
        }

        public String getIsVisible() {
            return isVisible;
        }

        public String getIsPakets() {
            return isPakets;
        }

        public String getTooltip() {
            return tooltip;
        }

        public String getSum() {
            return sum;
        }

        public String getCurrency() {
            return currency;
        }

        @Override
        public void parse(JSONObject jobj) {

            this.isEnabled      = jobj.optBoolean("name");
            this.name           = jobj.optString("name");
            this.title          = jobj.optString("title");
            this.type           = jobj.optString("type");
            this.example        = jobj.optString("example");
            this.value          = jobj.optString("value");
            this.buyData        = jobj.optString("buyData");
            this.isMandatory    = jobj.optString("isMandatory");
            this.isReadonly     = jobj.optString("isReadonly");
            this.isVisible      = jobj.optString("isVisible");
            this.isPakets       = jobj.optString("isPakets");
            this.tooltip        = jobj.optString("tooltip");
            this.sum            = jobj.optString("sum");
            this.currency       = jobj.optString("currency");
        }

        @Override
        public String toString() {
            return "\nXBuyData{" +
                    "isEnabled=" + isEnabled +
                    ", name='" + name + '\'' +
                    ", title='" + title + '\'' +
                    ", type='" + type + '\'' +
                    ", example='" + example + '\'' +
                    ", value='" + value + '\'' +
                    ", buyData='" + buyData + '\'' +
                    ", isMandatory='" + isMandatory + '\'' +
                    ", isReadonly='" + isReadonly + '\'' +
                    ", isVisible='" + isVisible + '\'' +
                    ", isPakets='" + isPakets + '\'' +
                    ", tooltip='" + tooltip + '\'' +
                    ", sum='" + sum + '\'' +
                    ", currency='" + currency + '\'' +
                    '}';
        }
    };

}
