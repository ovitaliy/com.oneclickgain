package com.xsolla.android.sdk.api.model.vpayment;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 */
public class XVPSummary implements IParseble {

//		{
//			"finance":{
//				"total_without_discount":{
//					"vc_amount":100
//				},
//				"total":{
//					"vc_amount":100
//				}
//			},
//			"purchase":{
//				"virtual_items":[
//				   {
//					"name":"Test product 007",
//					"image_url":"\/\/cdn3.xsolla.com\/img\/misc\/merchant\/default-item.png",
//					"quantity":1
//				   }
//				   ]
//			},
//			"skip_confirmation":false,
//			"api":{
//				"ver":"1.0.1"
//			}
//		}


    private int 		                        totalWithoutDiscount;
    private int 		                        total;

    private boolean                          isError = false;
    private String                           proceedError;

    private boolean                          isSkipConfirmation;
    private ArrayList<XVirtualItemSimple>    items;

    public boolean isSkipConfirmation() {
        return isSkipConfirmation;
    }

    public int getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    public String getTotal() {
        return Integer.toString(total);
    }

    public ArrayList<XVirtualItemSimple> getItems() {
        return items;
    }

    public boolean isError() {
        return isError;
    }

    public String getProceedError() {
        return proceedError;
    }

    public void setProceedError(String proceedError) {
        isError = true;
        this.proceedError = proceedError;
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONObject joFinance    = jobj.optJSONObject("finance");
        totalWithoutDiscount    = joFinance.optJSONObject("total_without_discount").optInt("vc_amount");
        total                   = joFinance.optJSONObject("total").optInt("vc_amount");
        isSkipConfirmation      = jobj.optBoolean("skip_confirmation");
        JSONObject joPurchase   = jobj.optJSONObject("purchase");
        JSONArray jaVItems      = joPurchase.optJSONArray("virtual_items");
        items = new ArrayList<>(jaVItems.length());
        for(int i = 0; i < jaVItems.length(); i++) {
            XVirtualItemSimple newItem = new XVirtualItemSimple();
            newItem.parse(jaVItems.optJSONObject(i));
            items.add(newItem);
        }
    }

    @Override
    public String toString() {
        return "XVPSummary{" +
                "totalWithoutDiscount=" + totalWithoutDiscount +
                ", total=" + total +
                ", isSkipConfirmation=" + isSkipConfirmation +
                ", items=" + items +
                '}';
    }
}
