package com.xsolla.android.sdk.api.model.psystems;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONObject;

/**
 *
 */
public class XPaymentSystemBase implements IParseble {

    protected int     id;//"id":129,
    protected String  name;// "name":"Rixty",

    public XPaymentSystemBase() {
    }

    public XPaymentSystemBase(int id, String name) {
        this.id = id;
        this.name = name;
    }

    protected void continueParse(JSONObject jobj) {};

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public void parse(JSONObject jobj) {
        id      = jobj.optInt("id");
        name    = jobj.optString("name");
        continueParse(jobj);
    }

    @Override
    public String toString() {
        return "\nXPaymentSystemBase{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
