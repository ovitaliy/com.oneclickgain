package com.xsolla.android.sdk.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.сheckout.XSummary.XFinance.XFinanceItemBase;

import java.util.ArrayList;

/**
 *
 */
public class FinanceItemsAdapter extends BaseAdapter {

    private Context                     mContext;
    private ArrayList<XFinanceItemBase> mElements;

    public FinanceItemsAdapter(Context mContext, ArrayList<XFinanceItemBase> mElements) {
        this.mContext   = mContext;
        this.mElements  = mElements;
    }

    @Override
    public int getCount() {
        return mElements.size();
    }

    @Override
    public XFinanceItemBase getItem(int position) {
        return mElements.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.xsolla_checkout_summary_finance, null);

            holder.tvTitle          = (TextView)convertView.findViewById(R.id.xsolla_tv_title);
            holder.tvPrice          = (TextView)convertView.findViewById(R.id.xsolla_tv_price);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        XFinanceItemBase item = getItem(position);

        holder.tvTitle.setText(item.getPrice());
        holder.tvPrice.setText(item.getPrice());

        return convertView;
    }

    static class ViewHolder{
        TextView    tvTitle, tvPrice;
    }

}
