package com.xsolla.android.sdk.view.generator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.сheckout.XSummary;
import com.xsolla.android.sdk.view.adapter.SummaryItemsAdapter;

import java.util.HashMap;

/**
 *
 */
public class SummaryGenerator {

    public static View drawSummary(Context context, XSummary summary, HashMap<String, String> translations) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView        = inflater.inflate(R.layout.xsolla_checkout_summary, null);

        TextView tvOrderSummary = (TextView)convertView.findViewById(R.id.xsolla_tv_orderSummary);
        TextView tvSubTotal = (TextView)convertView.findViewById(R.id.xsolla_tv_subtotal);
        TextView tvSubTotalValue = (TextView)convertView.findViewById(R.id.xsolla_tv_subtotal_value);
        TextView tvTotal = (TextView)convertView.findViewById(R.id.xsolla_tv_total);
        TextView tvTotalValue = (TextView)convertView.findViewById(R.id.xsolla_tv_total_value);

        tvOrderSummary.setText(translations.get("payment_summary_header"));

        tvSubTotal.setText(translations.get("payment_summary_subtotal"));
        tvSubTotalValue.setText(summary.getFinance().getSubTotal().getPrice());


        TextView tvSubItemTitle = (TextView)convertView.findViewById(R.id.xsolla_tv_discount);
        TextView tvSubItemPrice = (TextView)convertView.findViewById(R.id.xsolla_tv_discount_price);
        initBaseFinanceItem(summary.getFinance().getDiscount(), translations.get("payment_summary_discount"), tvSubItemTitle, tvSubItemPrice);

        tvSubItemTitle = (TextView)convertView.findViewById(R.id.xsolla_tv_fee);
        tvSubItemPrice = (TextView)convertView.findViewById(R.id.xsolla_tv_fee_price);
        initBaseFinance(summary.getFinance().getFee(), translations.get("payment_summary_fee"), tvSubItemTitle, tvSubItemPrice);

        tvSubItemTitle = (TextView)convertView.findViewById(R.id.xsolla_tv_tax);
        tvSubItemPrice = (TextView)convertView.findViewById(R.id.xsolla_tv_tax_price);
        initBaseFinanceItem(summary.getFinance().getTax(), translations.get("payment_summary_sales_tax_user"), tvSubItemTitle, tvSubItemPrice);

        tvSubItemTitle = (TextView)convertView.findViewById(R.id.xsolla_tv_vat);
        tvSubItemPrice = (TextView)convertView.findViewById(R.id.xsolla_tv_vat_price);
        initBaseFinanceItem(summary.getFinance().getVat(), translations.get("payment_summary_vat_user"), tvSubItemTitle, tvSubItemPrice);

        tvSubItemTitle = (TextView)convertView.findViewById(R.id.xsolla_tv_balance);
        tvSubItemPrice = (TextView)convertView.findViewById(R.id.xsolla_tv_balance_price);
        initBaseFinanceItem(summary.getFinance().getUserBalance(), translations.get("user_balance_label"), tvSubItemTitle, tvSubItemPrice);


        tvTotalValue.setText(summary.getFinance().getTotal().getPrice());

        String subscriptionsDesc = translations.get("subscription_notify_note_date");
        SummaryItemsAdapter adapter = new SummaryItemsAdapter(context, summary.getPurchase().getList(subscriptionsDesc), translations);
        LinearLayout llSummaryItemsContainer = (LinearLayout)convertView.findViewById(R.id.xsolla_ll_summary_item_container);
        for (int i = 0; i < adapter.getCount(); i++) {
            llSummaryItemsContainer.addView(adapter.getView(i, null, null));
        }

//        convertView.invalidate();

        return convertView;
    }

    private static void initBaseFinanceItem(XSummary.XFinance.XFinanceItemBase baseItem, String title, TextView tvTitle, TextView tvPrice) {
        if(baseItem != null && baseItem.getAmount() != 0) {
            ((View)tvTitle.getParent().getParent()).setVisibility(View.VISIBLE);
            tvTitle.setText(title);
            String price = baseItem.getPrice();
//            if(title.equals("Balance"))
//                price = "+" + price;
            tvPrice.setText(price);
        } else {
            ((View)tvTitle.getParent().getParent()).setVisibility(View.GONE);
        }
    }

    private static void initBaseFinance(XSummary.XFinance.XFinanceItemBase baseItem, String title, TextView tvTitle, TextView tvPrice) {
        if(baseItem != null) {
            tvTitle.setText(title);
            tvPrice.setText(baseItem.getPrice());
        } else {
            ((View)tvTitle.getParent().getParent()).setVisibility(View.GONE);
        }
    }
}
