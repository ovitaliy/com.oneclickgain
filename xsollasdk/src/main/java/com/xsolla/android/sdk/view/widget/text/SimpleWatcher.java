package com.xsolla.android.sdk.view.widget.text;

import android.widget.EditText;

import com.xsolla.android.sdk.util.validator.BaseValidator;

/**
 *
 */
public class SimpleWatcher extends BaseWatcher {

    public SimpleWatcher(EditText editText, BaseValidator validator) {
        super(editText, validator);
    }

}
