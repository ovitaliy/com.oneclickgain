package com.xsolla.android.sdk.view.adapter;

import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.shop.XPricepoint;
import com.xsolla.android.sdk.util.ImageLoader;
import com.xsolla.android.sdk.view.fragment.ItemFragment.OnListFragmentInteractionListener;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
public class PricepointsAdapter extends RecyclerView.Adapter<PricepointsAdapter.ViewHolder> {

    private String                              coins;
    private String                              buy;//pricepoint_option_button
    private ArrayList<XPricepoint>              mItemList;
    private OnListFragmentInteractionListener   mListener;
    private String[]                            mOfferStrings;//option_recommended_default//option_best_deal_default//option_offer

    public PricepointsAdapter(ArrayList<XPricepoint> mItemList, OnListFragmentInteractionListener listener, String coins, String buy, String[] offerStrings) {
        this.mItemList = mItemList;
        this.mListener = listener;
        this.coins = coins;
        this.buy = buy;
        this.mOfferStrings = offerStrings;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.xsolla_shop_vitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        XPricepoint pricepoint = mItemList.get(position);
        holder.mPricepoint = pricepoint;

        ImageLoader.getInstance().loadImage(holder.mImage, pricepoint.getImage());
        holder.showOfferLabel(pricepoint.getLabelType());
        String title = pricepoint.getOut() + " " + coins;
        holder.mTvTitle.setText(title);

        if(!"".equals(pricepoint.getDescription()))
            holder.mTvDescription.setText(pricepoint.getDescription());
        else
            holder.mTvDescription.setVisibility(View.GONE);

        if(pricepoint.isBonus()) {
            holder.mTvBonus.setVisibility(View.VISIBLE);
            holder.mTvBonus.setText(pricepoint.getBonusString(coins));
        } else {
            holder.mTvBonus.setVisibility(View.GONE);
        }

        if(pricepoint.isPriceChanged()) {
            holder.mTvPriceOld.setVisibility(View.VISIBLE);
            holder.mTvPriceOld.setText(pricepoint.getPriceWithoutDiscount());
            holder.mTvPriceOld.setPaintFlags(holder.mTvPriceOld.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.mTvPrice.setText(pricepoint.getPrice());
        } else {
            holder.mTvPriceOld.setVisibility(View.GONE);
            holder.mTvPrice.setText(pricepoint.getPriceWithoutDiscount());
        }

        holder.mTvBuy.setText(buy);

        holder.mTvLearnMore.setVisibility(View.GONE);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> map = new HashMap<String, Object>(1);
                map.put("out", holder.mPricepoint.getOut());
                mListener.onOnProductSelected(map, false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView, mImageBorder;
        public final ImageView mImage;
        public final TextView mOfferLable, mTvTitle, mTvDescription, mTvLearnMore, mTvBonus, mTvPrice, mTvPriceOld, mTvBuy;
        public XPricepoint mPricepoint;

        public ViewHolder(View itemView) {
            super(itemView);
            mView               = itemView;

            mImageBorder        = itemView.findViewById(R.id.xsolla_offer_label_border);
            mOfferLable         = (TextView) itemView.findViewById(R.id.xsolla_tv_offer_label);

            mImage              = (ImageView) itemView.findViewById(R.id.xsolla_iv);
            mTvTitle            = (TextView) itemView.findViewById(R.id.xsolla_tv_title);
            mTvDescription      = (TextView) itemView.findViewById(R.id.xsolla_tv_description);
            mTvLearnMore        = (TextView) itemView.findViewById(R.id.xsolla_tv_learn_more);
            mTvBonus            = (TextView) itemView.findViewById(R.id.xsolla_tv_bonus);
            mTvPrice            = (TextView) itemView.findViewById(R.id.xsolla_tv_price);
            mTvPriceOld         = (TextView) itemView.findViewById(R.id.xsolla_tv_price_old);
            mTvBuy              = (TextView) itemView.findViewById(R.id.xsolla_tv_buy);
        }

        public void showOfferLabel(int offerType) {
            int offerColor      = Color.LTGRAY;
            String offerText    = "";
            int visibility      = View.VISIBLE;
            switch (offerType) {
                case 0:
                    offerColor = Color.parseColor("#ff809efc");//R.color.xsolla_advice
                    offerText = mOfferStrings[0];
                    break;
                case 1:
                    offerColor = Color.parseColor("#ff9896de");//R.color.xsolla_best
                    offerText = mOfferStrings[1];
                    break;
                case 2:
                    offerColor = Color.parseColor("#fffcb07f");//R.color.xsolla_special
                    offerText = mOfferStrings[2];
                    break;
                default:
                    visibility = View.GONE;
            }
            mImageBorder.setBackgroundColor(offerColor);
            mOfferLable.setBackgroundColor(offerColor);
            mOfferLable.setText(offerText);
            mOfferLable.setVisibility(visibility);
        }

    }
}
