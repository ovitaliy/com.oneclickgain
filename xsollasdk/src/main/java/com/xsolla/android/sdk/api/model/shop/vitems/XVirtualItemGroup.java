package com.xsolla.android.sdk.api.model.shop.vitems;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 */
public class XVirtualItemGroup implements IParseble {

    private String  id;//"id":"3091",
    private String  external_id;//"external_id":null,
    private String  name;//"name":"Pets",

    private int     level;//"level":0

    private ArrayList<XVirtualItemGroup> listChildren;//"children":[{"id":"4293","external_id":"LOTROCAT027","name":"Account Upgrades","children":[], "level":1},],

    public XVirtualItemGroup() {
    }

    public XVirtualItemGroup( String id, String name) {
        this.id = id;
        this.name = name;

        this.external_id = "uncknown";
        this.level = 0;
        this.listChildren = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public void parse(JSONObject jobj) {
        id          = jobj.optString("id");
        external_id = jobj.optString("external_id");
        name        = jobj.optString("name");

        level = jobj.optInt("level");

        JSONArray jarrChilds = jobj.optJSONArray("children");
        if(jarrChilds != null) {
            listChildren = new ArrayList<>(jarrChilds.length());
            for (int i = 0; i < jarrChilds.length(); i++) {
                JSONObject jobjChild = jarrChilds.optJSONObject(i);
                XVirtualItemGroup newGroup = new XVirtualItemGroup();
                newGroup.parse(jobjChild);
                listChildren.add(newGroup);
            }
        }
    }

    @Override
    public String toString() {
        return "XVirtualItemGroup{" +
                "id='" + id + '\'' +
                ", external_id='" + external_id + '\'' +
                ", name='" + name + '\'' +
                ", level=" + level +
                ", listChildren=" + listChildren +
                '}';
    }
}
