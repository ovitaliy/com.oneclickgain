package com.xsolla.android.sdk.api.model.psystems;

import android.support.v4.util.ArrayMap;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 */
public class XPSManager implements IParseble {


    private ArrayList<XPaymentSystem>           listQuickPayments;//"quick_instances":[
    private ArrayList<XPaymentSystem>           listRegularPayments;//"regular_instances":[
    private ArrayList<XPaymentSystem>           listAllPayments;//"instances":[
    private ArrayMap<String, XPaymentSystem>    mapAllPayments;//"instances":[
    //"lastPayment":null,


    public ArrayList<XPaymentSystem> getList() {
        return listAllPayments;
    }

    public ArrayMap<String, XPaymentSystem> getMap() {
        return mapAllPayments;
    }

    private void addQuick(XPaymentSystem paymentSystem) {
        listQuickPayments.add(paymentSystem);
        add(paymentSystem);
    }

    private void addRegular(XPaymentSystem paymentSystem) {
        listRegularPayments.add(paymentSystem);
        add(paymentSystem);
    }

    private void add(XPaymentSystem paymentSystem){
        listAllPayments.add(paymentSystem);
        mapAllPayments.put(paymentSystem.getName(), paymentSystem);
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONArray jarrQuick     = jobj.optJSONArray("quick_instances");
        JSONArray jarrRegular   = jobj.optJSONArray("regular_instances");
        int mergedSize = jarrQuick.length() + jarrRegular.length();
        listAllPayments = new ArrayList<>(mergedSize);
        mapAllPayments = new ArrayMap<>(mergedSize);
        for (int i = 0; i < jarrQuick.length(); i++) {
            XPaymentSystem newItem = new XPaymentSystem();
            newItem.parse(jarrQuick.optJSONObject(i));
            addQuick(newItem);
        }
        for (int i = 0; i < jarrRegular.length(); i++) {
            XPaymentSystem newItem = new XPaymentSystem();
            newItem.parse(jarrRegular.optJSONObject(i));
            if(newItem.getId() != 1754)
                addRegular(newItem);
        }
    }

    @Override
    public String toString() {
        return "XPSManager{" +
                "listQuickPayments=" + listQuickPayments +
                ", listRegularPayments=" + listRegularPayments +
                ", listAllPayments=" + listAllPayments +
                ", mapAllPayments=" + mapAllPayments +
                '}';
    }
}
