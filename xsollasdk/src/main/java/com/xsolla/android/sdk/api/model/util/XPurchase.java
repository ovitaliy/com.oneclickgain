package com.xsolla.android.sdk.api.model.util;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 */
public class XPurchase implements IParseble {

    XQuantityItem   virtualCurrency, checkout;//virtual_currency, checkout
    XVirtualItems   virtualItems;//virtual_items
    XSubscription   subscription;//subscription
    XPaymentSystem  paymentSystem;//payment_system

    public XPurchase() {
    }

    public boolean isPurchase() {
        return virtualCurrency != null || checkout != null || virtualItems != null || subscription != null;
    }

    public boolean isPaymentSystem() {
        return paymentSystem != null;
    }

    public XPaymentSystem getPaymentSystem() {
        return paymentSystem;
    }

    public HashMap<String, Object> getPurchaseMap() {
        HashMap<String, Object> map = new HashMap<>();
        return map;
    }

    @Override
    public void parse(JSONObject jobj) {
        for(Iterator<String> iter = jobj.keys(); iter.hasNext();) {
            String key = iter.next();
            JSONObject jobjElem = jobj.optJSONObject(key);
            if(jobjElem != null)
                switch (key) {
                    case "virtual_currency":
                        this.virtualCurrency = new XQuantityItem();
                        virtualCurrency.parse(jobjElem);
                        break;
                    case "checkout":
                        this.checkout = new XQuantityItem();
                        checkout.parse(jobjElem);
                        break;
                    case "virtual_items":
                        this.virtualItems = new XVirtualItems();
                        virtualItems.parse(jobjElem);
                        break;
                    case "subscription":
                        this.subscription = new XSubscription();
                        subscription.parse(jobjElem);
                        break;
                    case "payment_system":
                        this.paymentSystem = new XPaymentSystem();
                        paymentSystem.parse(jobjElem);
                        break;
                    default:
                }
        }
    }

    @Override
    public String toString() {
        return "\n\tXPurchase{" +
                "\n\t\tvirtualCurrency=" + virtualCurrency +
                ", \n\t\tcheckout=" + checkout +
                ", \n\t\tvirtualItems=" + virtualItems +
                ", \n\t\tsubscription=" + subscription +
                ", \n\t\tpaymentSystem=" + paymentSystem +
                "\n\t}";
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
    * INNER CLASSES
    * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /**
     *
     */
    public class XQuantityItem implements IParseble {
        int     quantity;//quantity
        boolean isAllowModify;//allow_modify

        @Override
        public void parse(JSONObject jobj) {
            quantity        = jobj.optInt("quantity");
            isAllowModify   = jobj.optBoolean("allow_modify");
        }

        @Override
        public String toString() {
            return "\n\t\tXQuantytyItem{" +
                    "quantity=" + quantity +
                    ", isAllowModify=" + isAllowModify +
                    '}';
        }
    };

    /**
     *
     */
    public class XVirtualItem implements IParseble {
        int sku;//"sku":12,
        int amount;//"amount":1

        @Override
        public void parse(JSONObject jobj) {
            sku     = jobj.optInt("sku");
            amount  = jobj.optInt("amount");
        }

        @Override
        public String toString() {
            return "\n\t\t\tXVirtualItem{" +
                    "sku=" + sku +
                    ", amount=" + amount +
                    '}';
        }
    };

    /**
     *
     */
    public class XVirtualItems implements IParseble {
        boolean isAllowModify;//allow_modify
        ArrayList<XVirtualItem> items;// {}

        public XVirtualItems() {
            this.items = new ArrayList<>();
        }

        @Override
        public void parse(JSONObject jobj) {
            isAllowModify = jobj.optBoolean("allow_modify");
            JSONArray jarrItems = jobj.optJSONArray("items");
            if(jarrItems != null)
                for (int i = 0; i < jarrItems.length(); i++) {
                    JSONObject jobjMsg = jarrItems.optJSONObject(i);
                    XVirtualItem newItem = new XVirtualItem();
                    if(jobjMsg != null) {
                        newItem.parse(jobjMsg);
                        items.add(newItem);
                    }
                }
        }

        @Override
        public String toString() {
            return "\n\t\tXVirtualItems{" +
                    "isAllowModify=" + isAllowModify +
                    ", items=" + items +
                    '}';
        }
    };

    /**
     *
     */
    public class XSubscription implements IParseble {
        String  planId;//plain_id
        boolean isAllowModify;//allow_modify

        @Override
        public void parse(JSONObject jobj) {
            planId          = jobj.optString("plain_id");
            isAllowModify   = jobj.optBoolean("allow_modify");
        }

        @Override
        public String toString() {
            return "\n\t\tXSubscription{" +
                    "planId='" + planId + '\'' +
                    ", isAllowModify=" + isAllowModify +
                    '}';
        }
    };

    /**
     *
     */
    public class XPaymentSystem implements IParseble {
        private int id;//id
        private boolean isAllowModify;//allow_modify

        public int getId() {
            return id;
        }

        public boolean isAllowModify() {
            return isAllowModify;
        }

        @Override
        public void parse(JSONObject jobj) {
            id              = jobj.optInt("id");
            isAllowModify   = jobj.optBoolean("allow_modify");
        }

        @Override
        public String toString() {
            return "\n\t\tXPaymentSystem{" +
                    "id=" + id +
                    ", isAllowModify=" + isAllowModify +
                    '}';
        }
    };
}
