package com.xsolla.android.sdk.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.api.model.vpayment.XVPSummary;
import com.xsolla.android.sdk.api.model.vpayment.XVirtualItemSimple;
import com.xsolla.android.sdk.util.ImageLoader;
import com.xsolla.android.sdk.view.XsollaActivity;

public class VPSummaryFragment extends XFragment implements View.OnClickListener {

    private ImageView imageView, ivVirtualCurrency;
    private TextView tvTitle, tvItemTitle, tvItemDesc, tvPrice, tvTotalText;
    private CheckBox checkBox;
    private Button button;

    private OnFragmentInteractionListener mListener;

    private XVPSummary mSummary;
    private XUtils     mUtils;

    public VPSummaryFragment() {
        // Required empty public constructor
    }

    public static VPSummaryFragment newInstance() {
        return new VPSummaryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setTitle(mUtils.getTranslations().get("cart_page_title"));
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.xsolla_fragment_vpsummary, container, false);
        imageView = (ImageView) rootView.findViewById(R.id.xsolla_iv);
        ivVirtualCurrency = (ImageView) rootView.findViewById(R.id.xsolla_vc_image);
        tvTitle = (TextView) rootView.findViewById(R.id.xsolla_tv_orderSummary);
        tvItemTitle = (TextView) rootView.findViewById(R.id.xsolla_tv_title);
        tvItemDesc = (TextView) rootView.findViewById(R.id.xsolla_tv_description);
        tvPrice = (TextView) rootView.findViewById(R.id.xsolla_tv_total_value);
        tvTotalText = (TextView) rootView.findViewById(R.id.xsolla_tv_total);
        checkBox = (CheckBox) rootView.findViewById(R.id.xsolla_checkBox);
        button = (Button) rootView.findViewById(R.id.xsolla_button);

        if(mSummary.isError()) {
            LinearLayout llError = (LinearLayout) rootView.findViewById(R.id.llError);
            TextView tvError = (TextView) rootView.findViewById(R.id.tvError);
            llError.setVisibility(View.VISIBLE);
            tvError.setText(mSummary.getProceedError());
        }

        XVirtualItemSimple item = mSummary.getItems().get(0);
        ImageLoader.getInstance().loadImage(imageView, item.getImageUrl());
        ImageLoader.getInstance().loadImage(ivVirtualCurrency, mUtils.getProject().getVirtualCurrencyImage());
        tvTitle.setText(mUtils.getTranslations().get("cart_confirm_your_purchase"));
        tvItemTitle.setText(item.getName());
        tvItemDesc.setVisibility(View.GONE);//.setText("NO DESC");
        checkBox.setChecked(mSummary.isSkipConfirmation());
        checkBox.setText(mUtils.getTranslations().get("cart_dont_ask_again"));
        tvTotalText.setText(mUtils.getTranslations().get("payment_summary_total"));//"payment_summary_total"
        tvPrice.setText(mSummary.getTotal());
        button.setText(mUtils.getTranslations().get("cart_submit"));
        button.setOnClickListener(this);
        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            mSummary = ((XsollaActivity)getActivity()).getVPSummary();
            mUtils = ((XsollaActivity)getActivity()).getUtils();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        mListener.onClickBuyVp(checkBox.isChecked() ? 1 : 0);
    }

    public interface OnFragmentInteractionListener {
        void onClickBuyVp(int isConfirmationDisabled);
    }
}
