package com.xsolla.android.sdk.view.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.сheckout.XForm;

import java.util.ArrayList;

public class FormElementsAdapter extends BaseAdapter {

    final String TAG = getClass().getSimpleName();

    private Context mContext;
    private XForm mXForm;
    ArrayList<XForm.XFormElement> arrayList;

    public FormElementsAdapter(Context context, XForm xsollaForm) {
        this.mContext = context;
        this.mXForm = xsollaForm;
        arrayList = xsollaForm.getListVisible();
    }

    @Override
    public int getCount() {
        return mXForm.getListVisible().size();
    }

    @Override
    public XForm.XFormElement getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static final int TYPE_UNKNOWN = -1;
    public static final int TYPE_HIDDEN = 0;
    public static final int TYPE_TEXT = 1;
    public static final int TYPE_SELECT = 2;
    public static final int TYPE_VISIBLE = 3;
    public static final int TYPE_TABLE = 4;
    public static final int TYPE_CHECK = 5;
    public static final int TYPE_HTML = 6;
    public static final int TYPE_LABEL = 7;//HTML

    @Override
    public int getItemViewType(int position) {
        switch (getItem(position).getType()) {
            case "hidden":
                return TYPE_HIDDEN;
            case "text":
                return TYPE_TEXT;
            case "select":
                return TYPE_SELECT;
            case "isVisible":
                return TYPE_VISIBLE;
            case "table":
                return TYPE_TABLE;
            case "check":
                return TYPE_CHECK;
            case "html":
                return TYPE_HTML;
            case "label":
                return TYPE_LABEL;
            default:
                return TYPE_UNKNOWN;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return showView(position);
    }

    private View showView(int position) {

        XForm.XFormElement xsollaFormElement = getItem(position);
        switch (getItemViewType(position)) {
            case TYPE_HIDDEN:
                return renderSimple(xsollaFormElement);
            case TYPE_TEXT:
                return renderText(xsollaFormElement);
            case TYPE_SELECT:
                return renderSelect(xsollaFormElement);
            case TYPE_VISIBLE:
                return renderSimple(xsollaFormElement);
            case TYPE_TABLE:
                return renderSimple(xsollaFormElement);
            case TYPE_CHECK:
                return renderCheckbox(xsollaFormElement);
            case TYPE_LABEL:
                return renderLabel(xsollaFormElement);
            default:
                return renderSimple(xsollaFormElement);
        }
    }

    private View renderSimple(XForm.XFormElement xsollaFormElement) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.xsolla_form_element_simple, null);

        TextView tvTitle = (TextView) convertView.findViewById(R.id.text1);
        TextView tvValue = (TextView) convertView.findViewById(R.id.text2);
        String title = xsollaFormElement.getName() + " " + xsollaFormElement.getTitle();
        String value = xsollaFormElement.getType() + " " + xsollaFormElement.getValue();
        tvTitle.setText(title);
        tvValue.setText(value);
        return convertView;
    }

    private View renderText(XForm.XFormElement xsollaFormElement) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.xsolla_form_element_text, null);

        TextView tvTitle = (TextView) convertView.findViewById(R.id.text1);
        final EditText editText = (EditText) convertView.findViewById(R.id.editText);
        tvTitle.setText(xsollaFormElement.getTitle());
        editText.setTag(xsollaFormElement.getName());
        editText.setHint(xsollaFormElement.getExample());
        if (!"".equals(xsollaFormElement.getValue()))
            editText.setText(xsollaFormElement.getValue());
        TextWatcher watcher = null;
        editText.addTextChangedListener(new MyTextWatcher(editText));
        if (watcher != null) {
            editText.addTextChangedListener(watcher);
        }
        return convertView;
    }

    class MyTextWatcher implements TextWatcher {

        private EditText mEditText;

        public MyTextWatcher(EditText editText) {
            mEditText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            mXForm.updateElement(mEditText.getTag().toString(), s.toString());
        }
    }

    private View renderCheckbox(XForm.XFormElement xsollaFormElement) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.xsolla_form_element_check, null);

        CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);

        checkBox.setTag(xsollaFormElement.getName());
        checkBox.setText(xsollaFormElement.getTitle());
        checkBox.setChecked(false);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mXForm.updateElement(buttonView.getTag().toString(), isChecked ? "1" : "0");
            }
        });
        return convertView;
    }

    private View renderLabel(XForm.XFormElement xsollaFormElement) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.xsolla_form_element_label, null);

        TextView textView = (TextView) convertView.findViewById(R.id.text1);

        textView.setText(xsollaFormElement.getTitle());
        return convertView;
    }

    private View renderSelect(final XForm.XFormElement xsollaFormElement) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.xsolla_form_element_select, null);

        TextView textView = (TextView) convertView.findViewById(R.id.text1);
        Spinner spinner = (Spinner) convertView.findViewById(R.id.spinner);

        textView.setText(xsollaFormElement.getTitle());
        spinner.setTag(xsollaFormElement.getName());
        ArrayAdapter<XForm.XFormElement.XOption> spinnerArrayAdapter
                = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, xsollaFormElement.getOptions()); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                              @Override
                                              public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                  mXForm.updateElement(parent.getTag().toString(), xsollaFormElement.getOptions().get(position).getValue());
                                              }

                                              @Override
                                              public void onNothingSelected(AdapterView<?> parent) {

                                              }
                                          }
        );
        mXForm.updateElement(xsollaFormElement.getName(), xsollaFormElement.getOptions().get(0).getValue());
        return convertView;
    }

}
