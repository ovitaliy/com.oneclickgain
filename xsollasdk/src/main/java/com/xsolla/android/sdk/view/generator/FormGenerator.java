package com.xsolla.android.sdk.view.generator;

import android.content.Context;
import android.view.View;

import com.xsolla.android.sdk.api.model.сheckout.XForm;

import java.util.HashMap;

public interface FormGenerator {
    public abstract View generate(Context context, XForm form, HashMap<String, String> translations);

    public abstract String validate();
}
