package com.xsolla.android.sdk.view;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;

public class XsollaUIHelper {

//    public static void startXsollaActivity(Activity activity, String accessToken, boolean isModeSandbox) {
//        Intent intent = new Intent(activity, XsollaActivity.class);
//        intent.putExtra(XsollaActivity.ARG_TOKEN, accessToken);
//        intent.putExtra(XsollaActivity.ARG_IS_MODE_SANDBOX, isModeSandbox);
//        activity.startActivityForResult(intent, XsollaActivity.REQUEST_CODE);
//    }

    public static void startXsollaActivity(Activity activity, String accessToken, boolean isModeSandbox) {
        Intent intent = new Intent(activity, XsollaActivity.class);
        intent.putExtra(XsollaActivity.ARG_TOKEN, accessToken);
        intent.putExtra(XsollaActivity.ARG_IS_MODE_SANDBOX, isModeSandbox);
        activity.startActivityForResult(intent, XsollaActivity.REQUEST_CODE);
    }

    public static void replaceSingleView(ViewGroup container, View newContent) {
        container.removeAllViews();
        container.addView(newContent);
    }
}
