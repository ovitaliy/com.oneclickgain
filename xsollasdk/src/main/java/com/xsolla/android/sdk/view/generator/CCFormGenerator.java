package com.xsolla.android.sdk.view.generator;

import android.content.Context;
import android.text.Editable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.XsollaApiConst;
import com.xsolla.android.sdk.api.model.util.XTranslationsKeys;
import com.xsolla.android.sdk.api.model.сheckout.XDirectpayment;
import com.xsolla.android.sdk.api.model.сheckout.XForm;
import com.xsolla.android.sdk.util.validator.BaseValidator;
import com.xsolla.android.sdk.util.validator.CCValidator;
import com.xsolla.android.sdk.util.validator.CvvValidator;
import com.xsolla.android.sdk.util.validator.EmptyValidator;
import com.xsolla.android.sdk.util.validator.MonthValidator;
import com.xsolla.android.sdk.util.validator.SimpleValidator;
import com.xsolla.android.sdk.util.validator.YearValidator;
import com.xsolla.android.sdk.util.validator.ZipValidator;
import com.xsolla.android.sdk.view.widget.TooltipWindow;
import com.xsolla.android.sdk.view.widget.maskededittext.CCEditText;
import com.xsolla.android.sdk.view.widget.text.BaseWatcher;

import java.util.ArrayList;
import java.util.HashMap;

public class CCFormGenerator implements ICheckoutGenerator {

    ArrayList<BaseWatcher> watchers;
    XForm mXForm;

    public CCFormGenerator() {
        watchers = new ArrayList<>(4);
    }

    @Override
    public View generate(final Context context, final XDirectpayment directpayment, HashMap<String, String> translations) {
        int widthPixels = context.getResources().getDisplayMetrics().widthPixels;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.xsolla_form_cc, null);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL));

        this.mXForm = directpayment.getForm();

        XForm.XFormElement description = mXForm.getItem(XsollaApiConst.DESCRIPTION);
        TextView tvDescription = (TextView) rootView.findViewById(R.id.tvDescription);
        if (description == null) {
            tvDescription.setVisibility(View.GONE);
        } else {
            tvDescription.setText(description.getTitle());
        }

        XForm.XFormElement userID = mXForm.getItem(XsollaApiConst.V1);
        TextView tvUserId = (TextView) rootView.findViewById(R.id.text1);
        EditText edUserId = (EditText) rootView.findViewById(R.id.editText);
        initField(userID, edUserId, tvUserId, new EmptyValidator());

        XForm.XFormElement cardNumber = mXForm.getItem(XsollaApiConst.CARD_NUMBER);
       // cardNumber.setExample("");
        CCEditText edCardNumber = (CCEditText) rootView.findViewById(R.id.edCardNumber);
        TextView tvCardNumber = (TextView) rootView.findViewById(R.id.tvCardNumber);
        tvCardNumber.setText(translations.get(XTranslationsKeys.FORM_CC_CARD_NUMBER));
        initField(cardNumber,
                edCardNumber,
                tvCardNumber,
                new CCValidator(),
                translations.get(XTranslationsKeys.VALIDATION_MESSAGE_CARDNUMBER));


        TextView tvExpirationDate = (TextView) rootView.findViewById(R.id.tvExpirationDate);
        tvExpirationDate.setText(translations.get(XTranslationsKeys.FORM_CC_EXP_DATE));

        XForm.XFormElement cardMonth = mXForm.getItem(XsollaApiConst.CARD_EXP_MONTH);
        EditText edMonth = (EditText) rootView.findViewById(R.id.edMonth);
        initField(cardMonth,
                edMonth,
                null,
                new MonthValidator(),
                translations.get(XTranslationsKeys.VALIDATION_MESSAGE_CARD_MONTH));

        XForm.XFormElement cardYear = mXForm.getItem(XsollaApiConst.CARD_EXP_YEAR);
        EditText edYear = (EditText) rootView.findViewById(R.id.edYear);
        initField(cardYear,
                edYear,
                null,
                new YearValidator(),
                translations.get(XTranslationsKeys.VALIDATION_MESSAGE_CARD_YEAR));

        XForm.XFormElement cardCvv = mXForm.getItem(XsollaApiConst.CARD_CVV);
        EditText edCvv = (EditText) rootView.findViewById(R.id.edCvv);
        TextView tvCvv = (TextView) rootView.findViewById(R.id.tvCvv);
        if(cardNumber != null) {
            initCvvField(cardCvv,
                    edCvv,
                    tvCvv,
                    new CvvValidator(edCardNumber, cardNumber.isMandatory()),
                    translations.get(XTranslationsKeys.VALIDATION_MESSAGE_CVV));
            View cvvHint = rootView.findViewById(R.id.xsolla_cvv_info);
            cvvHint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //where_is_security_code
                    TooltipWindow tipWindow = new TooltipWindow(context);
                    tipWindow.showToolTip(v);
                }
            });
        }


        XForm.XFormElement cardZipCode = mXForm.getItem(XsollaApiConst.CARD_ZIP);
        EditText edZipCode = (EditText) rootView.findViewById(R.id.edZipCode);
        TextView tvZipCode = (TextView) rootView.findViewById(R.id.tvZipCode);
        initField(cardZipCode,
                edZipCode,
                tvZipCode,
                new ZipValidator(),
                "Zip is " + translations.get(XTranslationsKeys.VALIDATION_MESSAGE_REQUIRED).toLowerCase());

        XForm.XFormElement cardHolder = mXForm.getItem(XsollaApiConst.CARD_HOLDER);
        EditText edCardHolder = (EditText) rootView.findViewById(R.id.edCardHolder);
        TextView tvCardHolder = (TextView) rootView.findViewById(R.id.tvCardHolder);
        initField(cardHolder,
                edCardHolder,
                tvCardHolder,
                new SimpleValidator(),
                "Name is " + translations.get(XTranslationsKeys.VALIDATION_MESSAGE_REQUIRED).toLowerCase());


        XForm.XFormElement allowSubscription = mXForm.getItem(XsollaApiConst.ALLOW_SUBSCRIPTION);
//        CheckBox checkBox = (CheckBox) rootView.findViewById(R.id.checkBox);
//        if(allowSubscription != null) {
//            checkBox.setVisibility(View.VISIBLE);
//            checkBox.setTag(allowSubscription.getName());
//            checkBox.setText(allowSubscription.getTitle());
//            checkBox.setChecked(false);
//            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    mXForm.getForm().updateElement(buttonView.getTag().toString(), isChecked ? "1" : "0");
//                }
//            });
//        } else {
//            checkBox.setVisibility(View.GONE);
//        }

        return rootView;
    }

    private void initField(XForm.XFormElement element, EditText editText, TextView textView, BaseValidator validator, String errorMsg) {
        if (element != null) {
            ((View) editText.getParent()).setVisibility(View.VISIBLE);
            if (textView != null)
                textView.setText(element.getTitle());
            editText.setTag(element.getName());
            if (!"".equals(element.getExample()))
                editText.setHint(element.getExample());
            editText.invalidate();
            validator.setErrorMsg(errorMsg);
            addWatcher(mXForm, editText, validator);
        }
    }

    private void initField(XForm.XFormElement element, EditText editText, TextView textView, BaseValidator validator) {
        if (element != null) {
            ((View) editText.getParent()).setVisibility(View.VISIBLE);
            if (textView != null)
                textView.setText(element.getTitle());
            editText.setTag(element.getName());
            if (!"".equals(element.getExample()))
                editText.setHint(element.getExample());
            editText.invalidate();
            addWatcher(mXForm, editText, validator);
        }
    }

    private void initCvvField(XForm.XFormElement element, EditText editText, TextView textView, BaseValidator validator, String errorMsg) {
        if (element != null) {
            ((View) editText.getParent().getParent()).setVisibility(View.VISIBLE);
            if (textView != null)
                textView.setText(element.getTitle());
            editText.setTag(element.getName());
            if (!"".equals(element.getExample()))
                editText.setHint(element.getExample());
            editText.invalidate();
            validator.setErrorMsg(errorMsg);
            addWatcher(mXForm, editText, validator);
        }
    }

    private BaseWatcher addWatcher(final XForm xsollaForm, EditText editText, BaseValidator validator) {
        BaseWatcher watcher = new BaseWatcher(editText, validator) {
            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                xsollaForm.updateElement(mEditText.getTag().toString(), s.toString());
            }
        };
        editText.addTextChangedListener(watcher);
        watchers.add(watcher);
        return watcher;
    }

    @Override
    public String validate() {
        for (BaseWatcher bw : watchers) {
            if (!bw.validate())
                return bw.getErrorMessage();
        }
        return null;
    }

}
