package com.xsolla.android.sdk.api;

import android.os.Build;

import com.xsolla.android.sdk.api.model.IParseble;
import com.xsolla.android.sdk.api.model.additional.XSufficiency;
import com.xsolla.android.sdk.api.model.additional.XToken;
import com.xsolla.android.sdk.api.model.psystems.XCountryManager;
import com.xsolla.android.sdk.api.model.psystems.XPSAllManager;
import com.xsolla.android.sdk.api.model.psystems.XPSManager;
import com.xsolla.android.sdk.api.model.psystems.XPSQuickManager;
import com.xsolla.android.sdk.api.model.shop.XPricepointsManager;
import com.xsolla.android.sdk.api.model.shop.XSubscriptionsManager;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItemGroupsManager;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItemsManager;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItemsManagerMobile;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.api.model.vpayment.XProceed;
import com.xsolla.android.sdk.api.model.vpayment.XVPStatus;
import com.xsolla.android.sdk.api.model.vpayment.XVPSummary;
import com.xsolla.android.sdk.api.model.сheckout.XDirectpayment;
import com.xsolla.android.sdk.api.request.XsollaApi;
import com.xsolla.android.sdk.api.request.converter.XConverterFactory;
import com.xsolla.android.sdk.api.request.converter.YOSSLSocketFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Observable;

//import retrofit.RestAdapter;
//import retrofit.android.AndroidLog;
//import retrofit.client.OkClient;
//import retrofit.converter.Converter;


public class XRestAdapter {

    private static final String TAG = XRestAdapter.class.getSimpleName();

    private static final String BASE_URL_TOKEN = "https://livedemo.xsolla.com/sdk/";
    private static final String BASE_URL = "https://secure.xsolla.com/paystation2/api/";
    private static final String BASE_URL_SAND =  "https://sandbox-secure.xsolla.com/paystation2/api/";
    private static boolean isSandbox = false;

    private static Retrofit getAdapter(IParseble parseble) {
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
                .build();
        OkHttpClient client = new OkHttpClient.Builder()
                .connectionSpecs(Collections.singletonList(spec))
                .build();//addInterceptor(interceptor)
        return new Retrofit.Builder()
                .baseUrl(getDomain())
                .client(new OkHttpClient.Builder().build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(new XConverterFactory(parseble))
                .build();
//        return new RestAdapter.Builder()
//                .setLogLevel(RestAdapter.LogLevel.FULL)
//                .setConverter(converter)
//                .setClient(new OkClient())
//                .setEndpoint(getDomain())
//                .build();
    }


    private static Retrofit getTokenAdapter() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = getOkHttpBuilderWithTlsSupport()
                .addInterceptor(interceptor)
                .build();//addInterceptor(interceptor)
        return new Retrofit.Builder()
                .baseUrl(BASE_URL_TOKEN)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(new XConverterFactory(new XToken()))
                .build();
    }
    //https://gist.github.com/gokhangirgin/ac8fdb8843a70a420982
    public static OkHttpClient.Builder getOkHttpBuilderWithTlsSupport(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if(true){
            //tls 1.2
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .build();
                builder.connectionSpecs(Collections.singletonList(spec));
            }
            else{
                builder.sslSocketFactory(new YOSSLSocketFactory(), new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                });
            }

            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });
        }
        return builder;
    }

    public static Observable<XUtils> getUtilsObserver(Map<String, Object> map) {
        return getAdapter(new XUtils())
                .create(XsollaApi.class)
                .fetchUtils(map);
    }

    public static Observable<XVPSummary> getVPObserver(Map<String, Object> map) {
        return getAdapter(new XVPSummary())
                .create(XsollaApi.class)
                .fetchSummary(map);
    }

    public static Observable<XProceed> getVPProceed(Map<String, Object> map) {
        return getAdapter(new XProceed())
                .create(XsollaApi.class)
                .proceed(map);
    }

    public static Observable<XVPStatus> getVPStatus(Map<String, Object> map) {
        return getAdapter(new XVPStatus())
                .create(XsollaApi.class)
                .getVpStatus(map);
    }

    public static Observable<XPricepointsManager> getPricepointsObserver(Map<String, Object> map) {
        return getAdapter(new XPricepointsManager())
                .create(XsollaApi.class)
                .fetchPricepoints(map);
    }

    public static Observable<XVirtualItemGroupsManager> getGroupsObserver(Map<String, Object> map) {
        return getAdapter(new XVirtualItemGroupsManager())
                .create(XsollaApi.class)
                .fetchVIGroups(map);
    }

    public static Observable<XVirtualItemsManager> getItemsObserver(Map<String, Object> map) {
        return getAdapter(new XVirtualItemsManager())
                .create(XsollaApi.class)
                .fetchVItems(map);
    }

    public static Observable<XVirtualItemsManagerMobile> getMobileItemsObserver(Map<String, Object> map) {
        return getAdapter(new XVirtualItemsManagerMobile())
                .create(XsollaApi.class)
                .fetchMobileVItems(map);
    }

    public static Observable<XSubscriptionsManager> getSubscriptionsObserver(Map<String, Object> map) {
        return getAdapter(new XSubscriptionsManager())
                .create(XsollaApi.class)
                .fetchSubscriptions(map);
    }

    public static Observable<XSufficiency> getSufficiencyObserver(Map<String, Object> map) {
        return getAdapter(new XSufficiency())
                .create(XsollaApi.class)
                .sufficiency(map);
    }

    public static Observable<XPSQuickManager> getQuickPSObserver(Map<String, Object> map) {
        return getAdapter(new XPSQuickManager())
                .create(XsollaApi.class)
                .fetchQuickPS(map);
    }

    public static Observable<XPSAllManager> getAllPSObserver(Map<String, Object> map) {
        return getAdapter(new XPSAllManager())
                .create(XsollaApi.class)
                .fetchAllPS(map);
    }

    public static Observable<XPSManager> getXPSObserver(Map<String, Object> map) {
        return getAdapter(new XPSManager())
                .create(XsollaApi.class)
                .fetchXPS(map);
    }

    public static Observable<XCountryManager> getCountriesObserver(Map<String, Object> map) {
        return getAdapter(new XCountryManager())
                .create(XsollaApi.class)
                .fetchCountries(map);
    }

    public static Observable<XDirectpayment> getDirectpaymentObserver(Map<String, Object> map) {
        map.put("paymentWithSavedMethod", 0);
        return getAdapter(new XDirectpayment())
                .create(XsollaApi.class)
                .fetchDirectpayment(map);
    }

    public static void setSandbox(boolean isSandbox){
        XRestAdapter.isSandbox = isSandbox;
    }

    private static String getDomain(){
        return isSandbox ? BASE_URL_SAND : BASE_URL;
    }

    public static Observable<XToken> getToken(String country, String currency, String language) {
        String tokenJson = "";
        try {
            tokenJson = generateTokenJson(country, currency, language);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        HashMap<String, String> requestMap = new HashMap<>();
//        requestMap.put("data", tokenJson);

        return getTokenAdapter()
                .create(XsollaApi.class)
                .fetchToken(tokenJson);
//                new RestAdapter.Builder().
//                setLogLevel(RestAdapter.LogLevel.FULL)
//                .setLog(new AndroidLog("YOUR_LOG_TAG"))
//                .setConverter(new XConverter<>(new XToken()))
//                .setClient(new OkClient())
//                .setEndpoint(BASE_URL_TOKEN)
//                .build()
//                .create(XsollaApi.class)
//                .fetchToken(tokenJson);
    }

    private static String generateTokenJson() throws JSONException {
        JSONObject joTokenRequest = new JSONObject();

        JSONObject joUser = new JSONObject();
        addField(joUser, "id", "user_1", false);
        addField(joUser, "name", "John Smit", false);
        addField(joUser, "email", "support@xsolla.com", false);
        addField(joUser, "country", "US", true);

        JSONObject joSettings = new JSONObject();
        joSettings.put("project_id", 16184);
        joSettings.put("currency", "USD");
        joSettings.put("language", "en");

        joTokenRequest.put("user", joUser);
        joTokenRequest.put("settings", joSettings);
        return joTokenRequest.toString();
    }

    private static String generateTokenJson(String country, String currency, String language) throws JSONException {
        JSONObject joTokenRequest = new JSONObject();

        JSONObject joUser = new JSONObject();
        addField(joUser, "id", "user_1", false);
        addField(joUser, "name", "John Smit", false);
        addField(joUser, "email", "support@xsolla.com", false);
        addField(joUser, "country", country, true);

        JSONObject joSettings = new JSONObject();
        joSettings.put("project_id", 16184);
        joSettings.put("currency", currency);
        joSettings.put("language", language);

        joTokenRequest.put("user", joUser);
        joTokenRequest.put("settings", joSettings);
        return joTokenRequest.toString();
    }

    private static void addField(JSONObject jo, String fieldName, String value, boolean isAllowModify) throws JSONException {
        JSONObject newJo = new JSONObject();
        newJo.put("value", value);
        newJo.put("allow_modify", isAllowModify);
        jo.put(fieldName, newJo);
    }

}
