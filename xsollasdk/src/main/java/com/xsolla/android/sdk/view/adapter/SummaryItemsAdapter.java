package com.xsolla.android.sdk.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.сheckout.XSummary.XPurchase.XPurchaseItem;
import com.xsolla.android.sdk.util.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
public class SummaryItemsAdapter extends BaseAdapter {

    private Context                     mContext;
    private ArrayList<XPurchaseItem>    mElements;
    private HashMap<String, String>     translations;
    private String                      emptyTitleHolder;

    public SummaryItemsAdapter(Context mContext, ArrayList<XPurchaseItem> mElements, HashMap<String, String> translations) {
        this.mContext       = mContext;
        this.mElements      = mElements;
        this.translations   = translations;
        this.emptyTitleHolder = translations.get("simple_checkout_description");
    }

    @Override
    public int getCount() {
        return mElements.size();
    }

    @Override
    public XPurchaseItem getItem(int position) {
        return mElements.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.xsolla_checkout_summary_item, null);

            holder.imageView        = (ImageView)convertView.findViewById(R.id.xsolla_iv);
            holder.tvTitle          = (TextView)convertView.findViewById(R.id.xsolla_tv_title);
            holder.tvDescription    = (TextView)convertView.findViewById(R.id.xsolla_tv_description);
            holder.tvPrice          = (TextView)convertView.findViewById(R.id.xsolla_tv_price);
            holder.tvBonus          = (TextView)convertView.findViewById(R.id.xsolla_tv_bonus);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        XPurchaseItem item = getItem(position);
        holder.imageView.setImageBitmap(null);
        if(!"".equals(item.getImgUrl())) {
            ((View)holder.imageView.getParent()).setVisibility(View.VISIBLE);
            ImageLoader.getInstance().loadImage(holder.imageView, item.getImgUrl());
        } else {
            ((View)holder.imageView.getParent()).setVisibility(View.GONE);
        }

        String name = item.getTitle();
        if("".equals(name))
            name = emptyTitleHolder;
        holder.tvTitle.setText(name);//simple_checkout_description

        if(!"".equals(item.getDescription())) {
            holder.tvDescription.setText(item.getDescription());
            holder.tvDescription.setVisibility(View.VISIBLE);
        } else {
            holder.tvDescription.setVisibility(View.GONE);
        }
        if(item.isBonus()) {
            holder.tvBonus.setVisibility(View.VISIBLE);
        } else {
            holder.tvBonus.setVisibility(View.GONE);
        }



        holder.tvPrice.setText(item.getPrice());

        return convertView;
    }

    static class ViewHolder{
        ImageView   imageView;
        TextView    tvTitle, tvDescription, tvPrice, tvBonus;
    }

}
