package com.xsolla.android.sdk.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.shop.XPricepointsManager;
import com.xsolla.android.sdk.api.model.shop.XSubscriptionsManager;
import com.xsolla.android.sdk.api.model.util.XTranslationsKeys;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.view.XsollaActivity;
import com.xsolla.android.sdk.view.adapter.PricepointsAdapter;
import com.xsolla.android.sdk.view.adapter.SubscriptionsAdapter;
import com.xsolla.android.sdk.view.adapter.VirtualItemsAdapter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A fragment representing a list of Items.
 * <br>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ItemFragment extends XFragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    private XUtils mUtils;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ItemFragment newInstance(int columnCount) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.xsolla_fragment_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
//            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
//            } else {
//                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
//            }

            XUtils utils = ((XsollaActivity)getActivity()).getUtils();
            HashMap<String, String> translations = utils.getTranslations();
            String advice   = translations.get("option_recommended_default");//
            String best     = translations.get("option_best_deal_default");//
            String offer    = translations.get("option_offer");//
            String[] offersStrings = {advice, best, offer};
            String coins = utils.getProject().getVirtualCurrencyName();
            switch (mColumnCount) {
                case 0:
                    setTitle(mUtils.getTranslations().get(XTranslationsKeys.PRICEPOINT_PAGE_TITLE));
                    XPricepointsManager managerP = ((XsollaActivity)getActivity()).getPricepointsManager();
                    String buy = ((XsollaActivity)getActivity()).getUtils().getTranslations().get("pricepoint_option_button");//
                    recyclerView.setAdapter(new PricepointsAdapter(managerP.getList(), mListener, coins, buy, offersStrings));
                    break;
                case 1:
                    setTitle(mUtils.getTranslations().get(XTranslationsKeys.SUBSCRIPTION_MOBILE_PAGE_TITLE));
                    XSubscriptionsManager managerS = ((XsollaActivity)getActivity()).getSubscriptionsManager();
                    String periodPrefix = translations.get("subscription_package_rate_mobile");//
                    String periodMonth = translations.get("period_month1");//
                    String periodDays = translations.get("period_days");//
                    periodPrefix = periodPrefix.replace("{{period}}", "");
                    String[] strings = {periodPrefix, periodMonth, periodDays};
                    recyclerView.setAdapter(new SubscriptionsAdapter(managerS.getListPackages(), mListener, strings));
                    break;
                case 2:
                    setTitle(mUtils.getTranslations().get(XTranslationsKeys.VIRTUALITEM_PAGE_TITLE));
                    ArrayList<Object> managerV = ((XsollaActivity)getActivity()).getVirtulaItemsList();
                    String buy1 = translations.get("virtual_item_option_button");//
                    String learnMore = translations.get("option_description_expand");//
                    String collapse = translations.get("option_description_collapse");//
                    String[] arr = {buy1, learnMore, collapse, coins};
                    recyclerView.setAdapter(new VirtualItemsAdapter(managerV, mListener, arr, offersStrings, utils.getProject().getVirtualCurrencyImage()));
                    break;
                case 3:
                    break;
                default:
            }
//            recyclerView.setAdapter(new MyItemRecyclerViewAdapter(DummyContent.ITEMS, mListener));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
            mUtils = ((XsollaActivity)context).getUtils();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onOnProductSelected(HashMap<String, Object> map, boolean isVirtualPayment);
    }
}
