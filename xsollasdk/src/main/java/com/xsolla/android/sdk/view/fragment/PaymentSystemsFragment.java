package com.xsolla.android.sdk.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.psystems.XPaymentSystemsManager;
import com.xsolla.android.sdk.api.model.util.XTranslationsKeys;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.view.XsollaActivity;
import com.xsolla.android.sdk.view.adapter.XPSAdapter;

/**
 *
 */
public class PaymentSystemsFragment extends XFragment {

    private XPaymentSystemsManager mXPSManager;
    private XUtils                  mUtils;

    private String                  noXpsBaseString;

    private OnFragmentInteractionListener mListener;

    public PaymentSystemsFragment() {
        // Required empty public constructor
    }

    public static PaymentSystemsFragment newInstance() {
        return new PaymentSystemsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setTitle(mUtils.getTranslations().get(XTranslationsKeys.PAYMENT_METHODS_PAGE_TITLE  ));
        View rootView = inflater.inflate(R.layout.xsolla_fragment_ps, container, false);

        final TextView tvNoXps = (TextView) rootView.findViewById(R.id.xsolla_tv_no_xps);
        noXpsBaseString = mUtils.getTranslations().get(XTranslationsKeys.PAYMENT_METHOD_NO_DATA);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.xsolla_list);
        final XPSAdapter adapter = new XPSAdapter(mXPSManager.getAllManager().getMap(),
                                                    mXPSManager.getMergedList(), mListener);
        recyclerView.setAdapter(adapter);

        final SearchView searchView = (SearchView) rootView.findViewById(R.id.xsolla_searchView);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(!"".equals(newText)) {
                    adapter.getFilter().filter(newText);
                    String noXps = noXpsBaseString.replace("{{method}}", newText);
                    tvNoXps.setText(Html.fromHtml(noXps));
                } else {
                    adapter.getDefault();
                }
                return true;
            }
        });
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener   = (OnFragmentInteractionListener) context;
            mXPSManager = ((XsollaActivity)getActivity()).getXpsManager();
            mUtils      = ((XsollaActivity)getActivity()).getUtils();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onPaymentSystemSelected(long pid);
    }
}
