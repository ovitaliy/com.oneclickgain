package com.xsolla.android.sdk.util;

/**
 *
 */
public class PriceFormatter {

    public static String formatPrice(String currency, String amount){
        String[] splitResult = amount.split("\\.");
        if(splitResult.length > 1)
            if(Integer.parseInt(splitResult[1]) == 0)
                amount = splitResult[0];
        return appendCurrency(currency, amount);
    }

    public static String formatPriceSimple(String currency, String amount){
        return appendCurrency(currency, amount);
    }

    private static String appendCurrency(String currency, String amount){
        if(currency == null)
            currency = "USD";

        switch (currency) {
            case "USD":
            case "AUD":
                amount = "$" + amount;
                break;
            case "EUR":
                amount = "€" + amount;
                break;
            case "GBP":
                amount = "&pound;" + amount;
                break;
            case "BRL":
                amount = "R$" + amount;
                break;
            case "RUB":
                amount = amount + "₽";
                break;//&#8399;
            case "KRW":
                amount = "₩" + amount;
                break;//&#8399;
            case "CNY":
                amount = amount + "元";
                break;//&#8399;
            case "VEF":
                amount = "Bs" + amount;
                break;//&#8399;
            default:
                amount = amount + currency;
        }
        return amount;
    }

}
