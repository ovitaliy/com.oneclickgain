package com.xsolla.android.sdk.api.model.util;

import com.xsolla.android.sdk.api.model.IParseble;
import com.xsolla.android.sdk.util.PriceFormatter;

import org.json.JSONObject;

/**
 *
 */
public class XUser implements IParseble {


    int             savedPaymentMethodCount;//"saved_payment_method_count":0,
    boolean         isVatRequired;//"vat_required":false,

    //String          attributes;//"attributes":[
    String          local;//"local":"en",
    String          acceptLanguage;//"accept_language":"ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,cs;q=0.2",
    String          acceptEncoding;//"accept_encoding":"gzip, deflate",

    XRequisites     requisites; //"requisites":{},
    XCountry        country; //"country":{},
    XVCBalance      vcBalance; //"virtual_currency_balance":{},
    XUserBalance    userBalance;//"user_balance":{},

    public XUser() {
        this.requisites     = new XRequisites();
        this.country        = new XCountry();
        this.vcBalance      = new XVCBalance();
        this.userBalance    = new XUserBalance();
    }

    public int getSavedPaymentMethodCount() {
        return savedPaymentMethodCount;
    }

    public boolean isVatRequired() {
        return isVatRequired;
    }

    public String getLocal() {
        return local;
    }

    public String getAcceptLanguage() {
        return acceptLanguage;
    }

    public String getAcceptEncoding() {
        return acceptEncoding;
    }

    public XRequisites getRequisites() {
        return requisites;
    }

    public XCountry getCountry() {
        return country;
    }

    public boolean isVcBalance() {
        return vcBalance.isAmount();
    }

    public String getVcBalance() {
        return vcBalance.getAmount();
    }

    public boolean isUserBalance() {
        return userBalance.isAmount();
    }

    public String getUserBalance() {
        return PriceFormatter.formatPrice(userBalance.getCurrency(), Double.toString(userBalance.getAmount()));
    }

    @Override
    public void parse(JSONObject jobj) {

        this.savedPaymentMethodCount    = jobj.optInt("saved_payment_method_count");
        this.isVatRequired              = jobj.optBoolean("vat_required");

        //this.attributes = jobj.optString("attributes");
        this.local = jobj.optString("local");
        this.acceptLanguage = jobj.optString("accept_language");
        this.acceptEncoding = jobj.optString("accept_encoding");

        JSONObject jobjRequisites = jobj.optJSONObject("requisites");
        if(jobjRequisites != null)
            requisites.parse(jobjRequisites);
        JSONObject jobjCountry = jobj.optJSONObject("country");
        if(jobjCountry != null)
            country.parse(jobjCountry);
        JSONObject jobjVCBalance = jobj.optJSONObject("virtual_currency_balance");
        if(jobjVCBalance != null)
            vcBalance.parse(jobjVCBalance);
        JSONObject jobjUserBalance = jobj.optJSONObject("user_balance");
        if(jobjUserBalance != null)
            userBalance.parse(jobjUserBalance);
    }

    @Override
    public String toString() {
        return "\n\tXUser{" +
                "savedPaymentMethodCount=" + savedPaymentMethodCount +
                ", isVatRequired=" + isVatRequired +
                ", local='" + local + '\'' +
                ", acceptLanguage='" + acceptLanguage + '\'' +
                ", acceptEncoding='" + acceptEncoding + '\'' +
                ", requisites=" + requisites +
                ", country=" + country +
                ", vcBalance=" + vcBalance +
                ", userBalance=" + userBalance +
                '}';
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
    * INNER CLASSES
    * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     *
     */
    public class XRequisites implements IParseble {
        String  id;//"user_1",
        String  value;//"John Smith",
        boolean isVisible;//true

        public String getId() {
            return id;
        }

        public String getValue() {
            return !"null".equals(value) ? value : "";
        }

        public boolean isVisible() {
            return isVisible;
        }

        @Override
        public void parse(JSONObject jobj) {
            this.id         = jobj.optString("id");
            this.value      = jobj.optString("value");
            this.isVisible  = jobj.optBoolean("isVisible");
        }

        @Override
        public String toString() {
            return "\n\t\tXRequisites{" +
                    "id='" + id + '\'' +
                    ", value='" + value + '\'' +
                    ", isVisible=" + isVisible +
                    '}';
        }
    }

    /**
     *
     */
    public class XCountry implements IParseble {
        private String  value;//"value":"US",
        private boolean isAllowModify;//"allow_modify":tr

        public String getValue() {
            return value;
        }

        public boolean isAllowModify() {
            return isAllowModify;
        }

        @Override
        public void parse(JSONObject jobj) {
            this.value          = jobj.optString("value");
            this.isAllowModify  = jobj.optBoolean("allow_modify");
        }

        @Override
        public String toString() {
            return "\n\t\tXCountry{" +
                    "value='" + value + '\'' +
                    ", isAllowModify=" + isAllowModify +
                    '}';
        }
    }

    /**
     *
     */
    class XVCBalance implements IParseble {
        private double amount;//"amount":395

        public boolean isAmount() {
            return amount > 0;
        }

        public String getAmount() {
            return fmt(amount);
        }

        @Override
        public void parse(JSONObject jobj) {
            this.amount = jobj.optDouble("amount");
        }

        @Override
        public String toString() {
            return "\n\t\tXVCBalance{" +
                    "amount=" + amount +
                    '}';
        }
    }

    public static String fmt(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }


    /**
     *
     */
    class XUserBalance implements IParseble {
        private double   amount;// "amount":0.4,
        private String  currency;// "currency":"USD"

        public boolean isAmount() {
            return amount > 0;
        }

        public double getAmount() {
            return amount;
        }

        public String getCurrency() {
            return currency;
        }

        @Override
        public void parse(JSONObject jobj) {
            this.amount     = jobj.optDouble("amount");
            this.currency   = jobj.optString("currency");
        }

        @Override
        public String toString() {
            return "\n\t\tXUserBalance{" +
                    "amount=" + amount +
                    ", currency='" + currency + '\'' +
                    '}';
        }
    }
}
