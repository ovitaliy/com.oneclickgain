package com.xsolla.android.sdk.api.model.util;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 */
public class XUtils implements IParseble {
    private XUser                   user;// "user":{
    private XProject                project;//  "project":{
    private XPurchase               purchase;// "purchase":{
    private XSettings               settings;// "settings":{
    private HashMap<String, String> translations;// "translations":{
    //String settings;// ignore


    public XUtils() {
        user = new XUser();
        project = new XProject();
        purchase = new XPurchase();
        settings = new XSettings();
        translations = new HashMap<>(400);
    }

    public XUser getUser() {
        return user;
    }

    public XProject getProject() {
        return project;
    }

    public XPurchase getPurchase() {
        return purchase;
    }

    public XSettings getSettings() {
        return settings;
    }

    public HashMap<String, String> getTranslations() {
        return translations;
    }

    public ArrayList<XActiveComponent> getActiveComponents(){
        HashMap<String, XProject.XComponent> components = getProject().getComponents();
        HashMap<String, Boolean> componentsModifier = getSettings().getComponents();
        ArrayList<XActiveComponent> list = new ArrayList<>(components.size());
        for (Map.Entry<String, XProject.XComponent> entry : components.entrySet()) {
            String elemName = entry.getValue().getName();
            boolean isNameEmpty = "null".equals(elemName) || "".equals(elemName);
            boolean isElementActive = !componentsModifier.containsKey(entry.getKey())
                    || (componentsModifier.containsKey(entry.getKey()) && !componentsModifier.get(entry.getKey()));
            switch (entry.getKey()) {
                case "items":
                    if(entry.getValue().isEnabled()) {
                        String icon     = "\uE015";
                        String title    = isNameEmpty ? translations.get("virtualitem_page_title") : elemName;//
                        int action      = 0;
                        XActiveComponent component = new XActiveComponent(icon, title, action);
                        if(isElementActive)
                            list.add(component);
                    }
                    break;
//                case "simple_checkout":
//                    translations.get("virtualitem_page_title");
//                    break;
                case "subscriptions":
                    if(entry.getValue().isEnabled()) {
                        String icon     = "\uE016";
                        String title    = isNameEmpty ? translations.get("subscription_mobile_page_title") : elemName;//
                        int action      = 1;
                        XActiveComponent component = new XActiveComponent(icon, title, action);
                        if(isElementActive)
                            list.add(component);
                    }
                    break;
                case "virtual_currency":
                    if(entry.getValue().isEnabled()) {
                        String icon     = "\uE009";
                        String title    = isNameEmpty ? translations.get("pricepoint_page_title") : elemName;//
                        int action      = 2;
                        XActiveComponent component = new XActiveComponent(icon, title, action);
                        if(isElementActive)
                            if(list.size() >= 2)
                                list.add(1, component);
                            else
                                list.add(component);
                    }
                    break;
                default:
            }
        }
        return list;
    }

    public class XActiveComponent {
        String icon;//     = "\uE009";
        String title;//    = translations.get("pricepoint_page_title");//
        int action;//      = 2;

        public XActiveComponent(String icon, String title, int action) {
            this.icon = icon;
            this.title = title;
            this.action = action;
        }

        public String getIcon() {
            return icon;
        }

        public String getTitle() {
            return title;
        }

        public int getAction() {
            return action;
        }

        @Override
        public String toString() {
            return title;
        }
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONObject jobjUser = jobj.optJSONObject("user");
        if(jobjUser != null) {
            user.parse(jobjUser);
        }

        JSONObject jobjProject = jobj.optJSONObject("project");
        if(jobjProject != null) {
            project.parse(jobjProject);
        }

        JSONObject jobjPurchase = jobj.optJSONObject("purchase");
        if(jobjPurchase != null) {
            purchase.parse(jobjPurchase);
        }

        JSONObject jobjSettings = jobj.optJSONObject("settings");
        if(jobjSettings != null) {
            settings.parse(jobjSettings);
        }

        JSONObject jobjTranslations = jobj.optJSONObject("translations");
        if(jobjTranslations != null)
            for(Iterator<String> iter = jobjTranslations.keys(); iter.hasNext();) {
                String key = iter.next();
                String value = jobjTranslations.optString(key);
                translations.put(key, value);
            }
    }

    @Override
    public String toString() {
        return "XUtils{" +
                "\nuser=" + user +
                ", \nproject=" + project +
                ", \npurchase=" + purchase +
                ", \ntranslations=" + translations +
                '}';
    }
}
