package com.henesiz.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.henesiz.R;
import com.henesiz.fragments.HenezisNavigationBalanceFragment;
import com.henesiz.fragments.MenuFragment;
import com.henesiz.fragments.diy.DiyNavigationFragment;
import com.humanet.humanetcore.activities.BaseMainActivity;
import com.humanet.humanetcore.fragments.AlienLookFragment;
import com.humanet.humanetcore.fragments.CrowdFragment;
import com.humanet.humanetcore.fragments.VistoryFragment;
import com.humanet.humanetcore.fragments.balance.CurrencyFragment;
import com.humanet.humanetcore.fragments.balance.RatingsTabFragment;
import com.humanet.humanetcore.fragments.info.FeedbackFragment;
import com.humanet.humanetcore.fragments.info.InfoFragment;
import com.humanet.humanetcore.fragments.info.NavigationInfoFragment;
import com.humanet.humanetcore.fragments.vote.VoteTabFragment;

public class MainActivity extends BaseMainActivity {

    public static void startNewInstance(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void launchFirstFragment() {
        startFragment(new VistoryFragment(), false, true);
    }

    @Override
    protected MenuFragment newMenuFragmentInstance() {
        return new MenuFragment();
    }

    @Override
    public void onNavigateByViewId(int viewId) {
        switch (viewId) {

            case R.id.do_it:
                startFragment(new DiyNavigationFragment(), false, true);
                break;

            case R.id.alien_look:
                startFragment(new AlienLookFragment(), false, true);
                break;

            case R.id.balance:
                startFragment(HenezisNavigationBalanceFragment.newInstance(), false, true);
                break;

            case R.id.currency:
                startFragment(new CurrencyFragment(), true, true);
                break;

            case R.id.crowd:
                startFragment(new CrowdFragment(), true);
                break;

            case R.id.rating:
                startFragment(new RatingsTabFragment(), true, true);
                break;


            case R.id.navigation_info:
                startFragment(new NavigationInfoFragment(), false, true);
                break;

            case R.id.info:
                startFragment(new InfoFragment(), true, true);
                break;

            case R.id.feedback:
                startFragment(new FeedbackFragment(), true, true);
                break;


            case R.id.vote:
                startFragment(new VoteTabFragment(), true, true);
                break;

            case R.id.vistory:
                startFragment(new VistoryFragment(), false);
                break;


            default:
                Toast.makeText(this, "UNDER CONSTRUCTION", Toast.LENGTH_SHORT).show();

        }
    }


}
