package com.henesiz;

import com.henesiz.impls.AnalyticsImpl;
import com.henesiz.impls.BalanceChangeTypeImpl;
import com.henesiz.impls.CurrencyTabViewImpl;
import com.henesiz.impls.RatingImpl;
import com.henesiz.impls.VideoStatisticsImpl;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.fragments.balance.CurrencyFragment;
import com.humanet.humanetcore.fragments.balance.RatingsTabFragment;
import com.humanet.humanetcore.model.VideoStatistic;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.utils.BalanceHelper;
import com.humanet.humanetcore.utils.GsonHelper;

/**
 * Created by ovi on 1/26/16.
 */
public class HenesizApp extends App {

    static {
        API_APP_NAME = "henesiz";
        DEBUG = BuildConfig.DEBUG;

        GsonHelper.BALANCE_SERIALIZATION_KEY = "wow";

        COINS_CODE = "Wow!";
    }

    @Override
    public void onCreate() {
        super.onCreate();

        NavigationManager.init(new NavigationManagerImpl());

        BalanceHelper.setsImpl(new BalanceChangeTypeImpl());

        VideoStatistic.setVideoStatisticDelegator(new VideoStatisticsImpl());

        RatingsTabFragment.setImpl(new RatingImpl(this));

        CurrencyFragment.setImpl(new CurrencyTabViewImpl(this));

        AnalyticsHelper.setAnalytics(new AnalyticsImpl());
    }

    @Override
    public String getServerEndpoint() {
        return Constants.API;
    }
}
