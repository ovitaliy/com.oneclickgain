# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepattributes InnerClasses
-keep class com.clickandgain.**
-keepclassmembers class com.clickandgain.** {
    *;
}

-keep class com.humanet.humanetcore.api.listeners.**
-keep class com.clickandgain.api.response.**
-keep class com.humanet.humanetcore.api.response.user.**
-keep class com.humanet.humanetcore.api.response.video.**

-keepclassmembers class com.clickandgain.api.requests.** {
   *;
}
-keepclassmembers class com.humanet.humanetcore.api.requests.database.** {
   *;
}
-keepclassmembers class com.humanet.humanetcore.api.requests.user.** {
   *;
}
-keepclassmembers class com.humanet.humanetcore.api.requests.video.** {
   *;
}
-keepclassmembers class com.clickandgain.api.requests.game.** {
    *;
}

-keepclassmembers class ** {
    public void onEvent(**);
    public void onEventMainThread(**);
}

-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep class android.support.v7.app.** { *; }
-keep interface android.support.v7.app.** { *; }

#Warnings to be removed. Otherwise maven plugin stops, but not dangerous
-dontwarn android.support.**
-dontwarn com.sun.xml.internal.**
-dontwarn com.sun.istack.internal.**
-dontwarn org.codehaus.jackson.**
-dontwarn org.springframework.**
-dontwarn java.awt.**
-dontwarn javax.security.**
-dontwarn java.beans.**
-dontwarn javax.xml.**
-dontwarn java.util.**
-dontwarn org.w3c.dom.**
-dontwarn com.google.common.**
-dontwarn com.octo.android.robospice.persistence.**


## Gson SERIALIZER SETTINGS
# See https://code.google.com/p/google-gson/source/browse/trunk/examples/android-proguard-example/proguard.cfg
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
# Gson specific classes
-keep class sun.misc.Unsafe { *; }

-keepattributes *Annotation*,Signature

-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

-keep class com.octo.android.robospice.retrofit.** { *; }
-dontwarn com.octo.android.robospice.retrofit.**
-dontwarn com.octo.android.robospice.persistence.**
-dontwarn com.octo.android.robospice.SpiceService

-dontwarn rx.**
-dontwarn com.squareup.okhttp.**
-dontwarn org.apache.http.*
-dontwarn org.apache.http.**

-dontwarn com.google.appengine.api.urlfetch.*



-dontwarn java.nio.file.Files
-dontwarn java.nio.file.Path
-dontwarn java.nio.file.OpenOption
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement


-dontwarn com.appsflyer.**
-dontwarn com.mixpanel.**