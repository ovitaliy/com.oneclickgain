package com.clickandgain.api.requests;

import com.clickandgain.api.requests.game.GameEndRequest;
import com.clickandgain.api.requests.game.GamesEditRequest;
import com.clickandgain.api.requests.game.GamesStartAstraRequest;
import com.clickandgain.api.requests.game.GamesStartCredoRequest;
import com.clickandgain.api.requests.game.QuestionPickRequest;
import com.clickandgain.api.response.game.GameEndResponse;
import com.clickandgain.api.response.game.GameGetQuestionsResponse;
import com.clickandgain.api.response.game.GameStartResponse;
import com.clickandgain.api.sets.GameApiSet;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.model.game.QuestionPickResponse;
import com.clickandgain.views.widgets.game.SportCategoryPickerView;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.model.enums.GameType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ovi on 2/12/16.
 */
public class GamesSetApiTest extends BaseApiTestCase<GameApiSet> {

    private static final boolean ENABLED_ALL = false;

    private static final boolean ENABLED_ASTRA = ENABLED_ALL | false;
    private static final boolean ENABLED_CREDO = ENABLED_ALL | true;
    private static final boolean ENABLED_GG = ENABLED_ALL | false;
    private static final boolean ENABLED_ELSE = ENABLED_ALL | false;

    public GamesSetApiTest() {
        super(GameApiSet.class);
    }

    public void testAstraGame() {
        if (!ENABLED_ASTRA) return;

        GameStartResponse gameStartResponse = getApiSet().start(GamesStartAstraRequest.buildStartGameRequestParams(SportCategoryPickerView.getDefaultCategory()));
        assertNotNull(gameStartResponse);
        assertNull(gameStartResponse.getErrorsMessage());

        String gameToken = gameStartResponse.getGameToken();
        assertNotNull(gameToken);

        GameGetQuestionsResponse gameGetQuestionsResponse = getApiSet().questionsGet(GamesStartAstraRequest.buildGetQuestionsGameRequestParams(10, gameToken));
        assertNotNull(gameGetQuestionsResponse);
        assertNull(gameGetQuestionsResponse.getErrorsMessage());

        Question[] questions = gameGetQuestionsResponse.getQuestions();
        assertNotNull(questions);
        assertEquals(10, questions.length);

        for (Question question : questions) {
            assertTrue(question.getId() != 0);

            assertNotNull(question.getQuestionMedias());
            assertEquals(1, question.getQuestionMedias().length);

            QuestionMedia questionMedia = question.getQuestionMedias()[0];

            assertNotNull(questionMedia);
            assertTrue(questionMedia.getId() != 0);

            assertNotNull(questionMedia.getMedia());
            assertNotNull(questionMedia.getUrlAnswer());


            checkPickAnswerRequest(QuestionPickRequest.buildRequestParams(gameToken, 1, question.getId(), questionMedia.getId()));
        }
    }

    public void testCredoGame() {
        if (!ENABLED_CREDO) return;
        checkGame(GameType.CREDO);
    }

    public void testGeneralGame() {
        if (!ENABLED_GG) return;
        checkGame(GameType.GENERAL_GAME);
    }

    public void testElseGame() {
        if (!ENABLED_ELSE) return;
        checkGame(GameType.ELSE);
    }

    private void checkGame(final GameType gameType) {
        //start game
        GameStartResponse gameStartResponse = getApiSet().start(GamesStartCredoRequest.buildStartGameRequestParams(gameType, 6, 1, SportCategoryPickerView.getDefaultCategory()));
        assertNotNull(gameStartResponse);

        String gameToken = gameStartResponse.getGameToken();
        assertNotNull(gameToken);

        if (gameStartResponse.getErrorsMessage() != null) {
            GameEndResponse gameEndRequest = getApiSet().end(GameEndRequest.buildRequestParams(gameToken, gameType));
            assertNotNull(gameEndRequest);

            gameStartResponse = getApiSet().start(GamesStartCredoRequest.buildStartGameRequestParams(gameType, 6, 1, SportCategoryPickerView.getDefaultCategory()));
            assertNotNull(gameStartResponse);

            gameToken = gameStartResponse.getGameToken();
            assertNotNull(gameToken);
        }
        assertNull(gameStartResponse.getErrorsMessage());


        //get questions
        GameGetQuestionsResponse gameGetQuestionsResponse = getApiSet().questionsGet(GamesStartCredoRequest.buildGetQuestionsGameRequestParams(gameToken,  1));
        assertNotNull(gameGetQuestionsResponse);
        assertNull(gameGetQuestionsResponse.getErrorsMessage());

        Question[] questions = gameGetQuestionsResponse.getQuestions();
        assertNotNull(questions);
        assertEquals(1, questions.length);

        Question question = questions[0];

        assertTrue(question.getId() != 0);

        assertNotNull(question.getQuestionMedias());
        assertEquals(6, question.getQuestionMedias().length);

        QuestionMedia questionMedia = question.getQuestionMedias()[0];

        assertNotNull(questionMedia);
        assertTrue(questionMedia.getId() != 0);

        assertNotNull(questionMedia.getMedia());


        final int multiplier = 2;

        //edit game
        BaseResponse editGameResponse = getApiSet().edit(GamesEditRequest.buildEditGameRequestParams(gameToken, multiplier, 1, SportCategoryPickerView.getDefaultCategory()));
        assertNotNull(editGameResponse);
        assertNull(editGameResponse.getErrorsMessage());

        //edit question
        GameGetQuestionsResponse getEditedQuestionResponse = getApiSet().questionsGet(GamesEditRequest.buildEditQuestionRequestParams(gameToken, question.getId()));
        assertNotNull(getEditedQuestionResponse);
        assertNull(getEditedQuestionResponse.getErrorsMessage());
        questions = getEditedQuestionResponse.getQuestions();
        assertNotNull(questions);
        assertEquals(1, questions.length);

        question = questions[0];
        assertTrue(question.getId() != 0);
        assertNotNull(question.getQuestionMedias());
        assertEquals(multiplier, question.getQuestionMedias().length);

        // get additional 9 questions for game set
        GameGetQuestionsResponse getAdditionQuestionResponse = getApiSet().questionsGet(GamesEditRequest.buildGetQuestionRequestParams(gameToken, 9));
        assertNotNull(getAdditionQuestionResponse);
        assertNull(getAdditionQuestionResponse.getErrorsMessage());
        questions = getAdditionQuestionResponse.getQuestions();
        assertNotNull(questions);
        assertEquals(9, questions.length);

        List<Question> questionList = new ArrayList<>(10);
        questionList.add(question);
        Collections.addAll(questionList, questions);

        int i = 0;
        for (; i < 5; i++) {
            question = questionList.get(i);

            assertTrue(question.getId() != 0);
            assertNotNull(question.getQuestionMedias());
            assertEquals(multiplier, question.getQuestionMedias().length);

            questionMedia = question.getQuestionMedias()[0];

            checkPickAnswerRequest(QuestionPickRequest.buildRequestParams(gameToken, 1, question.getId(), questionMedia.getId()));
        }
        for (; i < 10; i++) {
            question = questionList.get(i);

            assertTrue(question.getId() != 0);
            assertNotNull(question.getQuestionMedias());
            assertEquals(multiplier, question.getQuestionMedias().length);

            questionMedia = question.getQuestionMedias()[0];

            checkPickAnswerRequest(QuestionPickRequest.buildRequestParams(gameToken, -1, question.getId(), questionMedia.getId()));
        }

    }

    public void checkPickAnswerRequest(ArgsMap argsMap) {
        QuestionPickResponse questionPickResponse = getApiSet().questionPick(argsMap);

        assertNotNull(questionPickResponse);
        assertNull(questionPickResponse.getErrorsMessage());

        assertFalse(questionPickResponse.getChange() == 0);
    }
}
