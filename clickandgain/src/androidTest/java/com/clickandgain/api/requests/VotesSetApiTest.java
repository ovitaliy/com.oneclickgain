package com.clickandgain.api.requests;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.requests.vote.AddVoteRequest;
import com.humanet.humanetcore.api.requests.vote.GetVoteRequest;
import com.humanet.humanetcore.api.requests.vote.GetVotesListRequest;
import com.humanet.humanetcore.api.response.vote.AddVoteResponse;
import com.humanet.humanetcore.api.response.vote.VoteListsResponse;
import com.humanet.humanetcore.api.response.vote.VoteResponse;
import com.humanet.humanetcore.api.sets.VoteApiSet;
import com.humanet.humanetcore.fragments.vote.VoteTabFragment;
import com.humanet.humanetcore.model.vote.Vote;
import com.humanet.humanetcore.model.vote.VoteOption;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ovi on 2/12/16.
 */
public class VotesSetApiTest extends BaseApiTestCase<VoteApiSet> {

    private static final boolean ENABLED_ALL = false;

    private static final boolean ENABLED_ADD_VOTE = ENABLED_ALL | false;
    private static final boolean ENABLED_GET_VOTE_LIST = ENABLED_ALL | false;
    private static final boolean ENABLED_GET_MY_VOTE_LIST = ENABLED_ALL | false;


    public VotesSetApiTest() {
        super(VoteApiSet.class);
    }


    public void testGetVoteList() {
        if (!ENABLED_GET_VOTE_LIST) return;

        List<Vote> voteList = getVoteList();

        assertNotNull(voteList);

        for (Vote vote : voteList) {
            checkVote(vote);
        }
    }

    public void testGetMyVoteList() {
        if (!ENABLED_GET_MY_VOTE_LIST) return;

        VoteListsResponse voteListsResponse = getApiSet().getMyVoteList(App.API_APP_NAME, new ArgsMap());
        assertNotNull(voteListsResponse);
        assertNull(voteListsResponse.getErrorsMessage());

        Vote[] voteList = voteListsResponse.getVotes();

        assertNotNull(voteList);

        for (Vote vote : voteList) {
            checkVote(vote);
        }
    }

    public void testAddVote() {
        if (!ENABLED_ADD_VOTE) return;

        final String newVoteDescription = "Test vote" + (System.currentTimeMillis() / 1000);

        AddVoteResponse addVoteResponse = getApiSet().addVote(App.API_APP_NAME,
                AddVoteRequest.buildRequestArgs(newVoteDescription, new String[]{"a", "b"}));

        assertNotNull(addVoteResponse);
        assertNull(addVoteResponse.getErrorsMessage());


        int newVoteId = addVoteResponse.getId();
        assertTrue(newVoteId != 0);

        VoteResponse voteResponse = getApiSet().getVote(App.API_APP_NAME, GetVoteRequest.buildRequestArgs(newVoteId));
        assertNotNull(voteResponse);
        assertNull(voteResponse.getErrorsMessage());

        VoteResponse.Response response = voteResponse.getResponse();

        assertTrue(response.getOptions().size() == 2);
        assertEquals(response.getOptions().get(0).getTitle(), "a");
        assertEquals(response.getOptions().get(1).getTitle(), "b");
    }

    private void checkVote(Vote vote) {
        assertNotNull(vote);

        assertTrue(vote.getId() != 0);
        assertNotNull(vote.getTitle());

        VoteResponse voteResponse = getApiSet().getVote(App.API_APP_NAME, GetVoteRequest.buildRequestArgs(vote.getId()));
        assertNotNull(voteResponse);
        assertNull(voteResponse.getErrorsMessage());

        VoteResponse.Response response = voteResponse.getResponse();
        assertNotNull(response);

        List<VoteOption> voteOptions = response.getOptions();
        assertNotNull(voteOptions);
        assertTrue(voteOptions.size() != 0);

        for (VoteOption voteOption : voteOptions) {
            assertNotNull(voteOption);
            assertTrue(voteOption.getId() != 0);
            assertNotNull(voteOption.getTitle());
        }
    }

    private List<Vote> getVoteList() {

        List<Vote> voteList = new ArrayList<>();

        String periods[] = {
                VoteTabFragment.NOW, VoteTabFragment.PAST
        };

        for (String period : periods) {

            VoteListsResponse voteListsResponse = getApiSet().getVoteList(App.API_APP_NAME, GetVotesListRequest.buildRequestArgs(period));
            assertNotNull(voteListsResponse);
            assertNull(voteListsResponse.getErrorsMessage());
            if (voteListsResponse.getVotes() != null)
                Collections.addAll(voteList, voteListsResponse.getVotes());

        }
        return voteList;

    }
}
