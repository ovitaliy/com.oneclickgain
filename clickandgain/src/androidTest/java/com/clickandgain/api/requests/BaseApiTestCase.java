package com.clickandgain.api.requests;

import android.test.InstrumentationTestCase;

import com.humanet.humanetcore.service.AppService;

import retrofit.RestAdapter;

/**
 * Created by ovi on 2/10/16.
 */
public abstract class BaseApiTestCase<API_SET>  extends InstrumentationTestCase {

    private static final String API_END_POINT = "http:/";

    private Class<API_SET> mClass;

    public BaseApiTestCase(Class<API_SET> aClass) {
        mClass = aClass;
    }

    protected RestAdapter restAdapter;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_END_POINT)
                .setConverter(new AppService.Converter())
                .build();


    }

    public API_SET getApiSet(){
        return restAdapter.create(mClass);
    }

}
