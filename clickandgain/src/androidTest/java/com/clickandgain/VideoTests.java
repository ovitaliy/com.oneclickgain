package com.clickandgain;

import android.content.Context;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.MediumTest;

import com.github.hiteshsondhi88.libffmpeg.FFmpegSync;
import com.github.hiteshsondhi88.libffmpeg.FfmpegHelper;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.utils.FilePathHelper;

/**
 * Created by ovitali on 08.09.2015.
 */
public class VideoTests extends InstrumentationTestCase {


    @MediumTest
    public void testResizeVideo() throws FFmpegNotSupportedException {
        Context context = App.getInstance();
        FFmpegSync ffmpeg = FFmpegSync.getInstance(context);

        String cmd = new FfmpegHelper.CmdBuilder(FfmpegHelper.Type.VIDEO)
                .addInput(FilePathHelper.getVideoTmpFile().getAbsolutePath())
                .addOutput(FilePathHelper.getVideoTmpFile2().getAbsolutePath())
                .addFilter(String.format("crop=%d:%d", 100, 100))
                .build();


        ffmpeg.loadBinary(null);

        //ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler());

    }


}
