package com.clickandgain.model.game;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import static com.humanet.humanetcore.db.ContentDescriptor.QuestionMedia.Cols;

/**
 * Created by ovitali on 03.09.2015.
 */
public class QuestionMedia implements Serializable {

    @SerializedName("id")
    private int mId;

    @SerializedName("url")
    private String mMedia;

    @SerializedName("url_answer")
    private String mUrlAnswer;

    private int mDownloaded;

    public int getId() {
        return mId;
    }

    public String getMedia() {
        return mMedia;
    }

    public String getUrlAnswer() {
        return mUrlAnswer;
    }

    public boolean isDownloaded() {
        return mDownloaded == 1;
    }

    public void setDownloaded(final boolean downloaded) {
        mDownloaded = downloaded ? 1 : 0;
    }

    public static ContentValues toContentValues(int questionId, int order, String gameKey, QuestionMedia questionMedia) {
        ContentValues contentValues = new ContentValues(5);

        long id = ((long) questionId << 32) + questionMedia.getId();
        contentValues.put("_id", id);
        contentValues.put(Cols.QUESTION_ID, questionId);
        contentValues.put(Cols.ORDER, order);
        contentValues.put(Cols.GAME_KEY, gameKey);
        contentValues.put(Cols.MEDIA_ID, questionMedia.mId);
        contentValues.put(Cols.MEDIA, questionMedia.mMedia);
        contentValues.put(Cols.URL_ANSWER, questionMedia.mUrlAnswer);
        contentValues.put(Cols.DOWNLOADED, questionMedia.mDownloaded);

        return contentValues;
    }

    public static QuestionMedia fromCursor(Cursor cursor) {
        QuestionMedia questionMedia = new QuestionMedia();

        questionMedia.mId = cursor.getInt(cursor.getColumnIndex(Cols.MEDIA_ID));
        questionMedia.mMedia = cursor.getString(cursor.getColumnIndex(Cols.MEDIA));
        questionMedia.mUrlAnswer = cursor.getString(cursor.getColumnIndex(Cols.URL_ANSWER));
        questionMedia.mDownloaded = cursor.getInt(cursor.getColumnIndex(Cols.DOWNLOADED));

        return questionMedia;
    }
}
