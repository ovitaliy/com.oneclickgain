package com.clickandgain.model.game;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.model.enums.Language;

import java.io.Serializable;

/**
 * Created by ovi on 4/13/16.
 */
public class ElseVideoInfo implements Serializable{


    @SerializedName("id")
    private int mId;

    @SerializedName("first_name")
    private String mFirstName;

    @SerializedName("last_name")
    private String mLastName;

    @SerializedName("city_rus")
    private String mCityRus;

    @SerializedName("city_end")
    private String mCityEng;

    @SerializedName("city_esp")
    private String mCityEsp;

    @SerializedName("country_rus")
    private String mCountryRus;

    @SerializedName("country_eng")
    private String mCountryEng;

    @SerializedName("country_esp")
    private String mCountryEsp;

    @SerializedName("views")
    private int mViews;

    @SerializedName("tags")
    private String mTags;


    public int getId() {
        return mId;
    }

    public String getAuthorName() {
        return mFirstName + " " + mLastName;
    }

    public String getCity() {
        switch (Language.getSystem()) {
            case ENG:
                return mCityEng;
            case RUS:
                return mCityRus;
            case ESP:
                return mCityEsp;

            default:
                throw new IllegalArgumentException("Undefined Language");
        }
    }

    public String getCountry() {
        switch (Language.getSystem()) {
            case ENG:
                return mCountryEng;
            case RUS:
                return mCountryRus;
            case ESP:
                return mCountryEsp;


            default:
                throw new IllegalArgumentException("Undefined Language");
        }

    }

    public int getViews() {
        return mViews;
    }

    public String getTags() {
        return mTags;
    }
}
