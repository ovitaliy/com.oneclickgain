package com.clickandgain.model.game;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ovitali on 03.09.2015.
 */
public class PromoQuestionResponse implements Serializable {

    int correct;

    @SerializedName("img_right")
    String rightImage;

    @SerializedName("text_right")
    String rightText;

    @SerializedName("text_wrong")
    String wrongText;

    public String getWrongImage() {
        return wrongImage;
    }

    public String getRightImage() {
        return rightImage;
    }

    public String getRightText() {
        return rightText;
    }

    public String getWrongText() {
        return wrongText;
    }

    @SerializedName("img_wrong")
    String wrongImage;

    public int getCorrect() {
        return correct;
    }


}
