package com.clickandgain.model.game;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 04.12.2015.
 */
public class PromoQuestion {
    int id;

    @SerializedName("text")
    String question;
    String cover;
    String media;

    public int getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String getCover() {
        return cover;
    }

    public boolean isVideo() {
        return media.endsWith(".mp4") || media.endsWith(".avi");
    }

    public String getMedia() {
        return media;
    }


}
