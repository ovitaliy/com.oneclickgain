package com.clickandgain.model.game;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import static com.humanet.humanetcore.db.ContentDescriptor.Question.Cols;

/**
 * Created by ovitali on 03.09.2015.
 */
public class Question implements Serializable {

    @SerializedName("id")
    private int mId;

    @SerializedName("cover")
    private String mCover;

    public void setQuestionMedias(final QuestionMedia[] questionMedias) {
        mQuestionMedias = questionMedias;
    }

    @SerializedName("media")
    private volatile QuestionMedia[] mQuestionMedias;


    private int mAswered;

    public int getId() {
        return mId;
    }

    public String getCover() {
        return mCover;
    }

    public QuestionMedia[] getQuestionMedias() {
        return mQuestionMedias;
    }

    public boolean isDownloaded() {
        for (QuestionMedia questionMedia : mQuestionMedias) {
            if (!questionMedia.isDownloaded()) return false;
        }
        return true;
    }

    public void setDownloaded(boolean downloaded) {
        for (QuestionMedia questionMedia : mQuestionMedias) {
            questionMedia.setDownloaded(downloaded);
        }
    }

    public boolean isAswered() {
        return mAswered == 1;
    }

    public void setAswered(boolean aswered) {
        mAswered = aswered ? 1 : 0;
    }

    public static ContentValues toContentValues(String gameKey, Question question) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Cols.GAME_KEY, gameKey);
        contentValues.put(Cols.QUESTION_ID, question.getId());
        contentValues.put(Cols.COVER, question.getCover());
        contentValues.put("_id", question.getId());
        return contentValues;
    }

    public static Question fromCursor(Cursor cursor) {
        Question question = new Question();

        question.mId = cursor.getInt(cursor.getColumnIndex(Cols.QUESTION_ID));
        question.mCover = cursor.getString(cursor.getColumnIndex(Cols.COVER));
        question.mAswered = cursor.getInt(cursor.getColumnIndex(Cols.ANSWERED));

        return question;
    }

}
