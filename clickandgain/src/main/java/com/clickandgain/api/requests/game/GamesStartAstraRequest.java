package com.clickandgain.api.requests.game;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.text.TextUtils;
import android.util.Log;

import com.clickandgain.api.response.game.GameGetQuestionsResponse;
import com.clickandgain.api.response.game.GameStartResponse;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.modules.games.exceptions.GameException;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.GameType;

import static com.clickandgain.views.widgets.game.SportCategoryPickerView.SportType;

/**
 *
 */
public class GamesStartAstraRequest extends BaseGamesStartRequest {

    private int mCountOfPlayedGames;
    @SportType
    private String mSport;

    public GamesStartAstraRequest(int countOfPlayedGames, @SportType String sport) {
        super();

        mCountOfPlayedGames = countOfPlayedGames;
        mSport = sport;
    }

    @Override
    public GameStartResponse loadDataFromNetwork() throws Exception {
        GameStartResponse startResponse = startGame();

        if (TextUtils.isEmpty(startResponse.getErrorsMessage()))
            mCountOfPlayedGames = 0;
      /*  else
            editGame(startResponse.getGameToken());*/

        loadQuestions(5 - mCountOfPlayedGames, startResponse.getGameToken());

        return startResponse;
    }

    private BaseResponse editGame(String gameKey) throws GameException {
        final ArgsMap argsMap = buildEditGameRequestParams(gameKey, mSport);
        log("games.edit", argsMap);

        final BaseResponse response = new Task<BaseResponse>() {
            @Override
            protected BaseResponse doTask() {
                return getService().edit(argsMap);
            }
        }.execute(gameKey, GameType.ASTRA);

        return response;
    }

    private GameStartResponse startGame() throws GameException {
        final ArgsMap argsMap = buildStartGameRequestParams(mSport);
        log("games.start", argsMap);

        final GameStartResponse response = new Task<GameStartResponse>() {
            @Override
            protected GameStartResponse doTask() {
                return getService().start(argsMap);
            }
        }.execute(null, GameType.ASTRA);

        return response;
    }

    private void loadQuestions(int count, String gameKey) throws GameException {
        final ArgsMap argsMap = buildGetQuestionsGameRequestParams(count, gameKey);

        log("questions.get", argsMap);

        ContentResolver contentResolver = getContentResolver();

        removeAllQuestions(gameKey);

        GameGetQuestionsResponse gameGetQuestionResponse = new Task<GameGetQuestionsResponse>() {
            @Override
            protected GameGetQuestionsResponse doTask() {
                return getService().questionsGet(argsMap);
            }
        }.execute(gameKey, GameType.ASTRA);


        Question questions[] = gameGetQuestionResponse.getQuestions();

        for (int i = 0; i < Math.min(questions.length, 5); i++) {
            ContentValues contentValues = Question.toContentValues(gameKey, questions[i]);
            contentResolver.insert(ContentDescriptor.Question.URI, contentValues);

            Log.v("question", "-->" + questions[i].getId());

            ContentValues[] contentValuesList = new ContentValues[questions[i].getQuestionMedias().length];

            for (int j = 0; j < questions[i].getQuestionMedias().length; j++) {
                QuestionMedia questionMedia = questions[i].getQuestionMedias()[j];
                contentValuesList[j] = QuestionMedia.toContentValues(questions[i].getId(), j, gameKey, questionMedia);

                Log.v("question", "----->" + questionMedia.getId());
            }
            contentResolver.bulkInsert(ContentDescriptor.QuestionMedia.URI, contentValuesList);
        }
    }

    //---
    public static ArgsMap buildStartGameRequestParams(@SportType String sport) {
        ArgsMap argsMap = new ArgsMap();
        argsMap.put("game_type", GameType.ASTRA.getRequestParam());
        argsMap.put("count", String.valueOf(5));
        argsMap.put("sport", sport);

        return argsMap;
    }

    public static ArgsMap buildGetQuestionsGameRequestParams(int count, String gameKey) {
        ArgsMap argsMap = new ArgsMap();
        argsMap.put("game_key", gameKey);
        argsMap.put("count", count);

        return argsMap;
    }

    public static ArgsMap buildEditGameRequestParams(String gameKey, @SportType String sport) {
        ArgsMap argsMap = new ArgsMap();
        argsMap.put("game_key", gameKey);
        argsMap.put("sport", sport);
        return argsMap;
    }


}
