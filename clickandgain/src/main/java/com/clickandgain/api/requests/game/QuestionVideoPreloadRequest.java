package com.clickandgain.api.requests.game;

import android.content.ContentValues;
import android.util.Log;

import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.CountingFileRequestBody;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.octo.android.robospice.request.SpiceRequest;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;

import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;


/**
 * Created by ovitali on 16.10.2015.
 */
public class QuestionVideoPreloadRequest extends SpiceRequest<Question> {

    private static final int BUFFER_SIZE = 4056;

    private Question mQuestion;

    public QuestionVideoPreloadRequest(Question question) {
        super(Question.class);
        mQuestion = question;
    }

    @Override
    public int getPriority() {
        return PRIORITY_LOW;
    }

    @Override
    public Question loadDataFromNetwork() throws Exception {
        try{
            load();
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return mQuestion;
    }

    private Question load() {
        int filesCounts = 0;
        for (QuestionMedia questionMedia : mQuestion.getQuestionMedias()) {
            filesCounts++;
            if (questionMedia.getUrlAnswer() != null) {
                filesCounts++;
            }
        }

        int currentFileIndex = 0;
        for (QuestionMedia questionMedia : mQuestion.getQuestionMedias()) {
            if (!questionMedia.isDownloaded()) {
                if (isCancelled()) {
                    return null;
                }
                loadItem(questionMedia.getMedia(), currentFileIndex++, filesCounts);

                if (questionMedia.getUrlAnswer() != null) {
                    if (isCancelled()) {
                        return null;
                    }
                    loadItem(questionMedia.getUrlAnswer(), currentFileIndex++, filesCounts);
                }
                if (isCancelled()) {
                    return null;
                }
                setMediaAsDownloaded(questionMedia);
            } else {
                currentFileIndex++;
                if (questionMedia.getUrlAnswer() != null) {
                    currentFileIndex++;
                }
            }
        }
        return mQuestion;
    }

    private void setMediaAsDownloaded(QuestionMedia media) {
        media.setDownloaded(true);
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(ContentDescriptor.QuestionMedia.Cols.DOWNLOADED, 1);

        App.getInstance().getContentResolver().update(ContentDescriptor.QuestionMedia.URI, contentValues,
                ContentDescriptor.QuestionMedia.Cols.MEDIA_ID + "=" + media.getId(), null);
    }

    private void loadItem(String videoUrl, int currentItemIndex, int itemsCount) {
        String videoName = FilePathHelper.getVideoFileName(videoUrl);

        File destinationFile = new File(FilePathHelper.getVideoCacheDirectory(), videoName);
        // File destinationFile2 = new File(FilePathHelper.getVideoCacheDirectory(), videoName.replace(".mp4", "_tmp.mp4"));

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(videoUrl)
                .addHeader("Content-Type", "application/json")
                .build();

        BufferedSource bufferedSource = null;
        BufferedSink sink = null;

        try {
            Response response = client.newCall(request).execute();

            long destinationLength = destinationFile.length();

            if (!destinationFile.exists() || destinationLength != response.body().contentLength()) {

                if (destinationLength > 0) {
                    Log.i("QuestionVideoPreload", "destinationLength > 0");
                }

                long contentLength = response.body().contentLength();
                long writtenBytes = destinationLength;

                Listener listener = new Listener(itemsCount, currentItemIndex);

                sink = Okio.buffer(Okio.appendingSink(destinationFile));

                bufferedSource = response.body().source();
                bufferedSource.skip(destinationLength);

                while (writtenBytes < contentLength) {
                    sink.write(response.body().source(), Math.min(BUFFER_SIZE, contentLength - writtenBytes));
                    listener.onRequestProgress(writtenBytes, contentLength);

                    writtenBytes += BUFFER_SIZE;
                }

              /*
              long start = System.currentTimeMillis();

                String cmd = new FfmpegHelper.CmdBuilder(FfmpegHelper.Type.VIDEO)
                        .addInput(destinationFile.getAbsolutePath())
                        .addOutput(destinationFile2.getAbsolutePath())
                        .addFilter("scale=200:-1 ")
                        .build(false);

                FFmpegSync.getInstance(App.getInstance()).execute(cmd, new ExecuteBinaryResponseHandler());

                FileUtils.copyFile(
                        destinationFile2,
                        destinationFile
                );

                Log.i("ResizeVideo", "at:"+(System.currentTimeMillis() - start));

                if (destinationFile2.delete())
                    destinationFile2.deleteOnExit();*/

            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedSource != null)
                try {
                    bufferedSource.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            if (sink != null)
                try {
                    sink.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    //--------------

    private class Listener implements CountingFileRequestBody.Listener {

        private long mLastProgress = -1;

        private final double mFilesCount;
        private final double mCurrentFile;

        public Listener(final int filesCount, final int currentFile) {
            mFilesCount = filesCount;
            mCurrentFile = currentFile;
        }

        @Override
        public void onRequestProgress(long bytesWritten, long contentLength) {
            long progress = (long) (bytesWritten * 100d / contentLength / mFilesCount);

            long partProgress = (long) (100d / mFilesCount * (mCurrentFile));

            if (mLastProgress != progress) {
                // Log.i("VideoPreloadRequest", "Progress" + progress);
                publishProgress(progress + partProgress);
                mLastProgress = progress + partProgress;
            }
        }
    }


}
