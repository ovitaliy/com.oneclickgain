package com.clickandgain.api.requests.game;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.util.Log;

import com.clickandgain.api.response.game.GameGetQuestionsResponse;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.modules.games.exceptions.GameException;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.GameType;

/**
 *
 */
public class GetCredoQuestionsRequest extends BaseGameRequest<GameGetQuestionsResponse> {

    private GameType mGameType;
    private String mGameToken;

    private int mQuestionCount = 1;

    public GetCredoQuestionsRequest(String gameToken, GameType gameType, int count) {
        super(GameGetQuestionsResponse.class);
        mGameToken = gameToken;
        mGameType = gameType;
        mQuestionCount = count;
    }


    @Override
    public GameGetQuestionsResponse loadDataFromNetwork() throws Exception {
        return loadQuestions(mGameToken);
    }


    private GameGetQuestionsResponse loadQuestions(String gameKey) throws Exception {
        final ArgsMap argsMap = GamesStartCredoRequest.buildGetQuestionsGameRequestParams(gameKey, mQuestionCount);

        log("questions.get", argsMap);
        GameGetQuestionsResponse gameGetQuestionResponse = new Task<GameGetQuestionsResponse>() {
            @Override
            protected GameGetQuestionsResponse doTask() {
                return getService().questionsGet(argsMap);
            }
        }.execute(gameKey, mGameType);

        validatePickResponse("questions.get", gameKey, mGameType, argsMap, gameGetQuestionResponse);

        ContentResolver contentResolver = App.getInstance().getContentResolver();

        for (Question question : gameGetQuestionResponse.getQuestions()) {
            ContentValues contentValues = Question.toContentValues(gameKey, question);
            contentResolver.insert(ContentDescriptor.Question.URI, contentValues);

            Log.v("question", "-->" + question.getId());

            for (int i = 0; i < question.getQuestionMedias().length; i++) {
                QuestionMedia questionMedia = question.getQuestionMedias()[i];
                contentValues = QuestionMedia.toContentValues(question.getId(), i, gameKey, questionMedia);

                Log.v("question", "----->" + questionMedia.getId());

                contentResolver.insert(ContentDescriptor.QuestionMedia.URI, contentValues);
            }
        }

        return gameGetQuestionResponse;
    }

    public void validatePickResponse(String requestName, String gameKey, GameType gameType, ArgsMap argsMap, GameGetQuestionsResponse response) throws GameException {
        for (Question question : response.getQuestions()) {
            for (int i = 0; i < question.getQuestionMedias().length; i++) {
                QuestionMedia questionMedia = question.getQuestionMedias()[i];
                if (questionMedia.getMedia() == null) {
                    logToCrashlitics(requestName, gameKey, gameType, argsMap, response);
                }
            }

        }
    }


}
