package com.clickandgain.api.requests.game;

import android.content.ContentResolver;
import android.content.ContentValues;

import com.clickandgain.api.response.game.GameGetQuestionsResponse;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.modules.games.exceptions.GameException;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.GameType;

import static com.clickandgain.views.widgets.game.SportCategoryPickerView.SportType;

/**
 *
 */
public class GamesEditRequest extends BaseGameRequest<BaseResponse> {

    private int mMultiplier;
    private int mRate;
    private String mGameKey;
    @SportType
    private String mSport;
    private Question mLastQuestion;
    private GameType mGameType;

    public GamesEditRequest(GameType gameType, Question lastQuestion, int multiplier, int rate, String gameKey, @SportType String sport) {
        super(BaseResponse.class);
        mGameType = gameType;
        mLastQuestion = lastQuestion;
        mMultiplier = multiplier;
        mRate = rate;
        mGameKey = gameKey;
        mSport = sport;
    }


    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        if (mMultiplier != 6 || mRate != 1) {
            editGame();
        }

        if (mMultiplier != 6)
            editQuestion();

        loadQuestion();

        return null;
    }

    private BaseResponse editGame() throws GameException {
        final ArgsMap argsMap = buildEditGameRequestParams(mGameKey, mMultiplier, mRate, mSport);

        log("games.edit", argsMap);

        BaseResponse response = new Task<BaseResponse>() {
            @Override
            protected BaseResponse doTask() {
                return getService().edit(argsMap);
            }
        }.execute(mGameKey, mGameType);

        return response;
    }

    private void editQuestion() throws GameException {
        final ArgsMap argsMap = buildEditQuestionRequestParams(mGameKey, mLastQuestion.getId());

        log("questions.get (edit question)", argsMap);

        GameGetQuestionsResponse gameGetQuestionResponse = new Task<GameGetQuestionsResponse>() {
            @Override
            protected GameGetQuestionsResponse doTask() {
                return getService().questionsGet(argsMap);
            }
        }.execute(mGameKey, mGameType);

        Question questions[] = gameGetQuestionResponse.getQuestions();

        ContentResolver contentResolver = App.getInstance().getContentResolver();

        String where = String.format("%s=%s", ContentDescriptor.Question.Cols.QUESTION_ID, mLastQuestion.getId());
        contentResolver.delete(ContentDescriptor.Question.URI, where, null);
        contentResolver.delete(ContentDescriptor.QuestionMedia.URI, where, null);

        for (Question question : questions) {
            ContentValues contentValues = Question.toContentValues(mGameKey, question);
            contentResolver.insert(ContentDescriptor.Question.URI, contentValues);

            for (int i = 0; i < question.getQuestionMedias().length; i++) {
                QuestionMedia questionMedia = question.getQuestionMedias()[i];
                contentValues = QuestionMedia.toContentValues(question.getId(), i, mGameKey, questionMedia);

                if (mLastQuestion.getId() == question.getId()) {
                    for (QuestionMedia lastQuestionMedia : mLastQuestion.getQuestionMedias()) {
                        if (lastQuestionMedia.getId() == questionMedia.getId()) {
                            contentValues.put(ContentDescriptor.QuestionMedia.Cols.DOWNLOADED,
                                    lastQuestionMedia.isDownloaded() ? 1 : 0);
                            break;
                        }
                    }
                }

                contentResolver.insert(ContentDescriptor.QuestionMedia.URI, contentValues);
            }
        }
    }

    private void loadQuestion() throws GameException {
        final ArgsMap argsMap = buildGetQuestionRequestParams(mGameKey, 9);

        log("questions.get", argsMap);
        GameGetQuestionsResponse gameGetQuestionResponse = new Task<GameGetQuestionsResponse>() {
            @Override
            protected GameGetQuestionsResponse doTask() {
                return getService().questionsGet(argsMap);
            }
        }.execute(mGameKey, mGameType);

        Question questions[] = gameGetQuestionResponse.getQuestions();

        ContentResolver contentResolver = App.getInstance().getContentResolver();

        if (questions != null) {
            for (Question question : questions) {
                ContentValues contentValues = Question.toContentValues(mGameKey, question);
                contentResolver.insert(ContentDescriptor.Question.URI, contentValues);
                for (int i = 0; i < question.getQuestionMedias().length; i++) {
                    QuestionMedia questionMedia = question.getQuestionMedias()[i];
                    contentValues = QuestionMedia.toContentValues(question.getId(), i, mGameKey, questionMedia);

                    contentResolver.insert(ContentDescriptor.QuestionMedia.URI, contentValues);
                }
            }
        }
    }

    public static ArgsMap buildEditGameRequestParams(String gameKey, int multiplier, int rate, @SportType String sport) {
        ArgsMap map = new ArgsMap();
        map.put("game_key", gameKey);
        map.put("multiplier", multiplier);
        map.put("rate", rate);
        map.put("sport", sport);
        return map;
    }

    public static ArgsMap buildEditQuestionRequestParams(String gameKey, int questionId) {
        ArgsMap map = new ArgsMap();
        map.put("game_key", gameKey);
        map.put("id_question", questionId);
        return map;
    }

    public static ArgsMap buildGetQuestionRequestParams(String gameKey, int count) {
        ArgsMap map = new ArgsMap();
        map.put("game_key", gameKey);
        map.put("count", count);
        return map;
    }


}
