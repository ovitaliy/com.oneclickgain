package com.clickandgain.api.requests.game;

import android.content.ContentResolver;

import com.clickandgain.api.response.game.GameStartResponse;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.db.ContentDescriptor;

/**
 * Created by ovi on 3/16/16.
 */
public abstract class BaseGamesStartRequest extends BaseGameRequest<GameStartResponse> {

    public BaseGamesStartRequest() {
        super(GameStartResponse.class);
    }

    protected final void removeAllQuestions(String gameKey) {
        String where = String.format("%s ='%s'", ContentDescriptor.Question.Cols.GAME_KEY, gameKey);

        getContentResolver().delete(ContentDescriptor.Question.URI, where, null);
        getContentResolver().delete(ContentDescriptor.QuestionMedia.URI, where, null);
    }


    private ContentResolver mContentResolver;

    protected final ContentResolver getContentResolver() {
        if (mContentResolver == null) {
            mContentResolver = App.getInstance().getContentResolver();
        }
        return mContentResolver;
    }
}
