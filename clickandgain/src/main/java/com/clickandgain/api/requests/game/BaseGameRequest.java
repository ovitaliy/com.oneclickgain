package com.clickandgain.api.requests.game;

import android.util.Log;

import com.clickandgain.api.GameRequestErrorController;
import com.clickandgain.api.response.game.GameGetQuestionsResponse;
import com.clickandgain.api.sets.GameApiSet;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.modules.games.exceptions.GameException;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.model.enums.GameType;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.octo.android.robospice.retry.DefaultRetryPolicy;
import com.octo.android.robospice.retry.RetryPolicy;

import static com.clickandgain.api.GameRequestErrorController.ACTION_ABORT;
import static com.clickandgain.api.GameRequestErrorController.ACTION_RETRY;
import static com.clickandgain.api.GameRequestErrorController.GAME_STARTED;
import static com.clickandgain.api.GameRequestErrorController.GameRequestError;

/**
 * Created by ovi on 3/16/16.
 */
public abstract class BaseGameRequest<T> extends RetrofitSpiceRequest<T, GameApiSet> {

    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy(0, 0, 0);
    }

    public BaseGameRequest(Class<T> clazz) {
        super(clazz, GameApiSet.class);
    }

    private GameRequestErrorController mGameRequestErrorController = new GameRequestErrorController();


    private GameRequestError checkResponseEndThrowOnGameError(BaseResponse response, String gameKey, GameType gameType) throws GameException {
        if (response == null) {
            throw new GameException(ACTION_ABORT, gameKey, gameType, "response is empty", null);
        }

        if (response.getErrorCodes() == null || response.getErrorCodes().length == 0 || response.getErrorCodes()[0] == 0 || response.getErrorCodes()[0] == GAME_STARTED)
            return null;

        return mGameRequestErrorController.validateError(response.getErrorCodes()[0]);
    }


    void checkQuestionsEndThrowOnGameError(GameGetQuestionsResponse response, String gameKey, GameType gameType) throws GameException {
        Question[] questions = response.getQuestions();

        if (questions == null)
            throw new GameException(ACTION_ABORT, gameKey, gameType, "question list is empty", response);

        for (Question question : questions) {
            if (question.getId() == 0)
                throw new GameException(ACTION_ABORT, gameKey, gameType, "question id is 0", response);

            if (question.getQuestionMedias() == null)
                throw new GameException(ACTION_ABORT, gameKey, gameType, "question doesn't contain any medias", response);

            for (QuestionMedia questionMedia : question.getQuestionMedias()) {
                if (questionMedia == null)
                    throw new GameException(ACTION_ABORT, gameKey, gameType, "question media is null", response);

                if (questionMedia.getMedia() == null)
                    throw new GameException(ACTION_ABORT, gameKey, gameType, "question media link is empty", response);

            }
        }
    }

    void log(String requestName, ArgsMap argsMap) {
        //GameApiService.log(requestName + " : " + argsMap);
        //Log.i("API: " + requestName, argsMap.toString());
    }

    void logToCrashlitics(String requestName, String key, GameType gameType, ArgsMap argsMap, BaseResponse response) throws GameException {
        Log.i("API: " + requestName, new Gson().toJson(response));

        if (!App.DEBUG) {
            Crashlytics.logException(new Throwable("request: " + requestName + "   \n  args: " + argsMap.toString() + "  \n response:" + new Gson().toJson(response)));
        }
        throw new GameException(ACTION_ABORT, key, gameType, null, null);
    }


    abstract class Task<T extends BaseResponse> {

        private T mResponse;

        protected abstract T doTask();

        public T execute(String gameKey, GameType gameType) throws GameException {

            mResponse = doTask();

            GameRequestError error = checkResponseEndThrowOnGameError(mResponse, gameKey, gameType);
            if (error == null) {
                if (mResponse instanceof GameGetQuestionsResponse)
                    checkQuestionsEndThrowOnGameError((GameGetQuestionsResponse) mResponse, gameKey, gameType);
                return mResponse;
            }

            if (error.getNextAction() == ACTION_RETRY) {
                try {
                    Thread.sleep(error.getTimeOut());
                } catch (InterruptedException ignore) {
                }
                return execute(gameKey, gameType);
            }

            throw new GameException(ACTION_ABORT, gameKey, gameType, "error code is " + error.getCode(), mResponse);
        }

    }


}
