package com.clickandgain.api.response.game;

import com.clickandgain.model.game.Question;
import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;

/**
 * Created by ovitali on 20.10.2015.
 */
public class GameGetQuestionsResponse extends BaseResponse {

    @SerializedName("questions")
    private Question[] mQuestions;

    public Question[] getQuestions() {
        return mQuestions;
    }
}
