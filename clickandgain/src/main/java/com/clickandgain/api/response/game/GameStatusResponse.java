package com.clickandgain.api.response.game;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 20.10.2015.
 */
public class GameStatusResponse {

    @SerializedName("balance")
    private float mBalanceChange;

    @SerializedName("count")
    private int mCount;

    @SerializedName("win")
    private int mWin;

    private int averageWinValue;

    public float getBalanceChange() {
        return mBalanceChange;
    }

    public int getCount() {
        return mCount;
    }

    public int getWin() {
        return mWin;
    }

    public int getAverageWinValue() {
        return averageWinValue;
    }
}
