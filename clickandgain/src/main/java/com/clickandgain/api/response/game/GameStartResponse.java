package com.clickandgain.api.response.game;

import com.humanet.humanetcore.api.response.BaseResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 20.10.2015.
 */
public class GameStartResponse extends BaseResponse {

    @SerializedName("game_key")
    String gameToken;

    public String getGameToken() {
        return gameToken;
    }
}
