package com.clickandgain.api.sets;

import com.clickandgain.api.response.game.GameEndResponse;
import com.clickandgain.api.response.game.GameGetQuestionResponse;
import com.clickandgain.api.response.game.GameGetQuestionsResponse;
import com.clickandgain.api.response.game.GameStartResponse;
import com.clickandgain.api.response.game.GameStatusResponse;
import com.clickandgain.model.game.PromoQuestionResponse;
import com.clickandgain.model.game.QuestionPickResponse;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by ovitali on 20.10.2015.
 */
public interface GameApiSet {

    @POST("/?games.start")
    GameStartResponse start(@Body ArgsMap  options);

    @POST("/?games.edit")
    BaseResponse edit(@Body ArgsMap  options);

    @POST("/?games.end")
    GameEndResponse end(@Body ArgsMap  options);

    @POST("/?games.status")
    GameStatusResponse status(@Body ArgsMap  options);

    @POST("/?questions.get")
    GameGetQuestionsResponse questionsGet(@Body ArgsMap  options);

    @POST("/?questions.pick")
    QuestionPickResponse questionPick(@Body ArgsMap  options);

    @POST("/?question.get")
    GameGetQuestionResponse questionGetPromo(@Body ArgsMap  options);

    @POST("/?question.pick")
    PromoQuestionResponse questionPickPromo(@Body ArgsMap options);
}
