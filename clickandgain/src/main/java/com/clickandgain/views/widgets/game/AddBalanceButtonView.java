package com.clickandgain.views.widgets.game;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.clickandgain.R;
import com.humanet.humanetcore.events.INavigatableActivity;

/**
 * Created by ovi on 4/11/16.
 */
public class AddBalanceButtonView extends TextView implements View.OnClickListener {

    public int mNavigateId;

    public AddBalanceButtonView(Context context) {
        this(context, null);
    }

    public AddBalanceButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AddBalanceButtonView);
        mNavigateId = a.getResourceId(R.styleable.AddBalanceButtonView_navigationId, R.id.currency_add);
        a.recycle();

        setText(R.string.game_balance_add);

        setPaintFlags(getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        ((INavigatableActivity) getContext()).onNavigateByViewId(mNavigateId);
    }
}
