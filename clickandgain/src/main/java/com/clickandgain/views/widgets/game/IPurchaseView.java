package com.clickandgain.views.widgets.game;

import org.json.JSONObject;

/**
 * Created by Uran on 09.06.2016.
 */
public interface IPurchaseView {

    void buyingDone(String code, JSONObject data, String signature);

}
