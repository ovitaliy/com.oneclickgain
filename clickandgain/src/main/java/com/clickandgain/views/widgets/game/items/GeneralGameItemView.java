package com.clickandgain.views.widgets.game.items;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.clickandgain.R;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.humanet.humanetcore.views.widgets.VideoSurfaceView;
import com.humanet.humanetcore.views.widgets.YellowProgressBar;

/**
 * Created by ovitali on 21.10.2015.
 */
public class GeneralGameItemView extends BaseCredoItem {

    private ItemView mItemView;

    public static GeneralGameItemView create(final Context context) {
        ItemView itemView = new ItemView(context);

        GeneralGameItemView credoLiveItemView = new GeneralGameItemView(itemView.mVideoSurfaceView, itemView.mProgressBar);
        credoLiveItemView.mItemView = itemView;
        itemView.mCredoLiveItemView = credoLiveItemView;

        return credoLiveItemView;
    }

    public ItemView getItemView() {
        return mItemView;
    }


    public GeneralGameItemView(final VideoSurfaceView videoSurfaceView, final YellowProgressBar progressBar) {
        super(videoSurfaceView, progressBar);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        mItemView.setOnClickListener(onClickListener);
    }

    public void setSelected(boolean selected) {
        mItemView.setSelected(selected);
    }

    public static class ItemView extends FrameLayout {
        private FrameLayout mSectorLayout;
        private VideoSurfaceView mVideoSurfaceView;
        private YellowProgressBar mProgressBar;
        private boolean mSelected;

        private GeneralGameItemView mCredoLiveItemView;

        public ItemView(final Context context) {
            super(context);

            mVideoSurfaceView = new VideoSurfaceView(context);
            addViewInLayout(mVideoSurfaceView, -1, new LayoutParams(-1, -1));

            mProgressBar = new YellowProgressBar(context);
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            layoutParams.gravity = Gravity.CENTER;
            addViewInLayout(mProgressBar, -1, layoutParams);
        }

        public GeneralGameItemView getCredoLiveItemView() {
            return mCredoLiveItemView;
        }

        @Override
        public void setSelected(boolean selected) {
            if (mSelected == selected)
                return;

            mSelected = selected;

            if (!mSelected && mSectorLayout != null) {
                removeView(mSectorLayout);
                mSectorLayout = null;
            } else if (mSelected && mSectorLayout == null) {
                mSectorLayout = new FrameLayout(getContext());

                ShapeDrawable shapeDrawable = new ShapeDrawable();
                Paint paint = shapeDrawable.getPaint();
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth(ConverterUtil.dpToPix(getResources(), 10));
                paint.setColor(Constants.GAME_ITEM_COLOR);

                mSectorLayout.setBackground(shapeDrawable);

                ImageView imageView = new ImageView(getContext());
                imageView.setImageResource(R.drawable.credo_btn_checked);
                int size = (int) ConverterUtil.dpToPix(getContext(), 30);
                LayoutParams layoutParams = new LayoutParams(size, size);
                layoutParams.gravity = Gravity.RIGHT;
                layoutParams.rightMargin = layoutParams.topMargin = (int) ConverterUtil.dpToPix(getContext(), 10);
                mSectorLayout.addView(imageView, layoutParams);

                addView(mSectorLayout);
            }
        }
    }
}
