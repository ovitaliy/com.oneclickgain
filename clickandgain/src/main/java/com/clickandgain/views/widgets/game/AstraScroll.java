package com.clickandgain.views.widgets.game;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clickandgain.R;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.views.utils.ConverterUtil;

/**
 * Created by ovitaliy on 04.09.2015.
 */
public class AstraScroll extends ViewGroup implements View.OnTouchListener {

    private TextView mStartValueView;
    private TextView mEndValueView;
    private TextView mMarkerView;
    private ProgressLineView mLineView;

    private int mMinX;
    private int mMaxX;
    private int mProgressHeight;

    private int mStartValue = Constants.ASRTA_GAME_QUESTION_MINIMUM;
    private int mLastValue = Constants.ASRTA_GAME_QUESTION_LIMIT;
    private int mCurrentValue = mStartValue;

    private int mShift;

    public AstraScroll(Context context) {
        super(context);
    }

    public AstraScroll(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOnTouchListener(this);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AstraScroll);
        mShift = a.getDimensionPixelSize(R.styleable.AstraScroll_shift, 0);
        a.recycle();

        mProgressHeight = (int) ConverterUtil.dpToPix(context, 5);

        mStartValueView = new TextView(context);
        mStartValueView.setGravity(Gravity.CENTER_HORIZONTAL);
        mStartValueView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.ft_normal));
        mStartValueView.setTextColor(getResources().getColor(R.color.light_gray1));
        addViewInLayout(mStartValueView, -1, generateDefaultLayoutParams());
        mStartValueView.setText(String.valueOf(mStartValue));

        mEndValueView = new TextView(context);
        mEndValueView.setGravity(Gravity.CENTER_HORIZONTAL);
        mEndValueView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.ft_normal));
        mEndValueView.setTextColor(getResources().getColor(R.color.light_gray1));
        mEndValueView.setText(String.valueOf(mLastValue));
        addView(mEndValueView, -1, generateDefaultLayoutParams());

        mMarkerView = new TextView(context);
        mMarkerView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.ft_mid));
        mMarkerView.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        mMarkerView.setBackgroundResource(R.drawable.progress_thumb_marker);
        mMarkerView.setTextColor(Color.WHITE);
        mMarkerView.setText(String.valueOf(mCurrentValue));
        LayoutParams layoutParams = new LayoutParams((int) ConverterUtil.dpToPix(context, 60), (int) ConverterUtil.dpToPix(context, 73));
        addView(mMarkerView, -1, layoutParams);

        mLineView = new ProgressLineView(context);
        addView(mLineView, -1, generateDefaultLayoutParams());
        mLineView.setWillNotDraw(false);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        mStartValueView.layout(mShift,
                b - mStartValueView.getMeasuredHeight() - t,
                mShift + mStartValueView.getMeasuredWidth(),
                b - t);
        mEndValueView.layout(
                r - l - mEndValueView.getMeasuredWidth() - mShift,
                b - mEndValueView.getMeasuredHeight() - t,
                r - l - mShift,
                b - t);

        int markerX = mMinX;

        mMarkerView.layout(
                markerX - mMarkerView.getMeasuredWidth() / 2,
                0,
                markerX + mMarkerView.getMeasuredWidth() / 2,
                mMarkerView.getMeasuredHeight());

        mLineView.layout(
                mMinX,
                b - t - mEndValueView.getMeasuredHeight() / 2 - mProgressHeight / 2,
                mMinX + mLineView.getMeasuredWidth(),
                b - t - mEndValueView.getMeasuredHeight() / 2 + mProgressHeight / 2
        );
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChild(mStartValueView, widthMeasureSpec, heightMeasureSpec);
        measureChild(mEndValueView, widthMeasureSpec, heightMeasureSpec);
        measureChild(mMarkerView, widthMeasureSpec, heightMeasureSpec);

        mMinX = mShift + (int) (mStartValueView.getMeasuredWidth() + ConverterUtil.dpToPix(getContext(), 10));
        int progressWidth = (int) (MeasureSpec.getSize(widthMeasureSpec) - mMinX - mEndValueView.getMeasuredWidth() - ConverterUtil.dpToPix(getContext(), 10)) - mShift;
        mMaxX = mMinX + progressWidth;

        mLineView.measure(
                MeasureSpec.makeMeasureSpec(progressWidth, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(mProgressHeight, MeasureSpec.EXACTLY));

        int height = mMarkerView.getMeasuredHeight() + mStartValueView.getMeasuredHeight();

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (MotionEvent.ACTION_UP == event.getAction() || MotionEvent.ACTION_CANCEL == event.getAction() || event.getAction() == MotionEvent.ACTION_MOVE) {
            float x = event.getX();
            x = Math.max(x, mMinX);
            x = Math.min(x, mMaxX);
            float itemXStep = mLineView.getMeasuredWidth() / (mLastValue - mStartValue);
            float delta = Math.round((x - mMinX) / itemXStep);

            if (event.getAction() != MotionEvent.ACTION_MOVE) {
                x = mMinX + delta * (itemXStep);
            }

            mCurrentValue = (int) delta + mStartValue;

            mMarkerView.setText(String.valueOf(mCurrentValue));
            movePointer(x);
            mLineView.setValueX(x - mMinX);
            mLineView.invalidate();
        }

        return true;
    }

    private void movePointer(float x) {
        mMarkerView.setX(x - mMarkerView.getMeasuredWidth() / 2);
    }

    public void setValue(int value) {
        mCurrentValue = value;
    }

    private class ProgressLineView extends View {
        private final Paint mGrayPaint;
        private final Paint mCirclePaint;
        private final Paint mSelectedPaint;

        private float mX = 0;

        public ProgressLineView(Context context) {
            super(context);
            mGrayPaint = new Paint();
            mGrayPaint.setStyle(Paint.Style.FILL);
            mGrayPaint.setColor(Color.LTGRAY);

            mSelectedPaint = new Paint();
            mSelectedPaint.setStyle(Paint.Style.FILL);
            mSelectedPaint.setColor(context.getResources().getColor(R.color.blue));

            mCirclePaint = new Paint();
            mCirclePaint.setStyle(Paint.Style.FILL);
            mCirclePaint.setColor(Color.BLACK);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            float height = canvas.getHeight();
            float width = canvas.getWidth() - height;

            float dotRadius = height / 2;

            canvas.drawRect(dotRadius, 0, width + dotRadius, height, mGrayPaint);

            float right = Math.min(mX, width + dotRadius);
            canvas.drawRect(dotRadius, 0, right, height, mSelectedPaint);

            float steps = mLastValue - mStartValue;
            float itemWidth = width / steps;

            for (int i = 0; i < steps + 1; i++) {
                float x = itemWidth * i + dotRadius;
                canvas.drawCircle(x, dotRadius, dotRadius, mCirclePaint);
            }

            super.onDraw(canvas);
        }

        public void setValueX(float x) {
            mX = x;
            invalidate();
        }
    }

    public int getValue() {
        return mCurrentValue;
    }


}
