package com.clickandgain.views.widgets.game;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.clickandgain.R;

/**
 * Created by ovi on 3/17/16.
 */
public class SportCategoryItemView extends ImageView {

    private Drawable mSelectedDrawable;
    private Drawable mNotSelectedDrawable;

    public SportCategoryItemView(Context context) {
        this(context, null);
    }

    public SportCategoryItemView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setScaleType(ScaleType.FIT_CENTER);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SportCategoryItemView);
        mSelectedDrawable = a.getDrawable(R.styleable.SportCategoryItemView_selectedSrc);
        a.recycle();


    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        mNotSelectedDrawable = drawable;

        super.setImageDrawable(drawable);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);

        super.setImageDrawable(isSelected() ? mSelectedDrawable : mNotSelectedDrawable);
    }



}
