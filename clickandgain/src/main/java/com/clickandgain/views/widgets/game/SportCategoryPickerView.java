package com.clickandgain.views.widgets.game;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.clickandgain.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by ovi on 3/17/16.
 */
public class SportCategoryPickerView extends LinearLayout implements View.OnClickListener {

    static final String FOOTBALL = "football";
    private static final String BASKETBALL = "basketball";
    private static final String HOCKEY = "hockey";
    private static final String MIX = "mix";


    @SportType
    public static String getDefaultCategory() {
        return FOOTBALL;
    }


    @Retention(RetentionPolicy.SOURCE)
    @StringDef({FOOTBALL, BASKETBALL, HOCKEY, MIX})
    public @interface SportType {
    }


    private SportCategoryItemView mCurrentSelected;

    public SportCategoryPickerView(Context context) {
        this(context, null);
    }

    public SportCategoryPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setGravity(Gravity.CENTER);
        setOrientation(VERTICAL);

        inflate(context, R.layout.layout_sport_category_picker, this);

        setOnClickOnAllButtons(this);

        findViewWithTag(getDefaultCategory()).callOnClick();
    }

    @Override
    public void onClick(View v) {
        if (mCurrentSelected != null)
            mCurrentSelected.setSelected(false);

        mCurrentSelected = (SportCategoryItemView) v;
        mCurrentSelected.setSelected(true);
    }

    private void setOnClickOnAllButtons(ViewGroup parent) {
        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);
            if (child instanceof ViewGroup)
                setOnClickOnAllButtons((ViewGroup) child);
            else if (child instanceof SportCategoryItemView)
                child.setOnClickListener(this);
        }
    }

    @NonNull
    @SportType
    public String getSelectedCategory() {
        String type = (String) mCurrentSelected.getTag();
        switch (type) {
            case FOOTBALL:
                return FOOTBALL;

            case BASKETBALL:
                return BASKETBALL;

            case HOCKEY:
                return HOCKEY;

            case MIX:
                return MIX;

            default:
                throw new RuntimeException("unrecognized sport type: " + type);
        }
    }


}
