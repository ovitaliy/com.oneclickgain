package com.clickandgain.service;

import android.os.Environment;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.utils.GsonHelper;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import com.squareup.okhttp.OkHttpClient;

import org.apache.commons.io.IOUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;

import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.OkClient;
import retrofit.converter.ConversionException;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedInput;
import roboguice.util.temp.Ln;

/**
 * Created by ovi on 3/22/16.
 */
public class GameApiService extends RetrofitGsonSpiceService {

    @Override
    public int getThreadCount() {
        return 1;
    }

    @Override
    protected String getServerUrl() {
        return Constants.GAMES_API_URL;
    }

    @Override
    protected Converter createConverter() {
        return new Converter();
    }

    protected RestAdapter.Builder createRestAdapterBuilder() {

        Ln.getConfig().setLoggingLevel(Log.ERROR);

        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new AndroidLog("GAME_API"))
                .setClient(new OkClient(new OkHttpClient()))
                .setEndpoint(getServerUrl())
                .setConverter(getConverter());
    }

    static class Converter extends GsonConverter {

        public Converter() {
            super(GsonHelper.getGson());
        }

        @Override
        public Object fromBody(TypedInput body, Type type) throws ConversionException {
            String data = null;

            try {
                data = IOUtils.toString(body.in());

               // Log.i("Response", data);
                JsonElement jsonElement = new JsonParser().parse(data);
                if (jsonElement instanceof JsonObject) {
                    JsonObject jsonObject = (JsonObject) jsonElement;
                    if (jsonObject.has("response")) {
                        jsonElement = jsonObject.get("response");
                    }
                }
                return GsonHelper.getGson().fromJson(jsonElement, type);
            } catch (Exception ex) {
                if (!App.DEBUG) {
                    if (data != null)
                        Crashlytics.logException(new Throwable(data));

                    Crashlytics.getInstance().core.logException(ex);
                }


                throw new ConversionException(ex);
            } finally {
                try {
                    body.in().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void log(String log) {
        PrintWriter out = null;
        try {
            File logFile = new File(Environment.getExternalStorageDirectory(), "game_api_request_log.txt");
            if (logFile.exists() || logFile.createNewFile()) {
                out = new PrintWriter(new BufferedWriter(new FileWriter(logFile, true)));
                out.println(log);
            } else {
                throw new RuntimeException("Log file cannot be created");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }


    }

}
