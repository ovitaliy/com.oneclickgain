package com.clickandgain.jobs;

import android.content.ContentResolver;
import android.database.Cursor;

import com.clickandgain.model.game.QuestionMedia;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.utils.FilePathHelper;

import java.io.File;
import java.util.LinkedList;

/**
 * Created by ovi on 1/16/16.
 */
public class ClearGameCacheJob extends Thread {

    public static void start(String gameToken) {
        new ClearGameCacheJob(gameToken).start();
    }

    private String mGameToken;

    private ClearGameCacheJob(String gameToken) {
        mGameToken = gameToken;
    }

    @Override
    public void run() {

        //  flag to delete/or not delete cached videos - they can be a part of other question
        boolean toRemoveFiles = true;

        ContentResolver contentResolver = App.getInstance().getContentResolver();

        String where = String.format("%s ='%s'", ContentDescriptor.QuestionMedia.Cols.GAME_KEY, mGameToken);

        if (toRemoveFiles) {
            Cursor cursor = contentResolver.query(ContentDescriptor.QuestionMedia.URI, null, where, null, null);

            final LinkedList<String> medias = new LinkedList<>();

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    QuestionMedia questionMedia = QuestionMedia.fromCursor(cursor);
                    medias.add(questionMedia.getMedia());
                    if (questionMedia.getUrlAnswer() != null) {
                        medias.add(questionMedia.getUrlAnswer());
                    }
                } while (cursor.moveToNext());

                cursor.close();
            }


            while (medias.size() > 0) {
                String videoName = FilePathHelper.getVideoFileName(medias.removeFirst());
                File destinationFile = new File(FilePathHelper.getVideoCacheDirectory(), videoName);

                cursor = contentResolver.query(ContentDescriptor.QuestionMedia.URI, null, ContentDescriptor.QuestionMedia.Cols.MEDIA + " LIKE '%" + videoName + "%'",
                        null, null);

                if ((cursor == null || cursor.getCount() <= 1) && destinationFile.exists())
                    destinationFile.delete();

                if (cursor != null)
                    cursor.close();
            }
        }

        contentResolver.delete(ContentDescriptor.Question.URI, where, null);
        contentResolver.delete(ContentDescriptor.QuestionMedia.URI, where, null);
    }
}
