package com.clickandgain.fragments.games;

import android.os.Bundle;

import com.clickandgain.modules.games.views.GameResultView;
import com.humanet.humanetcore.fragments.base.BaseSpiceFragment;
import com.humanet.humanetcore.modules.media.MediaFacade;

/**
 * Created by ovitali on 18.11.2015.
 */
public abstract class BaseGameVideoScreenFragment<T extends MediaFacade> extends BaseSpiceFragment
        implements GameResultView {

    protected String localVideoUrl;
    protected T mediaFacade;


    @Override
    public void onStop() {
        mediaFacade.closeMediaPlayer();
        unbindFromPresenter();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        bindToPresenter();
    }

    protected void resumePlaying() {
        if (localVideoUrl != null && getView() != null) {
            getView().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isVisible())
                        mediaFacade.play(localVideoUrl);
                }
            }, 100);
        }
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        if (localVideoUrl != null)
            outState.putString("videoUrl", localVideoUrl);
        super.onSaveInstanceState(outState);
    }
}
