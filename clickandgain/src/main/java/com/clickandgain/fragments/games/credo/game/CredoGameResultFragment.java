package com.clickandgain.fragments.games.credo.game;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clickandgain.ClickandgainApp;
import com.clickandgain.R;
import com.clickandgain.activities.games.CredoActivity;
import com.clickandgain.api.GameRequestErrorController;
import com.clickandgain.fragments.games.BaseGameVideoScreenFragment;
import com.clickandgain.model.game.QuestionPickResponse;
import com.clickandgain.modules.games.presenters.CredoGamePresenter;
import com.clickandgain.modules.games.presenters.PresenterHelper;
import com.clickandgain.modules.games.viewController.CredoGameResultViewController;
import com.clickandgain.modules.media.AstraMediaFacade;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.GameBalanceValue;
import com.humanet.humanetcore.api.listeners.SimplePendingRequestListener;
import com.humanet.humanetcore.api.requests.video.VideoPreloadRequest;
import com.humanet.humanetcore.model.enums.GameType;
import com.humanet.humanetcore.modules.media.IMediaEventListener;
import com.humanet.humanetcore.modules.media.MediaFacade;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.octo.android.robospice.request.listener.RequestProgressListener;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by ovitali on 10.09.2015.
 */
public class CredoGameResultFragment extends BaseGameVideoScreenFragment<AstraMediaFacade>
        implements View.OnClickListener, IMediaEventListener {

    public static CredoGameResultFragment newInstance(QuestionPickResponse questionPickResponse, GameType gameType) {
        CredoGameResultFragment fragment;
        switch (gameType) {
            case CREDO:
            case GENERAL_GAME:
                fragment = new CredoGameResultFragment();
                break;
            case ELSE:
                fragment = new ElseGameResultFragment();
                break;
            default:
                throw new IllegalArgumentException("Unexpected gameType");
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable("response", questionPickResponse);
        bundle.putSerializable("gameType", gameType);
        bundle.putFloat("balance", GameBalanceValue.get());
        bundle.putFloat("balanceChange", GameBalanceValue.get() + questionPickResponse.getChange());
        fragment.setArguments(bundle);

        return fragment;
    }

    public static final int LOADING = 0;
    public static final int HIDDEN_ANSWER = 1;
    public static final int PLAYING = 2;
    public static final int COMPLETE = 3;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({LOADING, HIDDEN_ANSWER, PLAYING, COMPLETE})
    public @interface Status {
    }

    protected float mBalance = 0;

    @Status
    protected int mCurrentState = LOADING;

    private int mPlayingPosition = Integer.MIN_VALUE;

    protected GameType mGameType;

    protected VideoPreloadRequest mVideoPreloadRequest;

    protected QuestionPickResponse mQuestionPickResponse;

    protected boolean isRotatable() {
        return true;
    }

    protected CredoGameResultViewController createViewController(View view) {
        return new CredoGameResultViewController(this, view);
    }

    @Override
    public void bindToPresenter() {
        getPresenter().setGameResultView(this);
    }

    @Override
    public void unbindFromPresenter() {
        getPresenter().setGameResultView(null);
    }

    protected CredoGamePresenter getPresenter() {
        return PresenterHelper.getPresenter(mGameType);
    }

    protected CredoGameResultViewController viewController;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mQuestionPickResponse = (QuestionPickResponse) getArguments().getSerializable("response");

        assert mQuestionPickResponse != null;

        mBalance = mQuestionPickResponse.getBalance();
        mGameType = (GameType) getArguments().getSerializable("gameType");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_credo_result, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewController = createViewController(view);
        viewController.setBalance(mQuestionPickResponse.getChange());
        viewController.setBalanceValues(
                getArguments().getFloat("balance"),
                getArguments().getFloat("balanceChange")
        );
        viewController.setMultiplier(getActivityIntExtraValue(CredoActivity.MULTIPLIER), getActivityIntExtraValue(CredoActivity.RATE));

        mediaFacade = new AstraMediaFacade(viewController.getSurfaceView());
        mediaFacade.setMediaEventListener(this);


        if (savedInstanceState != null) {
            //noinspection ResourceType
            mCurrentState = savedInstanceState.getInt("state");

            mPlayingPosition = savedInstanceState.getInt("playingPosition");
            localVideoUrl = savedInstanceState.getString("videoUrl");
        } else {
            mCurrentState = LOADING;
            viewController.manageVisualState(mCurrentState);
            getPresenter().cancelCurrentCacheRequest();
        }

        viewController.manageVisualState(mCurrentState);

    }

    @Override
    public void onResume() {
        super.onResume();

        if (isRotatable())
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

        viewController.getSurfaceView().setVisibility(View.VISIBLE);

        resumePlaying();

        if (mCurrentState == LOADING && localVideoUrl == null) {
            startVideoPreload();
        }
    }

    @Override
    public void onStop() {
        mediaFacade.closeMediaPlayer();
        viewController.getSurfaceView().setVisibility(View.GONE);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mVideoPreloadRequest != null) {
            ClickandgainApp.getVideoCacheSpiceManager().cancel(mVideoPreloadRequest);
            mVideoPreloadRequest = null;
        }
    }

    @Override
    public void onClick(View v) {
        mediaFacade.closeMediaPlayer();
        switch (v.getId()) {
            case R.id.btn_next:
                nextGame();
                v.setOnClickListener(null);
                break;

            case R.id.btn_back:
                endGame(true);
                v.setOnClickListener(null);
                break;

            case R.id.play_button:
                mCurrentState = PLAYING;
                viewController.manageVisualState(mCurrentState);
                mPlayingPosition = Integer.MIN_VALUE;
                mediaFacade.replay();
                break;
        }
    }

    /**
     * redirect to next question if it exists.
     * If no more question, calls end game to exit
     */
    protected void nextGame() {
        if (getPresenter().getQuestion() != null && GameBalanceValue.get() - getActivityIntExtraValue("rate") >= 0) {
            getPresenter().checkIfVideoDownloaded();
            ((CredoActivity) getActivity()).newQuestion(mBalance);
        } else {
            endGame(true);
        }
    }

    @Override
    public void gameStopped(@GameRequestErrorController.OnErrorAction int action) {
        ((CredoActivity) getActivity()).obtainServerError(getPresenter(), action);
    }

    protected void endGame(boolean isRequestMustBePostedToServer) {
        getPresenter().endGame(isRequestMustBePostedToServer);
        getActivity().finish();
    }

    @Override
    public void onVideoPlayingStarted(final MediaFacade mediaFacade, final MediaPlayer mediaPlayer) {
        if (mPlayingPosition != Integer.MIN_VALUE) {
            mediaPlayer.seekTo(mPlayingPosition);
        }
        if (mCurrentState == COMPLETE) {
            mediaPlayer.seekTo(mediaPlayer.getDuration());
            mediaPlayer.pause();
        }

        if (mCurrentState == HIDDEN_ANSWER) {
            int length = mediaPlayer.getDuration();
            int delay = Math.min(length / 3 * 2, Constants.ANSWER_DELAY);
            if (mPlayingPosition != Integer.MIN_VALUE)
                delay -= mPlayingPosition;

            if (getView() != null) {
                getView().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (isVisible()) {
                            mCurrentState = PLAYING;
                            viewController.manageVisualState(mCurrentState);
                        }

                    }
                }, delay);
            }
        }

        mPlayingPosition = Integer.MIN_VALUE;
    }

    @Override
    public void onVideoPlayingCompleted(final MediaFacade mediaFacade, final MediaPlayer mediaPlayer) {
        mCurrentState = COMPLETE;
        viewController.manageVisualState(mCurrentState);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        outState.putInt("playingPosition", mediaFacade != null ? mediaFacade.getPosition() : Integer.MIN_VALUE);
        outState.putInt("state", mCurrentState);

        super.onSaveInstanceState(outState);
    }


    @Override
    public void setResult(final int result) {

    }

    /**
     * start downloading video
     */
    protected void startVideoPreload() {
        ClickandgainApp.getVideoCacheSpiceManager().addListenerIfPending(String[].class, "game_video", new VideoPreloadingListener());
    }


    private void runPreloader(String videoUrl) {
        mVideoPreloadRequest = new VideoPreloadRequest(videoUrl);
        ClickandgainApp.getVideoCacheSpiceManager().execute(new VideoPreloadRequest(videoUrl), "game_video", DurationInMillis.ONE_SECOND, new VideoPreloadingListener());
        mCurrentState = LOADING;
        viewController.manageVisualState(mCurrentState);
    }


    //--- inner class --
    private class VideoPreloadingListener extends SimplePendingRequestListener<String[]> implements RequestProgressListener {
        @Override
        public void onRequestSuccess(String[] s) {
            localVideoUrl = s[0];
            resumePlaying();

            getPresenter().checkIfVideoDownloaded();
            mCurrentState = HIDDEN_ANSWER;
            viewController.manageVisualState(mCurrentState);

            mVideoPreloadRequest = null;
        }

        @Override
        public void onRequestNotFound() {
            runPreloader(mQuestionPickResponse.getMedia());
        }

        @Override
        public void onRequestProgressUpdate(RequestProgress progress) {
            viewController.setProgress((int) progress.getProgress());
        }
    }

    protected final int getActivityIntExtraValue(String key) {
        return getActivity().getIntent().getExtras().getInt(key);
    }


}
