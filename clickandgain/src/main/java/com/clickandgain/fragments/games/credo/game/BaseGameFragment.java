package com.clickandgain.fragments.games.credo.game;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.clickandgain.BuildConfig;
import com.clickandgain.R;
import com.clickandgain.activities.games.CredoActivity;
import com.clickandgain.api.GameRequestErrorController;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.model.game.QuestionPickResponse;
import com.clickandgain.modules.games.events.VideoLoadingListener;
import com.clickandgain.modules.games.presenters.CredoGamePresenter;
import com.clickandgain.modules.games.presenters.PresenterHelper;
import com.clickandgain.modules.games.views.GameView;
import com.clickandgain.modules.games.views.StoppableGameView;
import com.clickandgain.views.widgets.game.items.BaseCredoItem;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.GameBalanceValue;
import com.humanet.humanetcore.fragments.base.BaseSpiceFragment;
import com.humanet.humanetcore.model.enums.GameType;

abstract class BaseGameFragment<T extends BaseCredoItem> extends BaseSpiceFragment implements
        View.OnClickListener,
        IChangeStateBehaviour,
        VideoLoadingListener,
        StoppableGameView,
        GameView {

    public static final int STATE_LOADING = 1;
    public static final int STATE_PLAYING = 2;

    private T[] mItems;

    protected final CredoGamePresenter getPresenter() {
        return PresenterHelper.getPresenter(getGameType());
    }

    protected abstract GameType getGameType();

    protected abstract T[] showQuestion(View view, Question questions);

    protected abstract View createView(LayoutInflater inflater, ViewGroup container);

    private View mNextButton;
    private View mBackButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getPresenter().getGameQuestions() == null || getPresenter().getGameQuestions().length == 0) {
            getActivity().onBackPressed();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = createView(inflater, container);

        mNextButton = view.findViewById(R.id.btn_next);
        mNextButton.setOnClickListener(this);

        mBackButton = view.findViewById(R.id.btn_back);
        mBackButton.setOnClickListener(this);

        mItems = showQuestion(view, getPresenter().getQuestion());
        if (BuildConfig.DEBUG) {
            QuestionMedia[] questionMedias = getPresenter().getQuestion().getQuestionMedias();
            int index = 0;
            for (int i = 0; i < questionMedias.length; i++) {
                if (questionMedias[i].getMedia().contains("Y")) {
                    index = i;
                    break;
                }
            }

            Toast.makeText(getActivity(), String.valueOf(index), Toast.LENGTH_SHORT).show();
        }

        getPresenter().questionWasOpened();

        if (getPresenter().getNotAnsweredQuestionsCount() <= 1)
            getPresenter().loadNewQuestionsPart();

        return view;
    }

    @Override
    public void onPause() {
        stopMedia();
        unbindFromPresenter();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        bindToPresenter();
        resumeMedia();
    }

    protected void setLocalUrlsToItems() {
        for (int i = 0; i < mItems.length; i++)
            mItems[i].setLocalVideoUrl(getPresenter().getVideoUrl(i));

        manageState(STATE_PLAYING);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                endGame();
                break;
            case R.id.btn_next:
                next(null);
                break;
        }
    }

    protected void endGame() {
        disableClicks();

        getPresenter().endGame();
        getActivity().finish();
    }


    protected void next(QuestionMedia questionMedia) {
        disableClicks();

        int mediaId;
        int answer = 1;

        if (questionMedia != null) {
            mediaId = questionMedia.getId();
        } else {
            answer = -1;
            mediaId = getPresenter().getQuestion().getQuestionMedias()[0].getId();
        }

        pauseMedia();

        getLoader().show();

        getPresenter().setResult(answer, mediaId);
        getPresenter().removeLastQuestion();


        // be sure we will not receive caching events
        getPresenter().setVideoLoadingListener(null);
    }

    @Override
    public void publishQuestionResponse(final QuestionPickResponse response) {
        getLoader().dismiss();
        ((CredoActivity) getActivity()).startFragment(CredoGameResultFragment.newInstance(response, getGameType()), false);

        AppUser.getInstance().get().setGameBalanceValue(GameBalanceValue.get() + response.getChange());
        GameBalanceValue.getInstance().setBalance(GameBalanceValue.get() + response.getChange());
    }

    @Override
    public void unbindFromPresenter() {
        getPresenter().setGameView(null);
        getPresenter().setVideoLoadingListener(null);
    }

    @Override
    public void bindToPresenter() {
        getPresenter().setGameView(this);


        if (getPresenter().getQuestion() != null && !getPresenter().getQuestion().isDownloaded()) {
            getPresenter().setVideoLoadingListener(this);
            manageState(STATE_LOADING);
        } else {
            setLocalUrlsToItems();
        }
    }


    @Override
    public void gameStopped(@GameRequestErrorController.OnErrorAction int action) {
        ((CredoActivity) getActivity()).obtainServerError(getPresenter(), action);
    }

    private void disableClicks() {
        mBackButton.setOnClickListener(null);
        mNextButton.setOnClickListener(null);

        removeItemsClickListeners();
    }

    public T[] getItems() {
        return mItems;
    }

    protected abstract void removeItemsClickListeners();
}
