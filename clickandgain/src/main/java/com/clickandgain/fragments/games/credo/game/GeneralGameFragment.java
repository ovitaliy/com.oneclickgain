package com.clickandgain.fragments.games.credo.game;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.clickandgain.R;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.views.widgets.game.items.GeneralGameItemView;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.model.enums.GameType;
import com.humanet.humanetcore.views.utils.ConverterUtil;

// Квадратики
public class GeneralGameFragment extends MultiVideoGameFragment<GeneralGameItemView>
        implements View.OnClickListener {


    private GeneralGameItemView mCheckedItem;

    @Override
    protected GameType getGameType() {
        return GameType.GENERAL_GAME;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        return inflater.inflate(R.layout.fragment_credo_base_game, container, false);
    }

    protected GeneralGameItemView[] showQuestion(View view, final Question questions) {
        Context context = view.getContext();

        QuestionMedia[] medias = questions.getQuestionMedias();

        GeneralGameItemView[] allItems = new GeneralGameItemView[medias.length];

        final TextView questionView = (TextView) view.findViewById(R.id.question);
        questionView.setVisibility(View.GONE);
        //questionView.setText(R.string.game_general_game_question);

        ((LinearLayout.LayoutParams) questionView.getLayoutParams()).setMargins(0, 0, 0, (int) ConverterUtil.dpToPix(getResources(), 10));

        TableLayout tableLayout = new TableLayout(context);

        int rows = (int) Math.ceil(medias.length / 2d);
        int index = 0;

        int itemWidth = App.WIDTH / 2;
        int itemHeight = (int) ((float) itemWidth / 854f * 480f);

        for (int i = 0; i < rows; i++) {
            TableRow tableRow = new TableRow(context);
            for (int j = 0; j < 2; j++) {
                if (index == medias.length)
                    break;

                GeneralGameItemView itemView = GeneralGameItemView.create(context);
                itemView.setQuestionMedia(medias[index]);
                tableRow.addView(itemView.getItemView(), itemWidth, itemHeight);

                allItems[index] = itemView;

                itemView.setOnClickListener(this);

                index++;
            }
            if (tableRow.getChildCount() > 0)
                tableLayout.addView(tableRow);
        }
        final LinearLayout container = (LinearLayout) view.findViewById(R.id.container);
        container.addView(tableLayout, 1);

        return allItems;
    }

    @Override
    public void onClick(View v) {
        if (v instanceof GeneralGameItemView.ItemView) {
            if (mCheckedItem != null)
                mCheckedItem.setSelected(false);
            mCheckedItem = ((GeneralGameItemView.ItemView) v).getCredoLiveItemView();
            mCheckedItem.setSelected(true);

            next(mCheckedItem.getQuestionMedia());
        } else {
            super.onClick(v);
        }
    }

    @Override
    public void stopMedia() {
        if (getItems() != null) {
            synchronized (getItems()) {
                for (GeneralGameItemView credoLiveItemView : getItems()) {
                    credoLiveItemView.stop();
                }
            }
        }
    }

    public void pauseMedia() {
        if (getItems() != null) {
            synchronized (getItems()) {
                for (GeneralGameItemView itemView : getItems()) {
                    itemView.pause();
                }
            }
        }
    }

    @Override
    public void resumeMedia() {
        if (getItems() != null) {
            synchronized (getItems()) {
                for (GeneralGameItemView itemView : getItems()) {
                    itemView.resume();
                }
            }
        }
    }

    @Override
    protected void removeItemsClickListeners() {
        synchronized (getItems()) {
            for (GeneralGameItemView itemView : getItems()) {
                itemView.setOnClickListener(null);
            }
        }
    }
}
