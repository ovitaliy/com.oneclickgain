package com.clickandgain.fragments.games.credo.tutorials;

import android.view.View;

import com.clickandgain.R;
import com.clickandgain.views.widgets.game.SportCategoryPickerView;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.model.enums.GameType;

/**
 * Created by ovitali on 09.09.2015.
 */
public class CredoTutorialFragment extends BaseCredoTutorialFragment implements IMenuBindFragment {

    public static CredoTutorialFragment newInstance() {
        return new CredoTutorialFragment();
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_credo);
    }

    @Override
    protected GameType getGameType() {
        return GameType.CREDO;
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_credo_tutorial;
    }

    @Override
    public int getSelectedMenuItem() {
        return R.id.credo;
    }

    @Override
    protected String getSportCategory() {
        View view = getView();
        assert view != null;

        return ((SportCategoryPickerView) view.findViewById(R.id.sportCategoryPicker)).getSelectedCategory();
    }


}
