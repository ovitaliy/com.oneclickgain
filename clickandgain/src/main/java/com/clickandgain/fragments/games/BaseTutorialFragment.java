package com.clickandgain.fragments.games;

import android.widget.TextView;

import com.clickandgain.R;
import com.clickandgain.modules.games.views.StatusView;
import com.humanet.humanetcore.GameBalanceValue;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.modules.BalanceUtil;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by ovitali on 03.12.2015.
 */
public abstract class BaseTutorialFragment extends BaseTitledFragment implements StatusView, Observer {

    @Override
    public void onResume() {
        super.onResume();
        bindToPresenter();

        GameBalanceValue.getInstance().addObserver(this);
        setBalance(GameBalanceValue.get());

    }

    @Override
    public void onPause() {
        super.onPause();
        unbindFromPresenter();
        GameBalanceValue.getInstance().deleteObserver(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        setBalance((Float) data);
    }

    public void setBalance(float balance) {
        if (getView() != null) {
            ((TextView) getView().findViewById(R.id.currentBalance)).setText(getString(R.string.game_game_balance, BalanceUtil.balanceToString(balance)));
        }
    }
}
