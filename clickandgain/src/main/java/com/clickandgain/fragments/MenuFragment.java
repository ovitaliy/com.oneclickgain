package com.clickandgain.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clickandgain.ClickandgainApp;
import com.clickandgain.R;
import com.clickandgain.activities.SplashActivity;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.GameBalanceValue;
import com.humanet.humanetcore.fragments.BaseMenuFragment;
import com.humanet.humanetcore.model.UserInfo;


/**
 * Created by ovi on 1/25/16.
 */
public class MenuFragment extends BaseMenuFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);


        view.findViewById(R.id.logout).setOnClickListener(this);

        view.findViewById(R.id.astra).setOnClickListener(this);
        view.findViewById(R.id.credo).setOnClickListener(this);
        view.findViewById(R.id.vistory).setOnClickListener(this);
        view.findViewById(R.id.general_game).setOnClickListener(this);
        view.findViewById(R.id.else_game).setOnClickListener(this);
        view.findViewById(R.id.balance).setOnClickListener(this);
        view.findViewById(R.id.navigation_info).setOnClickListener(this);


        mBalanceTextView = (TextView) view.findViewById(R.id.user_balance);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logout:
                logout();
                break;

            case R.id.avatar:
            case R.id.first_name:
                mDrawerLayout.closeDrawers();
                mOnViewProfileListener.onViewProfile(AppUser.getInstance().getUid());
                break;
            case R.id.balance:
            case R.id.navigation_info:
            case R.id.vistory:
            case R.id.astra:
            case R.id.credo:
            case R.id.general_game:
            case R.id.else_game:
                mOnNavigateListener.onNavigateByViewId(v.getId());
                mDrawerLayout.closeDrawers();

                break;
        }
    }

    @Override
    protected void fillUserInfo(UserInfo userInfo) {
        super.fillUserInfo(userInfo);

        GameBalanceValue.getInstance().setBalance(userInfo.getGameBalanceValue());
    }

    @Override
    protected Class newSplashActivityInstance() {
        return SplashActivity.class;
    }


    @Override
    protected void logout() {
        ClickandgainApp.getGameSpiceManager().cancelAllRequests();

        super.logout();
    }
}
