package com.clickandgain.activities.games;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.clickandgain.R;
import com.clickandgain.api.GameRequestErrorController;
import com.clickandgain.fragments.games.credo.game.CredoGameFragment;
import com.clickandgain.modules.games.GameDataController;
import com.clickandgain.modules.games.events.QuestionReadyListener;
import com.clickandgain.modules.games.presenters.CredoGamePresenter;
import com.clickandgain.modules.games.presenters.PresenterHelper;
import com.clickandgain.views.widgets.game.SportCategoryPickerView;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.fragments.base.BaseSpiceFragment;
import com.humanet.humanetcore.model.enums.GameType;
import com.humanet.humanetcore.utils.AnalyticsHelper;

/**
 * Created by ovitali on 09.09.2015.
 */
public class CredoActivity extends BaseActivity {

    public static final String MULTIPLIER = "multiplier";
    public static final String RATE = "rate";
    public static final String GAME_TYPE = "game_type";
    public static final String BALANCE = "balance";
    public static final String SPORT = "sport";

    public static void startNewInstance(Context context, int multiplier, GameType gameType, float balance, int rate, String sport) {
        Intent intent = new Intent(context, CredoActivity.class);
        intent.putExtra(MULTIPLIER, multiplier);
        intent.putExtra(RATE, rate);
        intent.putExtra(GAME_TYPE, gameType);
        intent.putExtra(BALANCE, balance);
        intent.putExtra(SPORT, sport);

        context.startActivity(intent);
    }

    private float mBalance = 0;

    private CredoGamePresenter mCredoGamePresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        track();

        setContentView(R.layout.activity_base_fragment_no_toolbar_container);

        if (savedInstanceState == null) {
            mBalance = getIntent().getFloatExtra(BALANCE, 0);
            newQuestion(mBalance);
        }

    }

    @Override
    public void onBackPressed() {
        GameType gameType = (GameType) getIntent().getSerializableExtra(GAME_TYPE);
        PresenterHelper.getPresenter(gameType).endGame();
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mCredoGamePresenter != null)
            mCredoGamePresenter.setQuestionReadyListener(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        newQuestion(mBalance);
    }

    public void newQuestion(float balance) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        GameType gameType = (GameType) getIntent().getSerializableExtra(GAME_TYPE);
        BaseSpiceFragment credoGameFragment = GameDataController.newInstance(
                gameType,
                balance
        );
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_NONE);
        ft.replace(R.id.container, credoGameFragment, CredoGameFragment.class.getSimpleName()).commit();
    }

    private void track() {
        GameType gameType = (GameType) getIntent().getSerializableExtra(GAME_TYPE);
        switch (gameType) {
            case CREDO:
                AnalyticsHelper.trackEvent(this, AnalyticsHelper.GAME_CREDO);
                break;
            case ELSE:
                AnalyticsHelper.trackEvent(this, AnalyticsHelper.GAME_ELSE);
                break;
            case GENERAL_GAME:
                AnalyticsHelper.trackEvent(this, AnalyticsHelper.GAME_GENERAL_GAME);
                break;
        }
    }

    public void obtainServerError(final CredoGamePresenter presenter, final @GameRequestErrorController.OnErrorAction int action) {
        mCredoGamePresenter = presenter;

        if (action == GameRequestErrorController.ACTION_ABORT || action == GameRequestErrorController.ACTION_NO_INTERNET) {
            finish();
            if (action == GameRequestErrorController.ACTION_NO_INTERNET)
                Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            final int multiplier = getIntent().getIntExtra(MULTIPLIER, 2);
            final int rate = getIntent().getIntExtra(RATE, 2);
            @SportCategoryPickerView.SportType final String sport = getIntent().getStringExtra(SPORT);

            final GameType gameType = (GameType) getIntent().getSerializableExtra(GAME_TYPE);
            presenter.setQuestionReadyListener(new QuestionReadyListener() {
                @Override
                public void questionsAreReady(boolean isSuccess) {

                    CredoActivity.startNewInstance(
                            CredoActivity.this,
                            multiplier,
                            gameType,
                            getIntent().getFloatExtra(BALANCE, 0),
                            rate,
                            sport);
                    finish();
                    presenter.setQuestionReadyListener(null);
                }
            });
            presenter.restartGame(multiplier, rate, sport);
        }
    }


}
