package com.clickandgain.activities;

import android.os.Bundle;

import com.clickandgain.R;
import com.clickandgain.fragments.games.promo.GuessFragment;
import com.clickandgain.fragments.registration.OkFragment;
import com.clickandgain.fragments.registration.VerificationFragment;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.activities.BaseRegistrationActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.database.GetCountryRequest;
import com.humanet.humanetcore.api.requests.user.AuthRequest;
import com.humanet.humanetcore.api.requests.user.EditUserInfoRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.response.user.AuthResponse;
import com.humanet.humanetcore.fragments.profile.BaseProfileFragment;
import com.humanet.humanetcore.fragments.profile.ProfilePrivateInfoFragment;
import com.humanet.humanetcore.fragments.registration.BaseVerificationFragment;
import com.humanet.humanetcore.model.Country;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.utils.PrefHelper;


public class RegistrationActivity extends BaseRegistrationActivity implements
        OkFragment.OnCongratulationListener,
        BaseVerificationFragment.OnVerifyListener,
        BaseProfileFragment.OnProfileFillListener,
        ProfilePrivateInfoFragment.OnFillMainProfileListener,
        GuessFragment.OnGuessListener {

    GuessFragment mGuessFragment;


    private UserInfo mUserInfo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment_no_toolbar_container);

        getSpiceManager().execute(new AuthRequest(), new SimpleRequestListener<AuthResponse>() {
            @Override
            public void onRequestSuccess(final AuthResponse authResponse) {
                getSpiceManager().execute(new GetCountryRequest(), new SimpleRequestListener<Country[]>());
            }
        });

        if (getIntent().hasExtra(Constants.PARAMS.ACTION) && getIntent().getIntExtra(Constants.PARAMS.ACTION, 0) == Constants.ACTION.FILL_PROFILE) {
            startFragment(ProfilePrivateInfoFragment.newInstance(), false);
        } else {
            startFragment(VerificationFragment.newInstance(), false);
            mUserInfo = new UserInfo();
        }
    }


    @Override
    public void onVerify(boolean isNewUser, int countryId) {
        //isNewUser = true;
        if (isNewUser) {
            openFirstGame(countryId);
        } else {
            openMainActivity();
        }
    }

    @Override
    public void onRegistrationProcedureComplete() {
        openMainActivity();
    }

    public void openFirstGame(int countryId) {
        mGuessFragment = GuessFragment.newInstance(countryId);
        startFragment(mGuessFragment, false);
    }

    public void openMainActivity() {
        PrefHelper.setBooleanPref(Constants.PREF.REG, true);
        finish();

        AppUser.getInstance().clear();

        MainActivity.startNewInstance(this);
    }


    @Override
    public void onFillProfile(UserInfo userInfo) {
        //saveProfile(userInfo);
    }

    @Override
    public UserInfo getUser() {
        return mUserInfo;
    }

    @Override
    public void onGuess() {
        final ProfilePrivateInfoFragment profileFragment = new ProfilePrivateInfoFragment();
        if (mGuessFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mGuessFragment).commit();
        }
        startFragment(profileFragment, true);
    }

    @Override
    public void onFillMainProfileInfo(UserInfo userInfo) {
        AnalyticsHelper.trackEvent(this, AnalyticsHelper.FILL_PROFILE_BASE);

        mUserInfo = userInfo;
        startFragment(OkFragment.newInstance(Constants.CongratulationsType.PROFILE_OK), true);
        // startFragment(new ProfileInterestsFragment(), true);
        getSpiceManager().execute(new EditUserInfoRequest(EditUserInfoRequest.userInfoToArgsMap(userInfo)), new SimpleRequestListener<BaseResponse>());
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 1 || count == 0) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}
