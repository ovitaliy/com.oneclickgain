package com.clickandgain.modules.games.presenters;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.CallSuper;
import android.util.Log;

import com.clickandgain.ClickandgainApp;
import com.clickandgain.api.GameRequestErrorController;
import com.clickandgain.api.requests.game.BaseGamesStartRequest;
import com.clickandgain.api.requests.game.GameStatusRequest;
import com.clickandgain.api.requests.game.QuestionPickRequest;
import com.clickandgain.api.requests.game.QuestionVideoPreloadRequest;
import com.clickandgain.api.response.game.GameEndResponse;
import com.clickandgain.api.response.game.GameStartResponse;
import com.clickandgain.api.response.game.GameStatusResponse;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.model.game.QuestionPickResponse;
import com.clickandgain.modules.games.GameDataController;
import com.clickandgain.modules.games.events.QuestionReadyListener;
import com.clickandgain.modules.games.events.VideoLoadingListener;
import com.clickandgain.modules.games.exceptions.GameException;
import com.clickandgain.modules.games.views.GameResultView;
import com.clickandgain.modules.games.views.GameView;
import com.clickandgain.modules.games.views.StatusView;
import com.clickandgain.modules.games.views.StoppableGameView;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.GameType;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.octo.android.robospice.request.listener.RequestProgressListener;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by ovitali on 03.12.2015.
 */
public abstract class BaseGamePresenter {

    private static volatile StoppableGameView sStoppableGameView;

    private SpiceManager mSpiceManager;
    private GameType mGameType;
    private Question[] mQuestions;

    private StatusView mStatusView;
    private GameView mGameView;

    protected QuestionReadyListener mQuestionReadyListener;
    private VideoLoadingListener mVideoLoadingListener;

    public QuestionPickResponse mQuestionPickResponse;

    public BaseGamePresenter(final GameType gameType) {
        mSpiceManager = ClickandgainApp.getGameSpiceManager();
        mGameType = gameType;
    }

    public SpiceManager getSpiceManager() {
        return mSpiceManager;
    }

    public void setStatusView(StatusView statusView) {
        mStatusView = statusView;
        sStoppableGameView = mStatusView;
        if (mStatusView != null)
            checkStatus();
    }

    public void setGameView(GameView gameView) {
        mGameView = gameView;
        if(mGameView != null && mQuestionPickResponse != null) {
            mGameView.publishQuestionResponse(mQuestionPickResponse);
            mQuestionPickResponse = null;
        }
        sStoppableGameView = mGameView;
    }

    public void setGameResultView(GameResultView gameResultView) {
        sStoppableGameView = gameResultView;
    }

    public GameType getGameType() {
        return mGameType;
    }

    private void checkStatus() {
        getSpiceManager().execute(new GameStatusRequest(getGameToken(), mGameType), new GameRequestListener<GameStatusResponse>() {
            @Override
            public void onRequestSuccess(final GameStatusResponse gameStatusResponse) {
                if (mStatusView != null) {
                    mStatusView.updateStatus(gameStatusResponse);
                }
            }
        });
    }

    public void endGame() {
        endGame(true);
    }

    public void endGame(boolean isRequestMustBePostedToServer) {
        final String token = getGameToken();

        cancelCurrentCacheRequest();

        if (token != null) {
            GameDataController.endGame(getSpiceManager(), mGameType, isRequestMustBePostedToServer);
        }

        mQuestions = null;
    }

    public String getGameToken() {
        return GameDataController.getLastGameToken(mGameType);
    }

    /**
     * @param questionReadyListener - event will call when game will start and question will fetched
     */
    public void setQuestionReadyListener(final QuestionReadyListener questionReadyListener) {
        mQuestionReadyListener = questionReadyListener;
    }

    public void setVideoLoadingListener(final VideoLoadingListener videoLoadingListener) {
        mVideoLoadingListener = videoLoadingListener;
    }

    public Question[] reloadGameQuestions() {
        mQuestions = null;
        mQuestions = getGameQuestions();
        return mQuestions;
    }

    public Question[] getGameQuestions() {
        if (mQuestions == null && getGameToken() != null) {

            String where = String.format("%s='%s'",
                    ContentDescriptor.Question.Cols.GAME_KEY,
                    getGameToken()
            );

            ContentResolver contentResolver = App.getInstance().getContentResolver();
            Cursor questionCursor = contentResolver.query(ContentDescriptor.Question.URI, null,
                    where, null, null);

            assert questionCursor != null;
            int questionCount = questionCursor.getCount();
            ArrayList<Question> questions = new ArrayList<>(questionCount);
            if (questionCursor.moveToFirst()) {
                for (int j = 0; j < questionCount; j++) {
                    Question question = Question.fromCursor(questionCursor);

                    questionCursor.moveToNext();

                    Cursor mediaCursor = contentResolver.query(ContentDescriptor.QuestionMedia.URI, null,
                            ContentDescriptor.QuestionMedia.Cols.QUESTION_ID + "=" + question.getId(), null, ContentDescriptor.QuestionMedia.Cols.ORDER);

                    if (mediaCursor == null || !mediaCursor.moveToFirst())
                        continue;

                    questions.add(question);

                    int count = mediaCursor.getCount();

                    QuestionMedia[] questionMedias = new QuestionMedia[count];

                    for (int i = 0; i < count; i++) {
                        questionMedias[i] = QuestionMedia.fromCursor(mediaCursor);
                        mediaCursor.moveToNext();
                    }

                    mediaCursor.close();

                    question.setQuestionMedias(questionMedias);
                }
            }
            questionCursor.close();
            mQuestions = questions.toArray(new Question[questions.size()]);
        }
        return mQuestions;
    }

    public void checkIfVideoDownloaded() {
        Question[] questions = getGameQuestions();
        if (questions == null)
            return;

        for (Question question : questions) {
            if (!question.isDownloaded()) {
                cacheQuestionVideos(question);
                break;
            }
        }
    }


    protected void startGame(final BaseGamesStartRequest gameStartRequest) {
        getSpiceManager().addListenerIfPending(GameEndResponse.class, "", new GamePendingRequestListener<GameEndResponse>() {
            @Override
            public void onRequestNotFound() {
                startGameCall(gameStartRequest);
            }

            @Override
            public void onRequestSuccess(GameEndResponse gameEndResponse) {
                startGameCall(gameStartRequest);
            }
        });
    }

    private void startGameCall(final BaseGamesStartRequest gameStartRequest) {
        getSpiceManager().execute(gameStartRequest, "", DurationInMillis.ALWAYS_EXPIRED, new GameStartRequestListener());
    }


    class GameStartRequestListener extends GameRequestListener<GameStartResponse> {
        public void onRequestSuccess(final GameStartResponse gameStartResponse) {
            GameDataController.setLastGameToken(mGameType, gameStartResponse.getGameToken());
            GameDataController.setLastGameGameCreated(mGameType, System.currentTimeMillis());

            reloadGameQuestions();

            checkIfVideoDownloaded();

            if (mGameType.equals(GameType.ASTRA) && mQuestionReadyListener != null) {
                mQuestionReadyListener.questionsAreReady(true);
                mQuestionReadyListener = null;
            }
        }
    }

    private void cacheQuestionVideos(final Question question) {
        final String id = String.valueOf(question.getId());
        ClickandgainApp.getVideoCacheSpiceManager().addListenerIfPending(Question.class, id, new CacheQuestionVideosRequestListener(question));
    }

    private class CacheQuestionVideosRequestListener implements RequestProgressListener, PendingRequestListener<Question>, RequestListener<Question> {

        private Question mQuestion;

        public CacheQuestionVideosRequestListener(final Question question) {
            mQuestion = question;
        }

        @Override
        public void onRequestProgressUpdate(final RequestProgress progress) {
            if (mVideoLoadingListener != null) {
                mVideoLoadingListener.onVideoProgressChanged((int) progress.getProgress());
            }
        }

        @Override
        public void onRequestSuccess(final Question question) {
            if (getGameQuestions() == null || question == null) {
                return;
            }

            for (Question q : getGameQuestions()) {
                if (q.getId() == question.getId()) {
                    q.setDownloaded(true);
                    if (mVideoLoadingListener != null) {
                        mVideoLoadingListener.onVideoLoadingComplete();
                    }
                    break;
                }
            }

            checkIfVideoDownloaded();
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {

            handleInternetIssues(spiceException);
        }

        @Override
        public void onRequestNotFound() {
            QuestionVideoPreloadRequest questionVideoPreloadRequest = new QuestionVideoPreloadRequest(mQuestion);
            ClickandgainApp.getVideoCacheSpiceManager().execute(questionVideoPreloadRequest, "cache_media", DurationInMillis.ALWAYS_EXPIRED, this);
        }
    }


    public final void removeLastQuestion() {
        removeQuestion(getQuestion());
    }

    private void removeQuestion(Question question) {
        question.setAswered(true);

        final ContentResolver contentResolver = App.getInstance().getContentResolver();

        final ContentValues contentValues = new ContentValues(1);
        contentValues.put(ContentDescriptor.Question.Cols.ANSWERED, 1);

        final String where = String.format("%s='%s'",
                ContentDescriptor.Question.Cols.QUESTION_ID,
                question.getId()
        );

        contentResolver.update(ContentDescriptor.Question.URI, contentValues, where, null);
    }

    public void setResult(int answerId, int mediaId) {
        getSpiceManager().execute(new QuestionPickRequest(
                        mGameType,
                        getGameToken(),
                        answerId,
                        getQuestion().getId(),
                        mediaId),

                new GameRequestListener<QuestionPickResponse>() {
                    @Override
                    public void onRequestSuccess(final QuestionPickResponse response) {
                        if(mGameView != null)
                            mGameView.publishQuestionResponse(response);
                        else
                            mQuestionPickResponse = response;
                    }
                }
        );
    }

    public Question getQuestion() {
        if (mQuestions == null || mQuestions.length == 0)
            return null;

        Question result = null;
        for (Question question : mQuestions) {
            if (!question.isAswered()) {
                result = question;
                break;
            }
        }
        return result;
    }

    public void cancelCurrentCacheRequest() {
        ClickandgainApp.getVideoCacheSpiceManager().cancelAllRequests();
    }

    public String getVideoUrl(int index) {
        String videoName = getQuestion().getQuestionMedias()[index].getMedia();
        videoName = FilePathHelper.getVideoFileName(videoName);
        return new File(FilePathHelper.getVideoCacheDirectory(), videoName).getAbsolutePath();
    }


    static abstract class GameRequestListener<RESULT> implements RequestListener<RESULT> {

        @Override
        @CallSuper
        public void onRequestFailure(SpiceException spiceException) {
            ClickandgainApp.getVideoCacheSpiceManager().cancelAllRequests();

            if (spiceException.getCause() instanceof GameException) {
                final GameException gameException = (GameException) spiceException.getCause();
                Log.e("GamePresenter", gameException.toString());

                if (gameException.getGameKey() != null)
                    GameDataController.endGame(ClickandgainApp.getGameSpiceManager(), gameException.getGameType(), gameException.getGameKey());


                if (sStoppableGameView != null && sStoppableGameView.getView() != null) {
                    // sets delay to let to delete question from database
                    sStoppableGameView.getView().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (sStoppableGameView != null)
                                sStoppableGameView.gameStopped(gameException.getAction());
                        }
                    }, 50);

                }
            }
            handleInternetIssues(spiceException);
        }
    }

    static abstract class GamePendingRequestListener<RESULT> extends GameRequestListener<RESULT> implements PendingRequestListener<RESULT> {

    }

    private static void handleInternetIssues(SpiceException spiceException) {
        if (spiceException instanceof NoNetworkException && sStoppableGameView != null)
            sStoppableGameView.getView().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (sStoppableGameView != null)
                        sStoppableGameView.gameStopped(GameRequestErrorController.ACTION_NO_INTERNET);
                }
            }, 50);
    }


}
