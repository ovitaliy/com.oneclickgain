package com.clickandgain.modules.games.viewController;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.clickandgain.R;
import com.clickandgain.fragments.games.credo.game.CredoGameResultFragment;
import com.clickandgain.model.game.ElseVideoInfo;

/**
 * Created by ovi on 4/12/16.
 */
public class ElseGameResultViewController extends CredoGameResultViewController {

    private TextView mWatchesTextView;
    private TextView mTagsTextView;
    private TextView mAuthorTextView;
    private TextView mLocationTextView;

    public ElseGameResultViewController(CredoGameResultFragment gameResultFragment, View view) {
        super(gameResultFragment, view);

        mWatchesTextView = (TextView) view.findViewById(R.id.stream_item_watches);
        mTagsTextView = (TextView) view.findViewById(R.id.stream_item_tags);
        mAuthorTextView = (TextView) view.findViewById(R.id.stream_item_user_name);
        mLocationTextView = (TextView) view.findViewById(R.id.stream_item_locality);

        mAuthorTextView.setOnClickListener(this);
    }

    public void setVideo(ElseVideoInfo video) {
        String tags = video.getTags();

        if (TextUtils.isEmpty(tags)) {
            mTagsTextView.setVisibility(View.GONE);
        } else {
            mTagsTextView.setText(tags);
        }

        String userName = video.getAuthorName();
        if (userName != null) {
            userName = userName.replace(" ", "\n");
            mAuthorTextView.setText(userName);
        }

        mWatchesTextView.setText(String.valueOf(video.getViews()));


        String countryTitle = video.getCountry();
        String cityTitle = video.getCity();

        String locality = "";
        if (!TextUtils.isEmpty(countryTitle)) {
            locality += countryTitle;
        }
        if (!TextUtils.isEmpty(countryTitle) && !TextUtils.isEmpty(cityTitle)) {
            locality += "\n";
        }
        if (!TextUtils.isEmpty(cityTitle))
            locality += cityTitle;

        mLocationTextView.setText(locality);
    }

}
