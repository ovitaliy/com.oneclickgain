package com.clickandgain.modules.games.presenters;

import com.clickandgain.api.requests.game.GamesStartAstraRequest;
import com.clickandgain.modules.games.GameDataController;
import com.humanet.humanetcore.model.enums.GameType;
import com.humanet.humanetcore.utils.FilePathHelper;

import java.io.File;

/**
 * Created by ovitali on 03.12.2015.
 */
public class AstraPresenter extends BaseGamePresenter {

    private static AstraPresenter sInstance;

    public static AstraPresenter getInstance() {
        if (sInstance == null)
            sInstance = new AstraPresenter();
        return sInstance;
    }

    private AstraPresenter() {
        super(GameType.ASTRA);
    }


    public String getVideoUrl() {
        return super.getVideoUrl(0);
    }

    public String getAnswerVideoUrl() {
        String videoName = getQuestion().getQuestionMedias()[0].getUrlAnswer();
        videoName = FilePathHelper.getVideoFileName(videoName);
        return new File(FilePathHelper.getVideoCacheDirectory(), videoName).getAbsolutePath();
    }

    public void startGame(int countOfPlayedGame, String sport) {
        startGame(new GamesStartAstraRequest(countOfPlayedGame, sport));
    }

    public boolean isGameCreatedTimeValid() {
        final long created = GameDataController.getLastGameGameCreated(GameType.ASTRA);
        final long fifteenMinutes = 1000 * 60 * 15;

        return created + fifteenMinutes > System.currentTimeMillis();
    }

}
