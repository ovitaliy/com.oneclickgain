package com.clickandgain.modules.games.viewController;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.clickandgain.R;
import com.clickandgain.fragments.games.credo.game.CredoGameResultFragment;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.CircleView;
import com.humanet.humanetcore.views.widgets.VideoSurfaceView;
import com.humanet.humanetcore.views.widgets.YellowProgressBar;

import static com.clickandgain.fragments.games.credo.game.CredoGameResultFragment.COMPLETE;
import static com.clickandgain.fragments.games.credo.game.CredoGameResultFragment.HIDDEN_ANSWER;
import static com.clickandgain.fragments.games.credo.game.CredoGameResultFragment.LOADING;
import static com.clickandgain.fragments.games.credo.game.CredoGameResultFragment.PLAYING;

/**
 * Created by ovitali on 07.12.2015.
 */
public class CredoGameResultViewController implements View.OnClickListener {

    private YellowProgressBar mProgressBar;
    private View mPlayButton;

    /*private TextView mBalanceTextView;*/
    private TextView mBalanceChangeTextView;
    private VideoSurfaceView mSurfaceView;

    private View mNextButton;
    private View mBackButton;

    private TextView mRateTextView;
    private TextView mMultiplierTextView;

    private TextView mTotalBalanceTextView;

    private CredoGameResultFragment mGameResultFragment;

    private float mBalance;
    private float mBalanceChange;

    public CredoGameResultViewController(CredoGameResultFragment gameResultFragment, View view) {
        mGameResultFragment = gameResultFragment;

    /*    mBalanceTextView = (TextView) view.findViewById(R.id.balance);*/
        mBalanceChangeTextView = (TextView) view.findViewById(R.id.balance_change);
        mTotalBalanceTextView = (TextView) view.findViewById(R.id.balance_total);

        if (UiUtil.isLandscape(gameResultFragment.getActivity())) {
            mSurfaceView = (VideoSurfaceView) view.findViewById(R.id.video_view);
            mProgressBar = (YellowProgressBar) view.findViewById(R.id.progress_bar);
        } else {
            CircleView circleView = (CircleView) view.findViewById(R.id.video_view);
            mSurfaceView = circleView.getSurfaceView();
            mProgressBar = circleView.getProgressBar();
        }

        mPlayButton = view.findViewById(R.id.play_button);
        mPlayButton.setOnClickListener(this);

        mNextButton = view.findViewById(R.id.btn_next);
        mNextButton.setOnClickListener(this);

        mBackButton = view.findViewById(R.id.btn_back);
        mBackButton.setOnClickListener(this);

        mRateTextView = (TextView) view.findViewById(R.id.rate);
        mMultiplierTextView = (TextView) view.findViewById(R.id.multiplier);
    }

    public void setBalance(float change) {
        if (change > 0) {
            mBalanceChangeTextView.setTextColor(Color.GREEN);
            mBalanceChangeTextView.setText(BalanceUtil.balanceToString(change));
        } else {
            mBalanceChangeTextView.setTextColor(Color.RED);
            mBalanceChangeTextView.setText(BalanceUtil.balanceToString(change));
        }
    }

    public void setBalanceValues(float balance, float balanceChange) {
        mBalance = balance;
        mBalanceChange = balanceChange;
    }

    public VideoSurfaceView getSurfaceView() {
        return mSurfaceView;
    }

    @Override
    public void onClick(final View v) {
        mGameResultFragment.onClick(v);
    }

    public void manageVisualState(int state) {
        switch (state) {
            case LOADING:
                mProgressBar.setVisibility(View.VISIBLE);

                mTotalBalanceTextView.setVisibility(View.INVISIBLE);
//                mBalanceTextView.setVisibility(View.INVISIBLE);
                mBalanceChangeTextView.setVisibility(View.INVISIBLE);

                mMultiplierTextView.setVisibility(View.VISIBLE);
                mRateTextView.setVisibility(View.VISIBLE);

                mPlayButton.setVisibility(View.GONE);
                mNextButton.setVisibility(View.INVISIBLE);

                mBackButton.setVisibility(View.INVISIBLE);

                mTotalBalanceTextView.setText(BalanceUtil.balanceToString(mBalance));

                break;

            case HIDDEN_ANSWER:
                mTotalBalanceTextView.setVisibility(View.VISIBLE);
//                mBalanceTextView.setVisibility(View.INVISIBLE);
                mBalanceChangeTextView.setVisibility(View.INVISIBLE);

                mMultiplierTextView.setVisibility(View.VISIBLE);
                mRateTextView.setVisibility(View.VISIBLE);

                mProgressBar.setVisibility(View.INVISIBLE);
                mPlayButton.setVisibility(View.GONE);
                mNextButton.setVisibility(View.INVISIBLE);
                mBackButton.setVisibility(View.INVISIBLE);

                mTotalBalanceTextView.setText(BalanceUtil.balanceToString(mBalance));

                break;

            case PLAYING:
                mTotalBalanceTextView.setVisibility(View.VISIBLE);
//                mBalanceTextView.setVisibility(View.VISIBLE);
                mBalanceChangeTextView.setVisibility(View.VISIBLE);

                mMultiplierTextView.setVisibility(View.INVISIBLE);
                mRateTextView.setVisibility(View.INVISIBLE);

                mProgressBar.setVisibility(View.GONE);
                mPlayButton.setVisibility(View.GONE);
                mNextButton.setVisibility(View.VISIBLE);
                mBackButton.setVisibility(View.VISIBLE);

                mTotalBalanceTextView.setText(BalanceUtil.balanceToString(mBalanceChange));

                break;

            case COMPLETE:
                mTotalBalanceTextView.setVisibility(View.VISIBLE);
//                mBalanceTextView.setVisibility(View.VISIBLE);
                mBalanceChangeTextView.setVisibility(View.VISIBLE);

                mMultiplierTextView.setVisibility(View.INVISIBLE);
                mRateTextView.setVisibility(View.INVISIBLE);

                mProgressBar.setVisibility(View.GONE);
                mPlayButton.setVisibility(View.VISIBLE);

                mNextButton.setVisibility(View.VISIBLE);
                mBackButton.setVisibility(View.VISIBLE);

                mTotalBalanceTextView.setText(BalanceUtil.balanceToString(mBalanceChange));

                break;
        }
    }


    public void setProgress(final int progress) {
        mProgressBar.setProgress(progress);
    }

    @SuppressLint("DefaultLocale")
    public void setMultiplier(int multiplier, int rate) {
        mMultiplierTextView.setText(String.format("1/%d", multiplier));
        mRateTextView.setText(String.valueOf(rate));
    }

}
