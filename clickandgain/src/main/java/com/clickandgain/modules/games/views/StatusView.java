package com.clickandgain.modules.games.views;

import com.clickandgain.api.response.game.GameStatusResponse;

/**
 * Created by ovitali on 03.12.2015.
 */
public interface StatusView extends BaseView{

    void updateStatus(GameStatusResponse gameStatusResponse);

    void navigateToGame();

}
