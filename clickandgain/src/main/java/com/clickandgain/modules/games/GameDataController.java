package com.clickandgain.modules.games;

import android.os.Bundle;

import com.clickandgain.api.requests.game.GameEndRequest;
import com.clickandgain.api.response.game.GameEndResponse;
import com.clickandgain.fragments.games.credo.game.CredoGameFragment;
import com.clickandgain.fragments.games.credo.game.ElseGameFragment;
import com.clickandgain.fragments.games.credo.game.GeneralGameFragment;
import com.clickandgain.jobs.ClearGameCacheJob;
import com.clickandgain.views.widgets.game.SportCategoryPickerView;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.fragments.base.BaseSpiceFragment;
import com.humanet.humanetcore.model.enums.GameType;
import com.humanet.humanetcore.utils.PrefHelper;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;

/**
 * Created by ovitali on 23.09.2015.
 */
public class GameDataController {

    public static <T extends BaseSpiceFragment> T createBalanceBundle(T fragment, float balance) {
        Bundle bundle = new Bundle(1);
        bundle.putFloat("balance", balance);
        fragment.setArguments(bundle);

        return fragment;
    }

    public static BaseSpiceFragment newInstance(GameType gameDisplayType, float balance) {
        BaseSpiceFragment credoGameFragment = null;
        switch (gameDisplayType) {
            case CREDO:
                credoGameFragment = new CredoGameFragment();
                break;
            case GENERAL_GAME:
                credoGameFragment = new GeneralGameFragment();
                break;
            case ELSE:
                credoGameFragment = new ElseGameFragment();
                break;
        }

        credoGameFragment = GameDataController.createBalanceBundle(credoGameFragment, balance);

        return credoGameFragment;
    }

    public static void endGame(SpiceManager spiceManager, GameType type, boolean isRequestMustBePostedToServer) {
        String token = clearToken(type);
        endGame(spiceManager, type, isRequestMustBePostedToServer ? token : null);
    }


    public static void endGame(SpiceManager spiceManager, GameType type, String token) {
        ClearGameCacheJob.start(token);
        if (token != null)
            spiceManager.execute(new GameEndRequest(token, type), "", DurationInMillis.ALWAYS_EXPIRED, new SimpleRequestListener<GameEndResponse>());
    }

    //-----
    private static final String LAST_GAME = "last_game";
    private static final String CREATED = "created";
    private static final String SPORT = "sport";

    public static String getLastGameToken(GameType type) {
        return PrefHelper.getStringPref(type + LAST_GAME);
    }

    public static long getLastGameGameCreated(GameType type) {
        return PrefHelper.getLongPref(type + CREATED);
    }

    public static
    @SportCategoryPickerView.SportType
    String getSportType(GameType type) {
        @SportCategoryPickerView.SportType
        String sport = PrefHelper.getStringPref(type + SPORT);
        return sport;
    }

    public static void setLastGameGameCreated(GameType type, long value) {
        PrefHelper.setLongPref(type + CREATED, value);
    }

    public static void setLastGameToken(GameType type, String value) {
        PrefHelper.setStringPref(type + LAST_GAME, value);
    }

    public static void setSport(GameType type, String sport) {
        PrefHelper.setStringPref(type + SPORT, sport);
    }

    public static String clearToken(GameType type) {
        String token = PrefHelper.getStringPref(type + LAST_GAME);
        PrefHelper.setStringPref(type + LAST_GAME, null);
        return token;
    }

}
