package com.clickclap.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.clickclap.job.UploadGrimaceJob;
import com.humanet.humanetcore.jobs.UploadVideoJob;
import com.humanet.humanetcore.utils.NetworkUtil;

/**
 * Created by ovi on 24.05.2016.
 */
public class InternetReceiver extends BroadcastReceiver {

    public InternetReceiver() {
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        boolean offlineMode = NetworkUtil.isOfflineMode(context);

        if (!offlineMode) {
            UploadVideoJob.startUploader();
            UploadGrimaceJob.startUploader();
        }
    }

}
