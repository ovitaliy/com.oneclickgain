package com.clickclap.activities;

import android.content.Intent;
import android.os.Bundle;

import com.humanet.humanetcore.activities.BaseSplashActivity;

/**
 * Created by ovi on 1/25/16.
 */
public class SplashActivity extends BaseSplashActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    @Override
    public void openMainActivity() {
        MainActivity.startNewInstance(this);
    }

    @Override
    public void openRegistrationActivity() {
        startActivity(new Intent(this, RegistrationActivity.class));
    }
}
