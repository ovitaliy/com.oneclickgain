package com.clickclap.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.clickclap.ClickclapApp;
import com.clickclap.R;
import com.clickclap.fragments.CCFeedbackFragment;
import com.clickclap.fragments.CCNavigationBalanceFragment;
import com.clickclap.fragments.CCProfileFragment;
import com.clickclap.fragments.EmoticonsFragment;
import com.clickclap.fragments.MenuFragment;
import com.clickclap.fragments.grimaces.GrimaceFragment;
import com.clickclap.widget.helper.PrefHelper;
import com.humanet.humanetcore.activities.BaseMainActivity;
import com.humanet.humanetcore.fragments.AlienLookFragment;
import com.humanet.humanetcore.fragments.BaseMenuFragment;
import com.humanet.humanetcore.fragments.CrowdFragment;
import com.humanet.humanetcore.fragments.VistoryFragment;
import com.humanet.humanetcore.fragments.balance.CurrencyFragment;
import com.humanet.humanetcore.fragments.balance.RatingsTabFragment;
import com.humanet.humanetcore.fragments.info.InfoFragment;
import com.humanet.humanetcore.fragments.info.NavigationInfoFragment;
import com.humanet.humanetcore.fragments.vote.VoteTabFragment;

/**
 * Created by ovi on 20.05.2016.
 */
public class MainActivity extends BaseMainActivity {

    public static void startNewInstance(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkLaunchForGrimace();

        ClickclapApp.syncGrimaces();
    }

    @Override
    protected void launchFirstFragment() {
        startFragment(new EmoticonsFragment(), false);
    }

    @Override
    protected BaseMenuFragment newMenuFragmentInstance() {
        return new MenuFragment();
    }

    @Override
    public void onNavigateByViewId(int viewId) {
        switch (viewId) {

            case R.id.alien_look:
                startFragment(new AlienLookFragment(), false, true);
                break;

            case R.id.grimaces:
                startFragment(new GrimaceFragment(), false, true);
                break;

            case R.id.emoticons:
                startFragment(new EmoticonsFragment(), false, true);
                break;

            case R.id.balance:
                startFragment(CCNavigationBalanceFragment.newInstance(), false, true);
                break;

            case R.id.currency:
                startFragment(new CurrencyFragment(), true, true);
                break;

            case R.id.crowd:
                startFragment(new CrowdFragment(), true);
                break;

            case R.id.rating:
                startFragment(new RatingsTabFragment(), true, true);
                break;


            case R.id.navigation_info:
                startFragment(new NavigationInfoFragment(), false, true);
                break;

            case R.id.info:
                startFragment(new InfoFragment(), true, true);
                break;

            case R.id.feedback:
                startFragment(new CCFeedbackFragment(), true, true);
                break;


            case R.id.vote:
                startFragment(new VoteTabFragment(), true, true);
                break;

            case R.id.vistory:
                startFragment(new VistoryFragment(), false);
                break;


            default:
                Toast.makeText(this, "UNDER CONSTRUCTION", Toast.LENGTH_SHORT).show();

        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        checkLaunchForGrimace();
    }

    private void checkLaunchForGrimace() {
        if (PrefHelper.getIntPreference("create_grimace") > 0) {
            startFragment(new GrimaceFragment(), false, true);
            GrimaceCaptureActivity.startNewInstance(this, 100, PrefHelper.getIntPreference("create_grimace"));
            PrefHelper.setIntPreference("create_grimace", 0);
        }
    }

    @Override
    public void onViewProfile(int uid) {
        startFragment(CCProfileFragment.newInstance(uid), true);
    }
}
