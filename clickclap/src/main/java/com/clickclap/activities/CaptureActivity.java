package com.clickclap.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.clickclap.fragments.video.SelectEmoticonFragment;
import com.humanet.humanetcore.fragments.capture.VideoRecordFragment;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by ovi on 22.05.2016.
 */
public class CaptureActivity extends com.humanet.humanetcore.activities.CaptureActivity implements SelectEmoticonFragment.OnSelectSmileListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbar.setVisibility(View.GONE);
    }

    public static void startNewEmoticonInstance(Activity activity, int requestCode) {
        Intent intent = new Intent(activity, CaptureActivity.class);
        intent.putExtra("emoticon", true);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void launchRecording() {
        if (getIntent() != null && getIntent().getBooleanExtra("emoticon", false)) {
            NewVideoInfo.get().setVideoType(VideoType.EMOTICON_ALL);
            startFragment(SelectEmoticonFragment.newInstance(), false);
        } else {
            startFragment(VideoRecordFragment.newInstance(), false);
        }
    }

    @Override
    public void onSmileSelected(int smileId) {
        NewVideoInfo.get().setSmileId(smileId);
        startFragment(VideoRecordFragment.newInstance(), true);
    }
}
