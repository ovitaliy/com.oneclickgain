package com.clickclap.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.humanet.humanetcore.views.IInputTextView;

/**
 * Created by ovi on 23.05.2016.
 */
public class InputTextView extends EditText implements IInputTextView {

    public InputTextView(Context context) {
        super(context);
    }

    public InputTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public EditText getEditView() {
        return this;
    }
}
