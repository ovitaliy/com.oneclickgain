package com.clickclap.widgets.grimaces;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.clickclap.ClickclapApp;
import com.clickclap.R;
import com.clickclap.activities.GrimaceCaptureActivity;
import com.clickclap.api.requests.BuyGrimaceRequest;
import com.clickclap.api.requests.SetSellingGrimaceRequest;
import com.clickclap.models.Grimace;
import com.clickclap.models.enums.Emotion;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.views.utils.ConverterUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Denis on 31.03.2015.
 */
@SuppressLint("ViewConstructor")
public class GrimacePageView extends TableLayout {
    private HashMap<Integer, Grimace[]> mGrimaces;
    private ArrayList<Integer> mEmotions;
    private int mCellSize;
    private int mType;

    public static GrimacePageView newInstance(Context context, LinkedHashMap<Integer, Grimace[]> grimaces, int cellSize, int type) {
        //since linkedHashMap serialization problems
        ArrayList<Integer> emotions = new ArrayList<>();
        for (Map.Entry<Integer, Grimace[]> entry : grimaces.entrySet()) {
            emotions.add(entry.getKey());
        }

        return new GrimacePageView(context, grimaces, emotions, cellSize, type);
    }

    private GrimacePageView(Context context, HashMap<Integer, Grimace[]> grimaces, ArrayList<Integer> emotions, int cellSize, int type) {
        super(context);
        mGrimaces = grimaces;
        mEmotions = emotions;
        mCellSize = cellSize;
        mType = type;

        setLayoutParams(new ViewGroup.LayoutParams(cellSize*3, ViewGroup.LayoutParams.MATCH_PARENT));

        fill();
    }


    private void fill() {
        post(new Runnable() {
            @Override
            public void run() {
                Context context = getContext();
                if (context == null) {
                    return;
                }

                int margin = (int) ConverterUtil.dpToPix(context, 3);
                int cellSize = mCellSize - (margin * 2);
                removeAllViews();

                for (final int i : mEmotions) {
                    Grimace[] grimaces = mGrimaces.get(i);
                    TableRow row = new TableRow(getContext());
                    row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

                    for (final Grimace grimace : grimaces) {
                        final View cellView;

                        if (grimace != null) {
                            if (grimace.getId() == 0) {
                                cellView = new ImageView(context);
                                cellView.setBackgroundResource(R.drawable.btn_add);
                                cellView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        GrimaceCaptureActivity.startNewInstance((Activity) getContext(), 100, Emotion.getByIdNonNull(i).getId());
                                    }
                                });
                            } else {
                                cellView = new GrimaceCircleView(getContext());
                                ((GrimaceCircleView) cellView).setGrimace(grimace);
                                ((GrimaceCircleView) cellView).setType(mType);

                                if (mType != Grimace.TYPE_COLLECTION) {
                                    cellView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            switch (mType) {
                                                case Grimace.TYPE_USER:
                                                    grimace.setSelling(!grimace.isSelling());
                                                    setSellingGrimace(grimace.getId(), grimace.isSelling());
                                                    ((GrimaceCircleView) cellView).setGrimace(grimace);
                                                    break;
                                                case Grimace.TYPE_MARKET:
                                                    if (!grimace.isBought()) {
                                                        grimace.setBought(true);
                                                        buyGrimace(grimace.getId());
                                                        ((GrimaceCircleView) cellView).setGrimace(grimace);
                                                    }
                                                    break;
                                            }
                                        }
                                    });
                                }
                            }
                        } else {
                            cellView = new View(context);
                        }
                        TableRow.LayoutParams params = new TableRow.LayoutParams(cellSize, cellSize);
                        params.setMargins(margin, margin, margin, margin);
                        cellView.setLayoutParams(params);

                        row.addView(cellView);
                    }

                    addView(row, new TableLayout.LayoutParams(cellSize*3, cellSize));
                }
            }
        });
    }

    private void buyGrimace(int id) {
        ClickclapApp.getSpiceManager().execute(new BuyGrimaceRequest(id), new SimpleRequestListener<BaseResponse>());
    }

    private void setSellingGrimace(int id, boolean status) {
        ClickclapApp.getSpiceManager().execute(new SetSellingGrimaceRequest(id, status), new SimpleRequestListener<BaseResponse>());
    }
}
