package com.clickclap.widgets.grimaces;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.clickclap.models.Grimace;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by denisvasilenko on 24.09.15.
 */
public class GrimacesPagerAdapter extends PagerAdapter {
    int mCellSize;
    int mType;
    ArrayList<LinkedHashMap<Integer, Grimace[]>> mData;

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


    public void clear() {
        if (mData != null) {
            mData.clear();
            try {
                notifyDataSetChanged();
            } catch (IllegalStateException e) {
            }
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = GrimacePageView.newInstance(container.getContext(), mData.get(position), mCellSize, mType);
        container.addView(view, 0);
        return view;
    }


    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public boolean isNewDataIdentical(ArrayList<LinkedHashMap<Integer, Grimace[]>> newData) {
        if (mData == null) {
            return false;
        }

        if (newData.equals(mData)) {
            return true;
        }

        if (mData.size() != newData.size()) {
            return false;
        }

        boolean identical = true;
        if (newData.size() == mData.size()) {
            for (int i = 0; i < mData.size(); i++) {
                if (newData.get(i).size() != mData.get(i).size()) {
                    identical = false;
                } else {
                    for (int j = 0; j < newData.size(); j++) {
                        LinkedHashMap<Integer, Grimace[]> newList = newData.get(j);
                        LinkedHashMap<Integer, Grimace[]> dataList = mData.get(j);

                        for (Map.Entry<Integer, Grimace[]> entry : newList.entrySet()) {
                            int key = entry.getKey();

                            if (newList.get(key).length != dataList.get(key).length) {
                                return false;
                            }
                        }
                    }
                }
            }
        }

        return identical;
    }

    public void setData(final ArrayList<LinkedHashMap<Integer, Grimace[]>> data, int type) {
        mData = data;
        mType = type;
        notifyDataSetChanged();
    }

    public GrimacesPagerAdapter(int cellSize) {
        mCellSize = cellSize;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mData != null) {
            count = mData.size();
        }
        return count;
    }

}