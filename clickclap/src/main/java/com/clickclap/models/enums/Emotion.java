package com.clickclap.models.enums;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.clickclap.ClickclapApp;
import com.humanet.humanetcore.interfaces.CirclePickerItem;

import java.util.Locale;

/**
 * Created by Denis on 12.02.2015.
 */
public enum Emotion implements CirclePickerItem {
    SMILE,
    FUN,
    SAD,
    THINK,
    SILENCE,
    SURPRISE,
    FACEPALM,
    WINK,
    TEASE,
    KISS;


    public int getResId(Context context, String type) {
        String packageName = context.getPackageName();
        String emotionName = "e_" + name().toLowerCase();
        return context.getResources().getIdentifier(emotionName, type, packageName);
    }

    @Override
    @NonNull
    public String getDrawable() {
        return "assets://emoticons/e_" + toString().toLowerCase(Locale.getDefault()) + ".png";
    }

    @Override
    public int getId() {
        return ordinal() + 1;
    }

    @Override
    public String getTitle() {
        String title = null;
        Context context = ClickclapApp.getInstance();
        int resId = getResId(context, "string");
        if (resId != 0) {
            title = context.getString(resId);
        }
        return title;
    }

    @Nullable
    public static Emotion getById(int id) {
        if (id < 1) {
            id = 1;
        }
        for (Emotion emotion : values()) {
            if (emotion.getId() == id) {
                return emotion;
            }
        }
        return null;
    }

    @NonNull
    public static Emotion getByIdNonNull(int id) {
        Emotion emotion = getById(id);
        if (emotion == null)
            throw new NullPointerException("emoticon not selected");
        return emotion;
    }

}
