package com.clickclap.api.requests;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.sets.VideoApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class GuessVideoSmileRequest extends RetrofitSpiceRequest<BaseResponse, VideoApiSet> {
    int mVideoId;
    int mSmileId;

    public GuessVideoSmileRequest(int videoId, int smileId) {
        super(BaseResponse.class, VideoApiSet.class);
        mVideoId = videoId;
        mSmileId = smileId;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_video", mVideoId);
        map.put("id_smile", mSmileId);
        return getService().guessVideoSmile(App.API_APP_NAME, map);
    }
}
