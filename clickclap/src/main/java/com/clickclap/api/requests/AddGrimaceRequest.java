package com.clickclap.api.requests;

import com.clickclap.ClickclapApp;
import com.clickclap.api.responses.AddGrimaceResponse;
import com.clickclap.api.sets.GrimaceRestApi;
import com.humanet.humanetcore.api.ArgsMap;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class AddGrimaceRequest extends RetrofitSpiceRequest<AddGrimaceResponse, GrimaceRestApi> {
    private String mMedia;
    private int mSmileId;
    private int mEffectId;

    public AddGrimaceRequest(String url, int smileId, int effectId) {
        super(AddGrimaceResponse.class, GrimaceRestApi.class);
        mMedia = url;
        mSmileId = smileId;
        mEffectId = effectId;
    }

    @Override
    public AddGrimaceResponse loadDataFromNetwork() throws Exception {

        ArgsMap params = new ArgsMap(true);
        params.put("media", mMedia);
        params.put("id_effect", mEffectId);
        params.put("id_smile", mSmileId);
        return getService().addGrimace(ClickclapApp.API_APP_NAME,params);
    }
}
