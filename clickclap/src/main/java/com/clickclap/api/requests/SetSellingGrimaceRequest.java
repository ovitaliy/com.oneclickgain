package com.clickclap.api.requests;

import android.content.ContentValues;

import com.clickclap.ClickclapApp;
import com.clickclap.api.sets.GrimaceRestApi;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class SetSellingGrimaceRequest extends RetrofitSpiceRequest<BaseResponse, GrimaceRestApi> {
    int mGrimaceId;
    boolean mStatus;

    public SetSellingGrimaceRequest(int grimaceId, boolean status) {
        super(BaseResponse.class, GrimaceRestApi.class);
        mGrimaceId = grimaceId;
        mStatus = status;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_grimace", mGrimaceId);
        map.put("status", mStatus ? 1 : 0);

        BaseResponse response = getService().setSellingGrimace(ClickclapApp.API_APP_NAME, map);
        if (response != null && response.isSuccess()) {
            ContentValues values = new ContentValues(1);
            values.put(ContentDescriptor.Grimaces.Cols.SELLING, mStatus ? 1 : 0);
            ClickclapApp.getInstance().getContentResolver().update(ContentDescriptor.Grimaces.URI,
                    values,
                    ContentDescriptor.Grimaces.Cols.ID + " = " + mGrimaceId,
                    null);
        }

        return response;
    }
}
