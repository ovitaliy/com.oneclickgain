package com.clickclap.api.sets;

import com.clickclap.api.responses.AddGrimaceResponse;
import com.clickclap.api.responses.GrimacesListResponse;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;

import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public interface GrimaceRestApi {

    @POST(Constants.BASE_API_URL + "/grimace.add")
    AddGrimaceResponse addGrimace(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/grimace.get")
    GrimacesListResponse getGrimaces(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/grimace.buy")
    BaseResponse buyGrimace(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/grimace.selling")
    BaseResponse setSellingGrimace(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/grimace.share")
    BaseResponse shareGrimace(@Path("appName") String appName, @Body ArgsMap options);
}