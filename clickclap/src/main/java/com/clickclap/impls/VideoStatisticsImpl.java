package com.clickclap.impls;

import com.clickclap.R;
import com.humanet.humanetcore.model.VideoStatistic;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by ovi on 2/23/16.
 */
public class VideoStatisticsImpl implements VideoStatistic.VideoStatisticDelegator {

    @Override
    public VideoStatistic create(VideoStatistic videoStatistic, VideoType videoType, int duration) {
        switch (videoType) {
            case ALL:
            case MY_CHOICE:
                videoStatistic.setImageResId("assets://statistics/flow.png");
                videoStatistic.setTitleResId(R.string.menu_vistory);
                break;

            case EMOTICON_ALL:
            case EMOTICON_BEST:
            case EMOTICON_MY_CHOICE:
                videoStatistic.setImageResId("assets://statistics/emoticons.png");
                videoStatistic.setTitleResId(R.string.menu_emoticons);
                break;

            case VLOG:
                videoStatistic.setImageResId("assets://statistics/vlog.png");
                videoStatistic.setTitleResId(R.string.online_tab_vlog);
                break;

            case CROWD_ASK:
            case CROWD_GIVE:
                videoStatistic.setImageResId("assets://statistics/crowd.png");
                videoStatistic.setTitleResId(R.string.crowd);
                break;

            case NEWS:
                videoStatistic.setImageResId("assets://statistics/interest_news.png");
                videoStatistic.setTitleResId(R.string.interest_news);
                break;

        }

        return videoStatistic;
    }
}
