package com.clickclap.impls;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clickclap.R;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.fragments.profile.ProfileInterestsFragment;

/**
 * Created by ovi on 23.05.2016.
 */
public class ProfileInterestsFragmentImpl implements ProfileInterestsFragment.Delegator {

    @Override
    public View createTextView(Context context, String title) {
        TextView textView = new TextView(context);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setText(title.replace("\n", " "));
        textView.setTextSize(14);
        textView.setBackgroundResource(R.drawable.bg_drop_down_list_selected);
        textView.setTextColor(Color.BLACK);
        return textView;
    }
}
