package com.clickclap;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.provider.Settings;

import com.clickclap.api.listeners.GetGrimacesCacheRequestListener;
import com.clickclap.api.requests.GetGrimacesRequest;
import com.clickclap.impls.AnalyticsImpl;
import com.clickclap.impls.BalanceChangeTypeImpl;
import com.clickclap.impls.ClickclapVideoItemImpl;
import com.clickclap.impls.CurrencyTabViewImpl;
import com.clickclap.impls.ProfileInterestsFragmentImpl;
import com.clickclap.impls.RatingImpl;
import com.clickclap.impls.ToolbarHelperDelegator;
import com.clickclap.impls.VideoStatisticsImpl;
import com.clickclap.models.Grimace;
import com.clickclap.widget.service.BackgroundService;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.fragments.balance.CurrencyFragment;
import com.humanet.humanetcore.fragments.balance.RatingsTabFragment;
import com.humanet.humanetcore.fragments.profile.ProfileInterestsFragment;
import com.humanet.humanetcore.model.VideoStatistic;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.utils.BalanceHelper;
import com.humanet.humanetcore.utils.GsonHelper;
import com.humanet.humanetcore.utils.ToolbarHelper;
import com.humanet.humanetcore.views.widgets.CirclePickerView;
import com.humanet.humanetcore.views.widgets.ShadowImageButton;
import com.humanet.humanetcore.views.widgets.items.VideoItemView;

/**
 * Created by ovi on 20.05.2016.
 */
public class ClickclapApp extends com.humanet.humanetcore.App {

    static {
        API_APP_NAME = "cc";
        ShadowImageButton.iconFilterColor = Color.parseColor("#202527");
        GsonHelper.BALANCE_SERIALIZATION_KEY = "mc";
        COINS_CODE = "MC";
        DATABASE_VERSION = 47;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        VideoItemView.init(new ClickclapVideoItemImpl());

        CirclePickerView.init(true);

        ToolbarHelper.init(new ToolbarHelperDelegator());

        ProfileInterestsFragment.init(new ProfileInterestsFragmentImpl());

        BalanceHelper.setsImpl(new BalanceChangeTypeImpl());
        RatingsTabFragment.setImpl(new RatingImpl(this));

        CurrencyFragment.setImpl(new CurrencyTabViewImpl(this));

        VideoStatistic.setVideoStatisticDelegator(new VideoStatisticsImpl());

        NavigationManager.init(new NavigationManagerImpl());

        AnalyticsHelper.setAnalytics(new AnalyticsImpl());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.canDrawOverlays(this))
                startWidget(this);
        }

    }

    @Override
    public String getServerEndpoint() {
        return Constants.API;
    }

    public static void startWidget(Context context) {
        //if (checkWidgetWindowPermissionPermission(this))
        if (AppUser.getInstance().get() != null && AppUser.getInstance().get().isWidget()) {
            Intent serviceIntent = new Intent(context, BackgroundService.class);
            context.startService(serviceIntent);
            PendingIntent pintent = PendingIntent.getService(context, 0, serviceIntent, 0);
            AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarm.cancel(pintent);
            alarm.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), 60000, pintent);
        }
    }


    public static void syncGrimaces() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Cursor grimaceCursor = ClickclapApp.getInstance().getContentResolver().query(ContentDescriptor.Grimaces.URI,
                        null,
                        ContentDescriptor.Grimaces.Cols.TO_UPLOAD + " = 1",
                        null,
                        null);

                if (grimaceCursor == null || grimaceCursor.getCount() == 0) {
                    getSpiceManager().execute(
                            new GetGrimacesRequest(Grimace.TYPE_COLLECTION),
                            new GetGrimacesCacheRequestListener());
                }

                if (grimaceCursor != null && !grimaceCursor.isClosed()) {
                    grimaceCursor.close();
                }
            }
        }).start();
    }
}
