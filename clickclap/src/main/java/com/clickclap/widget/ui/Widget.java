package com.clickclap.widget.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.WindowManager;

import com.clickclap.ClickclapApp;
import com.clickclap.R;
import com.clickclap.activities.SplashActivity;
import com.clickclap.models.Grimace;
import com.clickclap.widget.enums.Messenger;
import com.clickclap.widget.helper.PrefHelper;
import com.humanet.humanetcore.utils.FilePathHelper;

import java.io.File;
import java.util.List;

/**
 * Created by Denis on 03.06.2015.
 */
public class Widget implements OpenWidgetListener, OnWidgetButtonMoveListener, OnPickerSelectListener {

    private static volatile Widget sInstance;

    public static Widget getInstance() {
        Widget localInstance = sInstance;
        if (localInstance == null) {
            synchronized (Widget.class) {
                localInstance = sInstance;
                if (localInstance == null) {
                    sInstance = localInstance = new Widget();
                }
            }
        }
        return localInstance;
    }

    private static final String TAG = Widget.class.getSimpleName();

    private final Context mContext;

    private WidgetButton mWidgetButton;
    private Dialog mPickerDialog;

    private WindowManager.LayoutParams mLayoutParams;
    private WindowManager mWindowManager;

    private boolean mPickerOpen;

    private Widget() {
        mContext = ClickclapApp.getInstance();
        init();
    }

    public void init() {
        mWidgetButton = new WidgetButton(mContext);

        mLayoutParams = new WindowManager.LayoutParams(
                mWidgetButton.getButtonSize(),
                mWidgetButton.getButtonSize(),
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);

        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        mWindowManager.addView(mWidgetButton, mLayoutParams);

        mWidgetButton.setOpenWidgetListener(this);
        mWidgetButton.setOnWidgetButtonMoveListener(this);
        mWidgetButton.setInitialPosition();
    }

    public void hide() {
        hidePicker();
        mWidgetButton.hide();
    }

    public boolean isVisible() {
        return mWidgetButton.isVisible() || mPickerOpen;
    }


    public void showButton() {
        mWidgetButton.show();
    }

    @Override
    public void onOpenWidget() {
        mWidgetButton.hide();

        mPickerOpen = true;

        WidgetPicker widgetPicker = new WidgetPicker(mContext);
        widgetPicker.setOnPickerSelectListener(this);
        mPickerDialog = new Dialog(mContext, R.style.widget_dialog);
        mPickerDialog.setContentView(widgetPicker);
        mPickerDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        mPickerDialog.show();

        mPickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mPickerOpen = false;
                showButton();
                mPickerDialog = null;
            }
        });
    }

    private void hidePicker() {
        if (mPickerDialog != null) {
            mPickerDialog.dismiss();
            mPickerDialog = null;
        }
    }

    @Override
    public void onWidgetButtonMoved(int x, int y) {
        mLayoutParams.x = x;
        mLayoutParams.y = y;

        mWindowManager.updateViewLayout(mWidgetButton, mLayoutParams);
    }

    @Override
    public int getWidgetXPosition() {
        return mLayoutParams.x;
    }

    @Override
    public int getWidgetYPosition() {
        return mLayoutParams.y;
    }

    @Override
    public void onNewRecordSmileSelected(int smileId) {
        hidePicker();
        showButton();

        PrefHelper.setIntPreference("create_grimace", smileId);
        Intent intent = new Intent(mContext, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    @Override
    public void onShareSmileSelected(Grimace grimace) {
        hidePicker();
        showButton();

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shareIntent.setType("image/*");

        String type = com.clickclap.widget.Utils.getCurLaunchedAppPackageName(mContext);
        File file = FilePathHelper.getGrimaceFileName(grimace.getUrl());


        File imagePath = FilePathHelper.getGrimacesPath();
        File newFile = new File(imagePath, file.getName());
        Uri contentUri = FileProvider.getUriForFile(mContext, "com.clickclap.fileprovider", newFile);


        Log.i(TAG, "file " + newFile.getName() + " is exists " + newFile.exists());

        shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);

        if (Messenger.getByPackageName(type) != null) {
            // gets the list of intents that can be loaded.
            List<ResolveInfo> resInfo = mContext.getPackageManager().queryIntentActivities(shareIntent, 0);
            if (!resInfo.isEmpty()) {
                for (ResolveInfo info : resInfo) {
                    if (info.activityInfo.packageName.toLowerCase().contains(type) || info.activityInfo.name.toLowerCase().contains(type)) {
                        shareIntent.setPackage(info.activityInfo.packageName);
                        break;
                    }
                }
            }
        }

        mContext.startActivity(shareIntent);
    }

    //---=---
    static Point getSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        Point size = new Point();
        wm.getDefaultDisplay().getSize(size);
        return size;
    }
}
