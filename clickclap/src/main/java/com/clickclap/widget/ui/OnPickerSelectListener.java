package com.clickclap.widget.ui;

import com.clickclap.models.Grimace;

/**
 * Created by ovi on 31.05.2016.
 */
public interface OnPickerSelectListener {

    void onNewRecordSmileSelected(int smileId);
    void onShareSmileSelected(Grimace grimace);

}
