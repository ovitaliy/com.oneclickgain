package com.clickclap.widget.service;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;

import com.clickclap.widget.Utils;
import com.clickclap.widget.helper.PrefHelper;
import com.clickclap.widget.ui.Widget;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.utils.NetworkUtil;

/**
 * Created by Denis on 28.05.2015.
 */
public class BackgroundService extends Service {
    private static final String TAG = BackgroundService.class.getSimpleName();
    private static final Handler sHandler = new Handler(Looper.getMainLooper());

    private static final String BCAST_CONFIGCHANGED = "android.intent.action.CONFIGURATION_CHANGED";

    @Nullable
    private volatile Widget mWidget;

    private static final int NOT_LAUNCHED_TIMEOUT = 1000;
    private static final int LAUNCHED_TIMEOUT = 200;

    private int mSleepTime = NOT_LAUNCHED_TIMEOUT;

    Thread mThread;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter filter = new IntentFilter();
        filter.addAction(BCAST_CONFIGCHANGED);
        registerReceiver(mBroadcastReceiver, filter);
    }

    private static final String[] EXCLUDE_LIST = new String[]{
//            "8c:3a:e3:99:55:19",//d
            "50:55:27:A9:9E:B5"//i
    };

    private boolean isExcludedDevice() {
        String macAddress = NetworkUtil.getMACAddress();
        if (macAddress != null) {
            for (String excludedMac : EXCLUDE_LIST) {
                if (excludedMac.equalsIgnoreCase(macAddress)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean isAllowedToShowWidget = AppUser.getInstance().get() != null && AppUser.getInstance().get().isWidget();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (!Settings.canDrawOverlays(getApplicationContext()))
                return Service.STOP_FOREGROUND_REMOVE;

        try {
            mWidget = Widget.getInstance();
        } catch (java.lang.SecurityException ignore) {

        }

        if (mWidget != null && !isExcludedDevice() && isAllowedToShowWidget) {
            if (mThread == null || !mThread.isAlive()) {
                mThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (!mThread.isInterrupted()) {
                            try {
                                Thread.sleep(mSleepTime);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            boolean appNotLaunched = !"com.clickclap".equals(Utils.getRunningTask(getApplicationContext()));

                            if (appNotLaunched && !mWidget.isVisible() && !isScreenLocked()) {
                                mSleepTime = LAUNCHED_TIMEOUT;
                                sHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!PrefHelper.isHided()) {
                                            mWidget.showButton();
                                        }
                                    }
                                });
                            }
                            if (!appNotLaunched || isScreenLocked()) {
                                mSleepTime = NOT_LAUNCHED_TIMEOUT;
                                sHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        PrefHelper.setHided(false);
                                        mWidget.hide();
                                    }
                                });
                            }
                        }
                    }
                });
                mThread.start();
            }
        }

        return Service.START_STICKY;
    }

    private boolean isScreenLocked() {
        KeyguardManager myKM = (KeyguardManager) getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
        return myKM.inKeyguardRestrictedInputMode();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        //Unregister receiver to avoid memory leaks
        if (mBroadcastReceiver != null) {
            try {
                unregisterReceiver(mBroadcastReceiver);
            } catch (Exception ignore) {
            }
        }

        if (mThread != null) {
            try {
                mThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mThread = null;
        }
    }

    public BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent myIntent) {
            if (myIntent.getAction().equals(BCAST_CONFIGCHANGED)) {
                if (mWidget != null) {
                    mWidget.hide();
                    mWidget.init();
                }
            }
        }
    };
}
