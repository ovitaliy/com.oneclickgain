package com.clickclap.widget.enums;

/**
 * Created by Denis on 28.05.2015.
 */
public enum Messenger {
    VIBER("viber"),
    WATSAPP("whatsapp"),
    VK("vkontakte.android"),
    FACEBOOK("facebook.katana"),
    FACEBOOK_MSGR("facebook.orca"),
    TELEGRAM("telegram"),
    GMAIL("android.gm"),
    INBOX("inbox"),
    MMS("android.mms"),
    EMAIL("android.email"),
    SKYPE("skype.raider"),
    LINE("line.android"),
    WECHAT("tencent.mm"),
    KIK("kik.android"),
    ICQ("icq.mobile"),
    QIP("qip.mobile"),
    YAHOO("yahoo.mobile"),
    HANGOUTS("android.talk");

    public static Messenger getByPackageName(String packageName) {
        if (packageName != null) {
            for (Messenger messenger : values()) {
                if (packageName.contains(messenger.getPackageName())) {
                    return messenger;
                }
            }
        }
        return null;
    }

    String mPackageName;

    Messenger(String packageName) {
        mPackageName = packageName;
    }

    public String getPackageName() {
        return mPackageName;
    }
}