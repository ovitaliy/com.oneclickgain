package com.clickclap.fragments.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clickclap.R;
import com.humanet.humanetcore.fragments.registration.BaseVerificationFragment;

/**
 * Created by ovi on 21.05.2016.
 */
public class VerificationFragment extends BaseVerificationFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_verification, container, false);
    }
}
