package com.clickclap.fragments.video;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.activities.RegistrationActivity;
import com.clickclap.models.enums.Emotion;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.activities.AgreementActivity;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.views.widgets.CirclePickerView;

import java.util.ArrayList;
import java.util.Arrays;

public class SelectEmoticonFragment extends BaseTitledFragment implements View.OnClickListener {


    private TextView mHeader1;
    private TextView mHeader2;

    private CirclePickerView mSmileContainer;
    private int mCurrentSmileId = -1;
    private OnSelectSmileListener mOnSelectSmileListener;

    public static SelectEmoticonFragment newInstance() {
        SelectEmoticonFragment fragment = new SelectEmoticonFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private void setHeaderText() {
        if (getActivity() instanceof RegistrationActivity) {
            mHeader1.setText(R.string.welcome1);
            mHeader2.setText(R.string.welcome2);
            mHeader2.setVisibility(View.VISIBLE);
        } else {
            mHeader1.setText(R.string.select_emotion);
            mHeader2.setVisibility(View.GONE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_emoticon, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        mSmileContainer = (CirclePickerView) v.findViewById(R.id.smiles_picker);
        v.findViewById(R.id.btn_next).setOnClickListener(this);
        v.findViewById(R.id.agreement).setOnClickListener(this);

        mHeader1 = (TextView) v.findViewById(R.id.header_1);
        mHeader2 = (TextView) v.findViewById(R.id.header_2);

        setHeaderText();

        if (!(getActivity() instanceof RegistrationActivity)) {
            v.findViewById(R.id.agreement).setVisibility(View.GONE);
        }

        loadSmilesFromStorage(v);
    }

    @Override
    public String getTitle() {
        return null;
    }

    private void openAgreement() {
        if (getActivity() instanceof RegistrationActivity) {
            Intent intent = new Intent(getActivity(), NavigationManager.getAgreementActivityClass());
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                if (mCurrentSmileId > 0) {
                    mOnSelectSmileListener.onSmileSelected(mCurrentSmileId);
                }
                break;
            case R.id.agreement:
                openAgreement();
                break;
            default:
                getView().findViewById(R.id.btn_next).setEnabled(true);
                break;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        loadSmilesFromStorage(getView());
    }

    private void loadSmilesFromStorage(View v) {
        if (mSmileContainer == null || mSmileContainer.getChildCount() > 2) {
        } else {
            mSmileContainer.fill(new ArrayList<CirclePickerItem>(Arrays.asList(Emotion.values())),
                    mCurrentSmileId,
                    new CirclePickerView.OnPickListener() {
                        @Override
                        public void onPick(View view, CirclePickerItem element) {
                            mCurrentSmileId = element.getId();
                            getView().findViewById(R.id.btn_next).setEnabled(true);
                        }
                    });


            if (mCurrentSmileId > 0) {
                if (v != null) {
                    v.findViewById(R.id.btn_next).setEnabled(true);
                }
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mOnSelectSmileListener = (OnSelectSmileListener) activity;
    }

    public interface OnSelectSmileListener {
        void onSmileSelected(int smileId);
    }
}
