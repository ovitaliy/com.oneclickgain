package com.clickclap.fragments.grimaces;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.clickclap.R;
import com.clickclap.activities.GrimaceCaptureActivity;
import com.clickclap.api.requests.GetGrimacesRequest;
import com.clickclap.api.responses.GrimacesListResponse;
import com.clickclap.events.GrimaceReloadEvent;
import com.clickclap.models.Grimace;
import com.clickclap.models.enums.Emotion;
import com.clickclap.widgets.dialogs.ActivateWidgetDialogFragment;
import com.clickclap.widgets.grimaces.GrimacesPagerAdapter;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.events.SimpleLoaderCallbacks;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.utils.NetworkUtil;
import com.humanet.humanetcore.views.behaviour.SimpleOnTabSelectedListener;
import com.humanet.humanetcore.views.dialogs.MissedPermissionDialog;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by ovi on 23.05.2016.
 */
public class GrimaceFragment extends BaseTitledFragment {
    private final static int LOADER_ID = 13231;

    public static int OVERLAY_PERMISSION_REQ_CODE = 1111;

    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    private GrimacesPagerAdapter mPagerAdapter;

    private LinearLayout mEmotionsContainer;

    private static final int COUNT_PER_PAGE = 3;

    private int mCellSize;

    private int mType;


    @Override
    public String getTitle() {
        return getString(R.string.menu_grimaces);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_grimace, container, false);
    }

    private String[] getTabNames() {
        return new String[]{
                getString(R.string.grimaces_tab1),
                getString(R.string.grimaces_tab2),
                getString(R.string.grimaces_tab3),
        };
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mCellSize = (int) ((float) App.WIDTH / (COUNT_PER_PAGE + 1));

        mPagerAdapter = new GrimacesPagerAdapter(mCellSize);

        mViewPager = (ViewPager) view.findViewById(R.id.grimaces_pager);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setLayoutParams(new RelativeLayout.LayoutParams(mCellSize * 3, RelativeLayout.LayoutParams.MATCH_PARENT));
        mViewPager.setX(mCellSize);

        mEmotionsContainer = (LinearLayout) view.findViewById(R.id.emotions_container);
        mEmotionsContainer.setLayoutParams(new RelativeLayout.LayoutParams(mCellSize, RelativeLayout.LayoutParams.MATCH_PARENT));

        mTabLayout = (TabLayout) view.findViewById(com.humanet.humanetcore.R.id.tabhost);
        final String[] tabNames = getTabNames();
        for (String tabName : tabNames) {
            mTabLayout.addTab(mTabLayout.newTab().setText(tabName));
        }

        mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);

        openTab(0);

        checkWidget();
    }

    private void loadGrimaces(int type) {
        getSpiceManager().execute(
                new GetGrimacesRequest(type),
                new SimpleRequestListener<GrimacesListResponse>());
    }


    private void checkWidget() {
        if (checkPermissions()) {
            if (AppUser.getInstance().get() != null && !AppUser.getInstance().get().isWidget()) {
                if (!NetworkUtil.isOfflineMode()) {
                    ActivateWidgetDialogFragment.newInstance().show(getActivity().getSupportFragmentManager(), "widget");
                    AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.BUY_WIDGET);
                }
            }
        } else {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getActivity().getPackageName()));
            startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (!checkPermissions()) {
                MissedPermissionDialog.show(getContext(), (dialogInterface, i) -> {
                });
            } else {
                checkWidget();
            }
        }
    }

    private boolean checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(getActivity()))
            return false;
        else return true;
    }

    private SimpleOnTabSelectedListener mOnTabSelectedListener = new SimpleOnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.setCurrentItem(tab.getPosition(), false);
            openTab(tab.getPosition());
        }
    };


    public void openTab(int position) {
        mViewPager.setCurrentItem(0);
        mPagerAdapter.clear();
        switch (position) {
            case 0:
                mType = Grimace.TYPE_COLLECTION;
                break;
            case 1:
                mType = Grimace.TYPE_USER;
                break;
            case 2:
                mType = Grimace.TYPE_MARKET;
                break;
        }
        Bundle args = new Bundle(1);
        args.putInt(Constants.PARAMS.TYPE, mType);
        getLoaderManager().restartLoader(LOADER_ID, args, mCursorLoaderCallbacks);
        loadGrimaces(mType);
    }

    private void loadCollection(Cursor data) {
        ArrayList<LinkedHashMap<Integer, Grimace[]>> pages = new ArrayList<>();
        mEmotionsContainer.removeAllViews();

        ArrayList<Integer> emotions = new ArrayList<>();
        ArrayList<Grimace> list = new ArrayList<>();

        if (data != null && data.moveToFirst()) {
            do {
                Grimace grimace = Grimace.fromCursor(data);
                list.add(grimace);
                int smileId = grimace.getSmileId();
                if (!emotions.contains(smileId)) {
                    emotions.add(smileId);
                }
            } while (data.moveToNext());
        }

        Collections.sort(emotions);

        for (int emotion : emotions) {
            addEmotionToContainer(emotion);
        }

        if (mType == Grimace.TYPE_COLLECTION) {//for grimace tab should be showed all grimaces
            for (Emotion emotion : Emotion.values()) {
                int smileId = emotion.getId();
                if (!emotions.contains(smileId)) {
                    emotions.add(smileId);
                    addEmotionToContainer(smileId);
                }
            }
        }

        mEmotionsContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (mEmotionsContainer.getHeight() > 0) {
                    mViewPager.getLayoutParams().height = mEmotionsContainer.getHeight();
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        //noinspection deprecation
                        mEmotionsContainer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        mEmotionsContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }
            }
        });
        mEmotionsContainer.invalidate();

        int positionOnCurPage = 0;
        int prevSmileId = 0;
        int pageNum = 0;
        for (Grimace grimace : list) {
            int curSmileId = grimace.getSmileId();
            if (curSmileId != prevSmileId) {
                pageNum = 0;
                positionOnCurPage = 0;
            } else if (positionOnCurPage >= COUNT_PER_PAGE) {
                pageNum++;
                positionOnCurPage = 0;
            }

            if (pageNum > pages.size() - 1) {
                LinkedHashMap<Integer, Grimace[]> page = new LinkedHashMap<>();
                for (int emotion : emotions) {
                    page.put(emotion, new Grimace[COUNT_PER_PAGE]);
                }
                positionOnCurPage = 0;
                pages.add(page);
            }

            LinkedHashMap<Integer, Grimace[]> page = pages.get(pageNum);

            Grimace[] grimaces = page.get(curSmileId);
            grimaces[positionOnCurPage] = grimace;

            prevSmileId = curSmileId;
            positionOnCurPage++;
        }

        if (mType == Grimace.TYPE_COLLECTION) {
            if (pages == null || pages.size() == 0) {
                LinkedHashMap<Integer, Grimace[]> page = new LinkedHashMap<>();
                for (int emotion : emotions) {
                    page.put(emotion, new Grimace[COUNT_PER_PAGE]);
                }
                pages.add(page);
            }

            //We should insert stub into end of any emotions row, wich will means than you can create new grimace
            ArrayList<Integer> emotionsWithStub = new ArrayList<>(emotions.size());
            for (LinkedHashMap<Integer, Grimace[]> page : pages) {
                for (Object o : page.entrySet()) {
                    Map.Entry row = (Map.Entry) o;
                    if (!emotionsWithStub.contains(row.getKey())) {
                        Grimace[] grimaces = (Grimace[]) row.getValue();
                        for (int i = 0; i < grimaces.length; i++) {
                            Grimace grimace = grimaces[i];
                            if (grimace == null) {
                                grimaces[i] = new Grimace();//stub
                                emotionsWithStub.add((Integer) row.getKey());
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (!mPagerAdapter.isNewDataIdentical(pages)) {
            mPagerAdapter.setData(pages, mType);
        }
    }

    private void addEmotionToContainer(final int emotionId) {
        final Activity context = getActivity();
        if (context != null) {
            int margin = (int) ConverterUtil.dpToPix(getActivity(), 5);
            ImageView imageView = new ImageView(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mCellSize - (margin * 2), mCellSize - (margin * 2));
            params.setMargins(margin, margin, margin, margin);
            imageView.setLayoutParams(params);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            ImageLoader.getInstance().displayImage(Emotion.getByIdNonNull(emotionId).getDrawable(), imageView);
            mEmotionsContainer.addView(imageView);

            if (mType == Grimace.TYPE_COLLECTION) {
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        GrimaceCaptureActivity.startNewInstance(context, 100, Emotion.getByIdNonNull(emotionId).getId());
                    }
                });
            }
        }
    }


    private LoaderManager.LoaderCallbacks<Cursor> mCursorLoaderCallbacks = new SimpleLoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            int type = args.getInt(Constants.PARAMS.TYPE);
            return new CursorLoader(getActivity(),
                    ContentDescriptor.Grimaces.URI,
                    null,
                    ContentDescriptor.Grimaces.Cols.TYPE + " = " + type,
                    null,
                    ContentDescriptor.Grimaces.Cols.ID_SMILE + " DESC");

        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, final Cursor data) {
            loadCollection(data);
        }

    };

    /**
     * catch event on grimace uploading complete.
     */
    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(GrimaceReloadEvent reloadEvent) {
        mViewPager.setCurrentItem(0);
        mPagerAdapter.clear();
        Bundle args = new Bundle(1);
        args.putInt(Constants.PARAMS.TYPE, mType);
        getLoaderManager().restartLoader(LOADER_ID, args, mCursorLoaderCallbacks);

        loadGrimaces(mType);
    }

}
