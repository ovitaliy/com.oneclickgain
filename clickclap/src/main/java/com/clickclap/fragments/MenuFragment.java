package com.clickclap.fragments;

import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.clickclap.ClickclapApp;
import com.clickclap.R;
import com.clickclap.activities.MainActivity;
import com.clickclap.activities.SplashActivity;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.fragments.BaseMenuFragment;
import com.humanet.humanetcore.model.UserInfo;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by ovi on 22.05.2016.
 */
public class MenuFragment extends BaseMenuFragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);


        view.findViewById(R.id.emoticons).setOnClickListener(this);
        view.findViewById(R.id.vistory).setOnClickListener(this);
        view.findViewById(R.id.grimaces).setOnClickListener(this);
        view.findViewById(R.id.balance).setOnClickListener(this);
        view.findViewById(R.id.navigation_info).setOnClickListener(this);


        setCopyright(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.avatar:
            case R.id.first_name:
                mDrawerLayout.closeDrawers();
                mOnViewProfileListener.onViewProfile(AppUser.getInstance().getUid());
                break;

            default:
                mOnNavigateListener.onNavigateByViewId(v.getId());
                mDrawerLayout.closeDrawers();

                break;
        }
    }


    @Override
    protected void fillUserInfo(UserInfo userInfo) {
        View v = getView();
        if (v == null || userInfo == null) {
            return;
        }

        v.findViewById(com.humanet.humanetcore.R.id.avatar).setOnClickListener(this);

        TextView textView;
        String text;

        textView = (TextView) v.findViewById(com.humanet.humanetcore.R.id.first_name);
        textView.setText(String.format(Locale.getDefault(), "%s %s", userInfo.getFName(), userInfo.getLName()));
        textView.setOnClickListener(this);

        textView = (TextView) v.findViewById(com.humanet.humanetcore.R.id.location);
        text = userInfo.getCountry() + ", " + userInfo.getCity();
        textView.setText(text);


        if (userInfo.getAvatarSmall() != null)
            ImageLoader.getInstance().displayImage(userInfo.getAvatarSmall(), (ImageView) v.findViewById(com.humanet.humanetcore.R.id.avatar));

        // set language instead balance(or rating) unlike in other apps
        textView = (TextView) getView().findViewById(R.id.language);
        textView.setText(userInfo.getLanguage().getTitle());

        ClickclapApp.startWidget(getActivity());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.canDrawOverlays(getActivity()))
                ClickclapApp.startWidget(getActivity());
        }
    }

    protected DrawerLayout.DrawerListener createDrowerToogle() {
        return new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,
                R.drawable.btn_menu,/* DrawerLayout object */
                com.humanet.humanetcore.R.string.app_name,  /* "open drawer" description for accessibility */
                com.humanet.humanetcore.R.string.app_name  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float moveFactor = (drawerView.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    mContentFrame.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    mContentFrame.startAnimation(anim);

                    lastTranslate = moveFactor;
                }
            }
        };
    }

    @Override
    protected void initDrawer() {
        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        ((MainActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.btn_menu));

        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                if (!isVisible())
                    return;

                if (getActivity().getSupportFragmentManager().getBackStackEntryCount() == 0) {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

                    ((MainActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.btn_menu));
                } else {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    ((MainActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.btn_back));
                }
            }
        });
    }

    @Override
    protected Class newSplashActivityInstance() {
        return SplashActivity.class;
    }


    private void setCopyright(View rootView) {
        int curYear = Calendar.getInstance().get(Calendar.YEAR);
        String param = "";
        if (curYear > 2015) {
            param = " - " + curYear;
        }
        TextView copyright = (TextView) rootView.findViewById(R.id.copyright);
        copyright.setText(String.format(getString(R.string.copyright), param));
    }
}
