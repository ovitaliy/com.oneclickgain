package com.humanet.humanetcore.jobs;

import com.humanet.humanetcore.db.DBHelper;

/**
 * Created by ovi on 4/21/16.
 * <p/>
 * Clear all data from data base
 * Call on logout
 */
public class ClearDatabaseJob extends Thread {
    @Override
    public void run() {
        DBHelper.getInstance().trancateTables(DBHelper.getInstance().getWritableDatabase());
    }
}
