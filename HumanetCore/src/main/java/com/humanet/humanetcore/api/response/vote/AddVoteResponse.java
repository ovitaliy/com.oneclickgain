package com.humanet.humanetcore.api.response.vote;

import com.humanet.humanetcore.api.response.BaseResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 17.11.2015.
 */
public class AddVoteResponse extends BaseResponse {
    @SerializedName("id")
    int mId;

    public int getId() {
        return mId;
    }
}
