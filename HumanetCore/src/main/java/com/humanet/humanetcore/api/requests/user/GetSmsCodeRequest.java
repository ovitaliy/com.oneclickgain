package com.humanet.humanetcore.api.requests.user;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetSmsCodeRequest extends RetrofitSpiceRequest<BaseResponse, UserApiSet> {
    String mPhone;

    public GetSmsCodeRequest(String phone) {
        super(BaseResponse.class, UserApiSet.class);
        mPhone = phone;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("phone", mPhone);
        Log.i("API: GetSmsCodeRequest", map.toString());
        return getService().getSmsCode(App.API_APP_NAME,map);
    }
}
