package com.humanet.humanetcore.api.response.feedback;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class AddMessageResponse extends BaseResponse {

    @SerializedName("id_list")
    private int mListId;

    public int getListId() {
        return mListId;
    }

}