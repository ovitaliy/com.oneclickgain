package com.humanet.humanetcore.api.sets;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.feedback.AddMessageResponse;
import com.humanet.humanetcore.api.response.feedback.MessageListsResponse;
import com.humanet.humanetcore.api.response.feedback.MessagesResponse;

import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public interface FeedbackRestApi {

    @POST(Constants.BASE_API_URL + "/message.get")
    MessagesResponse getMessages(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/message.getList")
    MessageListsResponse getMessageLists(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/message.add")
    AddMessageResponse addMessage(@Path("appName") String appName, @Body ArgsMap options);
}