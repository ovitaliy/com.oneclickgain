package com.humanet.humanetcore.api.requests.user;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.user.AuthResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.humanet.humanetcore.utils.PrefHelper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class AuthRequest extends RetrofitSpiceRequest<AuthResponse, UserApiSet> {
    String mGcmToken;

    @Override
    public int getPriority() {
        return PRIORITY_HIGH;
    }

    public AuthRequest(String gcmToken) {
        super(AuthResponse.class, UserApiSet.class);
        mGcmToken = gcmToken;
    }

    public AuthRequest() {
        super(AuthResponse.class, UserApiSet.class);
        mGcmToken = "";
    }

    @Override
    public AuthResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(false);
        map.put("token", mGcmToken);
        map.put("os", "android");

        Log.i("API: AuthRequest", map.toString());

        AuthResponse authResponse = getService().auth(App.API_APP_NAME,map);

        if (authResponse != null) {
            String token = authResponse.getToken();
            PrefHelper.setStringPref(Constants.PREF.TOKEN, token);
        }

        return authResponse;
    }
}
