package com.humanet.humanetcore.api.sets;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.video.FlowResponse;
import com.humanet.humanetcore.model.Category;

import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by denisvasilenko on 14.09.15.
 */
public interface FlowApiSet {

    @POST(Constants.BASE_API_URL + "/flow.get")
    FlowResponse getFlow(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/flow.getCategories")
    Category.Response getCategories(@Path("appName") String appName, @Body ArgsMap options);

}
