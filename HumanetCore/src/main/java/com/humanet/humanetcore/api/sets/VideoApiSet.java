package com.humanet.humanetcore.api.sets;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseCoinsResponse;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.response.video.AddVideoResponse;
import com.humanet.humanetcore.api.response.video.CommentsResponse;

import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by ovitali on 13.10.2015.
 */
public interface VideoApiSet {

    @POST(Constants.BASE_API_URL + "/video.add")
    AddVideoResponse addVideo(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/video.share")
    BaseResponse shareVideo(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/video.complaint")
    BaseResponse complaintVideo(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/video.guess")
    BaseResponse guessVideoSmile(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/video.pay")
    BaseCoinsResponse payForVideo(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/video.mark")
    BaseResponse markVideo(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/video.update")
    BaseResponse updateVideo(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/video.getComments")
    CommentsResponse getComments(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/video.comment")
    BaseResponse commentVideo(@Path("appName") String appName, @Body ArgsMap options);


    /**
     * edit user profile avatar
     */
    @POST(Constants.BASE_API_URL + "/user.edit")
    BaseResponse editUserInfo(@Path("appName") String appName, @Body ArgsMap options);
}
