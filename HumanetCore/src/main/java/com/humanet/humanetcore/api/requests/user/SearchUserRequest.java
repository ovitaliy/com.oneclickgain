package com.humanet.humanetcore.api.requests.user;

import android.content.ContentValues;
import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.user.UserSearchResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.UserFinded;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.List;
import java.util.Set;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class SearchUserRequest extends RetrofitSpiceRequest<UserSearchResponse, UserApiSet> {
    private ArgsMap mParams;

    public ArgsMap getParams() {
        return mParams;
    }

    public SearchUserRequest(ArgsMap argsMap) {
        super(UserSearchResponse.class, UserApiSet.class);
        mParams = argsMap;
    }

    @Override
    public UserSearchResponse loadDataFromNetwork() throws Exception {
        Log.i("API: SearchUser", mParams.toString());
        UserSearchResponse response = getService().searchUser(App.API_APP_NAME,mParams);

        Set<String> keys = mParams.keySet();
        String[] required = keys.toArray(new String[keys.size()]);
        for (int i = 0; i < required.length; i++) {
            required[i] = required[i] + "~" + mParams.get(required[i]);
        }

        if (response != null && response.isSuccess()) {
            List<UserFinded> list = response.getUsers();

            if (list != null) {

                ContentValues[] contentValues = new ContentValues[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    UserFinded user = list.get(i);

                    user.setRequiredFields(required);

                    contentValues[i] = user.toContentValues();
                }
                App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.UserSearchResult.URI, contentValues);
            }

        }
        return response;
    }
}

