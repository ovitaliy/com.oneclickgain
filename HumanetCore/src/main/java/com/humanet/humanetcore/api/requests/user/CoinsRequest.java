package com.humanet.humanetcore.api.requests.user;

import android.content.ContentValues;
import android.util.Log;

import com.google.gson.Gson;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.user.CoinsHistoryListResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.Balance;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class CoinsRequest extends RetrofitSpiceRequest<CoinsHistoryListResponse, UserApiSet> {

    private String mBalanceTabType;
    private int mPosition;

    private Double mAddFundsValue;

    private String mPurchaseResponse;
    private String mPurchaseSignature;

    public static class Builder {

        private CoinsRequest mCoinsRequest;

        /**
         * build base get coins request
         */
        public Builder(String balanceTabType, int position) {
            mCoinsRequest = new CoinsRequest();

            mCoinsRequest.mBalanceTabType = balanceTabType;
            mCoinsRequest.mPosition = position;
        }

        /**
         * call this method if you need to add some funds before get balance history
         */
        public Builder addFunds(double value, String purchaseResponse, String signature) {
            mCoinsRequest.mAddFundsValue = value;
            mCoinsRequest.mPurchaseResponse = purchaseResponse;
            mCoinsRequest.mPurchaseSignature = signature;
            return this;
        }

        public CoinsRequest build() {
            return mCoinsRequest;
        }

    }


    private CoinsRequest() {
        super(CoinsHistoryListResponse.class, UserApiSet.class);

    }

    private long mLastId;

    public void setLastId(long lastId) {
        mLastId = lastId;
    }

    @Override
    public CoinsHistoryListResponse loadDataFromNetwork() throws Exception {
        if (mAddFundsValue != null) {
            addFunds();
        }

        return getCoinsHistoryResponse();
    }

    private void addFunds() {
        ArgsMap map = new ArgsMap();
        map.put("coins", mAddFundsValue);
        map.put("purchase", mPurchaseResponse);
        map.put("signature", mPurchaseSignature);
        Log.i("API: AddCoins", map.toString());
        getService().addCoins(App.API_APP_NAME, map);
    }

    private CoinsHistoryListResponse getCoinsHistoryResponse() throws Exception {
        ArgsMap args = new ArgsMap(true);
        args.put("type", mBalanceTabType);
        if (mLastId > 0) {
            args.put("last", mLastId);
        } else if (mAddFundsValue != null){
           App.getInstance().getContentResolver().delete(ContentDescriptor.Balances.URI, ContentDescriptor.Balances.whereByTabType(mPosition), null);
        }
        Log.i("API: GetCoinsHistory", args.toString());
        CoinsHistoryListResponse response = getService().getCoinsHistory(App.API_APP_NAME, args);

        List<Balance> history = response.getHistory();

        if (history != null) {

            int count = history.size();
            List<ContentValues> contentValueses = new ArrayList<>(count);
            for (int i = 0; i < count; i++) {
                try {
                    Balance balance = history.get(i);
                    contentValueses.add(balance.toContentValues(mPosition));
                } catch (Exception ex) {
                    Log.e("GetBalanceJob", "Exception" + new Gson().toJson(history.get(i)));
                }
            }

            ContentValues[] contentValues = new ContentValues[contentValueses.size()];
            contentValues = contentValueses.toArray(contentValues);

            App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Balances.URI, contentValues);
        }

        return response;
    }
}
