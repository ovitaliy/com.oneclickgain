package com.humanet.humanetcore.api.response.feedback;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.model.Feedback;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class MessageListsResponse extends BaseResponse {

    @SerializedName("lists")
    List<Feedback> mLists;

    public List<Feedback> getLists() {
        return mLists;
    }
}