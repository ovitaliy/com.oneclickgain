package com.humanet.humanetcore.api.requests.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.SparseArray;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.sets.DatabaseApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.Country;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovitali on 12.10.2015.
 */
public class GetCountryRequest extends RetrofitSpiceRequest<Country[], DatabaseApiSet> {

    public GetCountryRequest() {
        super(Country[].class, DatabaseApiSet.class);
    }

    @Override
    public Country[] loadDataFromNetwork() throws Exception {
        Country[] countries = loadFromDataBase();

        if (countries.length > 200 && isDataValid(countries)) {
            return countries;
        }

        countries = getService().getCountries(App.API_APP_NAME, new ArgsMap(true)).getCountries();

        int count = countries.length;
        SparseArray<Country> countriesList = new SparseArray<>(count);
        ContentValues[] contentValues = new ContentValues[count];
        for (int i = 0; i < count; i++) {
            Country country = countries[i];
            countriesList.put(country.getId(), country);
            contentValues[i] = country.toContentValues();
        }

        App.getInstance().getContentResolver().delete(ContentDescriptor.Countries.URI,
                null,
                null);

        App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Countries.URI,
                contentValues);

        return countries;
    }

    private Country[] loadFromDataBase() {
        Cursor cursor = App.getInstance().getContentResolver().query(
                ContentDescriptor.Countries.URI,
                null,
                null, null, null
        );
        List<Country> countries = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            do {
                countries.add(Country.fromCursor(cursor));
            } while (cursor.moveToNext());
            cursor.close();
        }

        return countries.toArray(new Country[countries.size()]);
    }

    private boolean isDataValid(Country[] countries) {
        for (Country country : countries) {
            if (country.getTitle() == null)
                return false;
        }
        return true;
    }
}
