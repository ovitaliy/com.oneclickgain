package com.humanet.humanetcore.api.requests.user;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.user.CoinsHistoryListResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by ovi on 1/22/16.
 *
 * This reequest is part of {@link CoinsRequest}
 */
@Deprecated
public class AddCoinsRequest extends RetrofitSpiceRequest<CoinsHistoryListResponse, UserApiSet> {

    private double mCoins;

    public AddCoinsRequest(double coins) {
        super(CoinsHistoryListResponse.class, UserApiSet.class);

        mCoins = coins;
    }

    @Override
    public CoinsHistoryListResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap();
        map.put("coins", mCoins);
        Log.i("API: AddCoins", map.toString());
        return getService().addCoins(App.API_APP_NAME,map);
    }
}
