package com.humanet.humanetcore.api.requests.vote;

import android.content.ContentValues;
import android.database.Cursor;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.sets.VoteApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class SendVoteRequest extends RetrofitSpiceRequest<BaseResponse, VoteApiSet> {
    int mVoteId;
    int mOptionId;

    public SendVoteRequest(int voteId, int optionId) {
        super(BaseResponse.class, VoteApiSet.class);
        mVoteId = voteId;
        mOptionId = optionId;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_vote", mVoteId);
        map.put("id_option", mOptionId);
        BaseResponse response = getService().sendVote(App.API_APP_NAME, map);

        Cursor cursor = App.getInstance().getContentResolver().query(ContentDescriptor.Votes.URI,
                new String[]{ContentDescriptor.Votes.Cols.VOTES_TOTAL},
                "_id = " + mVoteId,
                null,
                null);

        int curTotalVotes = 0;
        try {
            if (cursor != null && cursor.moveToFirst()) {
                curTotalVotes = cursor.getInt(0);
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        ContentValues values = new ContentValues(3);
        values.put(ContentDescriptor.Votes.Cols.MY_VOTE, mOptionId);
        values.put(ContentDescriptor.Votes.Cols.VOTED, 1);
        values.put(ContentDescriptor.Votes.Cols.VOTES_TOTAL, curTotalVotes + 1);

        //Update vote info
        App.getInstance().getContentResolver().update(ContentDescriptor.Votes.URI,
                values,
                "_id = " + mVoteId,
                null);

        cursor = App.getInstance().getContentResolver().query(ContentDescriptor.VoteOptions.URI,
                new String[]{ContentDescriptor.VoteOptions.Cols.VOTES},
                ContentDescriptor.VoteOptions.Cols.VOTE_ID + " = " + mVoteId + " AND " + ContentDescriptor.VoteOptions.Cols.OPTION_ID + " = " + mOptionId,
                null,
                null);

        int curVotes = 0;
        try {
            if (cursor != null && cursor.moveToFirst()) {
                curVotes = cursor.getInt(0);
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        values.clear();
        values.put(ContentDescriptor.VoteOptions.Cols.VOTES, curVotes + 1);

        //Update vote option info
        App.getInstance().getContentResolver().update(ContentDescriptor.VoteOptions.URI,
                values,
                ContentDescriptor.VoteOptions.Cols.VOTE_ID + " = " + mVoteId + " AND " + ContentDescriptor.VoteOptions.Cols.OPTION_ID + " = " + mOptionId,
                null);

        return response;
    }
}
