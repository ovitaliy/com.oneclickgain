package com.humanet.humanetcore.api.requests.vote;

import android.content.ContentValues;
import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.vote.VoteListsResponse;
import com.humanet.humanetcore.api.sets.VoteApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.vote.Vote;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetMyVotesListRequest extends RetrofitSpiceRequest<VoteListsResponse, VoteApiSet> {

    public GetMyVotesListRequest() {
        super(VoteListsResponse.class, VoteApiSet.class);
    }

    @Override
    public VoteListsResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);

        Log.i("API: GetMyVotesList", map.toString());

        VoteListsResponse response = getService().getMyVoteList(App.API_APP_NAME,map);

        Vote[] voteList = response.getVotes();

        if (voteList != null) {
            int count = voteList.length;

            ContentValues[] contentValues = new ContentValues[count];
            for (int i = 0; i < count; i++) {
                ContentValues values = voteList[i].toContentValues();

                values.put(ContentDescriptor.Votes.Cols.MY_INITIATION, 1);
                contentValues[i] = values;
            }

            App.getInstance().getContentResolver().delete(ContentDescriptor.Votes.URI, ContentDescriptor.Votes.Cols.MY_INITIATION + " = 1", null);
            App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Votes.URI, contentValues);
        }
        return response;
    }
}
