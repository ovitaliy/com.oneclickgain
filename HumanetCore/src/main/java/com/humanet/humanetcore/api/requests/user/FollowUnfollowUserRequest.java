package com.humanet.humanetcore.api.requests.user;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class FollowUnfollowUserRequest extends RetrofitSpiceRequest<BaseResponse, UserApiSet> {
    boolean mFollow;
    int mUserId;

    public FollowUnfollowUserRequest(int userId, boolean follow) {
        super(BaseResponse.class, UserApiSet.class);
        mUserId = userId;
        mFollow = follow;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_user", mUserId);
        if (mFollow) {
            return getService().followUser(App.API_APP_NAME,map);
        } else {
            return getService().unfollowUser(App.API_APP_NAME,map);
        }
    }
}
