package com.humanet.humanetcore.api.sets;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.model.City;
import com.humanet.humanetcore.model.Country;

import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by ovitali on 12.10.2015.
 */
public interface DatabaseApiSet {

    @POST(Constants.BASE_API_URL + "/database.getCountries")
    Country.Response getCountries(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/database.getCities")
    City.Response getCities(@Path("appName") String appName, @Body ArgsMap options);

}
