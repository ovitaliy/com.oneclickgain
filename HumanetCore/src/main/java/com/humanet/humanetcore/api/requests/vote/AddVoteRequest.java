package com.humanet.humanetcore.api.requests.vote;

import android.content.ContentValues;
import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.response.vote.AddVoteResponse;
import com.humanet.humanetcore.api.sets.VoteApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.fragments.vote.VoteTabFragment;
import com.humanet.humanetcore.model.enums.Language;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class AddVoteRequest extends RetrofitSpiceRequest<BaseResponse, VoteApiSet> {
    String[] mOptions;
    String mDescription;

    public AddVoteRequest(String description, String[] options) {
        super(BaseResponse.class, VoteApiSet.class);
        mDescription = description;
        mOptions = options;
    }

    @Override
    public AddVoteResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = buildRequestArgs(mDescription, mOptions);

        Log.i("API: AddVote", map.toString());

        AddVoteResponse response = getService().addVote(App.API_APP_NAME, map);

        if (response != null && response.isSuccess()) {
            int voteId = response.getId();

            Language lang = AppUser.getInstance().get().getLanguage();

            ContentValues values = new ContentValues();
            values.put("_id", voteId);
            values.put(ContentDescriptor.Votes.Cols.TYPE, -1);
            values.put(ContentDescriptor.Votes.Cols.STATUS, VoteTabFragment.CREATED);
            values.put(ContentDescriptor.Votes.Cols.MY_INITIATION, 1);
            switch (lang) {
                case RUS:
                    values.put(ContentDescriptor.Votes.Cols.DESCR_RUS, mDescription);
                    values.put(ContentDescriptor.Votes.Cols.TITLE_RUS, mDescription);
                    break;
                case ENG:
                    values.put(ContentDescriptor.Votes.Cols.DESCR_ENG, mDescription);
                    values.put(ContentDescriptor.Votes.Cols.TITLE_ENG, mDescription);
                    break;
                case ESP:
                    values.put(ContentDescriptor.Votes.Cols.DESCR_ESP, mDescription);
                    values.put(ContentDescriptor.Votes.Cols.TITLE_ESP, mDescription);
                    break;
            }

            App.getInstance().getContentResolver().insert(ContentDescriptor.Votes.URI, values);
        }

        return response;
    }

    public static ArgsMap buildRequestArgs(String description, String[] options) {
        ArgsMap map = new ArgsMap();
        map.put("options", options);
        map.put("title", description);
        map.put("lang", Language.getSystem().getRequestParam());

        return map;
    }

}
