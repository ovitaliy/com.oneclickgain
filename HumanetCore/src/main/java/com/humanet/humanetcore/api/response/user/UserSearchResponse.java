package com.humanet.humanetcore.api.response.user;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.model.UserFinded;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class UserSearchResponse extends BaseResponse {

    @SerializedName("users")
    List<UserFinded> mUsers;

    public List<UserFinded> getUsers() {
        return mUsers;
    }

}