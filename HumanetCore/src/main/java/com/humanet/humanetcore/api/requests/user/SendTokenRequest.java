package com.humanet.humanetcore.api.requests.user;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class SendTokenRequest extends RetrofitSpiceRequest<BaseResponse, UserApiSet> {
    String mName;
    String mNumber;

    public SendTokenRequest(String name, String number) {
        super(BaseResponse.class, UserApiSet.class);
        mName = name;
        mNumber = number;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("name", mName);
        map.put("number", mNumber);
        Log.i("API: SendTokenRequest", map.toString());
        return getService().sendToken(App.API_APP_NAME,map);
    }
}
