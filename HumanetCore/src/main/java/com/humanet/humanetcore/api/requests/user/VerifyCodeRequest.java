package com.humanet.humanetcore.api.requests.user;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.user.VerifyCodeResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class VerifyCodeRequest extends RetrofitSpiceRequest<VerifyCodeResponse, UserApiSet> {
    String mCode;

    public VerifyCodeRequest(String code) {
        super(VerifyCodeResponse.class, UserApiSet.class);
        mCode = code;
    }

    @Override
    public VerifyCodeResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("code", mCode);
        Log.i("API: VerifyCodeRequest", map.toString());
        return getService().verifySmsCode(App.API_APP_NAME,map);
    }
}
