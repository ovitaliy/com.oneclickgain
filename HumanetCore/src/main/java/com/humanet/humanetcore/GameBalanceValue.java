package com.humanet.humanetcore;

import java.util.Observable;

/**
 * Created by ovi on 4/21/16.
 */
// TODO may be can be moved to 1clickgain module
public class GameBalanceValue extends Observable {

    private static volatile GameBalanceValue sInstance;

    public static GameBalanceValue getInstance() {
        GameBalanceValue localInstance = sInstance;
        if (localInstance == null) {
            synchronized (GameBalanceValue.class) {
                localInstance = sInstance;
                if (localInstance == null) {
                    sInstance = localInstance = new GameBalanceValue();
                }
            }
        }
        return localInstance;
    }

    private float mBalance;

    public void setBalance(float balance) {
        mBalance = balance;
        setChanged();
        notifyObservers(balance);
    }

    public static float get(){
        return sInstance.mBalance;
    }
}
