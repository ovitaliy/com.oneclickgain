package com.humanet.humanetcore.interfaces;

import com.octo.android.robospice.SpiceManager;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public interface SpiceContext {
    SpiceManager getSpiceManager();
}
