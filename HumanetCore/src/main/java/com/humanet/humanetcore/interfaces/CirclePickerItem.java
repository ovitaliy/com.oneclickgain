package com.humanet.humanetcore.interfaces;

import com.humanet.humanetcore.model.ILocationModel;

/**
 * Created by Denis on 05.03.2015.
 */
public interface CirclePickerItem extends ILocationModel {
    String getDrawable();
}