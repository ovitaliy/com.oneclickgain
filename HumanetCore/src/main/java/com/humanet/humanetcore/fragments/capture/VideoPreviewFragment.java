package com.humanet.humanetcore.fragments.capture;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.model.VideoInfo;
import com.humanet.humanetcore.views.widgets.CircleLayout;
import com.humanet.humanetcore.views.widgets.CircleView;


public class VideoPreviewFragment extends BaseVideoCreationFragment {

    public static final int ACTION_CREATE = 1;
    public static final int ACTION_VIEW = 3;

    /**
     * MediaPlayer for playing our video.
     */
    private OnVideoCommandSelected mListener;

    private int mCurrentAction;

    public static VideoPreviewFragment newInstance(int action) {
        VideoPreviewFragment fragment = new VideoPreviewFragment();
        Bundle args = new Bundle();
        args.putInt("action", action);
        fragment.setArguments(args);
        return fragment;
    }


    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_preview, container, false);

        mCurrentAction = getArguments().getInt("action");

        if (NewVideoInfo.get().isAvatar() == VideoInfo.NONE) {
            ((TextView) view.findViewById(R.id.header)).setText(R.string.video_recording_title);
        }


        mListView = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
        mNextButton = (ImageButton) view.findViewById(R.id.button_done);

        CircleView circleView = (CircleView) view.findViewById(R.id.video_view);

        mImageView = circleView.getPreviewView();
        mProgressBar = circleView.getProgressBar();
        mStartVideoButton = circleView.getPlayButton();
        mSurfaceView = circleView.getSurfaceView();

        circleView.setPlayButtonClickListener(this);

        mImage = VideoProcessTask.getInstance().getPreview();
        if (mImage == null) {
            VideoProcessTask.getInstance().applyVideoFilter();
            onVideoProcessStatus(Status.DONE);
        }

        mNextButton = (ImageButton) view.findViewById(R.id.video_displaying_button_next);
        mNextButton.setOnClickListener(this);

        view.findViewById(R.id.select_preview).setOnClickListener(this);
        view.findViewById(R.id.select_filter).setOnClickListener(this);
        view.findViewById(R.id.select_track).setOnClickListener(this);

        view.findViewById(R.id.video_displaying_make_new).setOnClickListener(this);

        return view;
    }

    @Override
    public void onVideoProcessStatus(Status status) {
        super.onVideoProcessStatus(status);

        if (mCurrentAction == ACTION_VIEW) {
            View v = getView();
            final boolean enabled = status.equals(Status.DONE);
            if (v != null) {
                v.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mNextButton != null)
                            mNextButton.setEnabled(enabled);
                    }
                });
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (OnVideoCommandSelected) activity;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int i = v.getId();
        if (i == R.id.select_preview) {
            mListener.onOpenFramePreview();

        } else if (i == R.id.select_filter) {
            mListener.onOpenFilter();

        } else if (i == R.id.select_track) {
            mListener.onOpenTrack();

        } else if (i == R.id.video_displaying_button_next) {
            if (mListener.onComplete(mCurrentAction)) {
                mNextButton.setEnabled(false);
            }

        } else if (i == R.id.video_displaying_make_new) {

            getActivity().onBackPressed();

        } else if (i == R.id.video_displaying_surface_view) {
        }
    }


    public interface OnVideoCommandSelected {
        void onOpenRecorder(int step);

        void onOpenFramePreview();

        void onOpenTrack();

        void onOpenFilter();

        boolean onComplete(int action);
    }
}
