package com.humanet.humanetcore.fragments.profile;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.ContactsActivity;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;

/**
 * Created by ovitali on 24.09.2015.
 */
public class TokenFragment extends BaseTitledFragment implements View.OnClickListener {

    @Override
    public String getTitle() {
        return getString(R.string.invite_token);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invite_token, container, false);

        int count = Math.max(AppUser.getInstance().get().getTokens(), 0);
        String text = getResources().getQuantityString(R.plurals.invite_token_lefts, count, count);

        TextView textView = (TextView) view.findViewById(R.id.title);
        textView.setText(Html.fromHtml(text));

        view.findViewById(R.id.btn_share).setOnClickListener(this);

        if (count == 0 && !App.DEBUG) {
            view.findViewById(R.id.btn_share).setVisibility(View.GONE);
        }

        // App.getJobManager().addJob(new SendTokenJob().setNumber("ovitaliy", "+380997746073"));

        return view;
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.btn_share) {
            startActivity(ContactsActivity.newTokenInstance(getActivity()));
            getActivity().finish();

        }
    }
}
