package com.humanet.humanetcore.fragments.capture;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.humanet.audio.AudioLoader;
import com.humanet.audio.AudioTrack;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.views.adapters.AudioListAdapter;
import com.humanet.humanetcore.views.widgets.CircleLayout;

import java.util.ArrayList;

/**
 * Created by Владимир on 28.10.2014.
 */
public class AudioFragment extends BaseVideoCreationFragment implements CircleLayout.OnItemSelectedListener {
    public static final String TAG = "AudioFragment";

    private AudioTrack mSelectedAudioTrack;

    private static int EMPTY;

    public static AudioFragment newInstance() {
        Bundle args = new Bundle();
        AudioFragment fragment = new AudioFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mNextButton.setOnClickListener(this);

        ((TextView) view.findViewById(R.id.title)).setText(R.string.video_record_track);
        mNextButton.setOnClickListener(this);

        mListView.setOnItemSelectedListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadAudioList();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int viewId = v.getId();
        if (viewId == R.id.button_done) {
            if (mSelectedAudioTrack == null) {
                NewVideoInfo.get().setAudioPath(null);
            } else {
                NewVideoInfo.get().setAudioPath(mSelectedAudioTrack.getPath());
            }
            VideoProcessTask.getInstance().applyAudio();
            getActivity().onBackPressed();

        }
    }


    /**
     * Look for audio on phone memory and load their.
     */
    private void loadAudioList() {
        ArrayList<AudioTrack> audioTracks = AudioLoader.getListFromMusicDirecotory(FilePathHelper.getAudioDirectory());

        EMPTY = audioTracks.size() / 2;
        audioTracks.add(EMPTY, getFakeTrack());


        AudioListAdapter audioListAdapter = new AudioListAdapter(getActivity());
        audioListAdapter.setData(audioTracks);
        mListView.setAdapter(audioListAdapter);

        int selectedIndex = 0;
        if (NewVideoInfo.get().getAudioPath() == null) {
            selectedIndex = EMPTY;
        } else {
            for (int i = 0; i < audioTracks.size(); i++) {
                if (tracksAreSame(NewVideoInfo.get().getAudioPath(), audioTracks.get(i))) {
                    selectedIndex = i;
                    break;
                }
            }
        }

        mListView.setSelectedItem(selectedIndex);
        mSelectedAudioTrack = audioTracks.get(selectedIndex);
    }

    private AudioTrack getFakeTrack() {
        AudioTrack fakeTrack = new AudioTrack();
        fakeTrack.setTitle(App.getInstance().getString(R.string.video_record_no_audio));
        fakeTrack.setPath(FilePathHelper.getAudioStreamFile().getAbsolutePath());
        return fakeTrack;
    }

    public boolean tracksAreSame(String original, AudioTrack track) {
        if (original == null) {
            return track.getPath() == null;
        }

        return track.getPath() != null && original.equals(track.getPath());
    }

    @Override
    public void onItemSelected(Object data) {
        mSelectedAudioTrack = (AudioTrack) data;
        NewVideoInfo.get().setAudioPath(mSelectedAudioTrack.getPath());
        if (mediaPlayer == null || !isVideoPlaying) {
            if (mSelectedAudioTrack != null)
                play(mSelectedAudioTrack.getPath(), false);
            else
                releaseMediaPlayer();
        }

        if (isVideoPlaying) {
            mStartVideoButton.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.VISIBLE);
            releaseMediaPlayer();
        }
    }


}
