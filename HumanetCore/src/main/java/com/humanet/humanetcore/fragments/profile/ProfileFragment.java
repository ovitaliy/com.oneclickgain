package com.humanet.humanetcore.fragments.profile;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.util.LongSparseArray;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.video.GetFlowRequest;
import com.humanet.humanetcore.api.response.video.FlowResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.events.SimpleLoaderCallbacks;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.model.VideoStatistic;
import com.humanet.humanetcore.model.enums.Screen;
import com.humanet.humanetcore.views.adapters.VideoListAdapter;
import com.humanet.humanetcore.views.adapters.ViewPagerAdapter;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.humanet.humanetcore.views.widgets.CirclePickerView;
import com.humanet.humanetcore.views.widgets.items.StatisticItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProfileFragment extends BaseViewProfileFragment implements
        View.OnClickListener,
        ViewPagerAdapter.ViewPagerAdapterDelegator {


    private VideoListAdapter mVideoListAdapter;

    @NonNull
    @Override
    public int[] getTabNames() {
        return new int[]{
                R.string.view_profile_tab_content,
                R.string.view_profile_tab_questionary,
                R.string.view_profile_tab_statistic,
        };
    }

    protected void loadInitialFlowList() {
        GetFlowRequest getFlowRequest = new GetFlowRequest();
        getFlowRequest.setUserId(getUserId());
        getFlowRequest.saveData(getScreen().ordinal(), false);
        getSpiceManager().execute(getFlowRequest, new SimpleRequestListener<FlowResponse>());

        reloadDbVideoList();
    }


    @Override
    public View getPageAt(ViewGroup container, int position) {
        if (position == 0) {
            return getVideoListView();
        } else if (position == 1) {
            return getQuestionnaireView();
        } else if (position == 2) {
            return getStatisticView();
        }

        throw new RuntimeException("wrong position: " + position);
    }


    protected View getVideoListView() {
        ListView videoListView = new ListView(getActivity());
        mVideoListAdapter = new VideoListAdapter(getActivity(), null, getUserId(), null, null, null);
        videoListView.setAdapter(mVideoListAdapter);
        loadInitialFlowList();

        return videoListView;
    }

    private Screen getScreen() {
        Screen screen;
        if (getUserId() == AppUser.getInstance().getUid()) {
            screen = Screen.MY_PROFILE;
        } else {
            screen = Screen.USER_PROFILE;
        }
        return screen;
    }

    public View getStatisticView() {
        ArrayList<CirclePickerItem> list = new ArrayList<>(11);

        if (getUserInfo() != null) {
            List<VideoStatistic> categories = getUserInfo().getCategoryStatisticList();

            if (categories != null) {
                int count = categories.size();
                LongSparseArray<VideoStatistic> map = new LongSparseArray<>();
                for (int i = 0; i < count; i++) {
                    VideoStatistic categoryStatistic = getUserInfo().getCategoryStatisticList().get(i);
                    if (categoryStatistic.getDuration() > 0 && categoryStatistic.getTitleResId() != 0) {
                        VideoStatistic item = map.get(categoryStatistic.getTitleResId());
                        if (item == null) {
                            item = categoryStatistic;
                        } else {
                            item.setDuration(item.getDuration() + categoryStatistic.getDuration());
                        }

                        map.put(categoryStatistic.getTitleResId(), item);
                    }
                }

                for (int i = 0; i < map.size(); i++) {
                    long key = map.keyAt(i);
                    VideoStatistic val = map.get(key);
                    list.add(new StatisticItem(i,
                            getString(val.getTitleResId()) + "\n" + ConverterUtil.formatMillisToTime(val.getDuration()),
                            val.getImageResId()));
                }
            }
        }

        FrameLayout frameLayout = new FrameLayout(getActivity());

        CirclePickerView circlePickerView = new CirclePickerView(getActivity());
        circlePickerView.setBackgroundResource(R.drawable.circle_picker_bg);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;

        frameLayout.addView(circlePickerView, params);

        Collections.reverse(list);
        circlePickerView.fill(list, -1, null);

        return frameLayout;
    }


    private void reloadDbVideoList() {
        Bundle bundle = new Bundle();

        getLoaderManager().restartLoader(101, bundle, new SimpleLoaderCallbacks<Cursor>() {

            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                String where = ContentDescriptor.Videos.Cols.SCREEN + "=" + getScreen().ordinal();
                return new CursorLoader(getActivity(), ContentDescriptor.Videos.URI, null, where, null, null);
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
                mVideoListAdapter.swapCursor(data);
            }
        });
    }
}