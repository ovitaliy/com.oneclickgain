package com.humanet.humanetcore.fragments.vote;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.vote.AddVoteRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.utils.AnalyticsHelper;

import java.util.ArrayList;

/**
 * Created by ovitali on 17.11.2015.
 */
public class NewVoteFragment extends BaseTitledFragment implements View.OnClickListener {
    @Override
    public String getTitle() {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_vote, container, false);
        view.findViewById(R.id.btn_save).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.btn_save) {
            saveNewVote();

        }
    }

    private void saveNewVote() {
        View view = getView();
        if (view == null)
            return;
        EditText newVote = (EditText) view.findViewById(R.id.new_vote);

        EditText newOption1 = (EditText) view.findViewById(R.id.new_option_1);
        EditText newOption2 = (EditText) view.findViewById(R.id.new_option_2);
        EditText newOption3 = (EditText) view.findViewById(R.id.new_option_3);

        if (validateField(newVote) & validateField(newOption1) & validateField(newOption2)) {

            String vote = newVote.getText().toString();

            ArrayList<String> optionsList = new ArrayList<>();

            optionsList.add(newOption1.getText().toString());
            optionsList.add(newOption2.getText().toString());
            if (!TextUtils.isEmpty(newOption3.getText().toString())) {
                optionsList.add(newOption3.getText().toString());
            }

            AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.POLL_CREATE);

            getLoader().show();

            getSpiceManager().execute(
                    new AddVoteRequest(vote, optionsList.toArray(new String[optionsList.size()])),
                    new SimpleRequestListener<BaseResponse>() {
                        @Override
                        public void onRequestSuccess(BaseResponse baseResponse) {
                            getLoader().dismiss();
                            getActivity().onBackPressed();
                        }
                    }
            );
        }

    }

    private boolean validateField(EditText editText) {
        String text = editText.getText().toString();
        if (TextUtils.isEmpty(text)) {
            editText.setError(getString(R.string.vote_new_field_required));
            return false;
        } else {
            editText.setError(null);
            return true;
        }
    }
}
