package com.humanet.humanetcore.fragments.capture;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.views.adapters.FrameListAdapter;
import com.humanet.humanetcore.views.widgets.CircleLayout;

import java.io.File;
import java.util.ArrayList;

public class FramePreviewFragment extends BaseVideoCreationFragment implements CircleLayout.OnItemSelectedListener {

    private ArrayList<String> mFrames;
    private FrameListAdapter mAdapter;

    private String mSelectedFrame;

    public static FramePreviewFragment newInstance() {
        return new FramePreviewFragment();
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mNextButton.setOnClickListener(this);

        ((TextView) view.findViewById(R.id.title)).setText(R.string.video_record_preview);

        mFrames = new ArrayList<>();
        mListView.setOnItemSelectedListener(this);

        File[] frames = FilePathHelper.getVideoFrameFolder().listFiles();

        mAdapter = new FrameListAdapter(getActivity());
        makeFramesList(frames);
        int selected = 0;
        for (int i = 0; i < mFrames.size(); i++) {
            String videoPreviewPath = NewVideoInfo.get().getOriginalImagePath();
            String framePreviewPath = mFrames.get(i);
            if (videoPreviewPath.equals(framePreviewPath)) {
                selected = i;
                break;
            }
        }

        mListView.setAdapter(mAdapter);
        mListView.setSelectedItem(selected);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mAdapter != null) {
            mAdapter.clearCache();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int i = v.getId();
        if (i == R.id.button_done) {
            getActivity().onBackPressed();
            NewVideoInfo.rebuildFilterPack(NewVideoInfo.get().getOriginalImagePath());

        }
    }

    private void makeFramesList(File[] paths) {
        mFrames.clear();
        for (File path : paths) {
            if (path.isDirectory())
                continue;
            mFrames.add(path.getAbsolutePath());
        }
        mAdapter.setData(mFrames);
    }

    public void onItemSelected(Object data) {
        mSelectedFrame = (String) data;
        NewVideoInfo.get().setOriginalImagePath(mSelectedFrame);
        NewVideoInfo.get().setImageFilterApplied(null);
        VideoProcessTask.getInstance().applyPreviewFilter(true);
    }

    @Override
    public boolean isCanPlayVideo() {
        return false;
    }
}
