package com.humanet.humanetcore.fragments;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.fragments.base.BaseVideoCategoryPickerFragment;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by ovitali on 27.08.2015.
 */
public class VistoryFragment extends BaseVideoCategoryPickerFragment implements IMenuBindFragment {

    @Override
    public String getTitle() {
        return getString(R.string.menu_vistory);
    }


    @Override
    protected String[] getTabNames() {
        return new String[]{
                getString(R.string.online_tab_news),
                getString(R.string.online_tab_vlog),
                getString(R.string.online_tab_my_choice),
        };
    }

    @Override
    protected VideoType[] getVideoTypes() {
        return new VideoType[]{
                VideoType.NEWS,
                VideoType.VLOG,
                VideoType.MY_CHOICE,
        };
    }


    @Override
    public int getSelectedMenuItem() {
        return R.id.vistory;
    }
}
