package com.humanet.humanetcore.fragments.capture;

import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpegSync;
import com.github.hiteshsondhi88.libffmpeg.FfmpegHelper;
import com.humanet.filters.FilterController;
import com.humanet.filters.videofilter.IFilter;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.events.VideoProcessListener;
import com.humanet.humanetcore.events.VideoProcessingFinishedEvent;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.views.utils.BitmapDecoder;
import com.humanet.humanetcore.views.utils.ImageCroper;
import com.humanet.humanetcore.views.widgets.CircleLayout;
import com.humanet.humanetcore.views.widgets.CircleView;
import com.humanet.humanetcore.views.widgets.YellowProgressBar;

import org.apache.commons.io.FileUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by ovitali on 12.01.2015.
 */
public abstract class BaseVideoCreationFragment extends BaseTitledFragment implements
        View.OnClickListener,
        VideoProcessListener {

    public static final int ONLY_PREVIEW = 0;
    public static final int ONLY_VIDEO = 1;
    public static final int ALL = 2;

    @IntDef({ONLY_PREVIEW, ONLY_VIDEO, ALL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FilterApplyingType {
    }

    protected ImageView mImageView;
    protected CircleLayout mListView;
    protected ImageButton mStartVideoButton;
    protected ImageButton mNextButton;
    protected SurfaceView mSurfaceView;

    protected Bitmap mImage;

    protected MediaPlayer mediaPlayer;
    protected boolean isVideoPlaying;

    protected YellowProgressBar mProgressBar;

    private static Thread mHandlingThread;

    private static Status mCurrentStatus = Status.NONE;

    private static File mOutputFile = FilePathHelper.getVideoTmpFile2();

    public enum Status {
        NONE, RUNNING, PREVIEW_COMPLETE, VIDEO_COMPLETE, AUDIO_COMPLETE, CANCELLED, DONE
    }

    @Override
    public String getTitle() {
        return getString(R.string.video_edit);
    }

    public boolean isCanPlayVideo() {
        return true;
    }


    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preview_base, container, false);

        mListView = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
        mNextButton = (ImageButton) view.findViewById(R.id.button_done);

        CircleView circleView = (CircleView) view.findViewById(R.id.video_view);
        mImageView = circleView.getPreviewView();

        mProgressBar = circleView.getProgressBar();

        circleView.setPlayButtonClickListener(this);

        mStartVideoButton = circleView.getPlayButton();
        mStartVideoButton.setOnClickListener(this);

        mSurfaceView = circleView.getSurfaceView();
        mSurfaceView.setOnClickListener(this);

        return view;
    }

    public void setNextButtonEnabled() {
        mNextButton.setEnabled(true);
    }

    private void checkVideoAndPlay() {
        if (!NewVideoInfo.get().isVideoFilterApplied() || !NewVideoInfo.get().isAudioApplied()) {
            VideoProcessTask.getInstance().obtainFilter(ONLY_VIDEO, true);
        } else {
            play(NewVideoInfo.get().getVideoPath(), true);
        }
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.play_button) {
            checkVideoAndPlay();

        } else if (viewId == R.id.video_displaying_surface_view) {
            pause();
        }
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VideoProcessingFinishedEvent event) {
        checkVideoAndPlay();
    }

    @Override
    public void onVideoProcessStatus(Status status) {
        View view = getView();
        if (view == null) return;

        view.post(new Runnable() {
            @Override
            public void run() {
                mImage = VideoProcessTask.getInstance().getPreview();
                mImageView.setImageBitmap(mImage);
            }
        });

        if (status.equals(Status.RUNNING) || status.equals(Status.NONE)) {
            view.post(new Runnable() {
                @Override
                public void run() {
                    mStartVideoButton.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.VISIBLE);
                    mNextButton.setEnabled(false);
                }
            });
        }

        if (status.equals(Status.PREVIEW_COMPLETE) || status.equals(Status.DONE)) {
            view.post(new Runnable() {
                @Override
                public void run() {
                    mImageView.setVisibility(View.VISIBLE);
                    mStartVideoButton.setVisibility(View.VISIBLE);
                }
            });
        }
        if (status.equals(Status.DONE)) {
            view.post(new Runnable() {
                @Override
                public void run() {
                    if (!isCanPlayVideo()) {
                        //play button shouldn't be visible on frame preview screen
                        mStartVideoButton.setVisibility(View.GONE);
                    } else {
                        mStartVideoButton.setVisibility(View.VISIBLE);
                    }
                    setNextButtonEnabled();
                    mProgressBar.setVisibility(View.GONE);
                }
            });
        }
    }

    public void play(String file, final boolean isVideo) {
        if (isAdded()) {
            releaseMediaPlayer();

            isVideoPlaying = isVideo;

            mediaPlayer = new MediaPlayer();

            try {
                mediaPlayer.setDataSource(file);
                mediaPlayer.prepare();
                if (isVideo)
                    mediaPlayer.setDisplay(mSurfaceView.getHolder());
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        releaseMediaPlayer();
                        mStartVideoButton.setVisibility(View.VISIBLE);
                        mImageView.setVisibility(View.VISIBLE);
                        isVideoPlaying = false;
                    }
                });

                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        if (isVideo) {
                            mStartVideoButton.setVisibility(View.INVISIBLE);
                            mImageView.setVisibility(View.INVISIBLE);
                        }
                    }
                });
                mediaPlayer.start();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void pause() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            mStartVideoButton.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.VISIBLE);
            isVideoPlaying = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseMediaPlayer();
        VideoProcessTask.getInstance().setListener(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        VideoProcessTask.getInstance().setListener(this);
        mStartVideoButton.setVisibility(View.GONE);
        onVideoProcessStatus(VideoProcessTask.getInstance().getStatus());
    }

    protected void releaseMediaPlayer() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying())
                mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public enum VideoProcessTask {
        INSTANCE;

        VideoProcessTask() {
            mTaskList = new LinkedList<>();
        }

        public static VideoProcessTask getInstance() {
            return INSTANCE;
        }

        private VideoProcessListener mVideoProcessListener;

        public Bitmap mPreviewBitmap = null;

        private volatile Queue<Runnable> mTaskList;

        public Bitmap getPreview() {
            return mPreviewBitmap;
        }

        public void setPreviewBitmap(Bitmap bitmap) {
            mPreviewBitmap = bitmap;
        }

        public Status setListener(VideoProcessListener videoProcessListener) {
            mVideoProcessListener = videoProcessListener;
            return mCurrentStatus;
        }

        public void changeStatus(Status newStatus) {
            Log.d("status", newStatus.name());
            if (!newStatus.equals(mCurrentStatus)) {
                mCurrentStatus = newStatus;
                if (mVideoProcessListener != null) {
                    mVideoProcessListener.onVideoProcessStatus(mCurrentStatus);
                }
            }
        }

        public Status getStatus() {
            return mCurrentStatus;
        }

        private void stopTasks() {
            mTaskList.clear();
        }

        public void applyPreviewFilter() {
            applyPreviewFilter(false);
        }

        public void applyPreviewFilter(boolean ignoreCheckApplying) {
            if (!NewVideoInfo.get().isImageFilterApplied() || mPreviewBitmap == null || ignoreCheckApplying) {
                obtainFilter(ONLY_PREVIEW);
            }
        }

        public void applyVideoFilter() {
            if (!NewVideoInfo.get().isVideoFilterApplied()) {
                obtainFilter(ONLY_VIDEO);
            }
        }

        public void applyAudio() {
            if (!NewVideoInfo.get().isAudioApplied()) {
                obtainFilter(ONLY_VIDEO);
            }
        }

        private void obtainFilter(@FilterApplyingType int filterApplyingType) {
            obtainFilter(filterApplyingType, false);
        }

        private void obtainFilter(final @FilterApplyingType int filterApplyingType, final boolean playAfterFinish) {

            IFilter filter = NewVideoInfo.get().getFilter();
            String originalImagePath = NewVideoInfo.get().getOriginalImagePath();
            String originalVideoPath = NewVideoInfo.get().getOriginalVideoPath();

            String audio = NewVideoInfo.get().getAudioPath();

            if (mHandlingThread != null && mHandlingThread.isAlive()) {
                mHandlingThread.interrupt();
            }

            stopTasks();

            if (filterApplyingType == ONLY_PREVIEW
                    || filterApplyingType == ALL
                    || !NewVideoInfo.get().isImageFilterApplied()
                    ) {

                filterImage(originalImagePath, filter);
            }

            if (filter == null && TextUtils.isEmpty(audio)) {
                //Let's just copy original file, since no filter applying
                copyFile();
                NewVideoInfo.get().setVideoFilterApplied(null);
                NewVideoInfo.get().setAudioApplied(null);
            } else {
                if (filterApplyingType == ONLY_VIDEO || filterApplyingType == ALL) {
                    filterVideo(originalVideoPath, filter, audio);
                }
            }

            if (mTaskList.size() == 0) {
                changeStatus(Status.DONE);
                return;
            }


            mHandlingThread = new Thread() {
                @Override
                public void run() {
                    long started = System.currentTimeMillis();
                    Log.d("ThreadFilter", "started " + Thread.currentThread().getName());

                    changeStatus(Status.RUNNING);

                    while (!mTaskList.isEmpty() && !isInterrupted()) {
                        Runnable cancelableCallable = mTaskList.poll();

                        if (cancelableCallable != null) {
                            try {
                                cancelableCallable.run();
                            } catch (Exception ex) {
                                Log.e("VideoProcessTask", "HandlingThread", ex);
                            }
                        }
                    }

                    if (!isInterrupted()) {
                        changeStatus(Status.DONE);
                        if (playAfterFinish) {
                            EventBus.getDefault().post(new VideoProcessingFinishedEvent());
                        }
                        Log.d("ThreadFilter", "finished at" + (System.currentTimeMillis() - started));
                    } else {
                        Log.d("ThreadFilter", "isInterrupted at" + (System.currentTimeMillis() - started));
                    }
                }
            };
            mHandlingThread.start();
        }

        private Status filterImage(final String input, final IFilter filter) {
            final String output = FilePathHelper.getImageTmpFile().getAbsolutePath();

            if (filter == null) {
                mTaskList.add(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap = BitmapDecoder.createSquareBitmap(input, App.WIDTH_WITHOUT_MARGINS, false);
                        mPreviewBitmap = ImageCroper.getCircularBitmap(bitmap, App.IMAGE_BORDER, App.getInstance().getResources().getColor(R.color.colorPrimary));
                        NewVideoInfo.get().setImagePath(null);
                        NewVideoInfo.get().setImageFilterApplied(null);
                    }
                });
                return Status.PREVIEW_COMPLETE;
            }

            List<Runnable> taskList = FilterController.getFilterToImageTasks(
                    FilePathHelper.getVideoCacheDirectory().getAbsolutePath(),
                    input,
                    output,
                    filter,
                    NewVideoInfo.get().getWidth(),
                    NewVideoInfo.get().getHeight(),
                    new FilterController.OperationCallback() {
                        @Override
                        public void onOperationComplete() {
                            Bitmap bitmap = BitmapDecoder.createSquareBitmap(output, App.WIDTH_WITHOUT_MARGINS, false);
                            if (bitmap != null) {
                                mPreviewBitmap = ImageCroper.getCircularBitmap(bitmap, App.IMAGE_BORDER, App.getInstance().getResources().getColor(R.color.colorPrimary));
                                NewVideoInfo.get().setImagePath(output);
                                NewVideoInfo.get().setImageFilterApplied(filter);
                            }
                        }
                    }
            );

            mTaskList.addAll(taskList);


            return Status.PREVIEW_COMPLETE;
        }


        private Status filterVideo(final String input, final IFilter filter, final String audio) {

            final String output = FilePathHelper.getVideoTmpFile3().getAbsolutePath();

            List<Runnable> taskList = new ArrayList<>();

            if (filter != null) {
                taskList.addAll(FilterController.getFilterToVideoTasks(
                        FilePathHelper.getVideoCacheDirectory().getAbsolutePath(),
                        input,
                        output,
                        filter,
                        NewVideoInfo.get().getWidth(),
                        NewVideoInfo.get().getHeight(),
                        new FilterController.OperationCallback() {
                            @Override
                            public void onOperationComplete() {
                                NewVideoInfo.get().setVideoFilterApplied(filter);
                            }
                        }
                ));
            } else {
                copyFile(new File(input), new File(output));
                NewVideoInfo.get().setVideoFilterApplied(null);
            }

            taskList.add(new MergeVideoRunnable(
                    output,
                    audio,
                    FilePathHelper.getVideoTmpFile2().getAbsolutePath()
            ));

            mTaskList.addAll(taskList);

            return Status.VIDEO_COMPLETE;
        }


        public void copyFile() {
            copyFile(new File(NewVideoInfo.get().getOriginalVideoPath()), mOutputFile);
        }

        public void copyFile(File srcFile, File dstFile) {
            try {
                FileUtils.copyFile(
                        srcFile,
                        dstFile
                );
            } catch (Exception ignore) {
            }
        }
    }

    private static final class MergeVideoRunnable implements Runnable {

        private String mVideoFile;
        private String mAudioFile;
        private String mOutputVideoFile;

        public MergeVideoRunnable(String videoFile, String audioFile, String outputVideoFile) {
            mVideoFile = videoFile;
            mAudioFile = audioFile;
            mOutputVideoFile = outputVideoFile;
        }

        @Override
        public void run() {
            String audioParams = mAudioFile.contains(".aac")
                    ? "-acodec copy -bsf:a aac_adtstoasc"
                    : "";

            String command = new FfmpegHelper.CmdBuilder(FfmpegHelper.Type.IMAGE)
                    .addInput(mVideoFile)
                    .addInput(mAudioFile)
                    .addOutput(mOutputVideoFile)
                    .setAdditionalParams("-vcodec copy")
                    .setAdditionalParams(audioParams)
                    .applyAudio(true)
                    .build(false);

            FFmpegSync.getInstance(App.getInstance()).execute(
                    command,
                    new ExecuteBinaryResponseHandler()
            );

            NewVideoInfo.get().setVideoPath(mOutputVideoFile);
            NewVideoInfo.get().setAudioApplied(mAudioFile);
        }
    }
}

