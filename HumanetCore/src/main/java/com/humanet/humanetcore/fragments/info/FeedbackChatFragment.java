package com.humanet.humanetcore.fragments.info;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.message.AddMessageRequest;
import com.humanet.humanetcore.api.requests.message.GetMessagesRequest;
import com.humanet.humanetcore.api.response.feedback.AddMessageResponse;
import com.humanet.humanetcore.api.response.feedback.MessagesResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.views.IInputTextView;
import com.humanet.humanetcore.views.adapters.FeedbackMessagesAdapter;
import com.humanet.humanetcore.views.utils.UiUtil;

import static com.humanet.humanetcore.model.Feedback.Type;

/**
 * Unneeded in this application adapter
 * Must be removed on code cleanup
 */

public class FeedbackChatFragment extends BaseTitledFragment implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener {

    private FeedbackMessagesAdapter mAdapter;
    private IInputTextView mCommentText;

    private int mFeedbackId;

    @Type
    private int mFeedbackType;

    public static FeedbackChatFragment newInstance(int feedbackId, @Type int type) {
        FeedbackChatFragment fragment = new FeedbackChatFragment();
        Bundle args = new Bundle();
        args.putInt("feedback_id", feedbackId);
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args.containsKey("feedback_id")) {
            mFeedbackId = args.getInt("feedback_id");
        }

        @Type int type = args.getInt("type");
        mFeedbackType = type;

        if (mFeedbackId > 0) {
            loadMessages();
        }
    }

    private void loadMessages() {
        getActivity().getSupportLoaderManager().restartLoader(3838, null, this);
        getSpiceManager().execute(new GetMessagesRequest(mFeedbackId), new SimpleRequestListener<MessagesResponse>());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback_messages, container, false);

        mAdapter = new FeedbackMessagesAdapter(getActivity(), null);
        ListView mListView = (ListView) view.findViewById(R.id.comments_list);

        mListView.setAdapter(mAdapter);

        mCommentText = (IInputTextView) view.findViewById(R.id.new_message);

        view.findViewById(R.id.comments_send).setOnClickListener(this);

        return view;
    }


    private void hideKeyboard() {
        UiUtil.hideKeyboard(mCommentText.getEditView());
    }

    private void sendComment(String message) {
        getLoader().show();

        hideKeyboard();
        mCommentText.setText("");

        getSpiceManager().execute(
                new AddMessageRequest(mFeedbackId, message, mFeedbackType),
                new SimpleRequestListener<AddMessageResponse>() {
                    @Override
                    public void onRequestSuccess(AddMessageResponse response) {
                        super.onRequestSuccess(response);
                        if (isAdded()) {
                            if (response != null && response.isSuccess()) {
                                mFeedbackId = response.getListId();
                                loadMessages();
                            }
                            getLoader().dismiss();
                        }
                    }
                });


        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.FEEDBACK_SEND);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.comments_send) {
            String comment = mCommentText.getText().toString();
            if (!comment.isEmpty()) {
                sendComment(comment);
            }

        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = ContentDescriptor.FeedbackMessages.Cols.ID_FEEDBACK + " = " + mFeedbackId;
        return new CursorLoader(getActivity(), ContentDescriptor.FeedbackMessages.URI, null, selection, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public String getTitle() {
        return getString(R.string.feedback_title);
    }
}
