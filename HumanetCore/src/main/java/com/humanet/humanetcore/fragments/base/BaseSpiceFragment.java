package com.humanet.humanetcore.fragments.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.humanet.humanetcore.LeakCannaryHelper;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.views.dialogs.ProgressDialog;
import com.octo.android.robospice.SpiceManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public abstract class BaseSpiceFragment extends Fragment {

    private volatile ProgressDialog mLoader;

    private SpiceManager mSpiceManager = new SpiceManager(NavigationManager.getSpiceServiceClass());

    public SpiceManager getSpiceManager() {
        return mSpiceManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LeakCannaryHelper.watch(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mSpiceManager.start(getActivity());
    }

    @Override
    public void onStop() {
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }

        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Object event) {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        boolean isDialogShowing = mLoader != null && mLoader.isShowing();
        outState.putBoolean("is_dialog_showing", isDialogShowing);

        if (isDialogShowing) {
            mLoader.dismiss();
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState == null)
            return;


        boolean isDialogShowing = savedInstanceState.getBoolean("is_dialog_showing");
        if (isDialogShowing) {
            getLoader().show();
        }
    }

    public ProgressDialog getLoader() {
        if (mLoader == null) {
            mLoader = new ProgressDialog(getActivity());
        }
        return mLoader;
    }
}
