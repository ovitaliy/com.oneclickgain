package com.humanet.humanetcore.fragments.balance;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.GameBalanceValue;
import com.humanet.humanetcore.api.response.user.CoinsHistoryListResponse;
import com.humanet.humanetcore.modules.BalanceUtil;

import java.util.Observable;
import java.util.Observer;

// TODO may be can be moved to 1clickgain module
public class GameCurrencyFragment extends InternalCurrencyFragment implements
        Observer {

    protected void obtainBalanceResponse(CoinsHistoryListResponse coinsHistoryListResponse) {
        GameBalanceValue.getInstance().setBalance(coinsHistoryListResponse.getBalance());
    }


    @Override
    public void onStart() {
        super.onStart();
        GameBalanceValue.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        GameBalanceValue.getInstance().deleteObserver(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        balanceTextView.setText(String.format("%s " + App.COINS_CODE, BalanceUtil.balanceToString((Float) data)));
    }
}
