package com.humanet.humanetcore.fragments;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.fragments.base.BaseVideoCategoryPickerFragment;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by ovitali on 27.08.2015.
 */
public class CrowdFragment extends BaseVideoCategoryPickerFragment implements IMenuBindFragment {

    @Override
    public String getTitle() {
        return getString(R.string.crowd);
    }


    @Override
    protected String[] getTabNames() {
        return new String[]{
                getString(R.string.crowd_ask),
                getString(R.string.crowd_give),
        };
    }

    @Override
    protected VideoType[] getVideoTypes() {
        return new VideoType[]{
                VideoType.CROWD_ASK,
                VideoType.CROWD_GIVE,
        };
    }


    @Override
    public int getSelectedMenuItem() {
        return R.id.balance;
    }
}
