package com.humanet.humanetcore.fragments.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.interfaces.OnNavigateListener;
import com.humanet.humanetcore.views.widgets.CirclePickerView;

import java.util.ArrayList;

/**
 * Created by ovitali on 26.08.2015.
 */
public abstract class BasePickerFragment extends BaseTitledFragment {

    protected CirclePickerItem mSelected;

    protected View mNextButton;

    protected OnNavigateListener mOnNavigateListener;

    protected abstract ArrayList<CirclePickerItem> getItems();

    protected int getPickerAngle() {
        return -90;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base_picker, container, false);

        mOnNavigateListener = (OnNavigateListener) getActivity();

        mNextButton = view.findViewById(R.id.btn_next);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelected != null)
                    mOnNavigateListener.onNavigateByViewId(mSelected.getId());
            }
        });

        CirclePickerView circlePickerView = (CirclePickerView) view.findViewById(R.id.picker);
        circlePickerView.fill(getItems(), getPickerAngle(),
                -1,
                new CirclePickerView.OnPickListener() {
                    @Override
                    public void onPick(View view,CirclePickerItem element) {
                        mSelected = element;
                    }
                });

        circlePickerView.getChildAt(circlePickerView.getChildCount() - 1).performClick();

        return view;
    }
}
