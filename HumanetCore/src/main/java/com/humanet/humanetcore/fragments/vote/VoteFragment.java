package com.humanet.humanetcore.fragments.vote;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.vote.GetVoteRequest;
import com.humanet.humanetcore.api.requests.vote.SendVoteRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.response.vote.VoteResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.events.SimpleLoaderCallbacks;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.vote.Vote;
import com.humanet.humanetcore.model.vote.VoteOption;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.views.adapters.VoteOptionsAdapter;

/**
 * Created by ovitali on 17.11.2015.
 */
public class VoteFragment extends BaseTitledFragment implements AbsListView.OnItemClickListener {

    private static final int VOTE_LOADER_ID = 1311;
    private static final int OPTIONS_LOADER_ID = 3212;

    @Override
    public String getTitle() {
        return null;
    }

    public static VoteFragment newInstance(Vote vote, boolean isMine) {
        VoteFragment voteFragment = new VoteFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("vote", vote);
        bundle.putBoolean("isMine", isMine);
        voteFragment.setArguments(bundle);

        return voteFragment;
    }

    private VoteOptionsAdapter mAdapter;
    private Vote mVote;
    private ListView mOptionsListView;
    private boolean mIsMine;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVote = (Vote) getArguments().getSerializable("vote");
        mIsMine = getArguments().getBoolean("isMine");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vote_view, container, false);

        TextView questionView = (TextView) view.findViewById(R.id.question);
        questionView.setText(mVote.getTitle());

        mAdapter = new VoteOptionsAdapter(getActivity(), null, mVote.getMyVote());
        mOptionsListView = (ListView) view.findViewById(R.id.options);
        mOptionsListView.setAdapter(mAdapter);

        initAdapter();

        getLoaderManager().initLoader(VOTE_LOADER_ID, null, mLoaderCallback);

        reloadVoteOptions();

        return view;
    }

    private void reloadVoteOptions() {
        getSpiceManager().execute(new GetVoteRequest(mVote.getId()), new SimpleRequestListener<VoteResponse>() {
            @Override
            public void onRequestSuccess(VoteResponse voteResponse) {
                getLoaderManager().restartLoader(VOTE_LOADER_ID, null, mLoaderCallback);
            }
        });
    }

    private SimpleLoaderCallbacks<Cursor> mLoaderCallback = new SimpleLoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            switch (id) {
                case VOTE_LOADER_ID:
                    return new CursorLoader(getActivity(),
                            ContentDescriptor.Votes.URI,
                            null,
                            "_id = " + mVote.getId(),
                            null,
                            null);

                case OPTIONS_LOADER_ID:
                    return new CursorLoader(getActivity(),
                            ContentDescriptor.VoteOptions.URI,
                            null,
                            ContentDescriptor.VoteOptions.Cols.VOTE_ID + " = " + mVote.getId(),
                            null,
                            null);
            }
            return null;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            switch (loader.getId()) {
                case VOTE_LOADER_ID:
                    if (data.moveToFirst()) {
                        mVote = Vote.fromCursor(data);
                        initAdapter();
                        data.close();
                        getLoaderManager().restartLoader(OPTIONS_LOADER_ID, null, mLoaderCallback);
                    } else {
                        mAdapter.changeCursor(null);
                    }

                    break;

                case OPTIONS_LOADER_ID:
                    mAdapter.swapCursor(data);
                    break;
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            switch (loader.getId()) {
                case OPTIONS_LOADER_ID:
                    mAdapter.changeCursor(null);
                    break;
            }
        }
    };

    private void initAdapter() {
        mAdapter.setVoted(mVote.getMyVote());
        //mAdapter.setFinished(mVote.getStatus() == VoteTabFragment.FINISHED);
        mAdapter.setVotedCount(mVote.getTotalVotes());
        if (mVote.isVoted() || mIsMine)
            mOptionsListView.setOnItemClickListener(null);
        else
            mOptionsListView.setOnItemClickListener(this);

        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mOptionsListView.setOnItemClickListener(null);
        final VoteOption option = VoteOption.fromCursor((Cursor) mAdapter.getItem(position));
        getLoader().show();
        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.POLL_PARTICIPATE);
        getSpiceManager().execute(new SendVoteRequest(option.getVoteId(), option.getId()), new SimpleRequestListener<BaseResponse>() {
            @Override
            public void onRequestSuccess(final BaseResponse baseResponse) {
                getLoader().dismiss();

                reloadVoteOptions();
            }
        });
    }
}
