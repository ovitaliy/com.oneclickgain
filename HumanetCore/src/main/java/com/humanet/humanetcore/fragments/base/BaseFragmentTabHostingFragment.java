package com.humanet.humanetcore.fragments.base;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.behaviour.SimpleOnTabSelectedListener;
import com.humanet.humanetcore.views.utils.UiUtil;

public abstract class BaseFragmentTabHostingFragment extends BaseTitledFragment {

    protected abstract String[] getTabNames();

    protected int getLayoutId() {
        return R.layout.fragment_base_tabhost;
    }

    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(getLayoutId(), container, false);

        final TabsAdapter tabsAdapter = new TabsAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        mViewPager.setAdapter(tabsAdapter);
        mViewPager.setOffscreenPageLimit(1);

        mTabLayout = (TabLayout) view.findViewById(R.id.tabhost);
        final String[] tabNames = getTabNames();
        for (String tabName : tabNames) {
            mTabLayout.addTab(mTabLayout.newTab().setText(tabName));
        }
        if (tabNames.length <= 1)
            mTabLayout.setVisibility(View.GONE);


        mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);
        mViewPager.addOnPageChangeListener(mOnViewPagerChangeListener);


        return view;
    }

    private ViewPager.SimpleOnPageChangeListener mOnViewPagerChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @SuppressWarnings("ConstantConditions")
        @Override
        public void onPageSelected(int position) {
            UiUtil.hideKeyboard(getActivity());
            mTabLayout.setOnTabSelectedListener(null);
            mTabLayout.getTabAt(position).select();
            mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);
        }
    };

    private SimpleOnTabSelectedListener mOnTabSelectedListener = new SimpleOnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.removeOnPageChangeListener(mOnViewPagerChangeListener);
            mViewPager.setCurrentItem(tab.getPosition(), false);
            mViewPager.addOnPageChangeListener(mOnViewPagerChangeListener);
        }
    };

    protected abstract Fragment getFragmentForTabPosition(int position);

    public final void openTab(int page) {
        mViewPager.setCurrentItem(page, false);
    }

    private class TabsAdapter extends FragmentStatePagerAdapter {

        final private int mCount;

        public TabsAdapter(FragmentManager fm) {
            super(fm);
            mCount = getTabNames().length;
        }

        @Override
        public Fragment getItem(int position) {
            return getFragmentForTabPosition(position);
        }

        @Override
        public int getCount() {
            return mCount;
        }
    }


}
