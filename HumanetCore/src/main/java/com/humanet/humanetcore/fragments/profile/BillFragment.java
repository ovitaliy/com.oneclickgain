package com.humanet.humanetcore.fragments.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.fragments.base.BaseSpiceFragment;

/**
 * Created by ovitali on 26.08.2015.
 */
public class BillFragment extends BaseSpiceFragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_bill, container, false);

        view.findViewById(R.id.btn_add).setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_add) {
            //TODO
        }
    }
}
