package com.humanet.humanetcore.fragments.vote;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;
import android.support.v4.app.Fragment;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.fragments.base.BaseFragmentTabHostingFragment;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by ovitali on 16.11.2015.
 */
public class VoteTabFragment extends BaseFragmentTabHostingFragment {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({FINISHED, ACTIVE, CREATED, DECLINED})
    public @interface VoteListStatus {
    }

    public static final int FINISHED = 0;
    public static final int ACTIVE = 1;
    public static final int CREATED = 3;
    public static final int DECLINED = 4;

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({NOW, PAST, MY})
    public @interface VotePeriod {
    }

    public static final String NOW = "now";
    public static final String PAST = "past";
    public static final String MY = "my";

    public static int getVoteTypeId(@VotePeriod final String period) {
        switch (period) {
            case NOW:
                return 0;
            case PAST:
                return 1;
            default:
                return 2;
        }
    }


    @Override
    public String getTitle() {
        return getString(R.string.vote_title);
    }


    @Override
    protected String[] getTabNames() {
        return new String[]{
                getString(R.string.vote_tab_active),
                getString(R.string.vote_tab_past),
                getString(R.string.vote_tab_my)
        };
    }

    @Override
    protected Fragment getFragmentForTabPosition(final int position) {
        switch (position) {
            case 0:
                return VoteListFragment.newInstance(NOW, getVoteTypeId(NOW));

            case 1:
                return VoteListFragment.newInstance(PAST, getVoteTypeId(PAST));

            default:
                return VoteListFragment.newInstance();

        }
    }
}
