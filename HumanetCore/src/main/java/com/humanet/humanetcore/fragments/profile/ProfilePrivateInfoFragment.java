package com.humanet.humanetcore.fragments.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.UploadingMedia;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.model.VideoInfo;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.model.enums.selectable.Education;
import com.humanet.humanetcore.model.enums.selectable.Job;
import com.humanet.humanetcore.modules.LocationDataLoader;
import com.humanet.humanetcore.views.adapters.location.SimpleSpinnerAdapter;
import com.humanet.humanetcore.views.behaviour.SimpleTextListener;
import com.humanet.humanetcore.views.utils.CircleBitmapDisplayer;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.CityAutoCompleteView;
import com.humanet.humanetcore.views.widgets.CountrySpinnerView;
import com.humanet.humanetcore.views.widgets.switch_button.SwitchButton;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Edit user avatar, name, last name, country, etc.
 */
public class ProfilePrivateInfoFragment extends BaseProfileFragment
        implements
        OnItemSelectedListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener, View.OnTouchListener {

    public static final int PICK_PHOTO_FOR_AVATAR = 100;
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

    private OnFillMainProfileListener mOnFillMainProfileListener;

    public static final int CAPTURE_PHOTO_FOR_AVATAR = 101;
    private EditText mFirstNameField;
    private EditText mLastNameField;

    private SwitchButton mGenderSwitchButton;
    private TextView mBirthDateView;
    //temp vars to store current ids


    private Activity mActivity;
    private UserInfo mUserInfo;

    private ImageView mAvatarImageView;

    private Date mSelectedBirthDay;

    private CheckBox mMaleCheckbox;
    private CheckBox mFemaleCheckbox;

    //-- edit views
    private Spinner mEducationSpinner, mJobSpinner, mLanguageSpinner;
    ;
    //-- edit views

    private LocationDataLoader mLocationDataLoader;


    private boolean mIsEdit;

    public static ProfilePrivateInfoFragment newInstance() {
        return newInstance(false);
    }

    public static ProfilePrivateInfoFragment newInstance(boolean edit) {
        ProfilePrivateInfoFragment f = new ProfilePrivateInfoFragment();
        Bundle args = new Bundle();
        args.putBoolean("edit", edit);
        f.setArguments(args);
        return f;
    }

    public static ProfilePrivateInfoFragment newInstance(String avatar) {
        ProfilePrivateInfoFragment f = new ProfilePrivateInfoFragment();
        Bundle args = new Bundle();
        args.putString("avatar", avatar);
        f.setArguments(args);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = getActivity();

        if (mOnProfileFillListener != null) {
            mUserInfo = mOnProfileFillListener.getUser();
        }
        if (mUserInfo == null) {
            mUserInfo = new UserInfo();
        }
        if (mActivity instanceof OnFillMainProfileListener) {
            mOnFillMainProfileListener = (OnFillMainProfileListener) mActivity;
        }


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mIsEdit = getArguments() != null && getArguments().containsKey("edit") && getArguments().getBoolean("edit");
        View view;

        if (mIsEdit) {
            view = inflater.inflate(R.layout.fragment_profile_1_edit, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment_profile_1, container, false);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initEditView(view);

        mAvatarImageView = (ImageView) view.findViewById(R.id.avatar);
        mAvatarImageView.setOnClickListener(this);


        // init views
        mFirstNameField = (EditText) view.findViewById(R.id.field_first_name);
        mFirstNameField.addTextChangedListener(new SimpleTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                mUserInfo.setFName(s.toString());
            }
        });


        mLastNameField = (EditText) view.findViewById(R.id.field_last_name);
        mLastNameField.addTextChangedListener(new SimpleTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                mUserInfo.setLName(s.toString());
            }
        });

        mBirthDateView = (TextView) view.findViewById(R.id.txt_birth_field);
        mBirthDateView.setOnClickListener(this);

        View nextButton = view.findViewById(R.id.btn_next);
        if (mOnFillMainProfileListener != null) {
            nextButton.setVisibility(View.VISIBLE);
            nextButton.setOnClickListener(this);
        } else {
            nextButton.setVisibility(View.GONE);
        }


        int cityId = AppUser.getInstance().get() != null ? AppUser.getInstance().get().getCityId() : -1;
        mLocationDataLoader = new LocationDataLoader(
                getSpiceManager(),
                (CityAutoCompleteView) view.findViewById(R.id.sp_city),
                (CountrySpinnerView) view.findViewById(R.id.sp_country),
                cityId
        );

        mLocationDataLoader.setItemColor(getResources().getColor(R.color.spinnerItemColor));

        boolean maleSelected = true;

        if (mUserInfo != null) {

            mFirstNameField.setText(mUserInfo.getFName());
            mLastNameField.setText(mUserInfo.getLName());

            maleSelected = mUserInfo.getGender() == 0;

            long birthday = mUserInfo.getBirthDate();
            if (birthday != 0) {
                mSelectedBirthDay = new Date(birthday * 1000);
                mBirthDateView.setText(DATE_FORMAT.format(mSelectedBirthDay));
                mBirthDateView.setTextColor(getResources().getColor(R.color.spinnerItemColor));
            }
            if (!TextUtils.isEmpty(mUserInfo.getAvatar()))
                reloadAvatarImage(mUserInfo.getAvatar());
        }

        mMaleCheckbox = (CheckBox) view.findViewById(R.id.male);
        mFemaleCheckbox = (CheckBox) view.findViewById(R.id.female);
        mGenderSwitchButton = (SwitchButton) view.findViewById(R.id.gender);

        mMaleCheckbox.setChecked(maleSelected);
        mFemaleCheckbox.setChecked(!maleSelected);
        mGenderSwitchButton.setChecked(!maleSelected);
        mGenderSwitchButton.setOnCheckedChangeListener(this);
        mMaleCheckbox.setOnCheckedChangeListener(this);
        mFemaleCheckbox.setOnCheckedChangeListener(this);

        if (mUserInfo.baseDataAdded) {
            Calendar c = Calendar.getInstance(Locale.getDefault());
            mBirthDateView.setText(DATE_FORMAT.format(c.getTime()));
        } else {
            checkUserData();
        }


        mFirstNameField.setOnTouchListener(this);
        mLastNameField.setOnTouchListener(this);
        mBirthDateView.setOnTouchListener(this);

        mLanguageSpinner = (Spinner) view.findViewById(R.id.sp_lang);
        SimpleSpinnerAdapter adapter = new SimpleSpinnerAdapter<>(Arrays.asList(Language.values()));
        adapter.setItemColor(getResources().getColor(R.color.spinnerItemColor));
        mLanguageSpinner.setAdapter(adapter);
        mLanguageSpinner.setOnItemSelectedListener(this);
        if (AppUser.getInstance().getLanguage() != null)
            mLanguageSpinner.setSelection(AppUser.getInstance().getLanguage().getId());
    }

    @Override
    public void onPause() {
        UiUtil.hideKeyboard(getActivity());
        super.onPause();
    }

    private void reloadAvatarImage(String url) {
        if (mAvatarImageView != null && url != null) {
            mAvatarImageView.setImageDrawable(null);
            DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder().displayer(new CircleBitmapDisplayer()).cacheInMemory(false).cacheOnDisk(false).build();
            ImageLoader.getInstance().displayImage(url, mAvatarImageView, displayImageOptions);
        }
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.txt_birth_field) {
            final Calendar c = Calendar.getInstance(Locale.getDefault());
            Date minimalDate = getMinimalDate();
            try {
                Date curDate = DATE_FORMAT.parse(mBirthDateView.getText().toString());
                c.setTimeInMillis(curDate.getTime());
            } catch (ParseException e) {
                c.setTimeInMillis(minimalDate.getTime());
            }
            DatePickerDialog d;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                d = new DatePickerDialog(mActivity, R.style.DialogTheme, mOnDateChangeListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
            } else {
                d = new DatePickerDialog(mActivity, mOnDateChangeListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
            }
            d.getDatePicker().setMaxDate(minimalDate.getTime());
            d.show();

        } else if (viewId == R.id.btn_next) {
            checkMandatoryFields(mUserInfo);

        } else if (viewId == R.id.avatar) {
            captureImage();//SelectImageSourceDialog.show(getActivity(), this);

        }
    }

    private DatePickerDialog.OnDateSetListener mOnDateChangeListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            final Calendar c = Calendar.getInstance(Locale.getDefault());
            c.set(year, monthOfYear, dayOfMonth);
            mSelectedBirthDay = c.getTime();
            mBirthDateView.setText(DATE_FORMAT.format(mSelectedBirthDay));
            mBirthDateView.setTextColor(getResources().getColor(R.color.spinnerItemColor));
            mUserInfo.setBirthDate(mSelectedBirthDay.getTime() / 1000);
        }
    };

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int i = parent.getId();
        if (i == R.id.sp_job) {
            Job job = (Job) mJobSpinner.getSelectedItem();
            mUserInfo.setJobType(job);
            mUserInfo.setJobTitle(job.getTitle());
        } else if (i == R.id.sp_education) {
            Education education = (Education) mEducationSpinner.getSelectedItem();
            mUserInfo.setDegree(education);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ProfilePrivateInfoFragment.PICK_PHOTO_FOR_AVATAR:
            case ProfilePrivateInfoFragment.CAPTURE_PHOTO_FOR_AVATAR:
                if (resultCode == Activity.RESULT_OK) {
                    String url = data.getData().toString();
                    MemoryCacheUtils.removeFromCache(url, ImageLoader.getInstance().getMemoryCache());
                    DiskCacheUtils.removeFromCache(url, ImageLoader.getInstance().getDiskCache());
                    setPhoto(url);
                }
                break;
        }
    }

    public void captureImage() {
        Intent intent = new Intent(getActivity(), NavigationManager.getCaptureActivityClass());
        intent.putExtra("update_avatar", VideoInfo.AVATAR);
        startActivityForResult(intent, CAPTURE_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        boolean maleSelected = false;
        int veidId = buttonView.getId();
        if (veidId == R.id.male) {
            maleSelected = isChecked;

        } else if (veidId == R.id.gender || veidId == R.id.female) {
            maleSelected = !isChecked;
        }

        mMaleCheckbox.setOnCheckedChangeListener(null);
        mFemaleCheckbox.setOnCheckedChangeListener(null);
        mGenderSwitchButton.setOnCheckedChangeListener(null);

        mMaleCheckbox.setChecked(maleSelected);

        mGenderSwitchButton.setChecked(!maleSelected);
        mFemaleCheckbox.setChecked(!maleSelected);

        mMaleCheckbox.setOnCheckedChangeListener(this);
        mFemaleCheckbox.setOnCheckedChangeListener(this);
        mGenderSwitchButton.setOnCheckedChangeListener(this);

        mUserInfo.setGender(maleSelected ? 0 : 1);
    }

    private void initEditView(View view) {
        SimpleSpinnerAdapter adapter;
        View v;
        if ((v = view.findViewById(R.id.sp_job)) != null) {
            mJobSpinner = (Spinner) v;
            adapter = new SimpleSpinnerAdapter<>(Job.values());
            adapter.setHasHitItem(true);
            adapter.setItemColor(getResources().getColor(R.color.spinnerItemColor));
            mJobSpinner.setAdapter(adapter);
            if (mUserInfo.getJobType() != null)
                mJobSpinner.setSelection(mUserInfo.getJobType().getId() + 1);
            mJobSpinner.setOnItemSelectedListener(this);
        }

        if ((v = view.findViewById(R.id.sp_job_name)) != null) {
            final EditText jobNameView = (EditText) v;
            if (mUserInfo.getProfession() != null)
                jobNameView.setText(mUserInfo.getProfession());
            jobNameView.addTextChangedListener(new SimpleTextListener() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mUserInfo.setProfession(s.toString());
                }
            });
        }

        if ((v = view.findViewById(R.id.sp_education)) != null) {
            mEducationSpinner = (Spinner) v;
            adapter = new SimpleSpinnerAdapter<>(Education.values());
            adapter.setItemColor(getResources().getColor(R.color.spinnerItemColor));
            adapter.setHasHitItem(true);
            mEducationSpinner.setAdapter(adapter);
            if (mUserInfo.getDegree() != null)
                mEducationSpinner.setSelection(mUserInfo.getDegree().getId() + 1);
            mEducationSpinner.setOnItemSelectedListener(this);
        }
    }


    private void checkUserData() {
        UserInfo info = mUserInfo;
        if (info.baseDataAdded) {
            Date d = new Date(info.getBirthDate());
            mBirthDateView.setText(DATE_FORMAT.format(d));
        }
    }

    protected void checkMandatoryFields(UserInfo info) {
        if (mLocationDataLoader.getSelectedCityId() > 0) {
            mUserInfo.setCity(mLocationDataLoader.getSelectedCityName());
            mUserInfo.setCityId(mLocationDataLoader.getSelectedCityId());
        }
        if (mLocationDataLoader.getSelectedCountryId() > 0) {
            mUserInfo.setCountry(mLocationDataLoader.getSelectedCountryName());
            mUserInfo.setCountryId(mLocationDataLoader.getSelectedCountryId());
        }

        mUserInfo.setLanguage((Language) mLanguageSpinner.getSelectedItem());

        Date minimalDate = getMinimalDate();
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setPositiveButton(R.string.ok, null);
        if (mUserInfo.getAvatar() == null) {
            builder.setMessage(R.string.profile_edit_error_no_avatar)
                    .create()
                    .show();
        } else if (TextUtils.isEmpty(mFirstNameField.getText()) || TextUtils.isEmpty(mLastNameField.getText())) {
            builder.setMessage(R.string.profile_edit_error_no_name)
                    .create()
                    .show();
        } else if (mLocationDataLoader.getSelectedCountryId() <= 0) {
            builder.setMessage(R.string.profile_edit_error_no_country)
                    .create()
                    .show();
        } else if (mLocationDataLoader.getSelectedCityId() <= 0) {
            builder.setMessage(R.string.profile_edit_error_no_city)
                    .create()
                    .show();
        } else if (mSelectedBirthDay == null) {
            builder.setMessage(R.string.profile_edit_error_no_birth_day)
                    .create()
                    .show();
        } else if (mSelectedBirthDay.getTime() > minimalDate.getTime()) {
            builder.setMessage(R.string.profile_edit_error_no_to_young)
                    .create()
                    .show();
        } else {
            info.baseDataAdded = true;

            if (mIsEdit)
                mOnProfileFillListener.onFillProfile(info);
            else
                mOnFillMainProfileListener.onFillMainProfileInfo(info);
        }
    }

    /**
     * shows avatar image in ImageView
     *
     * @param url - url of new image captured image. Url can be link to local file (starts with "file:///"). Be careful to do not save it in EditProfileRequest
     */
    public void setPhoto(String url) {
        mUserInfo.setAvatar(url);
        reloadAvatarImage(url);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        View focusedView = getActivity().getCurrentFocus();
        if (focusedView != null && !v.equals(focusedView)) {
            if (focusedView instanceof EditText) {
                UiUtil.hideKeyboard((EditText) focusedView);
            } else {
                focusedView.clearFocus();
            }
        }
        v.requestFocus();
        return false;
    }

    public interface OnFillMainProfileListener {
        void onFillMainProfileInfo(UserInfo userInfo);
    }


    private Date getMinimalDate() {
        Calendar minimalDate = Calendar.getInstance();
        minimalDate.set(Calendar.HOUR_OF_DAY, 0);
        minimalDate.set(Calendar.MINUTE, 0);
        minimalDate.add(Calendar.YEAR, -18);
        return minimalDate.getTime();
    }


    /**
     * catch event on avatar image uploading complete.
     * getSpiceManager().addListenerIfPending will not work because uploading request start from external thread
     *
     * @param uploadingMedia - contains url with new avatar
     */
    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UploadingMedia uploadingMedia) {
        if (isVisible())
            setPhoto(uploadingMedia.getThumb());
    }
}