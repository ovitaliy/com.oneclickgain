package com.humanet.humanetcore.fragments;

import android.app.Activity;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.video.GetCommentsRequest;
import com.humanet.humanetcore.api.requests.video.PostVideoCommentRequest;
import com.humanet.humanetcore.api.response.video.CommentsResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.events.OnViewProfileListener;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.Comment;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.views.IInputTextView;
import com.humanet.humanetcore.views.adapters.CommentListAdapter;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.items.VideoItemView;

import java.util.ArrayList;
import java.util.Collections;

public class CommentsFragment extends BaseTitledFragment implements View.OnClickListener {

    private Video mVideo;
    private Category mCategory;
    private VideoType mVideoType;

    private ArrayList<Comment> mComments;
    private ListView mListView;
    private CommentListAdapter mAdapter;
    private IInputTextView mCommentText;

    private OnViewProfileListener mOnViewProfileListener;

    private VideoItemView mVideoItemView;


    long start;

    public static CommentsFragment newInstance(Video video, Category category, VideoType videoType) {
        CommentsFragment fragment = new CommentsFragment();
        Bundle args = new Bundle();
        args.putSerializable("video", video);
        args.putSerializable("category", category);
        args.putSerializable("videoType", videoType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args.containsKey("video")) {
            mVideo = (Video) args.getSerializable("video");
        }
        if (args.containsKey("category"))
            mCategory = (Category) args.getSerializable("category");
        if (args.containsKey("videoType"))
            mVideoType = (VideoType) args.getSerializable("videoType");

        mComments = new ArrayList<>();
        getComments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comments, container, false);

        mAdapter = new CommentListAdapter(getActivity());
        mAdapter.setOnViewProfileListener(mOnViewProfileListener);
        mListView = (ListView) view.findViewById(R.id.comments_list);

        mVideoItemView = new VideoItemView(getActivity(), AppUser.getInstance().getUid(), mVideoType, mCategory);

        ((View) mVideoItemView.findViewById(R.id.stream_item_comment).getParent()).setVisibility(View.GONE);

        mVideoItemView.setData(mVideo, null);
        mListView.addHeaderView(mVideoItemView, null, false);

        mListView.setAdapter(mAdapter);
        mAdapter.setData(mComments);

        mCommentText = (IInputTextView) view.findViewById(R.id.new_message);
        setHint(mVideo.getCommentsCount());

        view.findViewById(R.id.comments_send).setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mOnViewProfileListener = (OnViewProfileListener) activity;
    }

    /**
     * Set hint to mCommentText;
     *
     * @param count How many comments we have.
     */
    private void setHint(int count) {
        String hint;
        if (count == 0) {
            hint = getResources().getString(R.string.no_comment);
        } else {
            hint = getResources().getQuantityString(R.plurals.stream_displaying_comment_text, count, count);
        }
        mCommentText.setHint(hint);
    }

    /**
     * Send request to getting comments from server.
     */
    private void getComments() {
        getLoader().show();

        start = System.currentTimeMillis();

        getSpiceManager().execute(
                new GetCommentsRequest(mVideo.getVideoId()),
                new GetCommentsResponseListener()
        );
    }

    private void hideKeyboard() {
        UiUtil.hideKeyboard(mCommentText.getEditView());
    }

    /**
     * Send comment to server.
     *
     * @param comment The comment that we send.
     */
    private void sendComment(String comment) {
        getLoader().show();

        getSpiceManager().execute(
                new PostVideoCommentRequest(mVideo.getVideoId(), comment),
                new GetCommentsResponseListener()
        );

        hideKeyboard();
        mCommentText.setText("");
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.comments_send) {
            String comment = mCommentText.getText().toString();
            if (!comment.isEmpty()) {
                sendComment(comment);
            }

        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.feedback);
    }

    private class GetCommentsResponseListener extends SimpleRequestListener<CommentsResponse> {

        @Override
        public void onRequestSuccess(CommentsResponse response) {
            mComments = new ArrayList<>();

            Log.i(CommentsFragment.class.getName(), "loaded in:" + (System.currentTimeMillis() - start));

            if (response.getComments() != null)
                Collections.addAll(mComments, response.getComments());

            mAdapter.setData(mComments);
            setHint(mComments.size());
            mVideo.setCommentsCount(mComments.size());
            mVideoItemView.setData(mVideo, null);

            if (mComments != null) {
                mListView.setSelection(mComments.size());
            }

            saveCommentsCount(mComments.size());

            getLoader().dismiss();
        }
    }

    private void saveCommentsCount(int commentsCount) {
        ContentValues values = new ContentValues(1);
        values.put(ContentDescriptor.Videos.Cols.COMMENTS_COUNT, commentsCount);
        getActivity().getContentResolver().update(
                ContentDescriptor.Videos.URI,
                values,
                ContentDescriptor.Videos.Cols.ID + " = " + mVideo.getVideoId(),
                null);
    }

    @Override
    public void onPause() {
        super.onPause();
        UiUtil.hideKeyboard(getActivity());
    }
}
