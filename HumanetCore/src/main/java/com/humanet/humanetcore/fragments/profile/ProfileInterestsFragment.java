package com.humanet.humanetcore.fragments.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.EditProfileActivity;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.model.enums.selectable.BaseItem;
import com.humanet.humanetcore.model.enums.selectable.QuestionnaireItem;
import com.humanet.humanetcore.utils.QuestionnaireHelper;
import com.humanet.humanetcore.views.widgets.CirclePickerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Edit user interests, hobbies, ect.
 */
public class ProfileInterestsFragment extends BaseProfileFragment implements View.OnClickListener {

    public static ProfileInterestsFragment newInstance() {
        ProfileInterestsFragment f = new ProfileInterestsFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    private UserInfo mUserInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_profile_3, container, false);

        view.findViewById(R.id.btn_next).setOnClickListener(this);

        LinearLayout pickersContainer = (LinearLayout) view.findViewById(R.id.container);

        mUserInfo = mOnProfileFillListener.getUser();

        QuestionnaireItem[] items = QuestionnaireItem.values();

        double radius = 0;
        int imageSize;

        radius = CirclePickerView.isResizeItemOnSelection() ? App.WIDTH_WITHOUT_MARGINS * 3d / 8d : (App.WIDTH_WITHOUT_MARGINS / 3d);

        imageSize = App.WIDTH_WITHOUT_MARGINS / (CirclePickerView.isResizeItemOnSelection() ? 8 : 4);
        if (radius * Math.PI * 2 < (imageSize) * 16) {
            double newImageSize = (int) (radius * Math.PI * 2d / 16);
            radius += (imageSize - newImageSize) / 2;
            imageSize = (int) (newImageSize + (imageSize - newImageSize) / 4);

            imageSize *= 0.8d;
        }
        LinearLayout.LayoutParams layoutParams;

        for (int i = 0; i < items.length; i++) {
            QuestionnaireItem item = items[i];

            View textView = sDelegator.createTextView(getActivity(), item.getTitle());
            pickersContainer.addView(textView, i * 2);

            final CirclePickerView picker = new CirclePickerView(getActivity());
            picker.setImageSize(imageSize);
            picker.setRadius(radius);
            picker.setBackgroundResource(R.drawable.circle_picker_bg);


            List<BaseItem> questionnaireItems = QuestionnaireHelper.getValues(item);
            BaseItem selectedValue = QuestionnaireHelper.getValue(mUserInfo, item);
            int selectedId = -1;
            if (selectedValue != null) {
                selectedId = selectedValue.getId();
                if (selectedValue.isOtherOption()) {
                    questionnaireItems.get(selectedId).setTitle(selectedValue.getTitle());
                }
            }

            picker.fill(new ArrayList<CirclePickerItem>(questionnaireItems),
                    selectedId, null);


            picker.setTag(item);

            int size = App.WIDTH_WITHOUT_MARGINS;
            layoutParams = new LinearLayout.LayoutParams(size, size);
            layoutParams.setMargins(App.MARGIN, 0, App.MARGIN, App.MARGIN);
            pickersContainer.addView(picker, i * 2 + 1, layoutParams);


        }

        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.scrollTo(0, 0);
            }
        }, 100);


        return view;
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.btn_next) {
            grabPickersInfo();
            ((EditProfileActivity) getActivity()).onFillProfile(mUserInfo);

        }
    }

    private void grabPickersInfo() {
        if (getView() == null)
            return;
        LinearLayout container = (LinearLayout) getView().findViewById(R.id.container);
        for (int i = 0; i < container.getChildCount(); i++) {
            View v = container.getChildAt(i);
            if (v instanceof CirclePickerView) {
                final CirclePickerView picker = (CirclePickerView) v;
                BaseItem selectedItem = (BaseItem) picker.getCurrentElement();
                QuestionnaireHelper.setValue(mUserInfo, selectedItem, (QuestionnaireItem) picker.getTag());
            }
        }
    }

    private static Delegator sDelegator = new Delegator() {
        @Override
        public View createTextView(Context context, String title) {
            TextView textView = new TextView(context);
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
            textView.setText(title.replace("\n", " "));
            textView.setTextSize(18);
            textView.setTextColor(context.getResources().getColor(R.color.infoTextColor));
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(App.WIDTH_WITHOUT_MARGINS, -2);
            layoutParams.setMargins(App.MARGIN, App.MARGIN, App.MARGIN, 0);
            textView.setLayoutParams(layoutParams);
            return textView;
        }
    };

    public static void init(Delegator delegator) {
        sDelegator = delegator;
    }

    public static Delegator getDelegator() {
        return sDelegator;
    }


    public interface Delegator {
        View createTextView(Context context, String title);
    }

}
