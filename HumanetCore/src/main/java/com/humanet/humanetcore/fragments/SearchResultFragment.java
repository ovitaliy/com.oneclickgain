package com.humanet.humanetcore.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.SearchActivity;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.UserFinded;
import com.humanet.humanetcore.views.adapters.SearchResultAdapter;

/**
 * Created by Deni on 23.06.2015.
 */
public class SearchResultFragment extends BaseTitledFragment implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemClickListener {
    ListView mListView;
    SearchResultAdapter mAdapter;

    public static SearchResultFragment newInstance() {
        SearchResultFragment fragment = new SearchResultFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_result, container, false);
        mListView = (ListView) view.findViewById(R.id.result);
        mListView.setOnItemClickListener(this);
        mAdapter = new SearchResultAdapter(getActivity(), null, 0);
        mListView.setAdapter(mAdapter);
        getActivity().getSupportLoaderManager().initLoader(133, null, this);

        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), ContentDescriptor.UserSearchResult.URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UserFinded user = UserFinded.fromCursor((Cursor) mAdapter.getItem(position));
        ((SearchActivity) getActivity()).openUserProfile(user.getId());
    }

    @Override
    public String getTitle() {
        return getString(R.string.search);
    }
}
