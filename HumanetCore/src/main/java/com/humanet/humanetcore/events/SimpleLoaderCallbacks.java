package com.humanet.humanetcore.events;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

/**
 * Created by ovitali on 21.09.2015.
 */
public class SimpleLoaderCallbacks<T> implements LoaderManager.LoaderCallbacks<T> {
    @Override
    public Loader<T> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<T> loader, T data) {

    }

    @Override
    public void onLoaderReset(Loader<T> loader) {

    }
}
