package com.humanet.humanetcore.events;

import com.humanet.humanetcore.fragments.capture.BaseVideoCreationFragment;

/**
 * Created by ovitali on 17.02.2015.
 */
public interface VideoProcessListener {
    void onVideoProcessStatus(BaseVideoCreationFragment.Status status);
}
