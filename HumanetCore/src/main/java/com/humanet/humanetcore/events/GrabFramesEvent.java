package com.humanet.humanetcore.events;

/**
 * Created by ovitali on 13.11.2015.
 */
public class GrabFramesEvent {

    private boolean mSuccesses;

    public boolean isSuccesses() {
        return mSuccesses;
    }

    public GrabFramesEvent(boolean successes) {
        mSuccesses = successes;
    }
}
