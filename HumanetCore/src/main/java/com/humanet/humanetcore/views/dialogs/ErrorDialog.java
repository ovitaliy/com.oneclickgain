package com.humanet.humanetcore.views.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by Denis on 24.05.2015.
 */
public class ErrorDialog extends DialogFragment implements DialogInterface.OnClickListener {

    public static ErrorDialog newInstance(int code, String message) {
        ErrorDialog errorDialog = new ErrorDialog();
        Bundle args = new Bundle(2);
        args.putInt("code", code);
        args.putString("msg", message);
        errorDialog.setArguments(args);
        return errorDialog;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle("Error")
                .setNeutralButton(android.R.string.ok, this)
                .setMessage(getArguments().getString("msg"));
        return adb.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dismiss();
    }
}
