package com.humanet.humanetcore.views.profile;

import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.views.BaseView;

/**
 * Created by Uran on 08.06.2016.
 */
public interface ViewProfileView extends BaseView{

    void showUser(UserInfo userInfo);

}
