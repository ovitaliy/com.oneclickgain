package com.humanet.humanetcore.views.widgets.search;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.model.ILocationModel;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.model.enums.selectable.BaseItem;
import com.humanet.humanetcore.model.enums.selectable.Education;
import com.humanet.humanetcore.model.enums.selectable.Job;
import com.humanet.humanetcore.modules.LocationDataLoader;
import com.humanet.humanetcore.views.adapters.location.SimpleSpinnerAdapter;
import com.humanet.humanetcore.views.behaviour.IGrabSearchInfo;
import com.humanet.humanetcore.views.widgets.CityAutoCompleteView;
import com.humanet.humanetcore.views.widgets.CountrySpinnerView;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ovitali on 26.10.2015.
 */
public class SearchProfileLayout extends ScrollView implements IGrabSearchInfo {

    private EditText mFirstNameField;
    private EditText mLastNameField;


    private LocationDataLoader mLocationDataLoader;

    private Spinner mEducationSpinner, mJobSpinner, mLanguageSpinner;
    private EditText mJobNameView;


    public SearchProfileLayout(Context context, SpiceManager spiceManager) {
        super(context);

        inflate(context, R.layout.layout_search_profile, this);

        mFirstNameField = (EditText) findViewById(R.id.first_name);
        mLastNameField = (EditText) findViewById(R.id.last_name);

        mJobSpinner = (Spinner) findViewById(R.id.sp_job);
        SimpleSpinnerAdapter<SpinnerItem> adapter = new SimpleSpinnerAdapter<>(changeFirstItemInArray(
                getContext().getString(R.string.search_profile_choose_occupation),
                Job.values()
        ));
        adapter.setHasHitItem(true);
        mJobSpinner.setAdapter(adapter);

        mLanguageSpinner = (Spinner) findViewById(R.id.sp_language);
        mLanguageSpinner.setAdapter(new SimpleSpinnerAdapter<>(Arrays.asList(Language.values())));
        mLanguageSpinner.setSelection(AppUser.getInstance().getLanguage().getId());

        mJobNameView = (EditText) findViewById(R.id.sp_job_name);

        mEducationSpinner = (Spinner) findViewById(R.id.sp_education);
        adapter = new SimpleSpinnerAdapter<>(changeFirstItemInArray(
                getContext().getString(R.string.search_profile_choose_education),
                Education.values()
        ));
        adapter.setHasHitItem(true);
        mEducationSpinner.setAdapter(adapter);

        mLocationDataLoader = new LocationDataLoader(
                spiceManager,
                (CityAutoCompleteView) findViewById(R.id.sp_city),
                (CountrySpinnerView) findViewById(R.id.sp_country),
                -1
        );
        mLocationDataLoader.setItemColor(getResources().getColor(R.color.infoTextColor));
    }

    @Override
    public void grabSearchInfo(ArgsMap params) {
        if (!TextUtils.isEmpty(mFirstNameField.getText().toString())) {
            params.put("first_name", mFirstNameField.getText().toString());
        }

        if (!TextUtils.isEmpty(mLastNameField.getText().toString())) {
            params.put("last_name", mLastNameField.getText().toString());
        }

        if (mLocationDataLoader.getSelectedCountryId() > 0) {
            params.put("id_country", mLocationDataLoader.getSelectedCountryId());
        }

        if (mLocationDataLoader.getSelectedCityId() > 0) {
            params.put("id_city", mLocationDataLoader.getSelectedCityId());
        }

        if (mEducationSpinner.getSelectedItemPosition() != 0) {
            params.put("degree", mEducationSpinner.getSelectedItemId());
        }

        if (mJobSpinner.getSelectedItemPosition() != 0) {
            params.put("job", mJobSpinner.getSelectedItemId());
        }

        params.put("lang", ((Language) mLanguageSpinner.getSelectedItem()).getRequestParam());

        if (!TextUtils.isEmpty(mJobNameView.getText().toString())) {
            params.put("craft", mJobNameView.getText().toString());
        }
    }

    private static class SpinnerItem implements ILocationModel {
        private int mId;
        private String mTitle;

        public SpinnerItem(int id, String title) {
            mId = id;
            mTitle = title;
        }

        @Override
        public int getId() {
            return mId;
        }

        @Override
        public String getTitle() {
            return mTitle;
        }
    }

    private static List<SpinnerItem> changeFirstItemInArray(String firstString, List<? extends BaseItem> list) {
        List<SpinnerItem> itemList = new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++) {
            BaseItem baseItem = list.get(i);

            if (i == 0)
                itemList.add(new SpinnerItem(baseItem.getId(), firstString));
            else
                itemList.add(new SpinnerItem(baseItem.getId(), baseItem.getTitle()));
        }

        return itemList;
    }


}
