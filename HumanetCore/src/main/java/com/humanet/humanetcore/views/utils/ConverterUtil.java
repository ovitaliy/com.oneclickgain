package com.humanet.humanetcore.views.utils;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by ovitali on 07.04.2015.
 * Project is Magazines4Free
 */
public class ConverterUtil {

    public static float dpToPix(Context context, float value) {
        return dpToPix(context.getResources(), value);
    }

    public static float dpToPix(Resources resources, float value) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, resources.getDisplayMetrics());
    }

    public static float pixToDp(Context context, float value) {
        return pixToDp(context.getResources(), value);
    }

    public static float pixToDp(Resources resources, float value) {
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return value / (metrics.densityDpi / 160f);
    }

    public static String formatMillisToTime(long millis) {
        return String.format(Locale.getDefault(),"%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        );
    }

    public static boolean isInteger(String s) {
        return !TextUtils.isEmpty(s) && TextUtils.isDigitsOnly(s);
    }


}
