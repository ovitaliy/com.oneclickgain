package com.humanet.humanetcore.views.dialogs.dialogViews;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.widgets.ShadowImageButton;

/**
 * Created by serezha on 13.07.16.
 */
public class ConfirmationDialog extends DialogFragment implements View.OnClickListener {

	private Runnable mButtonStateRunnable;
	private Runnable mPayRunnable;

	private String mConfirmationText;

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		View view = getView();

		return new AlertDialog.Builder(getActivity(), R.style.Dialog_No_Border)
				.setView(view)
				.setCancelable(true)
				.show();
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.dialog_confirmation, container, false);

		ShadowImageButton closeButton = (ShadowImageButton) view.findViewById(R.id.btn_close);
		ShadowImageButton confirmButton = (ShadowImageButton) view.findViewById(R.id.btn_confirm);

		if(mConfirmationText != null)
			((TextView) view.findViewById(R.id.confirmationText)).setText(mConfirmationText);

		closeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dismiss();
			}
		});
		confirmButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				executeRunnables();
				dismiss();
			}
		});

		return view;
	}

	@Override
	public void onClick(View view) {
	}

	public void setConfirmationText(String text) {
		mConfirmationText = text;
	}

	public void setButtonStateRunnable(Runnable buttonStateRunnable) {
		mButtonStateRunnable = buttonStateRunnable;
	}

	public void setPayRunnable(Runnable payRunnable) {
		mPayRunnable = payRunnable;
	}

	private void executeRunnables() {
		if(mButtonStateRunnable != null)
			mButtonStateRunnable.run();
		if(mPayRunnable != null)
			mPayRunnable.run();
	}

	public static class Builder {

		public ConfirmationDialog build() {
			ConfirmationDialog fragment = new ConfirmationDialog();

			return fragment;
		}


	}
}
