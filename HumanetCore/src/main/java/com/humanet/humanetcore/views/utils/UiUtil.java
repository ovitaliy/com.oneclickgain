package com.humanet.humanetcore.views.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.ColorRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.widgets.VideoSurfaceView;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public class UiUtil {

    public static void setTextValue(View v, int viewId, int value) {
        String sValue = value > 0 ? String.valueOf(value) : "";
        setTextValue(v, viewId, sValue);
    }

    public static void setTextValue(View v, int viewId, String value) {
        TextView view = (TextView) v.findViewById(viewId);
        view.setText(value);
    }

    public static String getTextValue(View v, int viewId) {
        TextView view = (TextView) v.findViewById(viewId);
        return view.getText().toString().trim();
    }

    public static int getIntValue(View v, int viewId) {
        try {
            return Integer.parseInt(getTextValue(v, viewId));
        } catch (Exception ex) {
            return 0;
        }
    }

    public static void removeMeFromParent(View view) {
        ViewGroup parent = (ViewGroup) view.getParent();
        parent.removeView(view);
    }

    public static void hideKeyboard(Activity target) {
        View focusedView = target.getCurrentFocus();
        if (focusedView == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) target.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);

        focusedView.clearFocus();
    }

    public static void hideKeyboard(EditText target) {
        InputMethodManager imm = (InputMethodManager) target.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(target.getWindowToken(), 0);
        target.clearFocus();
    }

    public static void setBackgroundDrawable(View v, Drawable drawable) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            v.setBackgroundDrawable(drawable);
        } else {
            v.setBackground(drawable);
        }
    }

    public static void setBackgroundDrawable(View v, Bitmap bitmap) {
        setBackgroundDrawable(v, new BitmapDrawable(v.getResources(), bitmap));
    }

    public static int getColor(Context context, @ColorRes int color) {
       /* if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            return context.getColor(color);
        else*/
        return context.getResources().getColor(color);
    }

    public static void openLink(Context context, String link) {
        try {
            Uri uri = Uri.parse(link);
            context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void share(Context context, String title, String text) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(shareIntent, title));
    }

    public static void resizedToFitParent(VideoSurfaceView view, View parent, float width, float height) {
        float parentWidth = parent.getMeasuredWidth();
        float parentHeight = parent.getMeasuredHeight();
        float scale = Math.max(parentWidth / width, parentHeight / height);
        float scaledWidth = (width * scale);
        float scaledHeight = (height * scale);

        if (view.setCalculatedSizes((int) scaledWidth, (int) scaledHeight)) {
            view.setCalculatedX((int) ((parentWidth - scaledWidth) / 2));
            view.setCalculatedY((int) ((parentHeight - scaledHeight) / 2));

            view.requestLayout();
        }
    }

    public static void changeScreenAutoRotation(Context context, boolean enabled) {
        android.provider.Settings.System.putInt(
                context.getContentResolver(),
                Settings.System.ACCELEROMETER_ROTATION,
                enabled ? 1 : 0
        );
    }

    public static boolean isLandscape(Context context) {
        return context.getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT;
    }

    public static String getDensityName(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            return "xxxhdpi";
        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        if (density >= 1.5) {
            return "hdpi";
        }
        if (density >= 1.0) {
            return "mdpi";
        }
        return "ldpi";
    }

    @Deprecated
    public static void addTab(LinearLayout tabhost, String name) {
        addTab(tabhost, name, 0);
    }

    @Deprecated
    public static void addTab(LinearLayout tabhost, String name, int id) {
        TextView textView = (TextView) LayoutInflater.from(tabhost.getContext()).inflate(R.layout.item_tab, tabhost, false);
        textView.setText(name);
        textView.setId(id);

        tabhost.addView(textView);
    }
}
