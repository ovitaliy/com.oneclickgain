package com.humanet.humanetcore.views.widgets.items;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.events.OnViewProfileListener;
import com.humanet.humanetcore.model.Comment;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Владимир on 26.11.2014.
 */
public class CommentView extends RelativeLayout implements View.OnClickListener {

    private ImageView mImageView;
    private TextView mNameTextView;
    private TextView mTextView;
    private TextView mTimeTextView;
    private Comment mData;

    private OnViewProfileListener mOnViewProfileListener;

    public CommentView(Context context) {
        super(context);
        inflate(context, R.layout.item_comment, this);
        mImageView = (ImageView) findViewById(R.id.author_image);
        mNameTextView = (TextView) findViewById(R.id.author_name);
        mTextView = (TextView) findViewById(R.id.message);
        mTimeTextView = (TextView) findViewById(R.id.time);

        mImageView.setOnClickListener(this);
        mNameTextView.setOnClickListener(this);
    }

    public void setData(Comment data) {
        mData = data;
        mNameTextView.setText(mData.getAuthor());
        mTextView.setText(mData.getText());
        long time = mData.getTime();
        long timestamp = time * 1000;
        DateFormat formatter = new SimpleDateFormat("hh:mm", Locale.getDefault());
        Date date = new Date(timestamp);
        mTimeTextView.setText(formatter.format(date));

        //setting image
        ImageLoader.getInstance().displayImage(mData.getAvatarUrl(), mImageView);

    }

    public void setOnViewProfileListener(OnViewProfileListener listener) {
        mOnViewProfileListener = listener;
    }

    @Override
    public void onClick(View v) {
        mOnViewProfileListener.onViewProfile(mData.getAuthorId());
    }
}
