package com.humanet.humanetcore.views.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.gif.GifPreloadRequest;
import com.humanet.humanetcore.api.requests.video.ShareVideoRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.interfaces.SpiceContext;
import com.octo.android.robospice.SpiceManager;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


public class ShareDialog extends DialogFragment implements View.OnClickListener {

    @IntDef({TYPE_SHOT, TYPE_EMOTICON, TYPE_GRIMACE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ShareType {
    }

    public static final int TYPE_EMOTICON = 3;
    public static final int TYPE_SHOT = 2;
    public static final int TYPE_GRIMACE = 4;

    public static final String TAG = "ShareDialog";

    private String mLink;
    private String mGifLink;
    private int mVideoId;
    private int mShareType;
    private boolean mCanBeClosed;

    View mCloseButton;

    private int mSelectedShareType;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getView();

        return new AlertDialog.Builder(getActivity(), R.style.Dialog_No_Border)
                .setView(view)
                .setCancelable(false)
                .show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_share, container, false);

        Bundle args = getArguments();
        if (args.containsKey("video_id")) {
            mVideoId = args.getInt("video_id");
        }

        if (args.containsKey("type")) {
            mShareType = args.getInt("type");
        }

        if (args.containsKey("closeable")) {
            mCanBeClosed = getArguments().getBoolean("closeable");
        }

        if (args.containsKey("link")) {
            mLink = getArguments().getString("link");
        }

        if (args.containsKey("gif_link")) {
            mGifLink = getArguments().getString("gif_link");
        }

        RadioGroup rg = (RadioGroup) view.findViewById(R.id.group_share);
        rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mSelectedShareType = checkedId;
            }
        });

        view.findViewById(R.id.btn_next).setOnClickListener(this);
        mCloseButton = view.findViewById(R.id.btn_close);
        mCloseButton.setOnClickListener(this);
        if (isCanBeClosed()) {
            mCloseButton.setVisibility(View.VISIBLE);
        } else {
            mCloseButton.setVisibility(View.GONE);
        }

        return view;
    }

    private String getText() {
        switch (mShareType) {
            case TYPE_GRIMACE:
                return App.getInstance().getString(R.string.share_grimace);

            case TYPE_EMOTICON:
                return App.getInstance().getString(R.string.share_emoticon);

            case TYPE_SHOT:
                return App.getInstance().getString(R.string.share_shot);

            default:
                throw new IllegalArgumentException("Unknown share type!");

        }
    }

    private void share(int shareMethod) {
        String text = getText();
        if (shareMethod == R.id.rb_gif) {
            sendLinkViaMessenger(mVideoId, text, mGifLink);
            //sendFileViaMessenger(mVideoId, text, mGifLink);
        } else if (shareMethod == R.id.rb_msg) {
            sendLinkViaMessenger(mVideoId, text, mLink);
        }

        if (shareMethod != 0) {
            //we should make close button available immediatly after sharing
            if (mCloseButton.getVisibility() != View.VISIBLE) {
                mCloseButton.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btn_next) {
            share(mSelectedShareType);

        } else if (i == R.id.btn_close) {
            dismiss();

        }
    }

    public boolean isCanBeClosed() {
        return mCanBeClosed;
    }

    private void sendFileViaMessenger(int videoId, String text, String link) {

        SpiceManager spiceManager = ((SpiceContext) getActivity()).getSpiceManager();
        GifPreloadRequest gifPreloadRequest = new GifPreloadRequest(link);
        spiceManager.execute(gifPreloadRequest, new SimpleRequestListener<String[]>() {
            @Override
            public void onRequestSuccess(String[] strings) {
                super.onRequestSuccess(strings);

                File fileDest = new File(strings[0]);

                Uri contentUri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".fileprovider", fileDest);

                Intent msg = new Intent(Intent.ACTION_SEND);
                msg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                msg.setType("image/gif");
                msg.putExtra(Intent.EXTRA_STREAM, contentUri);
                msg.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                //msg.putExtra(Intent.EXTRA_SUBJECT, text);
                getActivity().startActivity(Intent.createChooser(msg, App.getInstance().getString(R.string.share_via_messenger)));
            }
        });

        /*File gifsPath = FilePathHelper.getGifsPath();
        File newFile = new File(gifsPath, file.getName());
        File file = new File(gifPreloadRequest.getCacheDirectory())

        Uri contentUri = FileProvider.getUriForFile(getContext(), "com.bastogram.fileprovider", newFile);

        Intent msg = new Intent(Intent.ACTION_SEND);
        msg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        msg.setType("image*//*");
        msg.putExtra(Intent.EXTRA_STREAM, contentUri);
        msg.putExtra(Intent.EXTRA_SUBJECT, text);
        getActivity().startActivity(msg);
        spiceManager.execute(new ShareVideoRequest(videoId, false, "messenger"), new SimpleRequestListener<BaseResponse>());*/
    }

    private void sendLinkViaMessenger(int videoId, String text, String link) {
        Intent msg = new Intent(Intent.ACTION_SEND);
        msg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        msg.setType("text/plain");
        msg.putExtra(Intent.EXTRA_SUBJECT, text);
        msg.putExtra(Intent.EXTRA_TEXT, link);
        getActivity().startActivity(Intent.createChooser(msg, App.getInstance().getString(R.string.share_via_messenger)));
        SpiceManager spiceManager = ((SpiceContext) getActivity()).getSpiceManager();
        spiceManager.execute(new ShareVideoRequest(videoId, false, "messenger"), new SimpleRequestListener<BaseResponse>());
    }

    private File getTempFile(Context context, String url) {
        File file = null;
        try {
            String fileName = Uri.parse(url).getLastPathSegment();
            file = File.createTempFile(fileName, null, context.getCacheDir());
        } catch (IOException e) {
            // Error while creating file
        }
        return file;
    }

    public static String getGifUrl(int videoId) {
        return "http://thehumanet.com/gif/shot" + videoId + ".gif";
    }

    public static class Builder {
        Bundle mArgs;

        public Builder() {
            mArgs = new Bundle();
        }

        public ShareDialog build() {
            ShareDialog fragment = new ShareDialog();
            fragment.setArguments(mArgs);
            return fragment;
        }

        public Builder setLink(String link) {
            mArgs.putString("link", link);
            return this;
        }

        public Builder setGifLink(String link) {
            mArgs.putString("gif_link", link);
            return this;
        }

        public Builder setVideoId(int videoId) {
            mArgs.putInt("video_id", videoId);
            return this;
        }

        public Builder setShareType(@ShareType int shareType) {
            mArgs.putInt("type", shareType);
            return this;
        }

        public Builder setCanBeClosed(boolean canBeClosed) {
            mArgs.putBoolean("closeable", canBeClosed);
            return this;
        }
    }
}
