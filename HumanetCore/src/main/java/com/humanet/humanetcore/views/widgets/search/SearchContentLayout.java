package com.humanet.humanetcore.views.widgets.search;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.LoaderManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.ILocationModel;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.modules.CategoriesDataLoader;
import com.humanet.humanetcore.modules.LocationDataLoader;
import com.humanet.humanetcore.views.adapters.location.SimpleSpinnerAdapter;
import com.humanet.humanetcore.views.behaviour.CategoriesListView;
import com.humanet.humanetcore.views.behaviour.IGrabSearchInfo;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.humanet.humanetcore.views.widgets.CityAutoCompleteView;
import com.humanet.humanetcore.views.widgets.CountrySpinnerView;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by ovitali on 26.10.2015.
 */
@SuppressLint("ViewConstructor")
public class SearchContentLayout extends LinearLayout implements
        IGrabSearchInfo,
        CategoriesListView,
        AdapterView.OnItemSelectedListener,
        View.OnClickListener {

    private Spinner mVideoTypeSpinner;
    private Spinner mVideoCategorySpinner;

    private long mSelectedCreatedFromDate = 0L;
    private long mSelectedCreatedToDate = 0L;

    private LocationDataLoader mLocationDataLoader;
    private CategoriesDataLoader mCategoriesDataLoader;

    public SearchContentLayout(Context context, SpiceManager spiceManager, LoaderManager loaderManager) {
        super(context);


        mCategoriesDataLoader = new CategoriesDataLoader(spiceManager, loaderManager, this, true);

        int padding = (int) ConverterUtil.dpToPix(context, 10);
        setPadding(padding, padding, padding, padding);
        setOrientation(VERTICAL);

        inflate(context, R.layout.layout_search_content, this);

        findViewById(R.id.created_from).setOnClickListener(this);
        findViewById(R.id.created_to).setOnClickListener(this);

        mVideoTypeSpinner = (Spinner) findViewById(R.id.sp_video_type);
        VideoType[] videoTypes = getVideoTypes();
        List<VideoTypeModel> videoTypeModelList = new ArrayList<>(videoTypes.length);
        for (VideoType type : videoTypes) {
            videoTypeModelList.add(new VideoTypeModel(type));
        }
        SimpleSpinnerAdapter videoTypeAdapter = new SimpleSpinnerAdapter<>(videoTypeModelList);
        videoTypeAdapter.setHasHitItem(true);
        mVideoTypeSpinner.setAdapter(videoTypeAdapter);
        mVideoTypeSpinner.setOnItemSelectedListener(this);

        mVideoCategorySpinner = (Spinner) findViewById(R.id.sp_category);
        mVideoCategorySpinner.setAdapter(new SimpleSpinnerAdapter<>(new ArrayList<Category>(20)));

        mLocationDataLoader = new LocationDataLoader(spiceManager, (CityAutoCompleteView) findViewById(R.id.sp_city), (CountrySpinnerView) findViewById(R.id.sp_country), -1);
        mLocationDataLoader.setItemColor(getResources().getColor(R.color.infoTextColor));
    }

    private VideoType[] getVideoTypes() {
        return new VideoType[]{
                null,
                VideoType.ALL,
                VideoType.VLOG,
                VideoType.NEWS,
                VideoType.MARKET_SELL
        };
    }

    @Override
    public void grabSearchInfo(ArgsMap params) {
        if (mSelectedCreatedFromDate != 0L)
            params.put("video_created_at_from", mSelectedCreatedFromDate);
        if (mSelectedCreatedToDate != 0L)
            params.put("video_created_at_to", mSelectedCreatedToDate);

        if (mLocationDataLoader.getSelectedCountryId() > 0)
            params.put("video_id_country", mLocationDataLoader.getSelectedCountryId());

        if (mLocationDataLoader.getSelectedCityId() > 0)
            params.put("video_id_city", mLocationDataLoader.getSelectedCityId());

        if (mVideoTypeSpinner.getSelectedItemId() > 0)
            params.put("video_type", mVideoTypeSpinner.getSelectedItemId());

        if (mVideoCategorySpinner.getSelectedItemId() > 0)
            params.put("id_category", mVideoCategorySpinner.getSelectedItemId());

        TextView textView = (TextView) findViewById(R.id.tags);
        String tags = textView.getText().toString();
        if (!TextUtils.isEmpty(tags)) {
            params.put("tags", tags);
        }
    }

    @Override
    public void onClick(final View v) {
        int viewId = v.getId();
        if (viewId == R.id.created_from) {
            selectFromDate();

        } else if (viewId == R.id.created_to) {
            selectToDate();

        }
    }

    private void selectFromDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mSelectedCreatedFromDate);

        DatePickerDialog datePickerDialog;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme, mOnDateFromChangedListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
        } else {
            datePickerDialog = new DatePickerDialog(getContext(), mOnDateFromChangedListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
        }
        datePickerDialog.show();
    }

    private void selectToDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mSelectedCreatedToDate);

        DatePickerDialog datePickerDialog;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme, mOnDateToChangedListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
        } else {
            datePickerDialog = new DatePickerDialog(getContext(), mOnDateToChangedListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
        }
        datePickerDialog.show();

    }

    private DatePickerDialog.OnDateSetListener mOnDateFromChangedListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(final DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            mSelectedCreatedFromDate = calendar.getTimeInMillis();
            setDateButton(R.id.created_from, mSelectedCreatedFromDate);
        }
    };
    private DatePickerDialog.OnDateSetListener mOnDateToChangedListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(final DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            mSelectedCreatedToDate = calendar.getTimeInMillis();
            setDateButton(R.id.created_to, mSelectedCreatedToDate);
        }
    };

    private void setDateButton(int buttonId, long value) {
        Button button = (Button) findViewById(buttonId);
        button.setText(Constants.DATE_FORMAT.format(value));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int viewId = parent.getId();
        if (viewId == R.id.sp_video_type) {
            loadCategories(getVideoTypes()[position]);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    //------------------------

    private void loadCategories(VideoType videoType) {
        if (videoType != null)
            mCategoriesDataLoader.loadCategories(videoType);
        else
            setCategories(null);
    }

    @Override
    public void setCategories(final List<Category> categoryList) {
        if (categoryList != null && categoryList.size() > 0) {
            ((SimpleSpinnerAdapter<Category>) mVideoCategorySpinner.getAdapter()).set(categoryList);
            mVideoCategorySpinner.setVisibility(VISIBLE);
        } else {
            mVideoCategorySpinner.setVisibility(GONE);
        }
    }

    @Override
    public Activity getActivity() {
        return (Activity) getContext();
    }


    private class VideoTypeModel implements ILocationModel {
        private VideoType mVideoType;

        public VideoTypeModel(VideoType videoType) {
            mVideoType = videoType;
        }

        @Override
        public int getId() {
            return mVideoType != null ? mVideoType.getId() : -1;
        }

        @Override
        public String getTitle() {
            if (mVideoType == null)
                return getContext().getString(R.string.search_video_type);

            switch (mVideoType) {
                case ALL:
                    return getContext().getString(R.string.search_content_videostream);

                case NEWS:
                    return getContext().getString(R.string.online_tab_news).replace("\n", "").replace("/ ", "/");

                case VLOG:
                    return getContext().getString(R.string.online_tab_vlog).replace("\n", "").replace("/ ", "/");

                case MY_CHOICE:
                    return getContext().getString(R.string.online_tab_my_choice).replace("\n", "").replace("/ ", "/");

                case MARKET_SHARE:
                case MARKET_SELL:
                    return getContext().getString(R.string.balance_category_type_market).replace("\n", "").replace("/ ", "/");

                default:
                    throw new RuntimeException("unexpected video type");

            }
        }
    }


}
