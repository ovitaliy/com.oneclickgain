package com.humanet.humanetcore.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.BaseMainActivity;
import com.humanet.humanetcore.activities.CaptureActivity;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.video.ComplaintVideoRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.fragments.VideoListFragment;
import com.humanet.humanetcore.interfaces.SpiceContext;
import com.humanet.humanetcore.model.Video;

/**
 * Created by michael on 25.11.14.
 */
public class MoreDialog {
    private Context mContext;
    private Dialog mDialog;
    private View mView;
    private Video mVideo;
    private Video mVideoParent;
    private int mAction = -1;

    public MoreDialog(Context context, Video video, Video videoParent) {
        mContext = context;
        mVideo = video;
        mDialog = create();
        mVideoParent = videoParent;
        setupView();
    }

    public MoreDialog show() {
        mDialog.show();
        return this;
    }

    /*public void dismiss() {
        mDialog.dismiss();
    }*/

    private Dialog create() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_more, null);
        dialog.setContentView(mView);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });
        return dialog;
    }

    private void recordAnswer() {
        CaptureActivity.startNewInstance(mContext, mVideo);
    }

    private void watchResponses() {
        VideoListFragment fragment = new VideoListFragment.Builder(null)
                .setReplyId(mVideo.getVideoId())
                .build();

        ((BaseMainActivity) mContext).startFragment(fragment, true);
        dismiss();
    }

    private void setupView() {
        RadioButton responsesButton = (RadioButton) mView.findViewById(R.id.rb_watch_responses);
        if (mVideo.getReplays() > 0) {
            responsesButton.setVisibility(View.VISIBLE);
            responsesButton.setText(App.getInstance().getString(R.string.record_answer_watch, mVideo.getReplays()));
        } else {
            responsesButton.setVisibility(View.GONE);
        }

      /*  RadioButton finishGameButton = (RadioButton) mView.findViewById(R.id.rb_finish_game);
        if (mVideo.getGameId() == GameType.WHAT_WHERE_WHEN.getId() && mVideo.getReplyId() == 0 && mVideo.getAuthorId() == AppUser.getUid()) {
            finishGameButton.setVisibility(View.VISIBLE);
        } else {
            finishGameButton.setVisibility(View.GONE);
        }

        RadioButton setWinnerButton = (RadioButton) mView.findViewById(R.id.rb_select_winner);
        if (mVideoParent != null && mVideoParent.getGameId() == GameType.WHAT_WHERE_WHEN.getId() && mVideoParent.getAuthorId() == AppUser.getUid()) {
            setWinnerButton.setVisibility(View.VISIBLE);
        } else {
            setWinnerButton.setVisibility(View.GONE);
        }*/

        RadioGroup rg = (RadioGroup) mView.findViewById(R.id.group_share);
        rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mAction = checkedId;
                if (mAction == R.id.rb_share) {
                    int shareType = ShareDialog.TYPE_SHOT;

                    new ShareDialog.Builder()
                            .setShareType(shareType)
                            .setLink(mVideo.getShortUrl())
                            .setGifLink(ShareDialog.getGifUrl(mVideo.getVideoId()))
                            .setVideoId(mVideo.getVideoId())
                            .setCanBeClosed(true)
                            .build()
                            .show(((BaseActivity) mContext).getSupportFragmentManager(), ShareDialog.TAG);

                    dismiss();

                } else if (mAction == R.id.rb_complaint) {
                    ((SpiceContext) mContext).getSpiceManager().execute(new ComplaintVideoRequest(mVideo.getVideoId()), new SimpleRequestListener<BaseResponse>() {
                        @Override
                        public void onRequestSuccess(BaseResponse baseResponse) {
                            super.onRequestSuccess(baseResponse);
                            Toast.makeText(
                                    mContext,
                                    mContext.getResources().getString(R.string.complaint_send),
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }
                    });
                    dismiss();

                } else if (mAction == R.id.rb_answer) {
                    recordAnswer();
                    dismiss();

                } else if (mAction == R.id.rb_watch_responses) {
                    watchResponses();

                } else {
                    dismiss();

                }
            }
        });
    }

    private void dismiss() {
        mDialog.dismiss();
    }
}
