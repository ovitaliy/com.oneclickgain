package com.humanet.humanetcore.views.widgets.profile;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.widget.FrameLayout;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.views.widgets.CirclePickerView;
import com.humanet.humanetcore.views.widgets.items.StatisticItem;

import java.util.ArrayList;

/**
 * Created by Uran on 08.06.2016.
 */
public class QuestionnaireView extends FrameLayout {

    private CirclePickerView mQuestionnaireView;

    public QuestionnaireView(Context context) {
        super(context);

        mQuestionnaireView = new CirclePickerView(context);
        mQuestionnaireView.setBackgroundResource(R.drawable.circle_picker_bg);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;

        addView(mQuestionnaireView, params);
    }

    public void fillQuestionnaireInfo(@NonNull UserInfo userInfo) {
        ArrayList<CirclePickerItem> list = new ArrayList<>(7);

        int i = 1;

        if (userInfo.getSport() != null) {
            list.add(new StatisticItem(i++, userInfo.getSport()));
        }

        if (userInfo.getPet() != null) {
            list.add(new StatisticItem(i++, userInfo.getPet()));
        }

        if (userInfo.getHobbie() != null) {
            list.add(new StatisticItem(i++, userInfo.getHobbie()));
        }

        if (userInfo.getReligion() != null) {
            list.add(new StatisticItem(i++, userInfo.getReligion()));
        }

        if (userInfo.getInterest() != null) {
            list.add(new StatisticItem(i++, userInfo.getInterest()));
        }

        if (userInfo.getDegree() != null && userInfo.getDegree().getId() >= 0) {
            list.add(new StatisticItem(i++, userInfo.getDegree()));
        }
        if (userInfo.getJobType() != null && userInfo.getJobType().getId() >= 0) {
            list.add(new StatisticItem(i, userInfo.getJobType()));
        }

        mQuestionnaireView.internalFill(list, 90, 360d / list.size(), -1, null);
        mQuestionnaireView.invalidate();

    }

}
