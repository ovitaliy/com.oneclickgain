package com.humanet.humanetcore.views.widgets.items;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.utils.BitmapDecoder;
import com.humanet.humanetcore.views.utils.ImageCroper;
import com.humanet.humanetcore.views.utils.OnCacheUpdateListener;
import com.humanet.humanetcore.views.widgets.CircleImageView;
import com.humanet.humanetcore.views.widgets.CircleLayout;

/**
 * Created by Владимир on 03.12.2014.
 */
public class FrameItemView extends CircleLayout.ItemWrapper {

    private CircleImageView mImageView;
    private String mData;
    private OnCacheUpdateListener mCacheListener;

    public FrameItemView(Context context) {
        super(context);
        View.inflate(context, R.layout.item_circle, this);
        setGravity(Gravity.CENTER_HORIZONTAL);
        setOrientation(LinearLayout.VERTICAL);

        if (isInEditMode())
            return;

        mImageView = (CircleImageView) findViewById(R.id.item_image);
        mImageView.setBackgroundResource(R.drawable.circle_bg);
        mImageView.setBorderSize(getResources().getDimensionPixelOffset(R.dimen.stream_item_small_border));
    }


    public void setData(String data) {
        mData = data;
        setBitmap();
    }

    public void setOnCacheUpdateListener(OnCacheUpdateListener listener) {
        mCacheListener = listener;
    }

    private void setBitmap() {
        LoadImageTask task = new LoadImageTask(getContext(), mImageView, mData, mCacheListener);
        task.executeOnExecutor(LoadImageTask.SERIAL_EXECUTOR);
    }

    private static class LoadImageTask extends AsyncTask<Void, Void, Bitmap> {

        private CircleImageView mImageView;
        private String mImagePath;
        private Context mContext;

        OnCacheUpdateListener mCacheListener;

        private boolean fromCache = true;

        LoadImageTask(Context context, CircleImageView imageView, String imagePath, OnCacheUpdateListener cacheListener) {
            mImageView = imageView;
            mImagePath = imagePath;
            mContext = context;
            mCacheListener = cacheListener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            int w = CircleLayout.ItemWrapper.getW() - mContext.getResources().getDimensionPixelSize(R.dimen.stream_item_small_border) * 2;
            mImageView.getLayoutParams().width = w;
            mImageView.getLayoutParams().height = w;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap bitmap = mCacheListener.getCachedBitmap(mImagePath);
            // int size = getW() - mContext.getResources().getDimensionPixelSize(R.dimen.stream_item_small_border) * 4;
            if (bitmap == null) {
                fromCache = false;
                int camId = 0;
                int border = mContext.getResources().getDimensionPixelSize(R.dimen.stream_item_small_border);

                if (!TextUtils.isEmpty(mImagePath)) {
                    bitmap = BitmapDecoder.createSquareBitmap(mImagePath, 100, camId, false);
                }

                if (bitmap != null) {
                    bitmap = ImageCroper.getCircularBitmap(bitmap, border, mContext.getResources().getColor(R.color.colorPrimary));
                }

            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (fromCache) {
                if (mImageView != null && bitmap != null && !isCancelled()) {
                    mImageView.setImageBitmapWithoutRounding(bitmap);
                    mCacheListener.onCacheUpdated(mImagePath, bitmap);
                }
            } else {
                mImageView.setImageBitmapWithoutRounding(bitmap);
            }
        }
    }
}
