package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.humanet.humanetcore.R;

/**
 * Created by Владимир on 01.12.2014.
 */
public class BubbleView extends FrameLayout {

    private TextView mCountView;

    public BubbleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.view_bubble, this);
        mCountView = (TextView) findViewById(R.id.bubble_view_text);
        mCountView.setClickable(false);
        setCount(0);
    }

    public void setCount(int count) {
        mCountView.setText(String.valueOf(count));
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        getChildAt(0).setId(getId());
        getChildAt(0).setOnClickListener(l);
    }
}
