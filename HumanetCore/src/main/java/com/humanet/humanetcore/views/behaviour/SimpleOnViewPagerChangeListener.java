package com.humanet.humanetcore.views.behaviour;

import android.support.v4.view.ViewPager;

/**
 * Created by ovi on 2/19/16.
 */

/**
 * use {@link android.support.v4.view.ViewPager.SimpleOnPageChangeListener}
 */
@Deprecated
public class SimpleOnViewPagerChangeListener implements ViewPager.OnPageChangeListener {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
