package com.humanet.humanetcore.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.vote.Vote;

/**
 * Created by ovitali on 16.11.2015.
 */
public class VoteListAdapter extends CursorAdapter {

    private boolean mMyVotes;

    public final void setMyVotes(final boolean myVotes) {
        mMyVotes = myVotes;
    }

    public VoteListAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public final int getItemViewType(int position) {
        getCursor().moveToPosition(position);
        return Vote.fromCursor(getCursor()).getMyVote();
    }


    @Override
    public final View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vote_question, parent, false);
        new ViewHolder(view);
        return view;
    }

    @Override
    public final void bindView(View view, Context context, Cursor cursor) {
        Vote vote = Vote.fromCursor(cursor);
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        if (mMyVotes) {
            viewHolder.statusView.setVisibility(View.VISIBLE);
            viewHolder.statusView.setText(Vote.getVoteStatus(vote.getStatus()));

            viewHolder.checkBox.setVisibility(View.GONE);
        } else {
            viewHolder.checkBox.setVisibility(View.VISIBLE);
            viewHolder.checkBox.setChecked(vote.isVoted());

            viewHolder.statusView.setVisibility(View.GONE);
        }

        String title = vote.getTitle();
        if (title == null){
            title = vote.getDescr();
        }
        viewHolder.titleView.setText(title);
    }

    final class ViewHolder {
        final View itemView;

        final TextView titleView;
        final TextView statusView;
        final CheckBox checkBox;

        public ViewHolder(View itemView) {
            this.itemView = itemView;
            itemView.setTag(this);
            titleView = (TextView) itemView.findViewById(R.id.question);
            statusView = (TextView) itemView.findViewById(R.id.status);
            checkBox = (CheckBox) itemView.findViewById(R.id.voted);
        }
    }
}
