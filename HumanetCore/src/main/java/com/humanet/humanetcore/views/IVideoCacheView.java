package com.humanet.humanetcore.views;

/**
 * Created by ovi on 16.05.2016.
 */
public interface IVideoCacheView {

   void publishCachingProgress(float progress);
   void publishCachingComplete(boolean success);

}
