package com.humanet.humanetcore.views.adapters.location;

import com.humanet.humanetcore.model.City;
import com.humanet.humanetcore.views.adapters.BaseAutoCompleteAdapter;

/**
 * Created by ovitali on 28.08.2015.
 */
public class CityAutoCompleteAdapter extends BaseAutoCompleteAdapter<City> {

}
