package com.humanet.humanetcore.views.adapters;

import android.content.Context;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.utils.ConverterUtil;

/**
 * Created by Deni on 04.08.2015.
 */
public class TipsAdapter extends BaseAdapter {

    public TipsAdapter(Context context, String[] data) {
        this(context);
        mData = data;
    }

    public TipsAdapter(Context context) {
        mContext = context;
    }

    private Context mContext;
    private String[] mData;

    @Override
    public int getCount() {
        if (mData == null)
            return 0;
        return mData.length;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public String getItem(int position) {
        return mData[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = null;
        if (convertView == null) {
            textView = new TextView(mContext);
            textView.setMovementMethod(LinkMovementMethod.getInstance());
            textView.setTextColor(mContext.getResources().getColor(R.color.infoTextColor));
            textView.setTextSize(18);

            int padding = (int) ConverterUtil.dpToPix(mContext, 10);
            textView.setPadding(padding, 0, padding, padding / 2);

        } else {
            textView = (TextView) convertView;
        }
        textView.setText(Html.fromHtml(getItem(position)));

        return textView;
    }

    public void setData(String[] data) {
        mData = data;
        notifyDataSetChanged();
    }
}
