package com.humanet.humanetcore.views.widgets.items;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.humanet.humanetcore.views.widgets.CircleImageView;
import com.humanet.humanetcore.views.widgets.CircleLayout;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Владимир on 03.12.2014.
 */
public class OkItemView extends CircleLayout.ItemWrapper {

    private CircleImageView mImageView;
    private TextView mTitleView;

    public OkItemView(Context context) {
        super(context);
        init(context, 10);
    }

    public OkItemView(Context context, int imagePadding) {
        super(context);
        init(context, imagePadding);
    }

    private void init(Context context, int paddingDp) {
        View.inflate(context, R.layout.item_circle, this);
        setGravity(Gravity.CENTER);
        setOrientation(LinearLayout.VERTICAL);

        if (isInEditMode())
            return;

        mImageView = (CircleImageView) findViewById(R.id.item_image);
        mImageView.setBackgroundResource(R.drawable.circle_shape);
        int padding = (int) ConverterUtil.dpToPix(context, paddingDp);
        mImageView.setPadding(padding, padding, padding, padding);

        mTitleView = (TextView) findViewById(R.id.item_title);
    }


    public void setData(int title, int imageId) {
        if (title == 0)
            mTitleView.setText("");
        else
            mTitleView.setText(title);
        mImageView.setImageResource(imageId);
    }

    public void setData(String title, String image) {
        mTitleView.setText(title);
        ImageLoader.getInstance().displayImage(image, mImageView);
    }


}
