package com.humanet.humanetcore.views.widgets.search;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.fragments.profile.ProfileInterestsFragment;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.model.enums.selectable.BaseItem;
import com.humanet.humanetcore.model.enums.selectable.QuestionnaireItem;
import com.humanet.humanetcore.utils.QuestionnaireHelper;
import com.humanet.humanetcore.views.behaviour.IGrabSearchInfo;
import com.humanet.humanetcore.views.widgets.CirclePickerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovitali on 26.10.2015.
 */
public class SearchQuestionnaireLayout extends ScrollView implements IGrabSearchInfo, CirclePickerView.OnPickListener {

    int mInterest = -1;
    int mSport = -1;
    int mHobby = -1;
    int mReligion = -1;
    int mPets = -1;


    public SearchQuestionnaireLayout(Context context) {
        super(context);

        LinearLayout container = new LinearLayout(context);
        addView(container);
        container.setOrientation(LinearLayout.VERTICAL);

        QuestionnaireItem[] items = QuestionnaireItem.values();

        double radius = 0;
        int imageSize;

        radius = CirclePickerView.isResizeItemOnSelection() ? App.WIDTH_WITHOUT_MARGINS * 3d / 8d : (App.WIDTH_WITHOUT_MARGINS / 3d);

        imageSize = App.WIDTH_WITHOUT_MARGINS / (CirclePickerView.isResizeItemOnSelection() ? 8 : 4);
        if (radius * Math.PI * 2 < (imageSize) * 16) {
            double newImageSize = (int) (radius * Math.PI * 2d / 16);
            radius += (imageSize - newImageSize) / 2;
            imageSize = (int) (newImageSize + (imageSize - newImageSize) / 4);

            imageSize *= 0.8d;
        }

        LinearLayout.LayoutParams layoutParams;

        for (QuestionnaireItem item : items) {
            View textView = ProfileInterestsFragment.getDelegator().createTextView(getContext(), item.getSearchTitle());
            container.addView(textView);

            final CirclePickerView picker = new CirclePickerView(context);
            picker.setImageSize(imageSize);
            picker.setRadius(radius);
            picker.setBackgroundResource(R.drawable.circle_picker_bg);

            List<BaseItem> questionnaireItems = QuestionnaireHelper.getValues(item);
            final int selectedId = -1;

            picker.fill(new ArrayList<CirclePickerItem>(questionnaireItems), selectedId, this);

            picker.setTag(item);

            int size = App.WIDTH_WITHOUT_MARGINS;
            layoutParams = new LinearLayout.LayoutParams(size, size);
            layoutParams.setMargins(App.MARGIN, 0, App.MARGIN, App.MARGIN);
            container.addView(picker, layoutParams);
        }
    }

    @Override
    public void grabSearchInfo(ArgsMap argsMap) {
        if (mInterest > -1) {
            argsMap.put("interest", mInterest);
        }
        if (mSport > -1) {
            argsMap.put("sport", mSport);
        }
        if (mReligion > -1) {
            argsMap.put("religion", mReligion);
        }
        if (mHobby > -1) {
            argsMap.put("hobbie", mHobby);
        }
        if (mPets > -1) {
            argsMap.put("pet", mPets);
        }
    }


    @Override
    public void onPick(View view, CirclePickerItem element) {
        QuestionnaireItem questionnaireItem = (QuestionnaireItem) view.getTag();
        switch (questionnaireItem) {
            case HOBBIES:
                mHobby = element.getId();
                break;
            case INTERESTS:
                mInterest = element.getId();
                break;
            case SPORT:
                mSport = element.getId();
                break;
            case RELIGION:
                mReligion = element.getId();
                break;
            case PET:
                mPets = element.getId();
                break;


        }
    }
}
