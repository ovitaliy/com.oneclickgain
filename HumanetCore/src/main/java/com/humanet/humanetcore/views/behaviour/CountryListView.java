package com.humanet.humanetcore.views.behaviour;


import com.humanet.humanetcore.model.Country;

import java.util.List;

/**
 * Created by ovitali on 15.12.2015.
 */
public interface CountryListView {
    void setCountryList(List<Country> countries);
}
