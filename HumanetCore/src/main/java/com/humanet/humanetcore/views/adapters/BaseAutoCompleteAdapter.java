package com.humanet.humanetcore.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.ILocationModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

/**
 * Created by serezha on 05.08.16.
 */
public abstract class BaseAutoCompleteAdapter<T extends ILocationModel> extends BaseAdapter implements Filterable {

	private List<T> mAllItems = new ArrayList<>();
	private List<T> mFilteredItems = new ArrayList<>();

	@Override
	public int getCount() {
		return mFilteredItems.size();
	}

	@Override
	public T getItem(int i) {
		return mFilteredItems.get(i);
	}

	@Override
	public long getItemId(int i) {
		return mFilteredItems.get(i).getId();
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		TextView textView;

		if(view != null) {
			textView = (TextView) view;
		} else {
			textView = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_autocomplete, viewGroup, false);
		}

		textView.setText(getItem(i).getTitle());

		return textView;
	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				FilterResults filterResults = new FilterResults();

				if(charSequence == null) {
					return filterResults;
				}

				String start = charSequence.toString().toLowerCase(Locale.getDefault());

				List<T> result = new ArrayList<>();

				for(int i = 0; i < mAllItems.size(); i++) {
					T object = mAllItems.get(i);
					if(object.getTitle().toLowerCase(Locale.getDefault()).startsWith(start)) {
						result.add(object);
					}
				}

				filterResults.values = result;
				filterResults.count = result.size();

				return filterResults;
			}

			@Override
			protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
				if(filterResults.count > 0) {
					mFilteredItems = (List<T>) filterResults.values;
				} else {
					mFilteredItems.clear();
					mFilteredItems.addAll(mAllItems);
				}
				notifyDataSetChanged();
			}
		};
	}

	public void clear() {
		if(mAllItems != null && mAllItems.size() > 0) {
			mAllItems.clear();
			notifyDataSetChanged();
		}
	}

	public void set(Collection<T> list) {
		if(mAllItems.size() > 0) {
			mAllItems.clear();
			mFilteredItems.clear();
		}
		mAllItems.addAll(list);
		mFilteredItems.addAll(list);

		notifyDataSetChanged();
	}

}
