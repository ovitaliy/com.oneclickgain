package com.humanet.humanetcore.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.views.widgets.items.VideoItemView;

public class VideoListAdapter extends CursorAdapter {

    private Context mContext;
    private VideoType mVideoType;
    private Category mCategory;
    private Video mVideoParent;

    private int mCurrentUserId;

    private VideoItemView.OnVideoChooseListener mOnVideoChooseListener;


    public VideoListAdapter(Context context, Cursor cursor, int currentUserId, VideoType videoType, Category category, Video videoParent) {
        super(context, cursor, true);
        mContext = context;
        mCurrentUserId = currentUserId;
        mVideoType = videoType;
        mCategory = category;
        mVideoParent = videoParent;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        VideoItemView view = new VideoItemView(mContext, mCurrentUserId, mVideoType, mCategory);
        view.setOnVideoChooseListener(mOnVideoChooseListener);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ((VideoItemView) view).setData(Video.fromCursor(cursor), mVideoParent);
    }

    public void setOnVideoChooseListener(VideoItemView.OnVideoChooseListener onVideoChooseListener) {
        mOnVideoChooseListener = onVideoChooseListener;
    }
}
