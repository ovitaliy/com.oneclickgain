package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by ovitali on 03.09.2015.
 */
public class MenuButton extends Button {

    public MenuButton(Context context) {
        this(context, null);
    }

    public MenuButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setSaveEnabled(true);

    }


}
