package com.humanet.humanetcore.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;


/**
 * Created by Владимир on 01.10.2014.
 */
public class ProgressDialog {

    private Context mContext;
    private volatile Dialog mDialog;
    private ProgressDialogView mView;

    public ProgressDialog(Context context) {
        mContext = context;
        mDialog = create();
    }

    public void setTitle(String title) {
        mView.setTitle(title);
    }

    public void setTitle(int title) {
        mView.setTitle(mContext.getResources().getString(title));
    }

    public void showTitle() {
        mView.showTitle();
    }

    public void setCancelable(boolean isCancelable) {
        if (mDialog != null) {
            mDialog.setCancelable(isCancelable);
        }
    }

    public void setOnCancelListener(DialogInterface.OnCancelListener listener) {
        mDialog.setOnCancelListener(listener);
    }

    public void show() {
        if (mDialog != null && !isShowing()) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (isShowing()) {
            try {
                mDialog.dismiss();
            } catch (Exception ignore) {
                ignore.printStackTrace();
            }
        }
    }

    private Dialog create() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mView = new ProgressDialogView(mContext);
        dialog.setContentView(mView);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(mContext.getResources().getColor(android.R.color.transparent)));
        return dialog;
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public boolean isDismissed() {
        return mDialog == null || !mDialog.isShowing();
    }
}
