package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.humanet.humanetcore.R;

/**
 * Created by Uran on 05.07.2016.
 */
public class VoteProgressBar extends View {

    private final int mBackgroundColor;
    private final int mSelectionColor;

    private final Paint mPaint = new Paint();

    private float mProgress;

    public VoteProgressBar(Context context) {
        this(context, null);
    }

    public VoteProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);


        mBackgroundColor = getResources().getColor(R.color.gray_light);
        mSelectionColor = getResources().getColor(R.color.colorPrimary);

        setWillNotDraw(false);
    }

    public void setProgress(float progress) {
        mProgress = progress;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPaint.setColor(mBackgroundColor);
        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), mPaint);

        if (mProgress > 0) {
            mPaint.setColor(mSelectionColor);
            canvas.drawRect(0, 0, canvas.getWidth() * mProgress, canvas.getHeight(), mPaint);
        }
    }
}
