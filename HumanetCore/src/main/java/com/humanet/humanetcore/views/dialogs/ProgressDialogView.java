package com.humanet.humanetcore.views.dialogs;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.humanet.humanetcore.R;

public class ProgressDialogView extends RelativeLayout {
    private TextView mTitle;

    public ProgressDialogView(Context context) {
        super(context);
        inflate(context, R.layout.dialog_progress, this);
        mTitle = (TextView) findViewById(R.id.dialog_title);
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void hideTitle() {
        mTitle.setVisibility(GONE);
    }

    public void showTitle() {
        mTitle.setVisibility(VISIBLE);
    }
}
