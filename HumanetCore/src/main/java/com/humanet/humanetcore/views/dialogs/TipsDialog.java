package com.humanet.humanetcore.views.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.views.adapters.TipsAdapter;

import org.apache.commons.io.IOUtils;

/**
 * Created by Denis on 24.05.2015.
 */
public class TipsDialog extends DialogFragment implements DialogInterface.OnClickListener, AbsListView.OnScrollListener, View.OnClickListener {
    private TextView mTitleView;
    private String[] mTitles;

    private int mPrevVisibleItem = -1;

    public static TipsDialog newInstance() {
        return new TipsDialog();
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_tips, new LinearLayout(getActivity()));
        ListView listView = (ListView) view.findViewById(R.id.tips_list);
        View buttonClose = view.findViewById(R.id.btn_close);

        String lang = Language.getSystem().getRequestParam();

        String tipsText = "";
        try {
            tipsText = IOUtils.toString(getActivity().getAssets().open("texts/tips/" + lang + ".html"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        String split = "&";
        String[] tips = tipsText.split(split);
        TipsAdapter adapter = new TipsAdapter(getActivity(), tips);

        mTitles = new String[tips.length];
        for (int i = 0; i < tips.length; i++) {
            String tip = tips[i];
            mTitles[i] = tip.split("<br/>")[0];
        }

        listView.setAdapter(adapter);
        mTitleView = (TextView) view.findViewById(R.id.title);
        listView.setOnScrollListener(this);

        buttonClose.setOnClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(view);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dismiss();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem != mPrevVisibleItem) {
            if (mTitles.length > 0) {
                String title = mTitles[firstVisibleItem];
                mTitleView.setText(title);
                mPrevVisibleItem = firstVisibleItem;
            }
        }
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.btn_close) {
            dismiss();

        }
    }
}
