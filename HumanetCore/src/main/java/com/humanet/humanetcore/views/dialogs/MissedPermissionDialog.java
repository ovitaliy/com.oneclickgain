package com.humanet.humanetcore.views.dialogs;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

import com.humanet.humanetcore.R;

/**
 * Created by Uran on 08.07.2016.
 */
public class MissedPermissionDialog {

    public static void show(final Context context, @Nullable final DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(R.string.error_no_permissions);
        builder.setPositiveButton(R.string.settings, (dialogInterface, i) -> {
            showInstalledAppDetails(context, context.getPackageName());
            if (onClickListener != null)
                onClickListener.onClick(dialogInterface, i);
        });
        builder.setNegativeButton(R.string.cancel, onClickListener);

        builder.show();
    }


    public static void showInstalledAppDetails(Context context, String packageName) {
        try {
            //Open the specific App Info page:
            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + packageName));
            context.startActivity(intent);

        } catch (ActivityNotFoundException e) {
            //e.printStackTrace();

            //Open the generic Apps page:
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
            context.startActivity(intent);

        }
    }

}
