package com.humanet.humanetcore.model;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.model.enums.Language;

import java.lang.reflect.Type;
import java.util.Locale;

/**
 * Created by ovitali on 09.10.2015.
 */
public class RatingUser extends BaseUser{

    @SerializedName("id")
    private int mId;

    @SerializedName("first_name")
    private String mFirstName;

    @SerializedName("last_name")
    private String mLastName;

    @SerializedName("rating")
    private float mRating;

    @SerializedName("my_percent")
    private int mPercent;

    private String mCity;
    private String mCountry;

    private Language mLang;

    public int getId() {
        return mId;
    }

    public String getUserName() {
        return mFirstName + " " + mLastName;
    }

    public String getCity() {
        return mCity;
    }

    public String getCountry() {
        return mCountry;
    }

    public float getRating() {
        return mRating;
    }

    public Language getLang() {
        return mLang;
    }

    public static class Deserializer implements JsonDeserializer<RatingUser> {
        @Override
        public RatingUser deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            RatingUser ratingUser = new Gson().fromJson(json, RatingUser.class);

            try {

                Language lang = AppUser.getInstance().get().getLanguage();
                String slang = lang.toString().toLowerCase(Locale.getDefault());

                String key;

                key = "country_" + slang;
                ratingUser.mCountry = ((JsonObject) json).get(key).getAsString();

                key = "city_" + slang;
                ratingUser.mCity = ((JsonObject) json).get(key).getAsString();
            }catch (Exception ignore){

            }

            return ratingUser;
        }
    }

}
