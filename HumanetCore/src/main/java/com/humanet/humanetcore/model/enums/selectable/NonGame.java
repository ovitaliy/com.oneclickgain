package com.humanet.humanetcore.model.enums.selectable;

import java.util.List;

import static com.humanet.humanetcore.Constants.OTHER_OPTION;

/**
 * Created by Denis on 05.03.2015.
 */
public class NonGame extends BaseItem {

    private static String VALUES[] = new String[]{
            "struggle",
            "wrestling",
            "fighting_without_rules",
            "fighting",
            "diving",
            "boxing",
            "swim",
            "bike",
            "fitness_and_street_workout",
            "aerobics",
            "gymnastics",
            "cheerleading",
            "dancing",
            "bodybuilding",
            "shooting",
            OTHER_OPTION};



    public NonGame(int id, String item) {
        super(id, item);
    }

    public static NonGame getById(String id) {
        return BaseItem.getById(NonGame.class, VALUES, id);
    }


    public static List<NonGame> values() {
        return BaseItem.values(NonGame.class, VALUES);
    }
}
