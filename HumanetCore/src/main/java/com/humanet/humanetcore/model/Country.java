package com.humanet.humanetcore.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.Language;

/**
 * Created by Denis on 07.05.2015.
 */
public class Country implements ILocationModel {

    public Country() {
    }

    public Country(String title) {
        mTitle = title;
    }

    @SerializedName("id")
    private int mId;

    @SerializedName("country_rus")
    private String mTitleRus;

    @SerializedName("country_eng")
    private String mTitleEng;

    @SerializedName("country_esp")
    private String mTitleEsp;

    @SerializedName("code")
    private String mCode;

    private String mTitle;

    public String getTitleRus() {
        return mTitleRus;
    }

    public void setTitleRus(String titleRus) {
        mTitleRus = titleRus;
    }

    public String getTitleEng() {
        return mTitleEng;
    }

    public void setTitleEng(String titleEng) {
        mTitleEng = titleEng;
    }

    public String getTitleEsp() {
        return mTitleEsp;
    }

    public void setTitleEsp(String titleEsp) {
        mTitleEsp = titleEsp;
    }


    public String getCode() {
        return mCode;
    }

    public String getTitle() {
        if (TextUtils.isEmpty(mTitle)) {
            Language lang = Language.getSystem();
            switch (lang) {
                case RUS:
                    setTitle(mTitleRus);
                    break;
                case ESP:
                    setTitle(mTitleEsp);
                    break;
                case ENG:
                    setTitle(mTitleEng);
                    break;
            }
        }
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContentDescriptor.Countries.Cols.ID, mId);
        contentValues.put(ContentDescriptor.Countries.Cols.NAME_ENG, mTitleEng);
        contentValues.put(ContentDescriptor.Countries.Cols.NAME_RUS, mTitleRus);
        contentValues.put(ContentDescriptor.Countries.Cols.NAME_ESP, mTitleEsp);
        contentValues.put(ContentDescriptor.Countries.Cols.COUNTRY_CODE, mCode);
        return contentValues;
    }

    public static Country fromCursor(Cursor cursor) {
        Country country = new Country();
        country.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.ID)));
     /*   country.setPhoneCode(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.COUNTRY_CODE)));
        country.setPhoneLenght(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.PHONE_LENGHT)));
        */

        country.mTitleRus = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.NAME_RUS));
        country.mTitleEsp = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.NAME_ESP));
        country.mTitleEng = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.NAME_ENG));

        country.mCode = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Countries.Cols.COUNTRY_CODE));

        return country;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }


    public static class Response extends BaseResponse {
        private Country[] countries;

        public Country[] getCountries() {
            return countries;
        }
    }


}
