package com.humanet.humanetcore.model;

/**
 * Created by ovitali on 09.02.2015.
 */
public class CategoryStatistic extends Category {
    private int mImageResId;

    private void setImageResId(int categoryId) {
        switch (categoryId) {
            case 0:
//                mImageResId = R.drawable.category_emoticons;
                break;
            /*case 1:
                mImageResId = R.drawable.interest_movies;
                break;
            case 2:
                mImageResId = R.drawable.interest_fashion;
                break;
            case 3:
                mImageResId = R.drawable.interest_games;
                break;
            case 4:
                mImageResId = R.drawable.interest_history;
                break;
            case 5:
                mImageResId = R.drawable.interest_music;
                break;
            case 6:
                mImageResId = R.drawable.interest_news;
                break;
            case 7:
                mImageResId = R.drawable.interest_humor;
                break;
            case 8:
                mImageResId = R.drawable.interest_weapons;
                break;
            case 9:
                mImageResId = R.drawable.interest_science;
                break;
            case 10:
                mImageResId = R.drawable.interest_gadgets;
                break;
            case 11:
                mImageResId = R.drawable.hobby_travel;
                break;
            case 12:
                mImageResId = R.drawable.interest_health;
                break;
            case 13:
                mImageResId = R.drawable.interest_food;
                break;
            case 14:
                mImageResId = R.drawable.interest_celebrities;
                break;
            case 15:
                mImageResId = R.drawable.interest_entertainment;
                break;
            case 16:
                mImageResId = R.drawable.category_animals;
                break;*/
            case 17:
          //      mImageResId = R.drawable.category_sport;
                break;
        }
    }

    public CategoryStatistic(Category category, int statistic) {
        setId(category.getId());
        setTitle(category.getTitle());
        this.statistic = statistic;
        setImageResId(category.getId());
    }

    private int statistic;

    public int getStatistic() {
        return statistic;
    }

    public void setStatistic(int statistic) {
        this.statistic = statistic;
    }

    public int getImageResId() {
        return mImageResId;
    }
}
