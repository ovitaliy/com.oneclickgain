package com.humanet.humanetcore.model.enums.selectable;

import java.util.List;

import static com.humanet.humanetcore.Constants.OTHER_OPTION;

/**
 * Created by Denis on 05.03.2015.
 */
public class Pet extends BaseItem {
    private static final String[] VALUES = new String[]{
            "dog",
            "cat",
            "bird",
            "mouse",
            "fish",
            "rabbit",
            "pig",
            "reptile",
            "monkey",
            "hedgehog",
            "snake",
            "insect",
            "snail",
            "cow",
            "horse",
            OTHER_OPTION};

    public Pet(int id, String item) {
        super(id, item);
    }

    public static Pet getById(String id) {
        return BaseItem.getById(Pet.class, VALUES, id);
    }


    public static List<Pet> values() {
        return BaseItem.values(Pet.class, VALUES);
    }
}
