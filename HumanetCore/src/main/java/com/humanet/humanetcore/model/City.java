package com.humanet.humanetcore.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.Language;

/**
 * Created by Denis on 07.05.2015.
 */
@SuppressWarnings("unused")
public class City implements ILocationModel {

    public City() {
    }

    public City(String title) {
        mTitle = title;
    }

    @SerializedName("id")
    private int mId;

    @SerializedName("city_rus")
    private String mTitleRus;

    @SerializedName("city_eng")
    private String mTitleEng;

    @SerializedName("city_esp")
    private String mTitleEsp;

    @SerializedName("id_country")
    private int mCountryId;

    private String mTitle;

    public String getTitle() {
        if (TextUtils.isEmpty(mTitle)) {
            Language lang = Language.getSystem();
            switch (lang) {
                case RUS:
                    setTitle(mTitleRus);
                    break;
                case ESP:
                    setTitle(mTitleEsp);
                    break;
                case ENG:
                    setTitle(mTitleEng);
                    break;
            }
        }
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContentDescriptor.Cities.Cols.ID, mId);
        contentValues.put(ContentDescriptor.Cities.Cols.NAME_ENG, mTitleEng);
        contentValues.put(ContentDescriptor.Cities.Cols.NAME_RUS, mTitleRus);
        contentValues.put(ContentDescriptor.Cities.Cols.NAME_ESP, mTitleEsp);
        contentValues.put(ContentDescriptor.Cities.Cols.COUNTRY_ID, mCountryId);
        return contentValues;
    }

    public static City fromCursor(Cursor cursor) {
        City country = new City();
        country.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Cities.Cols.ID)));

        String title = "";
        Language lang = Language.getSystem();
        switch (lang) {
            case RUS:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Cities.Cols.NAME_RUS));
                break;
            case ESP:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Cities.Cols.NAME_ESP));
                break;
            case ENG:
                title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Cities.Cols.NAME_ENG));
                break;
        }

        country.setTitle(title);
        country.setCountryId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Cities.Cols.ID)));

        return country;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getCountryId() {
        return mCountryId;
    }

    public void setCountryId(int countryId) {
        mCountryId = countryId;
    }


    public static class Response extends BaseResponse{

        private City[] cities;

        public City[] getCities() {
            return cities;
        }
    }

}
