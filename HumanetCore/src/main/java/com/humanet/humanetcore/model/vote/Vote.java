package com.humanet.humanetcore.model.vote;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.fragments.vote.VoteTabFragment;
import com.humanet.humanetcore.model.enums.Language;

import java.io.Serializable;

import static com.humanet.humanetcore.fragments.vote.VoteTabFragment.VoteListStatus;

/**
 * Created by Denis on 28.04.2015.
 */
public class Vote implements Serializable {

    @SerializedName("id")
    private int mId;

    @SerializedName("title_rus")
    private String mTitleRus;

    @SerializedName("title_esp")
    private String mTitleEsp;

    @SerializedName("title_eng")
    private String mTitleEng;

    @SerializedName("description_rus")
    private String mDescrRus;

    @SerializedName("description_esp")
    private String mDescrEsp;

    @SerializedName("description_eng")
    private String mDescrEng;

    @SerializedName("my_vote")
    private int mMyVote;

    private int mTotalVotes;

    @VoteListStatus
    @SerializedName("status")
    private int mStatus;

    @SerializedName("description")
    // don't ask why
    private String mTitle;

    private String mDescr;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues(6);
        values.put("_id", mId);
        values.put(ContentDescriptor.Votes.Cols.TITLE_RUS, mTitleRus);
        values.put(ContentDescriptor.Votes.Cols.TITLE_ENG, mTitleEng);
        values.put(ContentDescriptor.Votes.Cols.TITLE_ESP, mTitleEsp);
        values.put(ContentDescriptor.Votes.Cols.DESCR_ENG, mDescrEng);
        values.put(ContentDescriptor.Votes.Cols.DESCR_ESP, mDescrEsp);
        values.put(ContentDescriptor.Votes.Cols.DESCR_RUS, mDescrRus);
        values.put(ContentDescriptor.Votes.Cols.STATUS, mStatus);
        values.put(ContentDescriptor.Votes.Cols.MY_VOTE, mMyVote);
        values.put(ContentDescriptor.Votes.Cols.VOTES_TOTAL, mTotalVotes);
        return values;
    }

    public static Vote fromCursor(Cursor cursor) {
        Vote vote = new Vote();
        vote.setId(cursor.getInt(cursor.getColumnIndex("_id")));
        @VoteListStatus int status = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.STATUS));
        vote.setStatus(status);

        vote.mTitleEng = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.TITLE_ENG));
        vote.mDescrEng = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.DESCR_ENG));

        vote.mTitleRus = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.TITLE_RUS));
        vote.mDescrRus = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.DESCR_RUS));

        vote.mTitleEsp = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.TITLE_ESP));
        vote.mDescrEsp = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.DESCR_ESP));

        vote.setMyVote(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.MY_VOTE)));
        vote.setTotalVotes(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.VOTES_TOTAL)));
        return vote;
    }

    public void setStatus(@VoteListStatus int status) {
        mStatus = status;
    }


    public String getTitle() {
        if (TextUtils.isEmpty(mTitle)) {
            switch (Language.getSystem()) {
                case ENG:
                    mTitle = mTitleEng;
                    break;
                case ESP:
                    mTitle = mTitleEsp;
                    break;
                case RUS:
                    mTitle = mTitleRus;
                    break;
            }
            if (TextUtils.isEmpty(mTitle)) {
                if (!TextUtils.isEmpty(mTitleEng)) {
                    mTitle = mTitleEng;
                } else if (!TextUtils.isEmpty(mTitleEsp)) {
                    mTitle = mTitleEsp;
                } else if (!TextUtils.isEmpty(mTitleRus)) {
                    mTitle = mTitleRus;
                }
            }
        }
        return mTitle;
    }

    public String getDescr() {
        if (TextUtils.isEmpty(mDescr)) {
            if (!TextUtils.isEmpty(mDescrEsp))
                mDescr = mDescrEsp;
            else if (!TextUtils.isEmpty(mDescrRus))
                mDescr = mDescrRus;
            else
                mDescr = mDescrEng;
        }
        return mDescr;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Boolean isVoted() {
        return mMyVote > 0;
    }

    public
    @VoteListStatus
    int getStatus() {
        return mStatus;
    }

    public void setDescr(String descr) {
        mDescr = descr;
    }

    public int getMyVote() {
        return mMyVote;
    }

    public void setMyVote(int myVote) {
        mMyVote = myVote;
    }

    public void setTotalVotes(int totalVotes) {
        mTotalVotes = totalVotes;
    }

    public int getTotalVotes() {
        return mTotalVotes;
    }

    public static int getVoteStatus(@VoteListStatus int status) {
        switch (status) {
            case VoteTabFragment.CREATED:
                return R.string.vote_status_wait;
            case VoteTabFragment.DECLINED:
                return R.string.vote_status_declined;
            default:
                return R.string.vote_status_approoved;
        }
    }

}
