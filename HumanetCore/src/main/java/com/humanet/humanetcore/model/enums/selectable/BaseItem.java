package com.humanet.humanetcore.model.enums.selectable;

import android.content.Context;
import android.text.TextUtils;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.interfaces.EditableCirclePickerItem;
import com.humanet.humanetcore.interfaces.IServerRequestParams;
import com.humanet.humanetcore.views.utils.ConverterUtil;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.humanet.humanetcore.Constants.OTHER_OPTION;

/**
 * Created by ovitali on 29.10.2015.
 */
public abstract class BaseItem implements EditableCirclePickerItem, IServerRequestParams, Serializable {

    private int id;
    private String item;
    private String mCustomTitle;

    protected String suffix = getClass().getSimpleName().toLowerCase(Locale.getDefault());

    public BaseItem(int id, String item) {
        this.id = id;
        this.item = item;
    }

    protected String getResourceName() {
        return suffix + "_" + item;
    }

    protected String getFolderName() {
        return suffix;
    }

    public int getId() {
        return id;
    }

    protected String getImageLink() {
        return "assets://" + getFolderName() + "/" + item + ".png";
    }

    @Override
    public String getDrawable() {
        if (!isOtherOption()) {
            return getImageLink();
        }
        return "drawable://" + R.drawable.other_option;
    }

    @Override
    public boolean isOtherOption() {
        return item.equals(OTHER_OPTION);
    }

    @Override
    public void setTitle(String title) {
        if (isOtherOption())
            mCustomTitle = title;
        else
            item = title;
    }

    public String getTitle() {
        if (isOtherOption())
            return mCustomTitle;

        Context context = App.getInstance();
        int id = context.getResources().getIdentifier(getResourceName(), "string", context.getPackageName());
        return App.getInstance().getString(id);
    }

    @Override
    public String getRequestParam() {
        if (isOtherOption()) {
            return getTitle();
        }
        return String.valueOf(getId());
    }


    /// --------

    protected static <T extends BaseItem> T getById(Class<T> clazz, String[] values, String sid) {
        if (TextUtils.isEmpty(sid))
            return null;
        try {
            int id = values.length - 1;
            String item = OTHER_OPTION;

            if (ConverterUtil.isInteger(sid)) {
                id = Integer.parseInt(sid);
                item = values[id - 1];
            }

            Constructor<T> constructor = clazz.getConstructor(int.class, String.class);
            BaseItem baseItem = constructor.newInstance(id, item);
            if (id == values.length - 1) {
                baseItem.mCustomTitle = sid;
            }

            return (T) baseItem;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    protected static <T> List<T> values(Class<T> clazz, String[] values) {
        try {
            ArrayList<T> resultArray = new ArrayList<>(values.length);
            for (int i = 0; i < values.length; i++) {
                Constructor<T> constructor = clazz.getConstructor(int.class, String.class);
                resultArray.add(constructor.newInstance(i + 1, values[i]));
            }
            return resultArray;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


}
