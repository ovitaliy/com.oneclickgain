package com.humanet.humanetcore.model.enums.selectable;

import java.util.List;

import static com.humanet.humanetcore.Constants.OTHER_OPTION;

/**
 * Created by Denis on 05.03.2015.
 */
public class Game extends BaseItem {

    private static String VALUES[] = new String[]{
            "basketball",
            "football",
            "baseball",
            "soccer",
            "hockey",
            "volleyball",
            "rugby",
            "tennis",
            "billiards",
            "water_polo",
            "handball",
            "curling",
            "biathlon",
            "cricket",
            "rowing",
            OTHER_OPTION};


    public Game(int id, String item) {
        super(id, item);
    }

    public static Game getById(String id) {
        return BaseItem.getById(Game.class, VALUES, id);
    }


    public static List<Game> values() {
        return BaseItem.values(Game.class, VALUES);
    }
}
