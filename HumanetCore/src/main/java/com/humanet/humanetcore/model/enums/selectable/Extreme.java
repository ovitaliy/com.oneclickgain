package com.humanet.humanetcore.model.enums.selectable;

import java.util.List;

import static com.humanet.humanetcore.Constants.OTHER_OPTION;

/**
 * Created by Denis on 05.03.2015.
 */
public class Extreme extends BaseItem {

    private static String VALUES[] = new String[]{
            "skate",
            "rollick",
            "surfing",
            "mount_the_board",
            "long_board",
            "base_jumping",
            "climbing",
            "parachuting",
            "bmh",
            "motorcycles",
            "snowboard",
            "wakeboard",
            "mountain_bike",
            "peytboll",
            "airsoft",
            OTHER_OPTION};


    public Extreme(int id, String item) {
        super(id, item);
    }

    public static Extreme getById(String id) {
        return BaseItem.getById(Extreme.class, VALUES, id);
    }


    public static List<Extreme> values() {
        return BaseItem.values(Extreme.class, VALUES);
    }
}
