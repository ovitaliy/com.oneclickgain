package com.humanet.humanetcore.model;

import android.support.annotation.IntDef;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.model.enums.selectable.Education;
import com.humanet.humanetcore.model.enums.selectable.Extreme;
import com.humanet.humanetcore.model.enums.selectable.Game;
import com.humanet.humanetcore.model.enums.selectable.Hobby;
import com.humanet.humanetcore.model.enums.selectable.HowILook;
import com.humanet.humanetcore.model.enums.selectable.Interest;
import com.humanet.humanetcore.model.enums.selectable.Job;
import com.humanet.humanetcore.model.enums.selectable.NonGame;
import com.humanet.humanetcore.model.enums.selectable.PartGames;
import com.humanet.humanetcore.model.enums.selectable.Pet;
import com.humanet.humanetcore.model.enums.selectable.Religion;
import com.humanet.humanetcore.model.enums.selectable.Sport;
import com.humanet.humanetcore.model.enums.selectable.Virtual;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class UserInfo extends BaseUser implements Serializable, Cloneable {

    public static final int MALE = 0;
    public static final int FEMALE = 1;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({MALE, FEMALE})
    public @interface Sex {

    }


    public Boolean baseDataAdded = false;

    @SerializedName("id")
    private int id;

    @SerializedName("gender")
    private int mGender = 1;

    @SerializedName("id_country")
    private int mCountryId = -1;

    @SerializedName("id_city")
    private int mCityId = -1;

    @SerializedName("birth_date")
    private long mBirthDate = -1;

    @SerializedName("lang")
    private Language mLanguage;

    @SerializedName("first_name")
    private String mFName;

    @SerializedName("last_name")
    private String mLName;

    @SerializedName("patronymic")
    private String mPatronymic;

    @SerializedName("degree")
    private Education mDegree;

    @SerializedName("school")
    private String mSchool;

    @SerializedName("university")
    private String mUniversity;

    @SerializedName("job")
    private Job mJobType;


    private String mJobTitle;

    @SerializedName("craft")
    private String mProfession;

    @SerializedName("hobbie")
    private Hobby mHobbie;

    @SerializedName("sport")
    private Sport mSport;

    @SerializedName("interest")
    private Interest mInterest;

    @SerializedName("pets")
    private Pet mPet;

    @SerializedName("religion")
    private Religion mReligion;

    @SerializedName("country_rus")
    private String mCountryRus;

    @SerializedName("country_eng")
    private String mCountryEng;

    @SerializedName("country_esp")
    private String mCountryEsp;

    @SerializedName("city_rus")
    private String mCityRus;

    @SerializedName("city_eng")
    private String mCityEng;

    @SerializedName("city_esp")
    private String mCityEsp;

    @SerializedName("widget")
    private int mWidget;

    /**
     * balance value.
     * Do NOT set SerializedName here.
     * Change serialization key in {@link com.humanet.humanetcore.utils.GsonHelper} in method setFieldNamingStrategy
     */
    private float balance;

    @SerializedName("rating")
    private float mRating;

    @SerializedName("phone")
    private String phone;

    @SerializedName("relationship")
    private int mRelationship;

    @SerializedName("tokens")
    private int mTokens;

    @SerializedName("category_stat")
    private Float[] mVideoStatistic;

    @SerializedName("extreme")
    private Extreme mExtreme;

    @SerializedName("game")
    private Game mGames;

    @SerializedName("non_game")
    private NonGame mNonGame;

    @SerializedName("virtual")
    private Virtual mVirtual;

    @SerializedName("how_i_look")
    private HowILook mHowILook;

    @SerializedName("part_games")
    private PartGames mPartGames;

    @SuppressWarnings("unused")
    @SerializedName("ggc")
    private float mGameBalanceValue;

    transient private List<VideoStatistic> mVideoStatisticsList;

    public float getBalance() {
        return balance;
    }

    public void setGameBalanceValue(float gameBalanceValue) {
        mGameBalanceValue = gameBalanceValue;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public List<VideoStatistic> getCategoryStatisticList() {
        if (mVideoStatisticsList == null && mVideoStatistic != null) {
            int length = mVideoStatistic.length;
            ArrayList<VideoStatistic> categoryStatistics = new ArrayList<>(length);
            for (int i = 0; i < mVideoStatistic.length; i++) {
                Float seconds = mVideoStatistic[i];
                if (seconds == null) {
                    seconds = 0.0F;
                }
                VideoStatistic categoryStatistic = VideoStatistic.create(i, (int) (seconds * 1000));
                if (categoryStatistic != null)
                    categoryStatistics.add(categoryStatistic);
            }

            Collections.sort(categoryStatistics, new Comparator<VideoStatistic>() {
                @Override
                public int compare(VideoStatistic lhs, VideoStatistic rhs) {
                    return rhs.getId() > lhs.getId() ? 1 : -1;
                }
            });

            mVideoStatisticsList = categoryStatistics;
        }
        return mVideoStatisticsList;
    }

    public String getFName() {
        return (mFName != null) ? mFName : "";
    }

    public void setFName(String FName) {
        this.mFName = FName;
    }

    public String getLName() {
        return (mLName != null) ? mLName : "";
    }

    public void setLName(String LName) {
        this.mLName = LName;
    }

    public String getPatronymic() {
        return (mPatronymic != null) ? mPatronymic : "";
    }

    public void setPatronymic(String patronymic) {
        mPatronymic = patronymic;
    }

    public boolean isWidget() {
        return mWidget == 1;
    }

    public void setWidget(boolean widget) {
        mWidget = widget ? 1 : 0;
    }

    public String getFullName() {
        String name = getFName();
        if (!TextUtils.isEmpty(mPatronymic)) {
            name += (" " + mPatronymic);
        }
        if (!TextUtils.isEmpty(mLName)) {
            name += (" " + mLName);
        }
        return name;
    }

    public Language getLanguage() {
        if (mLanguage == null) {
            mLanguage = Language.getSystem();
        }
        return mLanguage;
    }

    public void setLanguage(Language language) {
        mLanguage = language;
    }

    public int getGender() {
        return mGender;
    }

    public void setGender(int gender) {
        mGender = gender;
    }

    public int getCountryId() {
        return mCountryId;
    }

    public void setCountryId(int countryId) {
        mCountryId = countryId;
    }

    public int getCityId() {
        return mCityId;
    }

    public void setCityId(int cityId) {
        mCityId = cityId;
    }

    public long getBirthDate() {
        return mBirthDate;
    }

    public void setBirthDate(long birthDate) {
        mBirthDate = birthDate;
    }

    public Education getDegree() {
        return mDegree;
    }

    public void setDegree(Education degree) {
        mDegree = degree;
    }

    public String getSchool() {
        return mSchool;
    }

    public void setSchool(String school) {
        mSchool = school;
    }

    public String getUniversity() {
        return mUniversity;
    }

    public void setUniversity(String university) {
        mUniversity = university;
    }

    public Job getJobType() {
        return mJobType;
    }

    public void setJobType(Job jobType) {
        mJobType = jobType;
    }

    public String getJobTitle() {
        return mJobTitle;
    }

    public void setJobTitle(String jobTitle) {
        mJobTitle = jobTitle;
    }

    public Hobby getHobbie() {
        return mHobbie;
    }

    public void setHobbie(Hobby hobbie) {
        mHobbie = hobbie;
    }

    public Sport getSport() {
        return mSport;
    }

    public void setSport(Sport sport) {
        mSport = sport;
    }

    public Interest getInterest() {
        return mInterest;
    }

    public void setInterest(Interest interest) {
        mInterest = interest;
    }

    public Pet getPet() {
        return mPet;
    }

    public void setPet(Pet pet) {
        mPet = pet;
    }

    public Religion getReligion() {
        return mReligion;
    }

    public void setReligion(Religion religion) {
        mReligion = religion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        switch (Language.getSystem()) {
            case ENG:
                return mCountryEng;
            case RUS:
                return mCountryRus;
            default:
                return mCountryEsp;
        }
    }

    public void setCountry(String country) {
        switch (Language.getSystem()) {
            case ENG:
                mCountryEng = country;
            case RUS:
                mCountryRus = country;
            default:
                mCountryEsp = country;
        }
    }

    public String getCity() {
        switch (Language.getSystem()) {
            case ENG:
                return mCityEng;
            case RUS:
                return mCityRus;
            default:
                return mCityEsp;
        }
    }

    public void setCity(String city) {
        switch (Language.getSystem()) {
            case ENG:
                mCityEng = city;
            case RUS:
                mCityRus = city;
            default:
                mCityEsp = city;
        }
    }


    public String getProfession() {
        return mProfession;
    }

    public void setProfession(String profession) {
        mProfession = profession;
    }

    public void setCountryRus(String countryRus) {
        mCountryRus = countryRus;
    }

    public void setCountryEng(String countryEng) {
        mCountryEng = countryEng;
    }

    public void setCountryEsp(String countryEsp) {
        mCountryEsp = countryEsp;
    }

    public void setCityRus(String cityRus) {
        mCityRus = cityRus;
    }

    public void setCityEng(String cityEng) {
        mCityEng = cityEng;
    }

    public void setCityEsp(String cityEsp) {
        mCityEsp = cityEsp;
    }

    public int getRelationship() {
        return mRelationship;
    }

    public void setRelationship(int relationship) {
        mRelationship = relationship;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setTokens(int tokens) {
        mTokens = tokens;
    }

    public Extreme getExtreme() {
        return mExtreme;
    }

    public void setExtreme(Extreme extreme) {
        mExtreme = extreme;
    }

    public Game getGames() {
        return mGames;
    }

    public void setGames(Game games) {
        mGames = games;
    }

    public NonGame getNonGame() {
        return mNonGame;
    }

    public void setNonGame(NonGame nonGame) {
        mNonGame = nonGame;
    }

    public Virtual getVirtual() {
        return mVirtual;
    }

    public void setVirtual(Virtual virtual) {
        mVirtual = virtual;
    }

    public PartGames getPartGames() {
        return mPartGames;
    }

    public void setPartGames(PartGames partGames) {
        mPartGames = partGames;
    }

    public HowILook getHowILook() {
        return mHowILook;
    }

    public void setHowILook(HowILook howILook) {
        mHowILook = howILook;
    }

    public float getRating() {
        return mRating;
    }

    @Override
    public UserInfo clone() {
        try {
            return (UserInfo) super.clone();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public float getGameBalanceValue() {
        return mGameBalanceValue;
    }

    public boolean isMe() {
        return getId() == AppUser.getInstance().getUid();
    }

    public int getTokens() {
        return mTokens;
    }


    public static class UserResponse {

        @SerializedName("user")
        private UserInfo mUserInfo;

        public UserInfo getUserInfo() {
            return mUserInfo;
        }
    }

}