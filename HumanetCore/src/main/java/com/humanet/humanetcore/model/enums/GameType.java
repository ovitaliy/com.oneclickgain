package com.humanet.humanetcore.model.enums;

import com.humanet.humanetcore.interfaces.IServerRequestParams;

import java.util.Locale;

/**
 * Created by Deni on 23.07.2015.
 */
public enum GameType implements IServerRequestParams {
    ASTRA, CREDO, START,

    GENERAL_GAME {
        @Override
        public String getRequestParam() {
            return "gg";
        }
    },
    ELSE;

    @Override
    public String getRequestParam() {
        return toString();
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase(Locale.getDefault());
    }
}