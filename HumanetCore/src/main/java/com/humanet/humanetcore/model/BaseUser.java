package com.humanet.humanetcore.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ovi on 1/19/16.
 */
public class BaseUser {

    @SerializedName("photo")
    private String avatar;

    @SerializedName("photo_100")
    private String avatarSmall;


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatarSmall() {
        if (!TextUtils.isEmpty(avatarSmall)) {
            if (avatarSmall.endsWith("-100.png")) {
                String fileName = avatarSmall.substring(avatarSmall.lastIndexOf("/" + 1));
                String fixedFileName = "/100" + fileName.replace("-100.png", ".png");
                avatarSmall = avatarSmall.replace(fileName, fixedFileName);
            }
            return avatarSmall;
        }
        return avatar;
    }
}
