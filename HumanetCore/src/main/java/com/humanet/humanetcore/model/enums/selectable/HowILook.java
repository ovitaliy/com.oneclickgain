package com.humanet.humanetcore.model.enums.selectable;

import java.util.List;

import static com.humanet.humanetcore.Constants.OTHER_OPTION;

/**
 * Created by ovitali on 19.11.2015.
 */
public class HowILook extends BaseItem {
    private static String VALUES[] = new String[]{
            "visit_event",
            "tv",
            "view_events_in_bars",
            "webcast",
            "tv_replays",
            OTHER_OPTION};

    public HowILook(int id, String item) {
        super(id, item);
        suffix = "how_i_look";
    }

    public static HowILook getById(String id) {
        return BaseItem.getById(HowILook.class, VALUES, id);
    }

    public static List<HowILook> values() {
        return BaseItem.values(HowILook.class, VALUES);
    }
}
