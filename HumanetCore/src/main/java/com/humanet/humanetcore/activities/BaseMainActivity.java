package com.humanet.humanetcore.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseVideoManageActivity;
import com.humanet.humanetcore.events.IMenuBindActivity;
import com.humanet.humanetcore.events.INavigatableActivity;
import com.humanet.humanetcore.events.OnViewProfileListener;
import com.humanet.humanetcore.fragments.BaseMenuFragment;
import com.humanet.humanetcore.fragments.VideoListFragment;
import com.humanet.humanetcore.fragments.VistoryFragment;
import com.humanet.humanetcore.fragments.profile.BaseViewProfileFragment;
import com.humanet.humanetcore.interfaces.OnNavigateListener;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.utils.PrefHelper;
import com.humanet.humanetcore.utils.ToolbarHelper;
import com.humanet.humanetcore.views.dialogs.TipsDialog;

/**
 * Created by ovi on 1/25/16.
 */
public abstract class BaseMainActivity extends BaseVideoManageActivity implements
        OnNavigateListener,
        IMenuBindActivity,
        INavigatableActivity,
        OnViewProfileListener {

    private DrawerLayout mDrawerLayout;

    private BaseMenuFragment mMenuFragment;

    protected abstract void launchFirstFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        toolbar = ToolbarHelper.createToolbar(this);

        mMenuFragment = newMenuFragmentInstance();

        getSupportFragmentManager().beginTransaction().replace(R.id.menu_fragment, mMenuFragment).commit();

        // Set up the drawer.
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int backStack = getSupportFragmentManager().getBackStackEntryCount();
                if (backStack == 0)
                    mMenuFragment.toggle();
                else
                    onBackPressed();
            }
        });


        if (!PrefHelper.getBooleanPref("tips_were_shown")) {
            PrefHelper.setBooleanPref("tips_were_shown", true);
            TipsDialog.newInstance().show(getSupportFragmentManager(), "TipsDialog");
        }

        launchFirstFragment();


    }

    protected abstract BaseMenuFragment newMenuFragmentInstance();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void openSearch() {
        startActivity(SearchActivity.newIntent(this, true));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mMenuFragment.onOptionsItemSelected(item)) {
            return true;
        }
        int i = item.getItemId();
        if (i == R.id.action_search) {
            openSearch();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onOpenOnlineStream() {
        mDrawerLayout.closeDrawers();
        startFragment(new VistoryFragment(), false, true);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent.hasExtra(Constants.PARAMS.ACTION)) {
            if (intent.getIntExtra(Constants.PARAMS.ACTION, 0) == Constants.ACTION.PLAY_FLOW) {
                int currentVideoPosition = intent.getIntExtra(Constants.PARAMS.CURRENT_VIDEO, 0);
                Category category = (Category) intent.getSerializableExtra(Constants.PARAMS.CATEGORY);
                int replyId = intent.getIntExtra(Constants.PARAMS.REPLY_ID, 0);
                String title = intent.getStringExtra(Constants.PARAMS.TITLE);
                VideoListFragment fragment = new VideoListFragment.Builder(null)
                        .setCategoryType(category)
                        .setReplyId(replyId)
                        .setTitle(title)
                        .setCurrentPosition(currentVideoPosition)
                        .build();
                startFragment(fragment, true, true);
            } else if (intent.getIntExtra(Constants.PARAMS.ACTION, 0) == Constants.ACTION.SHOW_PROFILE) {
                startFragment(BaseViewProfileFragment.newInstance(AppUser.getInstance().getUid(), 1), true, true);
            }
        }
    }

    public void selectMenuItem(int id) {
        mMenuFragment.selectMenuItem(id);
    }


    @Override
    public void onViewProfile(int uid) {
        startFragment(BaseViewProfileFragment.newInstance(uid), true);
    }
}
