package com.humanet.humanetcore.activities;

import android.os.Bundle;
import android.widget.Toast;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.jobs.UploadVideoJob;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.utils.NetworkUtil;
import com.humanet.humanetcore.utils.PrefHelper;

public abstract class BaseSplashActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AnalyticsHelper.init(this);
        AnalyticsHelper.trackEvent(this, AnalyticsHelper.ENTER_IN_APP);

        if (PrefHelper.getBooleanPref(Constants.PREF.REG) && PrefHelper.getStringPref(Constants.PREF.TOKEN) != null) {
            openMainActivity();
        } else {
            if (!NetworkUtil.isOfflineMode()) {
                openRegistrationActivity();
            } else {
                Toast.makeText(this, "No Internet connection", Toast.LENGTH_LONG).show();
            }
        }

        finish();

        UploadVideoJob.startUploader();
    }

    public abstract void openMainActivity();

    public abstract void openRegistrationActivity();

}
