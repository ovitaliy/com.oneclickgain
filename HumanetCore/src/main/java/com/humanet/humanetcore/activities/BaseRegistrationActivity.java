package com.humanet.humanetcore.activities;

import android.content.IntentFilter;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.events.SmsReceiverListener;
import com.humanet.humanetcore.fragments.registration.BaseVerificationFragment;
import com.humanet.humanetcore.receivers.SmsReceiver;

/**
 * Created by ovi on 2/10/16.
 */
public abstract class BaseRegistrationActivity extends BaseActivity implements
        BaseVerificationFragment.OnVerifyListener,
        BaseVerificationFragment.VerificationContainer,
        SmsReceiverListener {


    private BaseVerificationFragment mVerificationFragment;

    private WakefulBroadcastReceiver mSmsBroadcastReceiver;


    public final void sendVerificationCode(CharSequence code) {
        if (mVerificationFragment == null)
            return;

        if (mVerificationFragment.isVisible())
            mVerificationFragment.sendVerificationCode(code);
    }


    public final void setVerificationFragment(BaseVerificationFragment verificationFragment) {
        mVerificationFragment = verificationFragment;

        if (mVerificationFragment != null && mSmsBroadcastReceiver == null) {
            mSmsBroadcastReceiver = new SmsReceiver(this);
            registerReceiver(mSmsBroadcastReceiver, new IntentFilter(SmsReceiver.SMS_RECEIVED));
        }

        if (mVerificationFragment == null && mSmsBroadcastReceiver != null) {
            unregisterReceiver(mSmsBroadcastReceiver);
            mSmsBroadcastReceiver = null;
        }
    }


}
