package com.humanet.humanetcore.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseVideoManageActivity;
import com.humanet.humanetcore.fragments.SearchFragment;
import com.humanet.humanetcore.fragments.SearchResultFragment;
import com.humanet.humanetcore.utils.ToolbarHelper;

/**
 * Created by Deni on 18.06.2015.
 */
public abstract class SearchActivity extends BaseVideoManageActivity {

    public static Intent newIntent(Context context, boolean grimaceSearch) {
        Intent intent = new Intent(context, NavigationManager.getSearchActivityClass());
        intent.putExtra("grimace", grimaceSearch);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment_container);
        toolbar = ToolbarHelper.createToolbar(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        startFragment(new SearchFragment(), false);
    }

    public void openSearchResult() {
        startFragment(SearchResultFragment.newInstance(), true);
    }


}