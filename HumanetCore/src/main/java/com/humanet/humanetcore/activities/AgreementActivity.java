package com.humanet.humanetcore.activities;

import android.os.Bundle;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.fragments.info.InfoFragment;
import com.humanet.humanetcore.utils.ToolbarHelper;

public abstract class AgreementActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment_container);

        toolbar = ToolbarHelper.createToolbar(this);

        startFragment(InfoFragment.newInstance(), false);
    }
}
