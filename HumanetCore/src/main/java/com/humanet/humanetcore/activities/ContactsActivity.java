package com.humanet.humanetcore.activities;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.GroupMembership;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.Groups;
import android.provider.Telephony;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.SendTokenRequest;
import com.humanet.humanetcore.api.requests.video.ShareVideoRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.events.NumberSelectedListener;
import com.humanet.humanetcore.model.ContactItem;
import com.humanet.humanetcore.model.FriendItem;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.views.adapters.ContactsListAdapter;
import com.humanet.humanetcore.views.behaviour.SimpleTextListener;
import com.humanet.humanetcore.views.dialogs.MissedPermissionDialog;
import com.humanet.humanetcore.views.dialogs.ProgressDialog;
import com.humanet.humanetcore.views.dialogs.SelectContactsTypeDialog;
import com.humanet.humanetcore.views.dialogs.SelectPhoneNumberDialogFragment;
import com.humanet.humanetcore.views.widgets.items.ContactItemView;
import com.humanet.humanetcore.views.widgets.items.GroupItemView;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.util.ArrayList;
import java.util.Locale;

public abstract class ContactsActivity extends BaseActivity implements NumberSelectedListener {

    public static Intent newShareInstance(Context context, String shareLink, int videoId) {
        Intent intent = new Intent(context, NavigationManager.getContactsActivityClass());
        intent.putExtra(Constants.PARAMS.ACTION, Constants.ACTION.SHARE);
        intent.putExtra("shareLink", shareLink);
        intent.putExtra("videoId", videoId);

        return intent;
    }

    public static Intent newPickInstance(Context context, int relationship) {
        Intent intent = new Intent(context, NavigationManager.getContactsActivityClass());
        intent.putExtra(Constants.PARAMS.ACTION, Constants.ACTION.GET_CONTACTS);
        intent.putExtra(Constants.PARAMS.RELATIONSHIP, relationship);

        return intent;
    }

    public static Intent newTokenInstance(Context context) {
        Intent intent = new Intent(context, NavigationManager.getContactsActivityClass());
        intent.putExtra(Constants.PARAMS.ACTION, Constants.ACTION.TOKEN);
        return intent;
    }

    private static final String TAG = ContactsActivity.class.getSimpleName();

    private ContactsListAdapter mAdapter;

    private ArrayList<Group> mGroups;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_friend_list);

        mGroups = new ArrayList<>();

        EditText filtersEditText = (EditText) findViewById(R.id.search_field);
        filtersEditText.addTextChangedListener(new SimpleTextListener() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateList(s.toString());
            }

        });

        int dummyHeight = getResources().getDimensionPixelOffset(R.dimen.round_button_size) + getResources().getDimensionPixelOffset(R.dimen.mrg_normal) * 2;
        View dummyView = new View(this);
        dummyView.setLayoutParams(new AbsListView.LayoutParams(dummyHeight, dummyHeight));

        ExpandableListView contactsListView = (ExpandableListView) findViewById(R.id.friend_list);
        contactsListView.addFooterView(dummyView, null, false);

        mAdapter = new ContactsListAdapter(this, mGroups, contactsListView);
        contactsListView.setAdapter(mAdapter);

        contactsListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                ContactItemView friend = (ContactItemView) v;
                friend.setCheck(!friend.getCheck());

                return true;
            }
        });
        contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ContactItemView friend = (ContactItemView) view;
                friend.setCheck(!friend.getCheck());
            }
        });

        contactsListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                boolean val = !mAdapter.getGroup(groupPosition).checked;
                ((GroupItemView) v).setCheck(val);

                int childCount = mAdapter.getChildrenCount(groupPosition);
                for (int i = 0; i < childCount; i++) {
                    mAdapter.getChild(groupPosition, i).setCheck(val);
                }
                mAdapter.notifyDataSetChanged();
                return true;
            }
        });


        RxPermissions.getInstance(this).request(Manifest.permission.READ_CONTACTS).subscribe(granted -> {
            if (granted) {
                Handler handler = new Handler(getMainLooper());
                handler.postDelayed(() -> SelectContactsTypeDialog.newInstance(mContactLoader).show(getSupportFragmentManager(), SelectContactsTypeDialog.TAG), 100);
            } else {
                MissedPermissionDialog.show(this, ((dialogInterface, i) -> finish()));
            }
        });


        findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int action = getIntent().getIntExtra(Constants.PARAMS.ACTION, 0);
                switch (action) {
                    case Constants.ACTION.SHARE:
                        share();
                        break;

                    case Constants.ACTION.GET_CONTACTS:
                        getContacts();
                        break;

                    case Constants.ACTION.TOKEN:
                        token();
                        break;

                }


            }
        });
    }

    public void token() {
        ArrayList<FriendItem> items = getSelectedContactsWithPhones();
        if (items.size() > 0) {
            FriendItem mItem = items.get(0);
            ArrayList<String> phones = mItem.getPhoneNumbers();
            if (phones.size() > 1) {
                SelectPhoneNumberDialogFragment.newInstance(mItem.getName(), phones).show(getSupportFragmentManager(), "select_number");
            } else {
                sendTokenToNumber(mItem.getName(), phones.get(0));
            }
        }
    }

    public void sendTokenToNumber(String username, String number) {
        AnalyticsHelper.trackEvent(this, AnalyticsHelper.TOKEN_SEND);
        number = "+" + number.replaceAll("\\D+", "");
        getSpiceManager().execute(new SendTokenRequest(username, number), new SimpleRequestListener<BaseResponse>() {
            @Override
            public void onRequestSuccess(BaseResponse baseResponse) {
                super.onRequestSuccess(baseResponse);
                UserInfo info = AppUser.getInstance().get();
                info.setTokens(info.getTokens() - 1);
                AppUser.getInstance().set(info);
                finish();
            }
        });

    }

    private ArrayList<FriendItem> getSelectedContactsWithPhones() {
        ArrayList<Long> ids = new ArrayList<>();
        for (int i = 0; i < mGroups.size(); i++) {
            ArrayList<ContactItem> originalFriendItems = mGroups.get(i).list;
            for (int j = 0; j < originalFriendItems.size(); j++) {
                ContactItem fr = originalFriendItems.get(j);
                if (fr.isCheck()) {
                    ids.add(fr.getId());
                }
            }
        }

        Cursor cursor = getContentResolver().query(Phone.CONTENT_URI,
                new String[]{Phone.CONTACT_ID, Phone.NUMBER, Phone.DISPLAY_NAME, Phone.TYPE},
                Phone.CONTACT_ID + " IN (" + TextUtils.join(", ", ids) + ") " +
                        "AND " +
                        Phone.TYPE + " IN (" + TextUtils.join(", ", new Integer[]{Phone.TYPE_MOBILE, Phone.TYPE_WORK_MOBILE, Phone.TYPE_WORK, Phone.TYPE_HOME}) + ")",
                null,
                Phone.CONTACT_ID + " ASC");

        ArrayList<FriendItem> items = new ArrayList<>();
        try {
            if (cursor != null && cursor.moveToFirst()) {
                long curId = 0;
                FriendItem item = null;
                do {
                    long id = cursor.getLong(0);
                    if (id != curId) {
                        if (item != null) {
                            items.add(item);
                        }
                        item = new FriendItem();
                        String name = cursor.getString(2);
                        item.setName(name);
                    }
                    String number = cursor.getString(1);
                    if (item != null && number != null)
                        item.addPhoneNumber(number);
                    curId = id;

                } while (cursor.moveToNext());

                if (item != null && !items.contains(item)) {
                    items.add(item);
                }
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return items;

    }

    public void getContacts() {
        ArrayList<FriendItem> list = getSelectedContactsWithPhones();

        Intent intent = new Intent();
        intent.putExtra(Constants.PARAMS.ACTION, Constants.ACTION.GET_CONTACTS);
        intent.putExtra(Constants.PARAMS.RELATIONSHIP, getIntent().getIntExtra(Constants.PARAMS.RELATIONSHIP, 0));
        intent.putParcelableArrayListExtra(Constants.PARAMS.CONTACTS, list);

        setResult(RESULT_OK, intent);
        finish();
    }

    public void share() {
        String shareLink = getIntent().getStringExtra("shareLink");
        int videoId = getIntent().getIntExtra("videoId", 0);

        ArrayList<FriendItem> items = getSelectedContactsWithPhones();

        setResult(RESULT_OK);
        finish();

        if (items.size() == 0)
            return;

        StringBuilder toContact = new StringBuilder();
        for (FriendItem item : items) {
            ArrayList<String> phoneNumbers = item.getPhoneNumbers();
            if (phoneNumbers != null && phoneNumbers.size() > 0) {
                String phoneNumber = phoneNumbers.get(0);
                toContact.append(phoneNumber);
                toContact.append(";");
            }
        }

        Intent smsIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // Android 4.4 and up
        {
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this);
            smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + Uri.encode(toContact.toString())));
            smsIntent.putExtra("sms_body", shareLink);
            if (defaultSmsPackageName != null) // Can be null in case that there is no default, then the user would be able to choose any app that supports this intent.
            {
                smsIntent.setPackage(defaultSmsPackageName);
            }
        } else {
            smsIntent = new Intent(Intent.ACTION_VIEW);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("address", toContact.toString());
            smsIntent.putExtra("sms_body", shareLink);
        }
        startActivity(smsIntent);

        if (videoId != 0) {
            String phones = toContact.toString().replace(";", ",").substring(0, toContact.length() - 1);

            phones = "+" + phones.replaceAll("\\D+", "");

            getSpiceManager().execute(new ShareVideoRequest(videoId, false, phones), new SimpleRequestListener<BaseResponse>());

            AnalyticsHelper.trackEvent(this, AnalyticsHelper.SHARE);
        }
    }


    public ArrayList<Group> loadAll() {
        ArrayList<Group> list = new ArrayList<>(1);
        Group group = new Group();
        group.name = getString(R.string.share_contact_group_all);
        group.list = getUsersWhere(null);
        list.add(group);
        return list;
    }

    public ArrayList<Group> loadFavorites() {
        ArrayList<Group> list = new ArrayList<>(1);
        Group group = new Group();
        group.name = getString(R.string.share_contact_group_favorites);
        group.list = getUsersWhere(ContactsContract.Contacts.STARRED + "='1'");

        list.add(group);
        return list;
    }

    private AsyncTask<Integer, Void, ArrayList<Group>> mContactLoader = new AsyncTask<Integer, Void, ArrayList<Group>>() {
        ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = new ProgressDialog(ContactsActivity.this);
            mDialog.setTitle(R.string.loading);
            mDialog.show();
        }

        @Override
        protected ArrayList<Group> doInBackground(Integer[] params) {
            int id = params[0];
            if (id == R.id.rb_fav) {
                return loadFavorites();
            } else if (id == R.id.rb_all) {
                return loadAll();
            } else if (id == R.id.rb_group) {
                return loadGroups();
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Group> groups) {
            super.onPostExecute(groups);
            mGroups = groups;
            updateList("");
            if (mDialog != null && !mDialog.isDismissed()) {
                mDialog.dismiss();
            }
        }
    };

    private ArrayList<Group> filter(String text) {
        text = text.toUpperCase(Locale.getDefault());

        int n = mGroups != null ? mGroups.size() : 0;

        ArrayList<Group> groups = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            ArrayList<ContactItem> originalFriendItems = mGroups.get(i).list;
            ArrayList<ContactItem> friendItems = new ArrayList<>();
            for (int j = 0; j < originalFriendItems.size(); j++) {
                ContactItem fr = originalFriendItems.get(j);
                if (fr.getName().toUpperCase(Locale.getDefault()).startsWith(text)) {
                    friendItems.add(fr);
                }
            }

            if (friendItems.size() > 0) {
                Group group = new Group();
                group.name = mGroups.get(i).name;
                group.list = friendItems;
                groups.add(group);
            }
        }

        return groups;
    }


    public ArrayList<Group> loadGroups() {
        ArrayList<Group> groups = new ArrayList<>();
        final String[] GROUP_PROJECTION = new String[]{
                Groups._ID,
                Groups.TITLE
        };

        Cursor c = getContentResolver().query(
                Groups.CONTENT_URI,
                GROUP_PROJECTION,
                null,
                null,
                null);

        if (c != null) {
            final int IDX_ID = c.getColumnIndex(Groups._ID);
            final int IDX_TITLE = c.getColumnIndex(Groups.TITLE);


            while (c.moveToNext()) {
                Group g = new Group();
                g.id = c.getString(IDX_ID);
                g.name = c.getString(IDX_TITLE);
                g.list = fetchGroupMembers(g.id);
                groups.add(g);
            }
            c.close();
        }

        Log.i(TAG, mGroups.toString() + mGroups.size());
        return groups;
    }

    public ArrayList<ContactItem> getUsersWhere(String where) {
        ArrayList<ContactItem> friendItems = new ArrayList<>();

        if (!TextUtils.isEmpty(where)) {
            where += " AND ";
        } else {
            where = "";
        }

        where += Data.HAS_PHONE_NUMBER + " = 1";

        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME},
                where,
                null,
                Data.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
        if (cursor != null && cursor.moveToFirst()) {
            do {
                int contactId =
                        cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                ContactItem item = new ContactItem(contactId, name);
                friendItems.add(item);
            } while (cursor.moveToNext());
            cursor.close();
        }

        return friendItems;
    }

    private void updateList(String text) {
        mAdapter.replace(filter(text));
    }

    private ArrayList<ContactItem> fetchGroupMembers(String groupId) {
        ArrayList<ContactItem> groupMembers = new ArrayList<>();
        String where = GroupMembership.GROUP_ROW_ID + "=" + groupId
                + " AND "
                + GroupMembership.MIMETYPE + "='"
                + GroupMembership.CONTENT_ITEM_TYPE + "'"
                + " AND "
                + Data.HAS_PHONE_NUMBER + " = 1";
        String[] projection = new String[]{GroupMembership.RAW_CONTACT_ID, Data.DISPLAY_NAME};
        Cursor cursor = getContentResolver().query(Data.CONTENT_URI, projection, where, null,
                Data.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
        while (cursor != null && cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(GroupMembership.RAW_CONTACT_ID));
            String name = cursor.getString(cursor.getColumnIndex(Data.DISPLAY_NAME));
            ContactItem item = new ContactItem(id, name);
            groupMembers.add(item);
        }
        if (cursor != null)
            cursor.close();
        return groupMembers;
    }

    @Override
    public void onNumberSelected(String userName, String number) {
        sendTokenToNumber(userName, number);
    }


    public static class Group {
        public String id;
        public String name;
        public boolean checked;
        public ArrayList<ContactItem> list = new ArrayList<>();
    }


}
