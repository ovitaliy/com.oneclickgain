package com.humanet.humanetcore.modules;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;

import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.database.CategoriesRequest;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.events.SimpleLoaderCallbacks;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.views.behaviour.CategoriesListView;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.List;

/**
 * oneClickGain
 * Created by ovitali on 17.12.2015.
 */
public class CategoriesDataLoader {

    private static final int LOADER_ID = 1001;

    private CategoriesRequest mCategoriesRequest;

    private SpiceManager mSpiceManager;
    private LoaderManager mLoaderManager;
    private CategoriesListView mListView;

    private boolean mAllowEmpty;

    public CategoriesDataLoader(final SpiceManager spiceManager, final LoaderManager loaderManager, final CategoriesListView listView, final boolean allowEmpty) {
        mSpiceManager = spiceManager;
        mLoaderManager = loaderManager;
        mListView = listView;
        mAllowEmpty = allowEmpty;
    }

    public void loadCategories(final VideoType videoType) {
        Bundle bundle = new Bundle(1);
        bundle.putString("where", buildWhereCondition(videoType));

        mListView.setCategories(new ArrayList<Category>(0));

        mLoaderManager.restartLoader(LOADER_ID + videoType.getId(), bundle, new SimpleLoaderCallbacks<Cursor>() {

            @Override
            public Loader<Cursor> onCreateLoader(final int id, final Bundle args) {
                String where = args.getString("where");
                return new CursorLoader(mListView.getActivity(),
                        ContentDescriptor.Categories.URI,
                        null,
                        where,
                        null,
                        null);
            }

            @Override
            public void onLoadFinished(final Loader<Cursor> loader, final Cursor data) {
                mListView.setCategories(cursorToList(data));
            }
        });

        if (mCategoriesRequest != null)
            mSpiceManager.cancel(mCategoriesRequest);

        mCategoriesRequest = new CategoriesRequest(videoType);
        mSpiceManager.execute(mCategoriesRequest, new SimpleRequestListener<Category[]>() {
            @Override
            public void onRequestSuccess(final Category[] categories) {
                mCategoriesRequest = null;
            }
        });
    }


    private String buildWhereCondition(VideoType videoType) {
        List<String> selections = new ArrayList<>();
        if (!mAllowEmpty)
            selections.add(ContentDescriptor.Categories.Cols.VIDEO_COUNT + " > 0");

        selections.add(ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + videoType.getId());

        return TextUtils.join(" AND ", selections);
    }

    private List<Category> cursorToList(Cursor cursor) {
        ArrayList<Category> categories = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                categories.add(Category.fromCursor(cursor));
            } while (cursor.moveToNext());
        }

        return categories;
    }
}
