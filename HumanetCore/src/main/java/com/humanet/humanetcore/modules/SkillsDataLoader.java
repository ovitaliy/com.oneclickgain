package com.humanet.humanetcore.modules;

import com.humanet.humanetcore.views.widgets.CircleImageView;
import com.humanet.humanetcore.views.widgets.CirclePickerView;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by serezha on 29.07.16.
 */
public class SkillsDataLoader {

	private SpiceManager mSpiceManager;
	private CirclePickerView mSkillsCirclePickerView;
	private CircleImageView mSkillsCircleImageView;



	public SkillsDataLoader(SpiceManager spiceManager, CirclePickerView skillsCirclePickerView, CircleImageView skillsCircleImageView) {

		mSpiceManager = spiceManager;
		mSkillsCirclePickerView = skillsCirclePickerView;
		mSkillsCircleImageView = skillsCircleImageView;

	}


}
