package com.humanet.humanetcore.utils;

import android.app.Activity;
import android.content.ContentValues;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.VideoInfo;

/**
 * Created by Uran on 07.06.2016.
 */
public class LocationWatcher implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    @NonNull
    private VideoInfo mVideoInfo;

    private GoogleApiClient mGoogleApiClient;

    private GetLocationListener mGetLocationListener;

    private LocationWatcher(@NonNull Activity activity, @NonNull VideoInfo videoInfo) {
        mVideoInfo = videoInfo;

        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    public static LocationWatcher create(Activity activity, VideoInfo videoInfo) {
        return new LocationWatcher(activity, videoInfo);
    }

    public void setGetLocationListener(GetLocationListener getLocationListener) {
        mGetLocationListener = getLocationListener;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, new LocationRequest(), this);
    }

    public void setVideoInfo(@NonNull VideoInfo videoInfo) {
        mVideoInfo = videoInfo;
    }

    @Override
    public void onLocationChanged(final Location location) {
        mVideoInfo.setLocation(location.getLatitude(), location.getLongitude());

        Log.e("LocationWatcher", String.format("location %s :  %s", location.getLatitude(), location.getLongitude()));

        dispose();

        if (mVideoInfo.getLocalId() > 0) {
            ContentValues contentValues = new ContentValues(2);
            contentValues.put(ContentDescriptor.VideosUpload.Cols.LATITUDE, mVideoInfo.getLatitude());
            contentValues.put(ContentDescriptor.VideosUpload.Cols.LONGITUDE, mVideoInfo.getLongitude());
            App.getInstance().getContentResolver().update(ContentDescriptor.VideosUpload.URI, contentValues, "_id = " + mVideoInfo.getLocalId(), null);
        }

        if (mGetLocationListener != null) {
            mGetLocationListener.onGotLocation();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("LocationWatcher", connectionResult.toString());
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("LocationWatcher", "onConnectionSuspended " + i);
    }

    public void dispose() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    @Override
    protected void finalize() throws Throwable {
        // Connection can be open if activity recording canceled before coordinates were got.
        // Connection must be close before this instance will destroyed.
        dispose();
        super.finalize();
    }

    public interface GetLocationListener {
        /**
         * notify of successful location receiving. use it for unbinding from watcher
         */
        void onGotLocation();
    }
}
