package com.humanet.humanetcore.utils;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseActivity;

/**
 * Created by ovitali on 24.09.2015.
 */
public class ToolbarHelper {

    public interface Delegator {
        Toolbar createToolbar(BaseActivity activity);
    }

    private static Delegator sDelegator;

    public static void init(Delegator delegator) {
        sDelegator = delegator;
    }

    public static Toolbar createToolbar(BaseActivity activity) {

        if (sDelegator == null) {
            sDelegator = new Delegator() {
                @Override
                public Toolbar createToolbar(BaseActivity activity) {
                    Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
                    activity.setSupportActionBar(toolbar);

                    ActionBar actionBar = activity.getSupportActionBar();
                    if (actionBar != null) {
                        actionBar.setDisplayHomeAsUpEnabled(true);
                        actionBar.setHomeButtonEnabled(true);
                    }

                    return toolbar;
                }
            };
        }

        return sDelegator.createToolbar(activity);

    }

}
