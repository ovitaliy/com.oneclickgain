package com.humanet.humanetcore.utils;

import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.model.enums.selectable.BaseItem;
import com.humanet.humanetcore.model.enums.selectable.Hobby;
import com.humanet.humanetcore.model.enums.selectable.Interest;
import com.humanet.humanetcore.model.enums.selectable.Pet;
import com.humanet.humanetcore.model.enums.selectable.QuestionnaireItem;
import com.humanet.humanetcore.model.enums.selectable.Religion;
import com.humanet.humanetcore.model.enums.selectable.Sport;

import java.util.List;

/**
 * Created by ovitali on 29.10.2015.
 */
public class QuestionnaireHelper {

    @SuppressWarnings("unchecked")
    public static <T extends BaseItem> List<T> getValues(QuestionnaireItem questionnaireItem) {
        switch (questionnaireItem) {
            case HOBBIES:
                return (List<T>) Hobby.values();
            case INTERESTS:
                return (List<T>) Interest.values();
            case SPORT:
                return (List<T>) Sport.values();
            case RELIGION:
                return (List<T>) Religion.values();
            case PET:
                return (List<T>) Pet.values();
            /*
            case GAME:
                return (List<T>) Game.values();
            case NON_GAME:
                return (List<T>) NonGame.values();
            case EXTREME:
                return (List<T>) Extreme.values();
            case VIRTUAL:
                return (List<T>) Virtual.values();

            case PART_GAMES:
                return (List<T>) PartGames.values();*/

            default:
                throw new RuntimeException("unsupported QuestionnaireItem");
        }
    }

    @SuppressWarnings("unchecked")
    public static BaseItem getValue(UserInfo userInfo, QuestionnaireItem questionnaireItem) {
        switch (questionnaireItem) {

            case HOBBIES:
                return userInfo.getHobbie();

            case INTERESTS:
                return userInfo.getInterest();

            case SPORT:
                return userInfo.getSport();

            case RELIGION:
                return userInfo.getReligion();

            case PET:
                return userInfo.getPet();

            /*case GAME:
                return userInfo.getGames();
            case NON_GAME:
                return userInfo.getNonGame();
            case EXTREME:
                return userInfo.getExtreme();
            case VIRTUAL:
                return userInfo.getVirtual();

            case PART_GAMES:
                return userInfo.getPartGames();*/
            default:
                throw new RuntimeException("unsupported QuestionnaireItem");
        }
    }

    @SuppressWarnings("unchecked")
    public static void setValue(UserInfo userInfo, BaseItem value, QuestionnaireItem questionnaireItem) {
        switch (questionnaireItem) {
            case HOBBIES:
                userInfo.setHobbie((Hobby) value);
                break;
            case INTERESTS:
                userInfo.setInterest((Interest) value);
                break;
            case SPORT:
                userInfo.setSport((Sport) value);
                break;
            case RELIGION:
                userInfo.setReligion((Religion) value);
                break;
            case PET:
                userInfo.setPet((Pet) value);
                break;

            /*case GAME:
                userInfo.setGames((Game) value);
                break;
            case NON_GAME:
                userInfo.setNonGame((NonGame) value);
                break;
            case EXTREME:
                userInfo.setExtreme((Extreme) value);
                break;
            case VIRTUAL:
                userInfo.setVirtual((Virtual) value);
                break;

            case PART_GAMES:
                userInfo.setPartGames((PartGames) value);
                break;
*/
            default:
                throw new RuntimeException("unsupported QuestionnaireItem");
        }
    }


}
