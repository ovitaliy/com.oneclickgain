package com.humanet.humanetcore.service;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.utils.GsonHelper;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import com.squareup.okhttp.OkHttpClient;

import org.apache.commons.io.IOUtils;

import java.lang.reflect.Type;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.ConversionException;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedInput;
import roboguice.util.temp.Ln;

public abstract class AppService extends RetrofitGsonSpiceService {

    @Override
    public int getThreadCount() {
        return 2;
    }


    @Override
    protected String getServerUrl() {
        return App.getInstance().getServerEndpoint();
    }

    @Override
    protected Converter createConverter() {
        return new Converter();
    }

    protected RestAdapter.Builder createRestAdapterBuilder() {

        Ln.getConfig().setLoggingLevel(Log.ERROR);

        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.NONE)
                //.setLog(new AndroidLog("SERVER RETROFIT"))
                .setClient(new OkClient(new OkHttpClient()))
                .setEndpoint(getServerUrl())
                .setConverter(getConverter());
    }

    public static class Converter extends GsonConverter {
        public Converter() {
            super(GsonHelper.getGson());
        }

        @Override
        public Object fromBody(TypedInput body, Type type) throws ConversionException {
            String data = null;

            try {
                data = IOUtils.toString(body.in());

                Log.i("API: Response", data);
                JsonElement jsonElement = new JsonParser().parse(data);
                if (jsonElement instanceof JsonObject) {
                    JsonObject jsonObject = (JsonObject) jsonElement;
                    if (jsonObject.has("response")) {
                        jsonElement = jsonObject.get("response");
                    }
                }
                return GsonHelper.getGson().fromJson(jsonElement, type);
            } catch (Exception ex) {
                if (!App.DEBUG) {
                    if (data != null)
                        Crashlytics.logException(new Throwable(data));

                    Crashlytics.getInstance().core.logException(ex);
                }

                throw new ConversionException(ex);
            } finally {
                try {
                    body.in().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
