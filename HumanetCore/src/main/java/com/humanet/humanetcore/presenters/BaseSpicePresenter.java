package com.humanet.humanetcore.presenters;

import android.support.annotation.NonNull;

import com.humanet.humanetcore.views.BaseView;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Uran on 08.06.2016.
 */
public abstract class BaseSpicePresenter<T extends BaseView> extends BasePresenter<T> {

    @NonNull
    private SpiceManager mSpiceManager;


    public BaseSpicePresenter(@NonNull SpiceManager spiceManager, @NonNull T view) {
        super(view);
        mSpiceManager = spiceManager;
    }

    @NonNull
    public SpiceManager getSpiceManager() {
        return mSpiceManager;
    }

    public void setSpiceManager(@NonNull SpiceManager spiceManager) {
        mSpiceManager = spiceManager;
    }


}
