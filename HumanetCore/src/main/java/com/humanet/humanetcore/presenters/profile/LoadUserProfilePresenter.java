package com.humanet.humanetcore.presenters.profile;

import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.GetUserInfoRequest;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.presenters.BaseSpicePresenter;
import com.humanet.humanetcore.views.profile.ViewProfileView;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Uran on 08.06.2016.
 */
public class LoadUserProfilePresenter extends BaseSpicePresenter<ViewProfileView> {

    public LoadUserProfilePresenter(SpiceManager spiceManager, ViewProfileView view) {
        super(spiceManager, view);
    }


    public void load(int userId) {
        getSpiceManager().execute(new GetUserInfoRequest(userId), new SimpleRequestListener<UserInfo>() {
            @Override
            public void onRequestSuccess(UserInfo userInfo) {
                getView().showUser(userInfo);
            }
        });
    }


}
