package com.humanet.humanetcore.presenters;

/**
 * Created by Uran on 09.06.2016.
 */

import android.support.annotation.NonNull;

import com.humanet.humanetcore.views.BaseView;

public abstract class BasePresenter<T extends BaseView> {

    @NonNull
    private T mView;

    public BasePresenter(@NonNull T view) {
        mView = view;
    }

    @NonNull
    public T getView() {
        return mView;
    }
}
