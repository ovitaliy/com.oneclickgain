package com.humanet.impls;

import android.content.res.Resources;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.model.enums.BalanceChangeType;
import com.humanet.humanetcore.utils.BalanceHelper;

import java.util.Locale;

/**
 * Created by ovi on 2/5/16.
 */
public class BalanceChangeTypeImpl implements BalanceHelper.BalanceTypeArray {

    enum Type implements BalanceChangeType {

        REGISTRATION,
        QUESTIONARY,
        INVITE,
        NEW_VIDEO,
        SHARE,
        VLOG,
        LIKE_SENT,
        LIKE_RECIEVE,

        NEW_VOTING,
        VOTING,

        FOLLOWING,
        NEW_FOLLOWER,


        MARKET_SOLD,
        MARKET_BOUGHT,
        MARKED_SHARED_GIVEN,
        MARKED_SHARED_GOT,

        COINS_ADD {
            @Override
            public int getId() {
                return 299;
            }
        };

        public int getId() {
            return ordinal() + 200;
        }

        public String getTitle() {
            Resources resources = App.getInstance().getResources();
            int stringId = resources.getIdentifier("balance.change_type." + toString().toLowerCase(Locale.getDefault()), "string", App.getInstance().getPackageName());
            return resources.getString(stringId);
        }

        public String getDrawable() {
            return null;
        }
    }


    public BalanceChangeType getById(int id) {
        for (Type type : Type.values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }


}
