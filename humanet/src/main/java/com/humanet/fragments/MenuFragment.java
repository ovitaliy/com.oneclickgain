package com.humanet.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.humanet.R;
import com.humanet.activities.SplashActivity;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.fragments.BaseMenuFragment;

/**
 * Created by ovi on 1/25/16.
 */
public class MenuFragment extends BaseMenuFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        view.findViewById(R.id.logout).setOnClickListener(this);

        view.findViewById(R.id.exchange).setOnClickListener(this);
        view.findViewById(R.id.do_it).setOnClickListener(this);
        view.findViewById(R.id.best_practice).setOnClickListener(this);
        view.findViewById(R.id.vistory).setOnClickListener(this);
        view.findViewById(R.id.grimace).setOnClickListener(this);
        view.findViewById(R.id.congratulations).setOnClickListener(this);
        view.findViewById(R.id.balance).setOnClickListener(this);
        view.findViewById(R.id.navigation_info).setOnClickListener(this);


        mBalanceTextView = (TextView) view.findViewById(R.id.user_balance);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logout:
                logout();
                break;

            case R.id.avatar:
            case R.id.first_name:
                mDrawerLayout.closeDrawers();
                mOnViewProfileListener.onViewProfile(AppUser.getInstance().getUid());
                break;

            default:
                mOnNavigateListener.onNavigateByViewId(v.getId());
                mDrawerLayout.closeDrawers();

                break;
        }
    }

    @Override
    protected Class newSplashActivityInstance() {
        return SplashActivity.class;
    }
}
