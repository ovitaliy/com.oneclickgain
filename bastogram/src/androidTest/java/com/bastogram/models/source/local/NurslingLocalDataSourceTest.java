package com.bastogram.models.source.local;

import org.junit.Test;

/**
 * Created by Uran on 14.06.2016.
 */
public class NurslingLocalDataSourceTest {

    NurslingLocalDataSource nurslingLocalDataSource = new NurslingLocalDataSource();


    @Test
    public void testGetList() throws Exception {
        List<Nursling> nurslings = nurslingLocalDataSource.getList().toBlocking().first();
        assertNotNull(nurslings);

    }

    @Test
    public void testSave() throws Exception {
        final String name = "test name";
        Nursling nursling = new Nursling();
        nursling.setName(name);

        Nursling saved = nurslingLocalDataSource.save(nursling).toBlocking().first();
        assertNotNull(saved);
        assertEquals(saved.getName(), name);

        nursling = nurslingLocalDataSource.getById(saved.getId()).toBlocking().first();

        assertEquals(nursling.getId(), saved.getId());
        assertEquals(nursling.getName(), saved.getName());

        nurslingLocalDataSource.delete(saved.getId()).toBlocking().last();
        Nursling deleted = nurslingLocalDataSource.getById(1).toBlocking().first();
        assertNull(deleted);
    }


}