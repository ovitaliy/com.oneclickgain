package com.bastogram.views;

import com.bastogram.models.Nursling;
import com.bastogram.models.Skill;
import com.humanet.humanetcore.views.BaseView;

/**
 * Created by Uran on 08.06.2016.
 */
public interface IPetsView extends BaseView {

    void showPetList(Nursling[] nurslings);



}
