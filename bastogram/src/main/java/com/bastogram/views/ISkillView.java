package com.bastogram.views;

import com.humanet.humanetcore.views.BaseView;

/**
 * Created by serezha on 20.07.16.
 */
public interface ISkillView extends BaseView {

	void showSkill();
}
