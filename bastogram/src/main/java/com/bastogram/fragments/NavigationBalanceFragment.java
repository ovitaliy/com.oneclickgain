package com.bastogram.fragments;

import com.bastogram.R;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.fragments.base.BasePickerFragment;
import com.humanet.humanetcore.interfaces.CirclePickerItem;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ovitali on 25.08.2015.
 */
public class NavigationBalanceFragment extends BasePickerFragment implements IMenuBindFragment {
    @Override
    public String getTitle() {
        return getString(R.string.menu_balance);
    }

    public static NavigationBalanceFragment newInstance() {
        return new NavigationBalanceFragment();
    }


    @Override
    protected ArrayList<CirclePickerItem> getItems() {
        return new ArrayList<CirclePickerItem>(Arrays.asList(SubItem.values()));
    }

    private enum SubItem implements CirclePickerItem {
        RATING {
            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_ratings;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_category_type_rating);
            }

            @Override
            public int getId() {
                return R.id.rating;
            }
        },

        MARKET {
            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_crowd;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.crowd);
            }

            @Override
            public int getId() {
                return R.id.crowd;
            }
        },
        CURRENCY {
            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_currency_navigation;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_category_type_currency);
            }

            @Override
            public int getId() {
                return R.id.currency;
            }
        },
    }

    @Override
    public int getSelectedMenuItem() {
        return R.id.balance;
    }
}