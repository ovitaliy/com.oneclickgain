package com.bastogram.fragments.profile;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.bastogram.fragments.PetVideoListFragment;
import com.bastogram.models.Nursling;
import com.bastogram.widgets.profile.PetsListView;
import com.bastogram.widgets.profile.SkillsView;
import com.humanet.humanetcore.activities.BaseMainActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.database.CategoriesRequest;
import com.humanet.humanetcore.fragments.profile.BaseViewProfileFragment;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.enums.VideoType;

import org.apache.commons.lang3.RandomUtils;

/**
 * Created by Uran on 06.06.2016.
 */
public class BastogramProfileFragment extends BaseViewProfileFragment implements AdapterView.OnItemClickListener {

    private SkillsView mSkillsView;
    private PetsListView mPetsListView;
    private Category mCategory;

    @Override
    public View getPageAt(ViewGroup container, int position) {

        switch (position) {
            case 0:
                mPetsListView = new PetsListView(getActivity());
                mPetsListView.setSpiceManager(getSpiceManager());
                mPetsListView.setOnItemClickListener(this);
                return mPetsListView;
            case 1:
                return getQuestionnaireView();
            case 2:
                mSkillsView = new SkillsView(getActivity(), getSpiceManager());
                return mSkillsView;
        }
        throw new IllegalArgumentException("unexpected position value:" + position);
    }

    @NonNull
    @Override
    public int[] getTabNames() {
        return new int[]{
                com.humanet.humanetcore.R.string.view_profile_tab_pets,
                com.humanet.humanetcore.R.string.view_profile_tab_questionary,
                com.humanet.humanetcore.R.string.view_profile_tab_skills,
        };
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mSkillsView != null)
            mSkillsView.update();

        if(mPetsListView != null)
            mPetsListView.update();
        //TODO: Implement other views this way
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Nursling nursling = (Nursling) mPetsListView.getAdapter().getItem(position);

        CategoriesRequest request = new CategoriesRequest(VideoType.NEWS);
        getSpiceManager().execute(request, new SimpleRequestListener<Category[]>() {
            @Override
            public void onRequestSuccess(Category[] categories) {
                mCategory = categories[RandomUtils.nextInt(0, categories.length)];

                PetVideoListFragment fragment = new PetVideoListFragment.Builder(VideoType.NEWS)
                        .setTitle(nursling.getName())
                        .setCategoryType(mCategory)
                        .setReplyId(0)
                        .setCurrentPosition(0)
                        .setNursling(nursling)
                        .build();
                ((BaseMainActivity) getActivity()).startFragment(fragment, true);
            }
        });

    }
}
