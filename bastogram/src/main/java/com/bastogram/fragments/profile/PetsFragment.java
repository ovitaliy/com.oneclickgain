package com.bastogram.fragments.profile;

import com.bastogram.R;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.fragments.base.BaseVideoCategoryPickerFragment;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by serezha on 19.07.16.
 */
public class PetsFragment extends BaseVideoCategoryPickerFragment implements IMenuBindFragment {

	@Override
	public String getTitle() {
		return getString(R.string.menu_nurslings);
	}


	@Override
	protected String[] getTabNames() {
		return new String[]{
				getString(R.string.online_tab_my_pets),
				getString(R.string.online_tab_all_pets),
				getString(R.string.online_tab_my_choice),
		};
	}

	@Override
	protected VideoType[] getVideoTypes() {
		return new VideoType[]{
				VideoType.VLOG,
				VideoType.NEWS,
				VideoType.MY_CHOICE, //TODO: Will be correct types for pets
		};
	}


	@Override
	public int getSelectedMenuItem() {
		return R.id.nurslings;
	}
}
