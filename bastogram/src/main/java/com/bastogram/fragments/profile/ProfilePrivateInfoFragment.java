package com.bastogram.fragments.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.bastogram.R;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.model.enums.selectable.Hobby;
import com.humanet.humanetcore.model.enums.selectable.Interest;
import com.humanet.humanetcore.model.enums.selectable.Pet;
import com.humanet.humanetcore.model.enums.selectable.Religion;
import com.humanet.humanetcore.model.enums.selectable.Sport;
import com.humanet.humanetcore.views.widgets.CirclePickerView;

import java.util.ArrayList;

/**
 * Created by ovi on 21.05.2016.
 */
public class ProfilePrivateInfoFragment extends com.humanet.humanetcore.fragments.profile.ProfilePrivateInfoFragment {

    public static ProfilePrivateInfoFragment newInstance(boolean edit) {
        ProfilePrivateInfoFragment f = new ProfilePrivateInfoFragment();
        Bundle args = new Bundle();
        args.putBoolean("edit", edit);
        f.setArguments(args);
        return f;
    }

    private CirclePickerView mHobbyPickerView;
    private CirclePickerView mSportsPickerView;
    private CirclePickerView mPetsPickerView;
    private CirclePickerView mInterestsPickerView;
    private CirclePickerView mReligionPickerView;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mHobbyPickerView = (CirclePickerView) view.findViewById(R.id.hobby);
        mSportsPickerView = (CirclePickerView) view.findViewById(R.id.sport);
        mPetsPickerView = (CirclePickerView) view.findViewById(R.id.pet);
        mInterestsPickerView = (CirclePickerView) view.findViewById(R.id.interests);
        mReligionPickerView = (CirclePickerView) view.findViewById(R.id.religion);
        int size = App.WIDTH;
        mHobbyPickerView.getLayoutParams().width = size;
        mHobbyPickerView.getLayoutParams().height = size;
        mSportsPickerView.getLayoutParams().width = size;
        mSportsPickerView.getLayoutParams().height = size;
        mPetsPickerView.getLayoutParams().width = size;
        mPetsPickerView.getLayoutParams().height = size;
        mInterestsPickerView.getLayoutParams().width = size;
        mInterestsPickerView.getLayoutParams().height = size;
        mReligionPickerView.getLayoutParams().width = size;
        mReligionPickerView.getLayoutParams().height = size;

        int currentHobbyId = AppUser.getInstance().get().getHobbie() != null ? AppUser.getInstance().get().getHobbie().getId() : -1;
        mHobbyPickerView.fill((ArrayList<Hobby>) Hobby.values(), currentHobbyId, null);
        int currentSportId = AppUser.getInstance().get().getSport() != null ? AppUser.getInstance().get().getSport().getId() : -1;
        mSportsPickerView.fill((ArrayList<Sport>) Sport.values(), currentSportId, null);
        int currentPetId = AppUser.getInstance().get().getPet() != null ? AppUser.getInstance().get().getPet().getId() : -1;
        mPetsPickerView.fill((ArrayList<Pet>) Pet.values(), currentPetId, null);
        int currentInterestId = AppUser.getInstance().get().getInterest() != null ? AppUser.getInstance().get().getInterest().getId() : -1;
        mInterestsPickerView.fill((ArrayList<Interest>) Interest.values(), currentInterestId, null);
        int currentReligionId = AppUser.getInstance().get().getReligion() != null ? AppUser.getInstance().get().getReligion().getId() : -1;
        mReligionPickerView.fill((ArrayList<Religion>) Religion.values(), currentReligionId, null);

    }

    @Override
    protected void checkMandatoryFields(UserInfo info) {
        info.setHobbie((Hobby) mHobbyPickerView.getCurrentElement());
        info.setSport((Sport) mSportsPickerView.getCurrentElement());
        info.setPet((Pet) mPetsPickerView.getCurrentElement());
        info.setInterest((Interest) mInterestsPickerView.getCurrentElement());
        info.setReligion((Religion) mReligionPickerView.getCurrentElement());
        super.checkMandatoryFields(info);
    }


}
