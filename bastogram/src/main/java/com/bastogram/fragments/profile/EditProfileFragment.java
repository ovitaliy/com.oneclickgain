package com.bastogram.fragments.profile;

import android.support.v4.app.Fragment;

import com.bastogram.R;
import com.humanet.humanetcore.activities.EditProfileActivity;
import com.humanet.humanetcore.fragments.base.BaseFragmentTabHostingFragment;

/**
 * Created by Uran on 10.06.2016.
 */
public class EditProfileFragment extends BaseFragmentTabHostingFragment {

    @Override
    protected String[] getTabNames() {
        return new String[]{
                getString(R.string.view_profile_tab_pets),
                getString(R.string.view_profile_tab_questionary),
                getString(R.string.view_profile_tab_skills),
        };
    }

    @Override
    protected Fragment getFragmentForTabPosition(int position) {
        switch (position) {
            case 0:
                return new ProfilePetsFragment();
            case 1:
                Fragment fragment = ProfilePrivateInfoFragment.newInstance(true);
                ((EditProfileActivity) getActivity()).setProfilePrivateInfoFragment(fragment);
                return fragment;
            case 2:
                return new EditSkillsFragment();
            default:
                throw new IllegalArgumentException("wrong position");
        }
    }

    @Override
    public String getTitle() {
        return getString(com.humanet.humanetcore.R.string.profile_edit);
    }


}
