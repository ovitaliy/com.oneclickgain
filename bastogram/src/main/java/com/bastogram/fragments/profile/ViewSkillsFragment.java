package com.bastogram.fragments.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bastogram.R;
import com.bastogram.models.BastogramUserInfo;
import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.Competence;
import com.bastogram.models.Portfolio;
import com.bastogram.models.SkillSubGroup;
import com.bastogram.presenters.profile.LoadPortfolioPresenter;
import com.bastogram.views.IPortfolioView;
import com.bastogram.widgets.profile.GridView;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.enums.selectable.Education;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.humanet.humanetcore.views.utils.UiUtil;

/**
 * Created by Uran on 08.06.2016.
 */
public class ViewSkillsFragment extends BaseTitledFragment implements IPortfolioView {

    private static final String SKILL_PARAM = "skill";

    private BastogramUserInfoSkill mBastogramUserInfoSkill;
    private SkillSubGroup mSkillSubGroup;

    private ViewGroup mContainer;

    public static Fragment newInstance(SkillSubGroup skill) {
        Fragment fragment = new ViewSkillsFragment();

        Bundle bundle = new Bundle(1);
        bundle.putSerializable(SKILL_PARAM, skill);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getTitle() {
        return getString(R.string.view_profile_tab_skills);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSkillSubGroup = (SkillSubGroup) getArguments().getSerializable(SKILL_PARAM);

        BastogramUserInfo user = (BastogramUserInfo) AppUser.getInstance().get();

        mBastogramUserInfoSkill = new BastogramUserInfoSkill();
        mBastogramUserInfoSkill.setSkillSubgroup(mSkillSubGroup.getId());
        for (BastogramUserInfoSkill infoSkill : user.getSkills()){
            if (infoSkill.getSkillSubGroup() == mSkillSubGroup.getId()){
                mBastogramUserInfoSkill = infoSkill.clone();
            }
        }

        if (mSkillSubGroup == null)
            throw new IllegalArgumentException("skill can not be null");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_skill, container, false);

        TextView titleView = (TextView) view.findViewById(R.id.title);
        titleView.setText(mSkillSubGroup.getTitle());

        GridView gridView = (GridView) view.findViewById(R.id.grid_view);

        String competenceTitle = "";
        for(Competence competence : mSkillSubGroup.getCompetences()) {
            if(competence.getId() == mBastogramUserInfoSkill.getSkill())
                competenceTitle = competence.getTitle();
        }

        gridView.addData(0, getString(R.string.profile_competion), competenceTitle);
        gridView.addData(1, getString(R.string.profile_specialization), splitStringByComma(mBastogramUserInfoSkill.getTags()));
        gridView.addData(2, getString(R.string.profile_experience), String.valueOf(mBastogramUserInfoSkill.getExperience()));

        Education education = Education.getById(String.valueOf(mBastogramUserInfoSkill.getEducation()));
        if (education != null)
            gridView.addData(3, getString(R.string.profile_level), education.getTitle());

        mContainer = (ViewGroup) view.findViewById(R.id.container);

        LoadPortfolioPresenter loadPortfolioPresenter = new LoadPortfolioPresenter(this);
        loadPortfolioPresenter.load(mBastogramUserInfoSkill);

        return view;
    }

    @Override
    public void showPortfolioList(Portfolio[] portfolios) {
        //if (!isVisible())
        //    return;


        for (int i = 0; i < portfolios.length; i++) {
            Portfolio portfolio = portfolios[i];
            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_portfolio, mContainer, false);

            UiUtil.setTextValue(view, R.id.name, portfolio.getName());
            UiUtil.setTextValue(view, R.id.link, portfolio.getLink());
            UiUtil.setTextValue(view, R.id.description, portfolio.getDescription());

            if (i != portfolios.length - 1)
                view.setPadding(0, 0, 0, (int) ConverterUtil.dpToPix(getContext(), 10));

            mContainer.addView(view);
        }


    }

    private String[] splitStringByComma(String string) {

        return string.split("\\s*,\\s*");
    }
}
