package com.bastogram.loadres;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import com.bastogram.R;
import com.bastogram.api.request.database.SkillsRequest;
import com.bastogram.models.Competence;
import com.bastogram.models.SkillGroup;
import com.bastogram.models.SkillSubGroup;
import com.bastogram.widgets.profile.CompetenciesSpinnerView;
import com.bastogram.widgets.profile.SkillSubGroupAutocompleteView;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.views.behaviour.SimpleOnItemSelectedListener;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by serezha on 04.08.16.
 */
public class SkillSubGroupDataLoader {

	private SpiceManager mSpiceManager;
	private SkillSubGroupAutocompleteView mSkillSubGroupAutocompleteView;
	private CompetenciesSpinnerView mCompetenciesSpinnerView;

	private SkillsRequest mSkillsRequest;

	private ArrayList<SkillSubGroup> mSkillSubGroups;
	private ArrayList<Competence> mCompetences;

	private int mUserSkillSubGroupId = -1;

	public SkillSubGroupDataLoader(Context context, SpiceManager spiceManager, SkillSubGroupAutocompleteView skillSubGroupAutocompleteView, CompetenciesSpinnerView competenciesSpinnerView, int skillSubGroupId) {
		mSpiceManager = spiceManager;
		mSkillSubGroupAutocompleteView = skillSubGroupAutocompleteView;
		mSkillSubGroupAutocompleteView.setSkillSubGroupDataLoader(this);
		mCompetenciesSpinnerView = competenciesSpinnerView;

		mUserSkillSubGroupId = skillSubGroupId;

		List<Competence> initialCompetences = new ArrayList<>(1);
		Competence initialCompetence = new Competence();
		initialCompetence.setTitleFromResources(context.getResources().getString(R.string.profile_competion));
		initialCompetences.add(initialCompetence);
		mCompetenciesSpinnerView.setCompetenciesList(initialCompetences);

		mSkillSubGroupAutocompleteView.setOnItemSelectedListener(new SimpleOnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				if(mSkillSubGroups != null) {
					mUserSkillSubGroupId = (int) l;
					for(SkillSubGroup skillSubGroup : mSkillSubGroups) {
						if(mUserSkillSubGroupId == skillSubGroup.getId()) {
							List<Competence> competenceList = new ArrayList<Competence>(skillSubGroup.getCompetences().length + 1);
							Competence competence = new Competence();
							competence.setTitleFromResources(context.getResources().getString(R.string.profile_competion));
							competenceList.add(competence);
							competenceList.addAll(Arrays.asList(skillSubGroup.getCompetences()));
							mCompetenciesSpinnerView.setCompetenciesList(competenceList);
							mCompetenciesSpinnerView.setSelection(0);
							break;
						}
					}
				}
			}
		});

		loadSkillSubgroups();

	}

	private void loadSkillSubgroups() {
		if(mSkillsRequest != null) {
			mSpiceManager.cancel(mSkillsRequest);
		}
		mSkillsRequest = new SkillsRequest();

		mSpiceManager.execute(mSkillsRequest, new SimpleRequestListener<SkillGroup[]>() {
			@Override
			public void onRequestSuccess(SkillGroup[] skillGroups) {

				mSkillSubGroups = new ArrayList<SkillSubGroup>();
				mCompetences = new ArrayList<Competence>();

				for(int i = 0; i < skillGroups.length; i++) {
					mSkillSubGroups.addAll(Arrays.asList(skillGroups[i].getSkillSubGroups()));
				}

				mSkillSubGroupAutocompleteView.setSkillSubGroupList(mSkillSubGroups);
				if(mUserSkillSubGroupId > -1) {
					for(int i = 0; i < mSkillSubGroups.size(); i++) {
						if(mUserSkillSubGroupId == mSkillSubGroups.get(i).getId()) {
							mSkillSubGroupAutocompleteView.setSelectedPosition(i);
							break;
						}
					}
					mUserSkillSubGroupId = -1;
				}
				mSkillsRequest = null;
			}
		});
	}

	public int getSelectedSkillSubGroupId() {
		return mSkillSubGroupAutocompleteView.getSelectedItemId();
	}

	public String getSelectedSkillSubGroupTitle() {
		return mSkillSubGroupAutocompleteView.getSelectedItem().getTitle();
	}

	public int getSelectedCompetenceId() {
		return (int) mCompetenciesSpinnerView.getSelectedItemId();
	}
}
