package com.bastogram.presenters.profile;

import android.support.annotation.NonNull;

import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.Portfolio;
import com.bastogram.models.source.PortfolioDataSource;
import com.bastogram.models.source.local.PortfolioLocalDataSource;
import com.bastogram.models.source.remote.PortfolioRemoteDataSource;
import com.bastogram.views.IPortfolioView;
import com.humanet.humanetcore.presenters.BasePresenter;

/**
 * Created by Uran on 09.06.2016.
 */
public class LoadPortfolioPresenter extends BasePresenter<IPortfolioView> {

    private PortfolioDataSource mPortfolioDataSource;

    public LoadPortfolioPresenter(@NonNull IPortfolioView view) {
        super(view);
        mPortfolioDataSource = new PortfolioDataSource(new PortfolioLocalDataSource(), new PortfolioRemoteDataSource());
    }

    public void load(BastogramUserInfoSkill skill) {

        //List<Portfolio> portfoliosList = mPortfolioDataSource.getListBySubgroup(skill.getSkillSubGroup());

        /*if(skill == null)
            portfoliosList = mPortfolioDataSource.getList();
        else
            portfoliosList = mPortfolioDataSource.getListBySkillId(skill.getId());*/

        /*Portfolio[] portfolios = new Portfolio[portfoliosList.size()];
        portfoliosList.toArray(portfolios);
        getView().showPortfolioList(portfolios);*/

        if(skill.getPortfolios() == null)
            return;

        Portfolio[] portfolios = new Portfolio[skill.getPortfolios().size()];
        skill.getPortfolios().toArray(portfolios);

        getView().showPortfolioList(portfolios);

    }


}
