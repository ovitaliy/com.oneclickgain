package com.bastogram.presenters.profile;

import android.support.annotation.NonNull;

import com.bastogram.views.ISkillView;
import com.humanet.humanetcore.presenters.BasePresenter;

/**
 * Created by serezha on 20.07.16.
 */
public class LoadSkillPresenter extends BasePresenter<ISkillView> {

	public LoadSkillPresenter(@NonNull ISkillView view) {
		super(view);
	}

	public void load() {
		getView().showSkill();
	}
}