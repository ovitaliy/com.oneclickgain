package com.bastogram.presenters.profile;

import android.support.annotation.NonNull;

import com.bastogram.models.Portfolio;
import com.bastogram.models.source.PortfolioDataSource;
import com.bastogram.models.source.local.PortfolioLocalDataSource;
import com.bastogram.models.source.remote.PortfolioRemoteDataSource;
import com.bastogram.views.IPortfolioView;
import com.humanet.humanetcore.presenters.BasePresenter;

/**
 * Created by serezha on 14.07.16.
 */
public class AddPortfolioPresenter extends BasePresenter<IPortfolioView> {

    private final PortfolioDataSource mPortfolioDataSource;

    public AddPortfolioPresenter(@NonNull IPortfolioView view) {
        super(view);
        mPortfolioDataSource = new PortfolioDataSource(new PortfolioLocalDataSource(), new PortfolioRemoteDataSource());
    }

    public void save(Portfolio portfolio) {
        if (portfolio.getId() < 0)
            mPortfolioDataSource.save(portfolio);
        else
            mPortfolioDataSource.edit(portfolio);

    }

}
