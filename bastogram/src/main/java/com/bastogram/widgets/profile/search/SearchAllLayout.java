package com.bastogram.widgets.profile.search;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.LoaderManager;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import com.bastogram.R;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.views.behaviour.IGrabSearchInfo;
import com.humanet.humanetcore.views.widgets.search.SearchContentLayout;
import com.humanet.humanetcore.views.widgets.search.SearchProfileLayout;
import com.humanet.humanetcore.views.widgets.search.SearchQuestionnaireLayout;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by serezha on 22.07.16.
 */
public class SearchAllLayout extends ScrollView implements IGrabSearchInfo {

	private SearchProfileLayout mSearchProfileLayout;
	private SearchQuestionnaireLayout mSearchQuestionnaireLayout;
	private SearchContentLayout mSearchContentLayout;

	public SearchAllLayout(Context context) {
		super(context);
	}

	public SearchAllLayout(Context context, SpiceManager spiceManager, LoaderManager loaderManager) {
		super(context);

		mSearchProfileLayout = new SearchProfileLayout(context, spiceManager);
		mSearchQuestionnaireLayout = new SearchQuestionnaireLayout(context);
		mSearchContentLayout = new SearchContentLayout(context, spiceManager, loaderManager);

		LinearLayout container = new LinearLayout(context);
		addView(container);
		container.setOrientation(LinearLayout.VERTICAL);

		container.addView(mSearchProfileLayout);
		container.addView(mSearchQuestionnaireLayout);
		container.addView(getContentTitleLayout());
		container.addView(mSearchContentLayout);

	}

	@Override
	public void grabSearchInfo(ArgsMap params) {
		mSearchProfileLayout.grabSearchInfo(params);
		mSearchQuestionnaireLayout.grabSearchInfo(params);
		mSearchContentLayout.grabSearchInfo(params);
	}

	private LinearLayout getContentTitleLayout() {
		LinearLayout contentTitleLayout = new LinearLayout(getContext());
		TextView title = new TextView(getContext());
		title.setText(getContext().getResources().getString(R.string.search_tab_content));
		title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		title.setAllCaps(true);
		title.setTypeface(Typeface.DEFAULT_BOLD);
		title.setGravity(Gravity.CENTER);
		TableRow.LayoutParams titleParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
		title.setLayoutParams(titleParams);
		contentTitleLayout.addView(title);
		contentTitleLayout.setBackgroundColor(getContext().getResources().getColor(R.color.colorPrimary));
		contentTitleLayout.setPadding(0, 20, 0, 20);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		params.setMargins(20, 10, 20, 0);
		contentTitleLayout.setLayoutParams(params);

		return contentTitleLayout;
	}
}
