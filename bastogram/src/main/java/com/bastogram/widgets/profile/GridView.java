package com.bastogram.widgets.profile;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.bastogram.R;
import com.humanet.humanetcore.views.utils.ConverterUtil;

/**
 * Created by Uran on 08.06.2016.
 */
public class GridView extends ViewGroup {

    private final int mColumnsCount = 2;

    private final Paint mLinesPaint;

    private final float mLineSize;
    private final int mMiddleLinesPadding;


    public GridView(Context context) {
        this(context, null);
    }

    public GridView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mLineSize = ConverterUtil.dpToPix(context, 1);
        mMiddleLinesPadding = (int) ConverterUtil.dpToPix(context, 5);

        mLinesPaint = new Paint();
        mLinesPaint.setColor(getResources().getColor(R.color.divider));
        mLinesPaint.setStrokeWidth(mLineSize);

        setWillNotDraw(false);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int width = r - l;
        int halfWidth = width / mColumnsCount;

        int n = getChildCount();

        int maxHeight = (int) ConverterUtil.dpToPix(getContext(), 120);

        for (int i = 0; i < n; i++) {
            maxHeight = Math.max(getChildAt(i).getMeasuredHeight(), maxHeight);
        }

        for (int i = 0; i < n; i++) {
            int x = (i % 2) * (halfWidth);
            int y = (i / 2) * maxHeight;

            getChildAt(i).layout(x, y, x + halfWidth, y + maxHeight);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int w = MeasureSpec.getSize(widthMeasureSpec);

        int maxHeight = (int) ConverterUtil.dpToPix(getContext(), 120);

        for (int i = 0; i < getChildCount(); i++) {
            maxHeight = Math.max(getChildAt(i).getMeasuredHeight(), maxHeight);
        }

        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).measure(
                    MeasureSpec.makeMeasureSpec(w / 2, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(maxHeight, MeasureSpec.EXACTLY)
            );
        }

        setMeasuredDimension(widthMeasureSpec, MeasureSpec.makeMeasureSpec(maxHeight * getChildCount() / 2, MeasureSpec.EXACTLY));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawLine(0, mLineSize / 2, canvas.getWidth(), mLineSize / 2, mLinesPaint);
        canvas.drawLine(0, canvas.getHeight() - mLineSize / 2, canvas.getWidth(), canvas.getHeight() - mLineSize / 2, mLinesPaint);
        canvas.drawLine(canvas.getWidth() / 2, 0, canvas.getWidth() / 2, canvas.getHeight(), mLinesPaint);

        int rowsCount = (int) Math.ceil(getChildCount() / mColumnsCount);
        int rowHeight = canvas.getHeight() / rowsCount;

        for (int i = 1; i < rowsCount; i++) {
            canvas.drawLine(
                    mMiddleLinesPadding, // startX
                    rowHeight * i - mLineSize / 2, //startY
                    canvas.getWidth() - mMiddleLinesPadding, // endX
                    rowHeight * i - mLineSize / 2, // endY
                    mLinesPaint);
        }


        super.onDraw(canvas);
    }

    public void addData(int itemIndex, String title, String... values) {
        if (itemIndex < 0 || itemIndex >= getChildCount())
            throw new IllegalArgumentException("itemIndex must be itemIndex < 0 || itemIndex >= getChildCount()");
        if (title == null)
            throw new IllegalArgumentException("title cannot be null");

        ViewGroup container = (ViewGroup) getChildAt(itemIndex);
        container.removeAllViews();

        container.addView(getTitleView(getContext(), title));

        for (String value : values) {
            container.addView(getItemView(getContext(), value, mMiddleLinesPadding));
        }
    }

    private static TextView getTitleView(Context context, String title) {
        TextView textView = new TextView(context);
        textView.setText(title);
        textView.setTextColor(context.getResources().getColor(R.color.dark_gray));
        return textView;
    }

    private static TextView getItemView(Context context, String title, int padding) {
        TextView textView = new TextView(context);
        textView.setBackgroundResource(R.drawable.round_bg);
        textView.setTextColor(Color.WHITE);
        textView.setText(title);
        textView.setPadding(padding, padding, padding, padding);
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.START;
        params.setMargins(0, 0, 0, 2);
        textView.setLayoutParams(params);
        return textView;
    }
}
