package com.bastogram.api.responses;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;

/**
 * Created by serezha on 08.08.16.
 */
public class PetResponse extends BaseResponse {

	@SerializedName("id_pet")
	private int mPetId;

	public int getPetId() {
		return mPetId;
	}
}
