package com.bastogram.api.responses;

import com.bastogram.models.Competence;
import com.bastogram.models.SkillGroup;
import com.bastogram.models.SkillSubGroup;
import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;

import java.util.List;

/**
 * Created by serezha on 29.07.16.
 */
public class SkillResponse extends BaseResponse {

	@SerializedName("skills_group")
	private List<SkillGroup> mSkillsGroup;
	@SerializedName("skills_subgroup")
	private List<SkillSubGroup> mSkillsSubgroup;
	@SerializedName("skills")
	private List<Competence> mSkills;

	public List<SkillGroup> getSkillsGroup() {
		return mSkillsGroup;
	}

	public List<SkillSubGroup> getSkillsSubgroup() {
		return mSkillsSubgroup;
	}

	public List<Competence> getSkills() {
		return mSkills;
	}
}
