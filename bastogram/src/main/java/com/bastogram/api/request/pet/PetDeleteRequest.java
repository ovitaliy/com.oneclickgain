package com.bastogram.api.request.pet;

import android.util.Log;

import com.bastogram.api.sets.PetApiSet;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by serezha on 03.08.16.
 */
public class PetDeleteRequest extends RetrofitSpiceRequest<BaseResponse, PetApiSet> {

	private int mPetId;

	public PetDeleteRequest(int petId) {
		super(BaseResponse.class, PetApiSet.class);

		mPetId = petId;
	}

	@Override
	public BaseResponse loadDataFromNetwork() throws Exception {

		ArgsMap map = new ArgsMap(true);
		map.put("id_pet", mPetId);

		Log.i("PetDeleteRequest", map.toString());

		return getService().petDelete(App.API_APP_NAME, map);
	}
}
