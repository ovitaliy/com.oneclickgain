package com.bastogram.api.sets;

import com.bastogram.api.responses.SkillResponse;
import com.humanet.humanetcore.Constants;

import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by ovitali on 12.10.2015.
 */
public interface DatabaseApiSet {

    @POST(Constants.BASE_API_URL + "/database.getSkills")
    SkillResponse getSkills(@Path("appName") String appName);

}
