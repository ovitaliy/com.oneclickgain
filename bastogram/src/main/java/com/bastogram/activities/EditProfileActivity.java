package com.bastogram.activities;

import android.os.Bundle;

import com.bastogram.fragments.profile.EditProfileFragment;

/**
 * Created by ovi on 24.05.2016.
 */
public class EditProfileActivity extends com.humanet.humanetcore.activities.EditProfileActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar.setNavigationOnClickListener(v -> {
            int backStack = getSupportFragmentManager().getBackStackEntryCount();
            if (backStack == 0)
                onBackPressed();
            else
                getSupportFragmentManager().popBackStack();
        });
    }

    @Override
    protected void launchStartFragment() {
        startFragment(new EditProfileFragment(), false);
    }
}
