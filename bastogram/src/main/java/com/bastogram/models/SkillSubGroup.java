package com.bastogram.models;

/**
 * Created by Uran on 10.06.2016.
 */
public class SkillSubGroup extends BaseSkillModel{

    private Competence[] mCompetences;

    public Competence[] getCompetences() {
        return mCompetences;
    }

    public void setCompetences(Competence[] competences) {
        mCompetences = competences;
    }
}
