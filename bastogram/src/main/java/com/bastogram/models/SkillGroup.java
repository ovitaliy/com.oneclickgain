package com.bastogram.models;

/**
 * Created by Uran on 10.06.2016.
 */
public class SkillGroup extends BaseSkillModel {


	private SkillSubGroup[] mSkillSubGroups;

	public SkillSubGroup[] getSkillSubGroups() {
		return mSkillSubGroups;
	}

	public void setSkillSubGroups(SkillSubGroup[] skillSubGroups) {
		mSkillSubGroups = skillSubGroups;
	}
}
