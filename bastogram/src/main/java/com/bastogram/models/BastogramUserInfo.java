package com.bastogram.models;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.model.UserInfo;

import java.util.Set;

/**
 * Created by serezha on 29.07.16.
 */
public class BastogramUserInfo extends UserInfo {

	@SerializedName("animals")
	private Set<Nursling> mNurslings;

	@SerializedName("skills")
	private Set<BastogramUserInfoSkill> mSkills;

	public Set<BastogramUserInfoSkill> getSkills() {
		return mSkills;
	}

	public Set<Nursling> getNurslings() {
		return mNurslings;
	}

	public void setSkills(Set<BastogramUserInfoSkill> skills) {
		mSkills = skills;
	}



	public static class UserResponse {

		@SerializedName("user")
		private BastogramUserInfo mUserInfo;

		public BastogramUserInfo getUserInfo() {
			return mUserInfo;
		}
	}
}
