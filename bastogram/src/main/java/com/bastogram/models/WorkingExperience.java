package com.bastogram.models;

import com.humanet.humanetcore.model.ILocationModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by serezha on 12.07.16.
 */
public class WorkingExperience implements ILocationModel {

	public static final long MINUTE = 60 * 1000;
	public static final long HOUR = 60 * MINUTE;
	public static final long DAY = 24 * HOUR;
	public static final long MONTH = 31 * DAY;
	public static final long YEAR = 12 * MONTH;


	private int mId;
	private String mTitle;
	private int mValue;

	public WorkingExperience(int id, String title, int value) {
		mId = id;
		mTitle = title;
		mValue = value;
	}

	@Override
	public int getId() {
		return mId;
	}

	@Override
	public String getTitle() {
		return mTitle;
	}

	public int getValue() {
		return mValue;
	}

	public void setId(int id) {
		mId = id;
	}

	public void setTitle(String title) {
		mTitle = title;
	}

	public void setValue(int value) {
		mValue = value;
	}

	private static final List<WorkingExperience> VALUES = new ArrayList<WorkingExperience>() {{
		add(new WorkingExperience(0, "Стаж", 0));
		add(new WorkingExperience(1, "1 год", 1));
		add(new WorkingExperience(2, "2 года", 2));
		add(new WorkingExperience(3, "3 года", 3));
		add(new WorkingExperience(4, "4 года", 4));
		add(new WorkingExperience(5, "5 лет", 5));
		add(new WorkingExperience(6, "6 лет", 6));
		add(new WorkingExperience(7, "7 лет", 7));
		add(new WorkingExperience(8, "8 лет", 8));
		add(new WorkingExperience(9, "9 лет", 9));
		add(new WorkingExperience(10, "10 лет", 10));
	}};

	private static long getCurrentTime() {
		return System.currentTimeMillis();
	}

	public static int getIdFromExperience(int experience) {
		/*long valueAfter;
		long valueBefore;
		for(int i = 1; i < VALUES.size() - 1; i++) {
			valueAfter = VALUES.get(i).getValue();
			valueBefore = VALUES.get(i + 1).getValue();
			if(experience <= valueAfter && experience >= valueBefore) {
				if(valueAfter - experience <= experience - valueBefore)
					return VALUES.get(i).getId();
				else
					return VALUES.get(i + 1).getId();
			}
		}*/

		return experience;
	}

	public static List<WorkingExperience> values() {
		return VALUES;
	}
}
