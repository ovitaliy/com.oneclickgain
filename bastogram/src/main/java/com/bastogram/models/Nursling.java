package com.bastogram.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.model.UserInfo;

import java.io.Serializable;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static com.humanet.humanetcore.model.UserInfo.Sex;

/**
 * Created by Uran on 10.06.2016.
 */
public class Nursling implements Serializable {
    public static final String TABLE_NAME = "pets";

    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_BIRTH_DATE = "birth_date";
    public static final String COLUMN_AVATAR = "avatar";
    public static final String COLUMN_RATING = "rating";
    public static final String COLUMN_KIND = "kind";
    public static final String COLUMN_GENDER = "gender";
    public static final String COLUMN_VACCINATION = "vaccination";
    public static final String COLUMN_CHILD_READY = "child_ready";

    public static final String CAT = "cat";

    public static final int BRILLIANT = 1;
    public static final int RUBY = 2;
    public static final int GOLD = 3;
    public static final int SILVER = 4;
    public static final int BRONZE = 5;

    @IntDef({BRILLIANT, RUBY, GOLD, SILVER, BRONZE})
    public @interface Bage {
    }

    @SerializedName("id")
    private int mId = -1;

    @SerializedName("name")
    private String mName;

    private Long mBirthDate;

    @SerializedName("age")
    private int mAge;

    @SerializedName("avatar")
    private String mAvatar;

    @SerializedName("animal")
    private String mAnimal;

    @SerializedName("rating")
    private int mRating;

    @SerializedName("type")
    private String mKind;

    @Nursling.Bage
    @SerializedName("bages")
    private List<Integer> mBages;

    @Sex
    @SerializedName("gender")
    private int mGender;

    @SerializedName("vaccine")
    private int mVaccination;

    @SerializedName("reproduction")
    private int mChildReady;

    public int getId() {
        return mId;
    }

    @Nullable
    public String getName() {
        return mName;
    }

    public Long getBirthDate() {
        return mBirthDate;
    }

    public String getKind() {
        return mKind;
    }

    public int getRating() {
        return mRating;
    }

    @Sex
    public int getGender() {
        return mGender;
    }

    public boolean isVaccinated() {
        return mVaccination == 1;
    }

    public int getVaccine() {
        return mVaccination;
    }

    public boolean isChildReady() {
        return mChildReady == 1;
    }

    public int getReproduction() {
        return mChildReady;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public int getAge() {
        return mAge;
    }

    public String getAnimal() {
        return mAnimal;
    }

    @Nursling.Bage
    public List<Integer> getBages() {
        return mBages;
    }

    public boolean isVaccination() {
        return mVaccination == 1;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setBirthDate(Long birthDate) {
        mBirthDate = birthDate;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }

    public void setRating(int rating) {
        mRating = rating;
    }

    public void setKind(String kind) {
        mKind = kind;
    }

    public void setGender(int gender) {
        mGender = gender;
    }

    public void setVaccination(boolean vaccination) {
        mVaccination = vaccination ? 1 : 0;
    }

    public void setChildReady(boolean childReady) {
        mChildReady = childReady ? 1 : 0;
    }

    public void setAge(int age) {
        mAge = age;
    }

    public void setAnimal(String animal) {
        mAnimal = animal;
    }

    public void setBages(@Nursling.Bage List<Integer> bages) {
        mBages = bages;
    }

    public void setId(int id) {
        mId = id;
    }

    public static ContentValues toContentValues(Nursling nursling) {
        ContentValues contentValues = new ContentValues();
        if (nursling.mId != -1)
            contentValues.put("_id", nursling.mId);
        contentValues.put(COLUMN_NAME, nursling.mName);
        contentValues.put(COLUMN_BIRTH_DATE, nursling.mBirthDate);
        contentValues.put(COLUMN_AVATAR, nursling.mAvatar);
        contentValues.put(COLUMN_RATING, nursling.mRating);
        contentValues.put(COLUMN_KIND, nursling.mKind);
        contentValues.put(COLUMN_GENDER, nursling.mGender);
        contentValues.put(COLUMN_VACCINATION, nursling.mVaccination);
        contentValues.put(COLUMN_CHILD_READY, nursling.mChildReady);
        return contentValues;
    }

    public static Nursling fromCursor(Cursor cursor) {
        Nursling nursling = new Nursling();
        nursling.mId = cursor.getInt(cursor.getColumnIndex("_id"));
        nursling.mName = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
        nursling.mBirthDate = cursor.getLong(cursor.getColumnIndex(COLUMN_BIRTH_DATE));
        nursling.mAvatar = cursor.getString(cursor.getColumnIndex(COLUMN_AVATAR));
        nursling.mRating = cursor.getInt(cursor.getColumnIndex(COLUMN_RATING));
        nursling.mKind = cursor.getString(cursor.getColumnIndex(COLUMN_KIND));
        nursling.setGender(cursor.getInt(cursor.getColumnIndex(COLUMN_GENDER)));
        nursling.mVaccination = cursor.getInt(cursor.getColumnIndex(COLUMN_VACCINATION));
        nursling.mChildReady = cursor.getInt(cursor.getColumnIndex(COLUMN_CHILD_READY));
        return nursling;
    }


    public static Nursling demo(int id) {
        Nursling nursling = new Nursling();
        nursling.mId = id;
        nursling.mBirthDate = (System.currentTimeMillis() - TimeUnit.DAYS.toMillis(new Random().nextInt(10000))) / 1000;
        nursling.mName = "Pet " + id;
        nursling.mKind = "Kind";
        nursling.mGender = UserInfo.MALE;
        nursling.mVaccination = 1;
        nursling.mChildReady = 1;
        nursling.mAvatar = "http://ru4.anyfad.com/items/t1@610158e3-69b4-4266-9e82-941902c8ffda/Nad-kem-smeetes-Umeyut-li-zhivotnye-smeyatsya.jpg";
        nursling.mRating = new Random().nextInt(1000);
        return nursling;
    }
}
