package com.bastogram.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Uran on 09.06.2016.
 */
public class Portfolio {

    public static final String TABLE_NAME = "portfolio";

    public static final String COLUMN_SKILL_ID = "skill_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_LINK = "link";
    public static final String COLUMN_DESCRIPTION = "description";


    @SerializedName("id")
    private int mId = -1;

    @SerializedName("skill_id")
    private int mSkillId;

    @SerializedName("title")
    private String mName;

    @SerializedName("url")
    private String mLink;

    @SerializedName("description")
    private String mDescription;


    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getSkillId() {
        return mSkillId;
    }

    public void setSkillId(int skillId) {
        mSkillId = skillId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public String getDescription() {
        return mDescription;
    }


    public void setDescription(String description) {
        mDescription = description;
    }

    public static ContentValues toContentValues(Portfolio portfolio) {
        ContentValues contentValues = new ContentValues();
        if(portfolio.getId() != -1)
            contentValues.put("_id", portfolio.mId);
        contentValues.put(COLUMN_SKILL_ID, portfolio.mSkillId);
        contentValues.put(COLUMN_NAME, portfolio.mName);
        contentValues.put(COLUMN_LINK, portfolio.mLink);
        contentValues.put(COLUMN_DESCRIPTION, portfolio.mDescription);
        return contentValues;
    }

    public static Portfolio fromCursor(Cursor cursor) {
        Portfolio portfolio = new Portfolio();
        portfolio.mId = cursor.getInt(cursor.getColumnIndex("_id"));
        portfolio.mSkillId = cursor.getInt(cursor.getColumnIndex(COLUMN_SKILL_ID));
        portfolio.mName = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
        portfolio.mLink = cursor.getString(cursor.getColumnIndex(COLUMN_LINK));
        portfolio.mDescription = cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION));
        return portfolio;
    }

    public static Portfolio demo(int id) {
        Portfolio portfolio = new Portfolio();
        portfolio.mId = id;
        portfolio.mSkillId = 42;
        portfolio.mName = "portfolio " + id;
        portfolio.mLink = "http://google.com ";
        portfolio.mDescription = "Description Description Description Description Description Description Description Description Description Description Description Description Description Description ";

        return portfolio;
    }

}
