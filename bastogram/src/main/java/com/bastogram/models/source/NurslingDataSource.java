package com.bastogram.models.source;

import com.bastogram.models.Nursling;

/**
 * Created by Uran on 14.06.2016.
 */
public class NurslingDataSource extends BaseDataSource<Nursling> {

    public NurslingDataSource(IDataSource<Nursling> localDataSource, IDataSource<Nursling> remoteDataSource) {
        super(localDataSource, remoteDataSource);
    }

}
