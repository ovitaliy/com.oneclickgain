package com.bastogram.db;

import android.database.sqlite.SQLiteDatabase;

import com.bastogram.models.Nursling;
import com.bastogram.models.Portfolio;
import com.bastogram.models.Skill;
import com.humanet.humanetcore.db.DBHelper;

/**
 * Created by Uran on 14.06.2016.
 */
public class DBImpl implements DBHelper.Delegate {

    @Override
    public void createTables(SQLiteDatabase db) {
        String createTableQuery;

        createTableQuery = "CREATE TABLE " + Nursling.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Nursling.COLUMN_NAME + " TEXT,"
                + Nursling.COLUMN_AVATAR + " TEXT, "
                + Nursling.COLUMN_KIND + " TEXT, "
                + Nursling.COLUMN_RATING + " INTEGER, "
                + Nursling.COLUMN_BIRTH_DATE + " INTEGER, "
                + Nursling.COLUMN_GENDER + " INTEGER, "
                + Nursling.COLUMN_VACCINATION + " INTEGER, "
                + Nursling.COLUMN_CHILD_READY + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + Portfolio.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Portfolio.COLUMN_SKILL_ID + " INTEGER, "
                + Portfolio.COLUMN_NAME + " TEXT,"
                + Portfolio.COLUMN_LINK + " TEXT, "
                + Portfolio.COLUMN_DESCRIPTION + " TEXT "
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + Skill.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Skill.COLUMN_TITLE + " TEXT,"
                + Skill.COLUMN_COMPETENCE + " TEXT, "
                + Skill.COLUMN_SPECIALIZATION + " TEXT, "
                + Skill.COLUMN_EXPERIENCE + " INTEGER, "
                + Skill.COLUMN_LEVEL + " INTEGER, "
                + Skill.COLUMN_SKILL_ITEM + " INTEGER "
                + ");";
        db.execSQL(createTableQuery);

    }

    @Override
    public void dropTable(SQLiteDatabase db) {
        DBHelper.dropTable(db, Nursling.TABLE_NAME);
        DBHelper.dropTable(db, Portfolio.TABLE_NAME);
        DBHelper.dropTable(db, Skill.TABLE_NAME);
    }
}
